
- on android
Error : ERR_CLEARTEXT_NOT_PERMITTED
Fix :
https://stackoverflow.com/questions/54752716/why-am-i-seeing-neterr-cleartext-not-permitted-errors-after-upgrading-to-cordo/55787059#55787059


platforms/android/app/src/main/res/xml/network_security_config.xml
Put :
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
  <base-config cleartextTrafficPermitted="true">
      <trust-anchors>
          <certificates src="system" />
      </trust-anchors>
  </base-config>  
    <domain-config cleartextTrafficPermitted="true">
        <domain>localhost</domain>
        <domain includeSubdomains="true">192.168.0.19:8100</domain>
    </domain-config>
</network-security-config>



Also in config.xml
xmlns:android="http://schemas.android.com/apk/res/android"
<application android:usesCleartextTraffic="true" />

- Cannot /GET
error : some error is there
Fix : Just build and check console and see if there is any errors

- ERROR in node_modules/@ionic-native/sqlite/ngx/index.d.ts(1,35): error TS2307: Cannot find module '@ionic-native/core'.
Erorr :
Fix :
npm install @ionic-native/core --save
