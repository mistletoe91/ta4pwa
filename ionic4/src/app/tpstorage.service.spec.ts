import { TestBed } from '@angular/core/testing';

import { TpstorageProvider } from './tpstorage.service';

describe('TpstorageProvider', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TpstorageProvider = TestBed.get(TpstorageProvider);
    expect(service).toBeTruthy();
  });
});
