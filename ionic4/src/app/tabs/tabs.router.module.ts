import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../testpage/testpage.module#TestpagePageModule'
          }
        ]
      },
      {
        path: 'categories',
        children: [
          {
            path: '',
            loadChildren: '../categories/categories.module#CategoriesPageModule'
          }
        ]
      },
      {
        path: 'loyalty',
        children: [
          {
            path: '',
            loadChildren: '../loyalty/loyalty.module#LoyaltyPageModule'
          }
        ]
      },
      {
        path: 'deals',
        children: [
          {
            path: '',
            loadChildren: '../deals/listing/deals-listing.module#DealsListingPageModule'
          }
        ]
      },
      {
        path: 'dealsclaimhistory',
        children: [
          {
            path: '',
            loadChildren: '../dealsclaimhistory/listing/deals-listing.module#DealsListingPageModule'
          }
        ]
      },
      {
        path: 'deals/detail/:productId',
        children: [
          {
            path: '',
            loadChildren: '../deals/details/deals-details.module#DealsDetailsPageModule'
          }
        ]
      },
      {
        path: 'user',
        children: [
          {
            path: '',
            loadChildren: '../user/profile/user-profile.module#UserProfilePageModule'
          },
          {
            path: 'friends',
            loadChildren: '../user/friends/user-friends.module#UserFriendsPageModule'
          }
        ]
      },
      {
        path: 'notifications',
        children: [
          {
            path: '',
            loadChildren: '../notifications/notifications.module#NotificationsPageModule'
          }
        ]
      },
      {
        path: 'notificationsoriginal',
        children: [
          {
            path: '',
            loadChildren: '../notificationsoriginal/notifications.module#NotificationsPageModule'
          }
        ]
      },
      {
        path: 'chats',
        children: [
          {
            path: '',
            loadChildren: '../chats/chats.module#ChatsPageModule'
          }
        ]
      },
    ]
  },
  // /app/ redirect
  {
    path: '',
    redirectTo: 'app/notifications',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), HttpClientModule],
  exports: [RouterModule],
  providers: [ ]
})
export class TabsPageRoutingModule {}
