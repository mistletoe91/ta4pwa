import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {   Router   } from '@angular/router';
@Component({
  selector: 'forms-validations-page',
  templateUrl: './forms-validations.page.html',
  styleUrls: [
    './styles/forms-validations.page.scss'
  ]
})
export class FormsValidationsPage implements OnInit {

  validationsForm: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  countries: [];
  genders: Array<string>;

  validations = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'terms': [
      { type: 'pattern', message: 'You must accept terms and conditions.' }
    ]
  };

  constructor(private router: Router) {
    this.genders = [
      'Female',
      'Male',
      'Other'
    ];
   }

  ngOnInit(): void {
    this.validationsForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'terms': new FormControl(true, Validators.pattern('true'))
    });
  }

  onSubmit(values) {
    this.router.navigateByUrl('/registerbusiness2referrals');
    console.log(values);
  }
}
