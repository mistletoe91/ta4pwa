import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TpstorageProvider } from './tpstorage.service';
import {  Platform } from '@ionic/angular';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite/ngx';
import { CONST} from './const';
import { Router } from '@angular/router';
//Default Route : walkthrough
const routes: Routes = [
  { path: '', redirectTo: '/app/home', pathMatch: 'full' },
  { path: 'walkthrough', loadChildren: './walkthrough/walkthrough.module#WalkthroughPageModule' },
  { path: 'getting-started', loadChildren: './getting-started/getting-started.module#GettingStartedPageModule' },
  { path: 'auth/login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'auth/signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'auth/forgot-password', loadChildren: './forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'app', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'contact-card', loadChildren: './contact-card/contact-card.module#ContactCardPageModule' },
  { path: 'forms-and-validations', loadChildren: './forms/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'forms-filters', loadChildren: './forms/filters/forms-filters.module#FormsFiltersPageModule' },
  { path: 'page-not-found', loadChildren: './page-not-found/page-not-found.module#PageNotFoundModule' },
  { path: 'showcase', loadChildren: './showcase/showcase.module#ShowcasePageModule' },
  { path: 'firebase', loadChildren: './firebase/firebase-integration.module#FirebaseIntegrationModule' },
  { path: 'video-playlist', loadChildren: './video-playlist/video-playlist.module#VideoPlaylistPageModule' },
  { path: 'sendareferral-info', loadChildren: './sendareferral-info/sendareferral-info.module#SendareferralInfoPageModule' },
  { path: 'loyaltycustomerhistory', loadChildren: './loyaltycustomerhistory/sendareferral-info.module#SendareferralInfoPageModule' },
  { path: 'dealscustomerhistory', loadChildren: './dealscustomerhistory/sendareferral-info.module#SendareferralInfoPageModule' },

  { path: 'referrallist', loadChildren: './referrallist/loyalty.module#LoyaltyPageModule' },
  { path: 'loyalty', loadChildren: './loyalty/loyalty.module#LoyaltyPageModule' },
  { path: 'loyaltydetail', loadChildren: './loyaltydetail/loyaltydetail.module#LoyaltydetailPageModule' },

  { path: 'dealslist', loadChildren: './dealslist/loyalty.module#LoyaltyPageModule' },

  { path: 'notif', loadChildren: './notif/notif.module#NotifPageModule' },
  { path: 'buddychat', loadChildren: './buddychat/buddychat.module#BuddychatPageModule' },
  { path: 'referral', loadChildren: './referral/referral.module#ReferralPageModule' },
  { path: 'invite', loadChildren: './invite/invite.module#InvitePageModule' },
  { path: 'sendareferral-options', loadChildren: './sendareferral-options/invite.module#InvitePageModule' },
  { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  { path: 'contactimportprogress', loadChildren: './contactimportprogress/contactimportprogress.module#ContactimportprogressPageModule' },

  { path: 'referraloptions', loadChildren: './referraloptions/testpage.module#TestpagePageModule' },

  { path: 'testpage', loadChildren: './testpage/testpage.module#TestpagePageModule' },
  { path: 'loyaltyoptions', loadChildren: './loyaltyoptions/testpage.module#TestpagePageModule' },
  { path: 'dealsoptions', loadChildren: './dealsoptions/testpage.module#TestpagePageModule' },
  { path: 'chats', loadChildren: './chats/chats.module#ChatsPageModule' },
  { path: 'viewphoto-modal', loadChildren: './viewphoto-modal/viewphoto-modal.module#ViewphotoModalPageModule' },
  { path: 'rewards', loadChildren: './rewards/rewards.module#RewardsPageModule' },
  { path: 'howitworks', loadChildren: './howitworks/howitworks.module#HowitworksPageModule' },
  { path: 'earn-dollars', loadChildren: './earn-dollars/earn-dollars.module#EarnDollarsPageModule' },

  { path: 'affiliate', loadChildren: './affiliate/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'address', loadChildren: './address/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness', loadChildren: './registerbusiness/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness2referrals', loadChildren: './registerbusiness2referrals/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness3loyalty', loadChildren: './registerbusiness3loyalty/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness4location', loadChildren: './registerbusiness4location/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness5marketing', loadChildren: './registerbusiness5marketing/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'registerbusiness6done', loadChildren: './registerbusiness6done/validations/forms-validations.module#FormsValidationsPageModule' },

  { path: 'referraldetails', loadChildren: './referraldetails/validations/forms-validations.module#FormsValidationsPageModule' },

  { path: '**', redirectTo: 'page-not-found' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(
    private platform: Platform,
    public tpStorageService: TpstorageProvider,
    private sqlite: SQLite,
    private router: Router
  ) {
      this.platform.ready().then((readySource) => {
          //this.dropAllStorage ();
          this.checkUserAndRedirectUser  ();
      });
  }


  dropAllStorage (){
    console.log("DROP Everything and start fresh ");
    this.dropTable ('cache_users_local');
    this.dropTable ('cache_tapally_contacts');
    this.dropTable ('cache_tapally_friends');
    this.dropTable ('key_val');
    this.tpStorageService.clear();
  }

     dropTable(tbl) {
         this.sqlite.create({
           name: 'gr.db',
           location: 'default'
         }).then((db: SQLiteObject) => {
           console.log ("DROPPED TABLE "+tbl);
           db.executeSql( "DROP TABLE "+tbl, [])
           .then(res => {
           })
           .catch(e => {
               //Error in operation
           });//create table

         });//create database
    }//end function

   deleteFromTable() {
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("DELETE   FROM cache_users_local", [])
             .then(res => {
             })
             .catch(e => {
                 //Error in operation
             });
         })
         .catch(e => {
             //Error in operation
         });//create table

       });//create database
  }//end function

  fnGotToPage (pg){
    pg = '/app/home';
    console.log ("Redirecting to "+ pg);
    this.router.navigateByUrl(pg);
  }

   checkUserAndRedirectUser() {


                              this.tpStorageService.getItem('userUID').then((userId__: any) => {
                                 if(userId__){
                                     //user exist - now check if name exists
                                     this.tpStorageService.getItem('myDisplayName').then((user_full_name: any) => {
                                        if(user_full_name){
                                            //user full name exists
                                            this.sendToFirstTab ();
                                        } else {
                                            this.sendToDisplayName ();
                                        }
                                     }).catch(e => {
                                        this.sendToDisplayName ();
                                     });
                                 } else {
                                     console.log ("RECORD DOES NOT EXISTS");
                                     this.sendToTutorialPage ();
                                 }
                              }).catch(e => {
                                 console.log ("RECORD DOES NOT EXISTS");
                                 this.sendToTutorialPage ();//for_prod
                              });

   }//end function

   sendToFirstTab (){
     this.fnGotToPage ('/app/home');// /app/loyalty
   }

   sendToDisplayName (){
     this.fnGotToPage ('forms-and-validations');// forms-and-validations
   }

   sendToTutorialPage (){
     this.fnGotToPage ('walkthrough');
   }

/*
   checkUserAndRedirectUser__OLD() {
       console.log ("CHECK1");

       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         console.log ("CHECK2");
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("SELECT *  FROM cache_users_local", [])
             .then(res => {
                  console.log ("CHECK 4");
                 if(res.rows.length>0){
                         console.log (res.rows.item(0));

                         //User exists so go to next page
                         if(!res.rows.item(0).uid || res.rows.item(0).uid == undefined){
                             this.fnGotToPage ('walkthrough' );
                         }

                         console.log ("App component Trying to get userUID");
                         this.tpStorageService.getItem('userUID').then((res: any) => {
                            console.log ("Inside this.tpStorageService.getItem");
                            if (res && res != undefined) {
                              console.log ("userUID Found in tpStorageService");
                            } else {
                              console.log ("userUID NOT Found in tpStorageService so setting to "+ res.rows.item(0).uid);
                              this.tpStorageService.setItem('userUID', res.rows.item(0).uid);
                            }
                     		}).catch(e => { });
                        this.tpStorageService.getItem('newname').then((res: any) => {
                           if (res && res != undefined) {
                           } else {
                             this.tpStorageService.setItem('newname', res.rows.item(0).user_name);
                           }
                       }).catch(e => { });
                       this.tpStorageService.getItem('useremail').then((res: any) => {
                          if (res && res != undefined) {
                          } else {
                            this.tpStorageService.setItem('useremail', res.rows.item(0).useremail);
                          }
                      }).catch(e => { });


                         console.log ("++++--->");
                         console.log (res.rows.item(0).user_name);


                         if(!res.rows.item(0).user_name || res.rows.item(0).user_name == undefined){
                             this.fnGotToPage  ('DisplaynamePage');
                         }
                 } else {

                         this.tpStorageService.getItem('userUID').then((userId__: any) => {
                            if(userId__){
                                //A situation when we have userId in storage but not in sqlite
                                //this.userService.saveUserIdInSqlLite (userId__);
                                this.fnGotToPage ('/app/loyalty');
                            } else {
                                console.log ("RECORD DOES NOT EXISTS");
                                this.fnGotToPage ('walkthrough');
                            }
                         }).catch(e => {
                            console.log ("RECORD DOES NOT EXISTS");
                            this.fnGotToPage ('walkthrough');//for_prod
                         });

                 }//endif
             })
             .catch(e => {
                 //Error in operation
                 console.log ("ERROR11:");
                 console.log (e);
             });

         })
         .catch(e => {
             //Error in operation
             console.log ("ERROR12:");
             console.log (e);
         });//create table

       });//create database
  }//end function
*/

}


/*
Modals :::
{ path: 'referral-modal', loadChildren: './referral-modal/referral-modal.module#ReferralModalPageModule' },
*/
