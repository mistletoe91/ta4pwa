import { Component, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DealsListingModel } from './deals-listing.model';

@Component({
  selector: 'app-deals-listing',
  templateUrl: './deals-listing.page.html',
  styleUrls: [
    './styles/deals-listing.page.scss',
    './styles/deals-listing.shell.scss',
    './styles/deals-listing.ios.scss'
  ]
})
export class DealsListingPage implements OnInit {
  listing: DealsListingModel;
  items = [];
  @HostBinding('class.is-shell') get isShell() {
    return (this.listing && this.listing.isShell) ? true : false;
  }
  askForReferral (){
    this.router.navigateByUrl('/referral');
  }


    goto_fab (){
      //mytodo : check tpstorage if business is already registered then send to loyaltyoptions
      if(true){
        this.router.navigateByUrl('/dealsoptions');//
      } else {
        this.router.navigateByUrl('/registerbusiness');//
      }

    }


  constructor(private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe((resolvedRouteData) => {
      const listingDataStore = resolvedRouteData['data'];

      listingDataStore.state.subscribe(
        (state) => {
          this.listing = state;
        },
        (error) => {}
      );
    },
    (error) => {});
    this.loadItems ();
  }


    loadItems (){
      console.log ("loadItems");
      for(let b=0;b<1;b++){
        let record = {
            uid : "jd9oijoij12oi3j123",
            name : "Myself",
            date : "29 June 2019",
            claimed : 1,
         };
         this.items.push ( record );
      }//end for
      for(let b=0;b<3;b++){
        let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Johny",
          date : "24 June 2019",
          claimed : 1
         };
         this.items.push ( record );
      }//end for
      for(let b=0;b<1;b++){
        let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Gary Gobin",
          date : "22 June 2019",
          claimed : 1,
         };
         this.items.push ( record );
      }//end for
      for(let b=0;b<1;b++){
        let record = {
            uid : "jd9oijoij12oi3j123",
            name : "Michakel Cobrin",
            date : "15 June 2019",
            claimed : 1,
            note_from_business : ""
         };
         this.items.push ( record );
      }//end for
      for(let b=0;b<4;b++){
        let record = {
            uid : "jd9oijoij12oi3j123",
            name : "John Martin",
            date : "13 June 2019",
            claimed : 1
         };
         this.items.push ( record );
      }//end for

    }
}
