import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { UsernameValidator } from '../../validators/username.validator';
import { PasswordValidator } from '../../validators/password.validator';
import { PhoneValidator } from '../../validators/phone.validator';

import { counterRangeValidator } from '../../components/counter-input/counter-input.component';
import { CountryPhone } from './country-phone.model';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { TpstorageProvider } from '../../tpstorage.service';
@Component({
  selector: 'forms-validations-page',
  templateUrl: './forms-validations.page.html',
  styleUrls: [
    './styles/forms-validations.page.scss'
  ]
})
export class FormsValidationsPage implements OnInit {
  fullname: string;
  validationsForm: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  countries: Array<CountryPhone>;
  genders: Array<string>;
  userImage: string;

  validations = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ]
  };

  constructor(private router: Router,public tpStorageService: TpstorageProvider,public userService:UserService) {
  }

  loadFullName (){
    console.log ("Calling loadFullName");
    this.userService.getuserdetails().then((res)=>{
      console.log ("oading full name");
      console.log (res);
      if(res){
        this.fullname=res['displayName'];
      }else{
        this.fullname='';
      }
    }).catch(err=>{
    });

  }  

  ngOnInit(): void {
    console.log ("ngOnInit");
    this.tpStorageService.getItem('userUID').then((res: any) => {
          this.loadFullName ();
    }).catch(e => {
          console.log ("Does not found userUID");
          //user not avail so go back to login
          this.router.navigateByUrl('/walkthrough');
    });


    //  We just use a few random countries, however, you can use the countries you need by just adding them to this list.
    // also you can use a library to get all the countries from the world.
    this.countries = [
      new CountryPhone('UY', 'Uruguay'),
      new CountryPhone('US', 'United States'),
      new CountryPhone('ES', 'España'),
      new CountryPhone('BR', 'Brasil'),
      new CountryPhone('FR', 'France')
    ];

    this.genders = [
      'Female',
      'Male',
      'Other'
    ];

    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });

    const country = new FormControl(this.countries[0], Validators.required);

    const phone = new FormControl('', Validators.compose([
      Validators.required,
      PhoneValidator.invalidCountryPhone(country)
    ]));
    this.country_phone_group = new FormGroup({
      country: country,
      phone: phone
    });

    this.validationsForm = new FormGroup({
      'name': new FormControl('', Validators.required)
    });
  }


  onSubmit(values) {
    if(values.name.trim()!=null ){
      this.userService.updatedisplayname(values.name).then((res: any) => {
        if (res.success) {
          this.router.navigateByUrl('/app/loyalty');
        }
      })
      } else {

      }

    //this.router.navigateByUrl('/app/loyalty');
    //console.log(values);
  }//end function
}
