import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-loyalty',
  templateUrl: './loyalty.page.html',
  styleUrls: [
    './loyalty.page.scss',
    './loyalty.responsive.scss'
  ]
})
export class LoyaltyPage implements OnInit {

  constructor(private router: Router) {
  }
  ngOnInit() {
  }
  gotocontactspage (){
    this.router.navigateByUrl('/notifications');//
  }
  goto (pg){
    this.router.navigateByUrl(pg);// "/referral"
  }
  askForReferral (){
    console.log ("Clicked " +  new Date());
    this.router.navigateByUrl('/referral');//   /showcase/app-shell/data-store-list
  }
  onDetail() {
      this.router.navigateByUrl('/referraldetails');
  }


  goto_fab (){
    //mytodo : check tpstorage if business is already registered then send to loyaltyoptions
    if(true){
      this.router.navigateByUrl('/loyaltyoptions');//
    } else {
      this.router.navigateByUrl('/registerbusiness');//
    }

  }

}
