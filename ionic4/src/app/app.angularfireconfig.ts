//------------------------------------//
/*
	Environment
	1 = Prod
	0 = Dev
*/
export const myEnv = false;
//------------------------------------//
export var config = {};
if(myEnv){
	//PROD
  config = {
    apiKey: "AIzaSyB9ZN4YPIdFZgLfHWo5HbsBp2gOiJeWKCI",
  	authDomain: "tapallyionic3.firebaseapp.com",
  	databaseURL: "https://tapallyionic3.firebaseio.com",
  	projectId: "tapallyionic3",
  	storageBucket: "tapallyionic3.appspot.com",
  	messagingSenderId: "392933828452"
	};
	//BEFORE PUBLISHING COPY AND PASTE THE PROD DATABASE
} else {
	//DEV
	config = {
		apiKey: "AIzaSyB9ZN4YPIdFZgLfHWo5HbsBp2gOiJeWKCI",
		authDomain: "tapallyionic3.firebaseapp.com",
		databaseURL: "https://tapallytest.firebaseio.com",
		projectId: "tapallyionic3",
		storageBucket: "tapallytest",
		messagingSenderId: "392933828452"
	};
}//endif

// Push Notification
export const server_key ={
	key : "AAAAW3yuZ2Q:APA91bGpdKl6yizA9wV1ZBgfvWaVjn1bWeDa76yNb5HTJlUuIzR8IQcSHOmyoKgCXlbBfZiVBkyTbVTF-QwlG_V4bKtiuOdTZhp4q4hICeDiwB3FV9kKLbYR513vt1XyLoAh31v__RB0"
}
// TApAlly Key : not used right now
export const tapallyKey ={
	key : "djoiasjdDKASKD21893DAJ"
}
// Twilio API key
export let apiKey = "TKgzw8rHwnYloYWUQqhJDPhTVMpQMIk4";
//default image of profile image
export const env ={
    userPic:"https://firebasestorage.googleapis.com/v0/b/fir-chat-1f142.appspot.com/o/notouch%2Fuser.png?alt=media&token=79404d85-679a-48d3-9f73-edc7e53b4c2f"
}
//Animal Pics for icons
export const userPicArr ={
	0:
  "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
	1: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F1.png?alt=media&token=d718091f-5d10-4edc-80e1-a2a10ef574ff",
	2: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F2.png?alt=media&token=425fbd27-514c-486b-962e-0c1648897e60",
	3: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F3.png?alt=media&token=dba66456-cca8-404f-ad6f-22d85b5c87ab",
	4: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F4.png?alt=media&token=aa684de9-c64e-4f0f-ae44-6b816ac346ff",
	5 : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F5.png?alt=media&token=bcfe7d7d-7959-4e02-b2f2-2a4b48531fbe",
	6 : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F6.png?alt=media&token=b202b42f-540a-46d6-a204-0ac2f8d57038",
	7 : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F7.png?alt=media&token=ffb1565c-8c14-446c-8126-02b0c4ababa1",
	8 : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8.png?alt=media&token=39038611-f4f0-4871-b7ae-86e9e51ce284",
	9 : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8174%20-%20Crocodile%20Face.png?alt=media&token=228f0333-9cce-41d2-bf1c-5732dc615e2b",
	10: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8175%20-%20Deer%20Face.png?alt=media&token=c2e983b3-584c-4ce9-ae6b-217003e08f89",
	11: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8176%20-%20Dinosaur%20Face.png?alt=media&token=fac6d998-22f7-4129-a117-fffee163c62e",
	12: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8177%20-%20Dog%20Face.png?alt=media&token=e8fb1752-ed1f-451d-a0e6-8bd1eb16d737",
	13: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8178%20-%20Donkey%20Face.png?alt=media&token=5173617a-fe87-4570-8090-e6c90fe5312a",
	14: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8179%20-%20Duckling%20Face.png?alt=media&token=e1c780b7-609c-4096-8eaf-cf1ace7af73b",
	15: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8180%20-%20Duck%20Face.png?alt=media&token=600b227e-aa3a-4e9c-b52d-f953989f3479",
	16: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8181%20-%20Eagle%20Face.png?alt=media&token=82a8904d-0755-45f9-b221-3575a672ed4c",
	17: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8182%20-%20Elephant%20Face.png?alt=media&token=72201d6f-f5d4-4f94-a27d-22bfc238b28a",
	18: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8183%20-%20Fox%20Face.png?alt=media&token=bc85c616-fe34-41cc-9f8f-895b2fd95d9c",
	19: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8184%20-%20Frog%20Face.png?alt=media&token=2d7d3978-3cfd-4164-af2e-575e02bf459a",
	20: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8185%20-%20Giraffe%20Face.png?alt=media&token=6a7d3c69-89ce-411d-8649-cfb7211cc9b6",
	21: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8186%20-%20Goat%20Face.png?alt=media&token=372fc443-48fe-451d-8098-94f67866f582",
	22: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8187%20-%20Hen%20Face.png?alt=media&token=5988a35e-4029-4268-a613-e2c144662054",
	23: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8188%20-%20Hippopotamus%20Face.png?alt=media&token=f23c42d8-0f9a-4210-bc4d-4f8deae0d011",
	24: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8189%20-%20Horse%20Face.png?alt=media&token=f68d155d-9ca7-46d3-881c-30d0f41103dc",
	25: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8190%20-%20Kangaroo%20Face.png?alt=media&token=5c27d5f0-96f1-426a-b00e-ace6a1dfb32c",
	26: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8191%20-%20Koala%20Bear%20Face.png?alt=media&token=ca5453ed-80a8-4873-896b-1e0928bea96d",
	27: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8192%20-%20Lamb%20Face.png?alt=media&token=51da019d-6247-41e7-928f-23eeafb7442c",
	28: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8193%20-%20Leopard%20Face.png?alt=media&token=ded0bd0d-5400-451e-8ee7-a4a43d1329e5",
	29: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8194%20-%20Lion%20Face.png?alt=media&token=5a6557f0-3967-4f29-9428-373276549f96",
	30: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8195%20-%20Monkey%20Face.png?alt=media&token=06844636-a82f-4478-a5c8-ac95b0a3168a",
	31: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8196%20-%20Moutain%20Goat%20Face.png?alt=media&token=e4a3d71c-aca9-4665-8ba3-e8e8511e9b2e",
	32: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8197%20-%20Mouse%20Face.png?alt=media&token=a3848a65-fbd8-4511-bbf8-b7e202b07a92",
	33: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8198%20-%20Octopus%20Face.png?alt=media&token=8ad94d18-0ef1-4cc0-8bb0-7e319be9c11b",
	34: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8199%20-%20Owl%20Face.png?alt=media&token=2fa15989-9d96-4684-b526-f5a185c23947",
	35: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8200%20-%20Panda%20Face.png?alt=media&token=87439c6b-a4d3-4e57-97d9-3d02303c6012",
	36: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8201%20-%20Parrot%20Face.png?alt=media&token=5ca706d4-54a9-4cfb-842d-9c120c8604c7",
	37: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8202%20-%20Pig%20Face.png?alt=media&token=2ab495b3-a291-4281-a317-a17d1255518d",
	38: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8203%20-%20Rabbit%20Face.png?alt=media&token=bf8ffc53-8786-4bc3-a60f-c42fb529bf6e",
	39: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8204%20-%20Rhino%20Face.png?alt=media&token=4ef8a4b5-237f-4afa-abab-cdd0c063e956",
	40: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8205%20-%20Sea%20Lion%20Face.png?alt=media&token=8c42ff62-fdb1-488e-9164-73e9ad15bdd3",
	41: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8206%20-%20Shark%20Face.png?alt=media&token=0a51ef3a-9385-4043-a32c-daa255bdbf04",
	42: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8207%20-%20Snake%20Face.png?alt=media&token=9ab811c2-bf5e-40a9-8ca1-9adb4d73292b",
	43: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8208%20-%20Tiger%20Face.png?alt=media&token=33b57683-9409-42b4-a1e9-76f4a78ee16c",
	44: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8209%20-%20Turtle%20Face.png?alt=media&token=bdaf2962-5a6a-4823-8388-4f7b9d1ab831",
	45: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8210%20-%20Whale%20Face.png?alt=media&token=6b0b2bae-f2c4-49e7-bf06-07725a43796d",
	46: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8211%20-%20Wolf%20Face.png?alt=media&token=d8654953-fdd6-46f1-b50a-cbceaf3d5cca",
	47: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F8212%20-%20Zebra%20Face.png?alt=media&token=c2523721-0054-4054-9e23-bfb75412a967",
	48: "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F9.png?alt=media&token=cf956513-c831-4e0e-8333-e46b080e970c"
}

//Object length
//console.log (Object.keys(userPicArr).length);
