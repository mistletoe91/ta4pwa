import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router ,NavigationExtras} from '@angular/router';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: [
    './styles/notifications.page.scss',
    './styles/notifications.shell.scss'
  ]
})
export class NotificationsPage implements OnInit {
  notifications: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute
  ) { }
  addbuddy (){
    this.router.navigateByUrl('/invite');
  }
  gotobuddypage (friend){
    let navigationExtras: NavigationExtras = {
          queryParams: {
            buddy_uid: 'test'
          }
    };
    this.router.navigate(['/buddychat'],navigationExtras);
  }

  ngOnInit(): void {
    if (this.route && this.route.data) {
      this.route.data.subscribe(resolvedData => {
        const dataSource = resolvedData['data'];
        if (dataSource) {
          dataSource.source.subscribe(pageData => {
            if (pageData) {
              this.notifications = pageData;
            }
          });
        }
      });
    }
  }
}
