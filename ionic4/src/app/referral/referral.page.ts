import { Component, OnInit ,HostBinding } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { Platform} from '@ionic/angular';
import { ReferralModalPage } from '../referral-modal/referral-modal.page';
import { CatsService } from '../../services/cats.service';
@Component({
  selector: 'app-referral',
  templateUrl: './referral.page.html',
  styleUrls: ['./referral.page.scss'],
})
export class ReferralPage implements OnInit {
  myrequests = [];
  requestcounter = null;
  items;
  showheader:boolean = true;
  isVirgin:boolean = false;
  timestamp:any = new Date();


  constructor(platform: Platform,public catservice: CatsService, public modalController: ModalController,private router: Router) {
    this.timestamp = new Date();
    console.log ("ReferralPage : Referral Page Construction "+  this.timestamp.getTime());
  }
  async initializeItems() {
      this.timestamp = new Date();
      console.log ("ReferralPage : initializeItems "+  this.timestamp.getTime());
      this.catservice.getCats().then((res: any) => {
           this.timestamp = new Date();
           setTimeout(() => {
            this.items = res;
            console.log ("ReferralPage : this.items = res "+  this.timestamp.getTime());
          },  1);
      }).catch(e => {
      });
  }

  ngOnInit (){
  }

  ionViewDidEnter() {
     this.timestamp = new Date();
     console.log ("ReferralPage : ngOnInit" +  this.timestamp.getTime() );

     this.timestamp = new Date();
     console.log ("ReferralPage : Platform Ready "+  this.timestamp.getTime());
     this.initializeItems();
  }
  askForReferral (){
    this.router.navigateByUrl('/referral');
  }
  async selCategory(item_id) {
    const modal = await this.modalController.create({
      component: ReferralModalPage,
      componentProps: {
        'item_id': item_id
      }
    });
    return await modal.present();
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
