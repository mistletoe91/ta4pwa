import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy,RouterModule } from '@angular/router';
import { Contacts } from '@ionic-native/contacts/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import {enableProdMode} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentsModule } from './components/components.module';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ServiceWorkerModule } from '@angular/service-worker';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { ReactiveFormsModule } from '@angular/forms';
import { HTTP } from '@ionic-native/http/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { SQLite   } from '@ionic-native/sqlite/ngx';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpModule, Http } from '@angular/http';
import { ReferralModalPageModule } from './referral-modal/referral-modal.module';
import { ViewphotoModalPageModule } from './viewphoto-modal/viewphoto-modal.module';
// Firebase config
import { config,myEnv } from './app.angularfireconfig';

import { AuthenticationService } from './authentication.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { FcmService } from '../services/fcm.service';
import { SMS } from '@ionic-native/sms/ngx';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
enableProdMode();
import * as firebase from 'firebase';
//firebase.initializeApp(config);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule,
    IonicModule.forRoot(),
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    ReferralModalPageModule,
    ViewphotoModalPageModule,
    ComponentsModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: myEnv }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {provide: SQLite, useClass: SQLite},
    Camera,
    FcmService,
    HTTP,
    NativeStorage,
    AuthenticationService,
    Contacts,
    SplashScreen,
    StatusBar,
    SMS,
    PhotoViewer
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
