import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: [
    './styles/categories.page.scss',
    './styles/categories.shell.scss',
    './styles/categories.responsive.scss'
  ]
})
export class CategoriesPage {

  constructor(private router: Router) {
  }

  askForReferral (){
    this.router.navigateByUrl('/showcase/app-shell/data-store-list');// "/referral"
  }
  fnSendReferral (){
    console.log ("Send Referral");
  }

  fnmaincardwork (){
    console.log ("Going");
    this.router.navigateByUrl('/sendareferral-info');
  }


      goto_fab (){
        //mytodo : check tpstorage if business is already registered then send to loyaltyoptions
        if(true){
          this.router.navigateByUrl('/referraloptions');//
        } else {
          this.router.navigateByUrl('/registerbusiness');//
        }

      }

}
