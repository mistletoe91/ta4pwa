import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {   Router   } from '@angular/router';
import { ReferralincentiveTypeService } from '../../../services/referralincentive-type.service';
@Component({
  selector: 'forms-validations-page',
  templateUrl: './forms-validations.page.html',
  styleUrls: [
    './styles/forms-validations.page.scss'
  ]
})
export class FormsValidationsPage implements OnInit {

  validationsForm: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  countries: [];
  rtlist ;
  rtlistComplete;
  selectOptions;
  selectOptionsTP;
    showincentivedetail:boolean = false;
    referral_incentive_detail;
    referral_incentive_detail_placeholder;
  validations = {
    'referral_incentive_detail_txt': [
      { type: 'required', message: 'Name is required.' }
    ]
  };

  constructor(private router: Router,public rtService : ReferralincentiveTypeService) {
    this.rtlistComplete = this.rtService.getList();
   }

  ngOnInit(): void {
    this.validationsForm = new FormGroup({
      'referral_incentive_detail_txt': new FormControl('', Validators.required)
    });
  }


  onSubmit(values) {
    this.gotonextpage ();
    console.log(values);
  }

  onChange(e) {
  }


  gotonextpage (){
    this.router.navigateByUrl('/registerbusiness6done');
  }

  onChange_incentivetype(e) {
    this.referral_incentive_detail = 'Incentive Detail';
    this.referral_incentive_detail_placeholder = '';
    if (e) {

      switch (e ) {
          case '1':
          case '2':
              //cash
              //Gift Card
              this.referral_incentive_detail = 'Enter Referral Amount (Numbers Only)';
              this.referral_incentive_detail_placeholder = 'E.g. 10 For $10';
              this.showincentivedetail = true;
              break;
          case '3':
              //Extra service
              this.referral_incentive_detail = 'Enter Service Detail'
              this.referral_incentive_detail_placeholder = 'E.g. 2 Hours Extra Serivce'
              this.showincentivedetail = true;
              break;
          case '4':
              //Other
              this.referral_incentive_detail = 'Enter Incentive Detail'
              this.referral_incentive_detail_placeholder = 'E.g. Any Other Incentive Detail'
              this.showincentivedetail = true;
              break;
          default:
             this.showincentivedetail = false;
      }//end switch
    }
    else {
      this.showincentivedetail = false;
    }
  }
}
