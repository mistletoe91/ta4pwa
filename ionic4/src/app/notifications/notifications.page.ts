import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router ,NavigationExtras} from '@angular/router';
import { AlertController, ModalController, ToastController,Platform} from '@ionic/angular';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../const';
import { connreq } from '../../models/interfaces/request';
import { TpstorageProvider } from '../tpstorage.service';
import { LoadingService } from '../../services/loading.service';

//import { FcmService } from '../../services/fcm.service';
import { config,myEnv, userPicArr } from '../app.angularfireconfig';
import * as firebase from 'firebase/app';
if (!firebase.apps.length) {firebase.initializeApp(config);}

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: [
    './styles/notifications.page.scss',
    './styles/notifications.shell.scss'
  ]
})
export class NotificationsPage implements OnInit {
  firebuddychats = firebase.database().ref('/buddychats');
  firedata = firebase.database().ref('/chatusers');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  fireInvitationSent = firebase.database().ref('/invitationsent');
  firePhones = firebase.database().ref('/phones');

  lastMsgReader:any = {};
  blockRequest: any = [];
  subscriptionAddedObj = {};
  subscriptionAddedFriendsObj = {};

  notifications: any;
  userId;
  myfriends;
  myfriends_FromFirebase;
  isNoRecord:boolean = false;
  myfriends_backup;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
		private platform: Platform,
		public loadingProvider: LoadingService,
    public alertCtrl: AlertController,
		private sqlite: SQLite,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		public tpStorageService: TpstorageProvider
    ) {



      this.tpStorageService.get('userUID').then((myUserId: any) => {
        this.userId = myUserId;
        setTimeout(() => {
          //This should be in constructor
          this.getmyfriends_ (false );
        },  1);
      }).catch(e => {

        //mytodo : redirect to login page
      });

   }
   ionViewDidEnter (){
     this.fnGetLocalCache ();
   }
   sendReferral (buddy_uid){
     console.log ("buddy_uid:" + buddy_uid);
   }

   initializeItems (){
     if(!this.myfriends_backup){
       //init it first time
       this.myfriends_backup = this.myfriends;
     } else {
       //subsequent time pick from backup
       this.myfriends = this.myfriends_backup;
     }
   }

   getItems(ev) {
     // Reset items back to all of the items
     this.initializeItems();

     // set val to the value of the ev target
     var val = ev.target.value;

     // if the value is an empty string don't filter the items
     if (val && val.trim() != '') {
       this.myfriends = this.myfriends.filter((item) => {
         return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
       })
     }
   }



         fnGetLocalCache() {


             this.sqlite.create({
               name: 'gr.db',
               location: 'default'
             }).then((db: SQLiteObject) => {

               //Get Friends
               db.executeSql(CONST.create_table_statement_tapally_friends, [])
               .then(res => {
                   db.executeSql("SELECT * FROM cache_tapally_friends", [])
                   .then(res => {
                             ////////console.log ("Going to read from  cache_tapally_friends <--------<<<<<-------------");
                             ////////console.log (res.rows.length + " Records");
                             if(res.rows.length>0){

                                    //if row exists
                                    ////////console.log ("RESETTING myfriends = []");
                                    this.myfriends = [];
                                    let uniqueFriends = {};
                                    this.isNoRecord = false;
                                    for (var i = 0; i < res.rows.length; i++) {
                                               ////////////console.log (res.rows.item(i));
                                              let isBlock_ = false ;
                                              if(res.rows.item(i).isBlock){
                                                isBlock_ = true ;
                                              }
                                              let isActive_ = true ;
                                              if(!res.rows.item(i).isactive){
                                                isActive_ = false ;
                                              }

                                              let  record = {};
                                              if(res.rows.item(i).gpflag>0){
                                                //Group
                                              } else {
                                                //////////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                                 //Individual
                                                 let lastMssg = '';
                                                 if(res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message!= "undefined"){
                                                   lastMssg = res.rows.item(i).lastMessage_message;
                                                 }
                                                 let tmofmsg = '';
                                                 if(res.rows.item(i).timeofmsg){
                                                   tmofmsg = res.rows.item(i).timeofmsg;
                                                 }
                                                 record = {
                                                     uid : res.rows.item(i).uid,
                                                     displayName : res.rows.item(i).displayName,
                                                     gpflag : res.rows.item(i).gpflag,
                                                     isActive : isActive_,
                                                     isBlock : isBlock_,
                                                     lastMessage : {
                                                       message: lastMssg,
                                                       type: res.rows.item(i).lastMessage_type,
                                                       dateofmsg: res.rows.item(i).lastMessage_dateofmsg,
                                                       selectCatId : res.rows.item(i).selectCatId,
                                                       referral_type : res.rows.item(i).referral_type,
                                                       timeofmsg: tmofmsg,
                                                       isRead: false,
                                                       isStarred: false
                                                     },
                                                     mobile : res.rows.item(i).mobile,
                                                     selection : res.rows.item(i).selection,
                                                     unreadmessage : 0,
                                                     photoURL : res.rows.item(i).photoURL,
                                                     localphotoURL : '',
                                                     deviceToken:res.rows.item(i).deviceToken
                                                 };
                                              }

                                              ////////console.log ("Check1 ");
                                              if(res.rows.item(i).gpflag>1){
                                                //group
                                              } else {
                                                //individual
                                                if(!uniqueFriends ["individual"+res.rows.item(i).uid]){
                                                  var lengthOfArr = this.myfriends.push ( record ) ;
                                                  uniqueFriends ["individual"+res.rows.item(i).uid] = true;
                                                }
                                              }
                                    }//end for

                                    console.log ("From cache ");
                                    console.log (this.myfriends);

                                    if (this.myfriends.length == 0) {
                                        this.isNoRecord = true;
                                    }

                             } else {
                               //nothing in cache
                               //get from firebase
                               this.getmyfriends_ (true );
                             }
                   })
                   .catch(e => {
                       //Error in operation
                   });
               })
               .catch(e => {
               });//create table

             });//create database
      }//end function

      //no need to send insert statement because contacts page will do that
      updateContactsTable (db, allChatListing__){
          if(!allChatListing__.mobile || allChatListing__.mobile =="undefined"){
            return ;
          }
          if(allChatListing__.photoURL == null){
            allChatListing__.photoURL = ""; //mytodo set default pic
          }

          db.executeSql("UPDATE cache_tapally_contacts SET uid='"+allChatListing__.uid+"',photoURL='"+allChatListing__.photoURL+"' where mobile = '"+allChatListing__.mobile+"'", [])
          .then(res => {}).catch(e => {
            console.log ("ERROR UPDATE cache_tapally_contacts");
            console.log (e);

           });

      }//end function

      fnDeleteAndCreateCacheOfList( ) {
              this.sqlite.create({
                name: 'gr.db',
                location: 'default'
              }).then((db: SQLiteObject) => {
                //Update  Local Cahce
                db.executeSql(CONST.create_table_statement_tapally_friends, [])
                .then(res => {
                    db.executeSql("DELETE  FROM cache_tapally_friends", []).then(res => {
                                         this.isNoRecord = false;
                                         let uniquemyFriends = {};
                                         for (var i = 0; i < this.myfriends_FromFirebase.length; i++) {

                                           let recGotInserted = false;
                                           if(this.myfriends_FromFirebase[i].gpflag>0){
                                                //Group
                                           } else {
                                                //Individual
                                                   //do not insert duplicate
                                                   if(uniquemyFriends ["indivial"+this.myfriends_FromFirebase[i].uid]){
                                                     console.log ("SKipping Record");
                                                     continue;
                                                   } else {
                                                     uniquemyFriends ["indivial"+this.myfriends_FromFirebase[i].uid] = true;
                                                   }

                                                  let isactive_ = 0;
                                                  if(this.myfriends_FromFirebase[i].isActive){
                                                    isactive_ = 1;
                                                  }
                                                  let isBlock_ = 0;
                                                  if(this.myfriends_FromFirebase[i].isBlock){
                                                    isBlock_ = 1;
                                                  }
                                                  let selection_ = 0;
                                                  if(this.myfriends_FromFirebase[i].selection){
                                                    selection_ = 1;
                                                  }

                                                  console.log ("INSERTING INTO cache_tapally_friends");
                                                  console.log ("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES ("+isactive_+",'"+this.myfriends_FromFirebase[i].uid+"','"+this.myfriends_FromFirebase[i].gpflag+"','"+selection_+"',"+isBlock_+",0,'"+this.myfriends_FromFirebase[i].displayName+"','"+this.myfriends_FromFirebase[i].mobile+"','"+this.myfriends_FromFirebase[i].photoURL+"','"+this.myfriends_FromFirebase[i].lastMessage.message+"','"+this.myfriends_FromFirebase[i].lastMessage.type+"','"+this.myfriends_FromFirebase[i].lastMessage.dateofmsg+"','"+this.myfriends_FromFirebase[i].groupimage+"','"+this.myfriends_FromFirebase[i].groupName+"','"+this.myfriends_FromFirebase[i].lastmsg+"','"+this.myfriends_FromFirebase[i].dateofmsg+"','"+this.myfriends_FromFirebase[i].timeofmsg+"','"+this.myfriends_FromFirebase[i].selectCatId+"','"+this.myfriends_FromFirebase[i].referral_type+"','"+this.myfriends_FromFirebase[i].deviceToken+"')");


                                                   db.executeSql("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES ("+isactive_+",'"+this.myfriends_FromFirebase[i].uid+"','"+this.myfriends_FromFirebase[i].gpflag+"','"+selection_+"',"+isBlock_+",0,'"+this.myfriends_FromFirebase[i].displayName+"','"+this.myfriends_FromFirebase[i].mobile+"','"+this.myfriends_FromFirebase[i].photoURL+"','"+this.myfriends_FromFirebase[i].lastMessage.message+"','"+this.myfriends_FromFirebase[i].lastMessage.type+"','"+this.myfriends_FromFirebase[i].lastMessage.dateofmsg+"','"+this.myfriends_FromFirebase[i].groupimage+"','"+this.myfriends_FromFirebase[i].groupName+"','"+this.myfriends_FromFirebase[i].lastmsg+"','"+this.myfriends_FromFirebase[i].dateofmsg+"','"+this.myfriends_FromFirebase[i].timeofmsg+"','"+this.myfriends_FromFirebase[i].selectCatId+"','"+this.myfriends_FromFirebase[i].referral_type+"','"+this.myfriends_FromFirebase[i].deviceToken+"')", [])
                                                   .catch(e => {
                                                         console.log ("ERROR inserting in cache_tapally_friends");
                                                         console.log (e);
                                                   });
                                                    console.log ("1 ");
                                                   //Also update cache_tapally_contacts
                                                   this.updateContactsTable (db,this.myfriends_FromFirebase[i]);
                                                    console.log ("2 ");
                                                   //Update in ARray as well
                                                   console.log (this.myfriends_FromFirebase);
                                                   for(let g=0;g<this.myfriends.length;g++){
                                                       console.log (this.myfriends_FromFirebase[i].uid +":"+ this.myfriends[g].uid);
                                                       if(this.myfriends_FromFirebase[i].uid == this.myfriends[g].uid){
                                                          console.log ("3 ");
                                                          //update photo url
                                                          if(this.myfriends[g].photoURL != this.myfriends_FromFirebase[i].photoURL){
                                                               this.myfriends[g].photoURL = this.myfriends_FromFirebase[i].photoURL;
                                                          }//endif
                                                          console.log ("4 ");
                                                          this.myfriends[g].displayName = this.myfriends_FromFirebase[i].displayName;
                                                          this.myfriends[g].lastMessage = this.myfriends_FromFirebase[i].lastMessage;
                                                          console.log ("5 ");
                                                          recGotInserted = true;
                                                          break;
                                                       }
                                                   }//endfor

                                                   console.log ("FOR DONE ");

                                           }//endif

                                           if(!recGotInserted) {
                                              this.myfriends.push ( this.myfriends_FromFirebase[i]);
                                           }

                                         }//end for

                                         if (this.myfriends_FromFirebase.length == 0) {
                                             this.isNoRecord = true;
                                         }

                                         //Subscribe to new msg  jaswinder
                                         //mytodo subscribe to last message

                    }).catch(e => {
                        //Error in operation
                    });
                })
                .catch(e => {
                });//create table

              });//create database
      }//end function

   getmyfriends_ (updateFriends){
       this.firefriends.child(this.userId).on('value', (snapshot) => {
            this.myfriends_FromFirebase = [];
            let allfriends = snapshot.val();
            if (allfriends != null) {
                  // I have some friends
                 ////console.log ("I have friends " );
                  let CounterFriends_ = 0;
                  for (let i in allfriends) {
                     ////console.log ("iiii ")     ;
                     let lastMsgActual__ = {
                       message: "",
                       type: "",
                       dateofmsg: "",
                       timeofmsg: "",
                       isRead: "",
                       isStarred: "",
                       referral_type : "",
                       selectCatId : ""
                     };
                     if(!allfriends[i].isBlock) {
                       allfriends[i].isBlock = false;
                     }
                     allfriends[i].unreadmessage=lastMsgActual__;
                     allfriends[i].lastMessage=lastMsgActual__;
                     allfriends[i].business='';
                     allfriends[i].gpflag=0;
                     allfriends[i].read=false;
                     this.myfriends_FromFirebase.push(allfriends[i]);
                     CounterFriends_++;
                  }//end for
                  //////console.log ("this.cnter "+ this.cnter);

                 //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
                 let friendsSoFar = 0;
                 for (let y in this.myfriends_FromFirebase) {

                   if(true) {
                    this.firebuddychats.child(this.userId).child(this.myfriends_FromFirebase[y].uid).limitToLast(1).once('value', (snapshot_last_msg) => {
                           let lastMsg__ = snapshot_last_msg.val();
                           if(lastMsg__){
                             for(var key in lastMsg__){
                               let lastMsgActual__ = {
                                 message: lastMsg__[key].message,
                                 type: lastMsg__[key].type,
                                 dateofmsg: lastMsg__[key].dateofmsg,
                                 timeofmsg: lastMsg__[key].timeofmsg,
                                 referral_type : lastMsg__[key].referral_type,
                                 selectCatId : lastMsg__[key].selectCatId ,
                                 isRead: false,
                                 isStarred: false
                               };
                               this.myfriends_FromFirebase[y].unreadmessage = lastMsgActual__;
                               this.myfriends_FromFirebase[y].lastMessage = lastMsgActual__;
                               break;
                             }//end for
                           }//endif

                           if(++friendsSoFar >= CounterFriends_){
                              console.log ("I have Friends and calling done");
                              this.doneLoadingFriendsFromFirebase (updateFriends);
                           }
                    });
                    this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends_FromFirebase[y].uid] = true;
                  } else {
                    //this friend has already been subscripted
                  }//endif
                 }//end for

            } else {
                 console.log ("I dont have any friend (:");
                 this.doneLoadingFriendsFromFirebase (updateFriends);
            }//endif


       });//get all friends
   }

   doneLoadingFriendsFromFirebase (updateFriends) {
     console.log ("Done Loading Friends from Firebase  ");
     console.log ( this.myfriends_FromFirebase );

     if(updateFriends){
       console.log ("this.myfriends UPDATED");
       this.myfriends = this.myfriends_FromFirebase;
     }

     this.fnDeleteAndCreateCacheOfList ();
   }



  addbuddy (){
    this.router.navigateByUrl('/invite');
  }
  gotobuddypage (friend){
    let navigationExtras: NavigationExtras = {
          queryParams: {
            buddy_uid: friend.uid
          }
    };
    this.router.navigate(['/buddychat'],navigationExtras);
  }

  askForReferral (){
    this.router.navigateByUrl('/referral');
  }

  ngOnInit(): void {
    /*
    if (this.route && this.route.data) {
      this.route.data.subscribe(resolvedData => {
        const dataSource = resolvedData['data'];
        if (dataSource) {
          dataSource.source.subscribe(pageData => {
            if (pageData) {
              this.notifications = pageData;
            }
          });
        }
      });
    }
    */
  }
}
