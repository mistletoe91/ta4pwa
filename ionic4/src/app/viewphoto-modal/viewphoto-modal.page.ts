import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
@Component({
  selector: 'app-viewphoto-modal',
  templateUrl: './viewphoto-modal.page.html',
  styleUrls: ['./viewphoto-modal.page.scss'],
})
export class ViewphotoModalPage implements OnInit {
  url:string;
  constructor(private modalController: ModalController,private navParams: NavParams,
  private router: Router) { }
 
  ngOnInit() {
     console.table(this.navParams);
  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
}
