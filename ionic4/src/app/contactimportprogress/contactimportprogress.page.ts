import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { ModalController, MenuController, Events,AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import {  Platform } from '@ionic/angular';
import { connreq } from '../../models/interfaces/request';
import { TpstorageProvider } from '../tpstorage.service';
import { UserService } from '../../services/user.service';
import { LoadingService } from '../../services/loading.service';
import { ContactService } from '../../services/contact.service';

@Component({
  selector: 'app-contactimportprogress',
  templateUrl: './contactimportprogress.page.html',
  styleUrls: ['./contactimportprogress.page.scss'],
})
export class ContactimportprogressPage implements OnInit {
  userId: any;
  sourcePage: any;
  constructor(
    private route: ActivatedRoute,
    public contactProvider: ContactService,
    public tpStorageService: TpstorageProvider,
    public events: Events,
    public router: Router) {

    }



  ngOnInit (){

    this.route.queryParams.subscribe(params => {
          if (params && params.source) {
            this.sourcePage  = params.source;
            console.log ("paramData");
            console.log (this.sourcePage);
          }
    });

    this.tpStorageService.getItem('userUID').then((res: any) => {
       if(res){
         this.userId = res;
       }
    }).catch(e => { });

    //first delete the records
    this.contactProvider.deleteFromTable()
    .then(res => {

      //then insert them back
      this.contactProvider.getSimJsonContacts()
      .then(res => {
          this.tpStorageService.setItem('contactsImportedFromDevice', '1');
          this.events.subscribe('contactInsertingDone', () => {
            this.events.unsubscribe('contactInsertingDone');
            console.log ("> "+this.contactProvider.contactInsertingDone);
            if(!this.contactProvider.contactInsertingDone){
            } else {
              this.sendToContactsPage ();
            }
          });
      }).catch(e => {
        //mytodo error handling 

       });

    }).catch(e => {

      //mytodo : error handling : send to back or retry
    });


  }

sendToContactsPage() {
  let navigationExtras: NavigationExtras = {
        queryParams: {
          source: "contactimportprogresspage"
        }
  };
  this.router.navigate([ '/contact'], navigationExtras );
}

}
