import { Component, OnInit } from '@angular/core';
import {   Router   } from '@angular/router';
@Component({
  selector: 'app-sendareferral-info',
  templateUrl: './sendareferral-info.page.html',
  styleUrls: ['./sendareferral-info.page.scss'],
})
export class SendareferralInfoPage implements OnInit {
  counter=0;
  items = [];
  constructor(private router: Router) {

  }

  requestservice (){

  }

  sendareferral (){
    this.router.navigateByUrl('/sendareferral-options');
  }

  ngOnInit() {
    this.loadItems ();
  }

  loadItems (){
    console.log ("loadItems");
    for(let b=0;b<2;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Myself",
          date : "29 June 2019",
          redeemed : 1,
          note_from_business : "A Free Ice Creame was redeemed"
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<3;b++){
      let record = {
        uid : "jd9oijoij12oi3j123",
        name : "Johny",
        date : "24 June 2019",
        redeemed : 0
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
        uid : "jd9oijoij12oi3j123",
        name : "Gary Gobin",
        date : "22 June 2019",
        redeemed : 1,
        note_from_business : "A waffle was redeemed"
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Michakel Cobrin",
          date : "15 June 2019",
          redeemed : 0,
          note_from_business : ""
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<4;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "John Martin",
          date : "13 June 2019",
          redeemed : 1,
          note_from_business : "A Free Ice Creame was redeemed"
       };
       this.items.push ( record );
    }//end for

  }
}
