import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarnDollarsPage } from './earn-dollars.page';

describe('EarnDollarsPage', () => {
  let component: EarnDollarsPage;
  let fixture: ComponentFixture<EarnDollarsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarnDollarsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarnDollarsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
