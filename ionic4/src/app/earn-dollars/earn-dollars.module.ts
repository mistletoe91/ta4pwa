import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EarnDollarsPage } from './earn-dollars.page';

const routes: Routes = [
  {
    path: '',
    component: EarnDollarsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EarnDollarsPage]
})
export class EarnDollarsPageModule {}
