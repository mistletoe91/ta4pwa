import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router ,NavigationExtras} from '@angular/router';
import { AlertController, ModalController, ToastController,Platform} from '@ionic/angular';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../const';
import { connreq } from '../../models/interfaces/request';
import { TpstorageProvider } from '../tpstorage.service';
//import { UserService } from '../../services/user.service';
import { LoadingService } from '../../services/loading.service';
import { AngularFireModule } from '@angular/fire';
//import { ContactService } from '../../services/contact.service';
//import { RequestsService } from '../../services/requests.service';
//import { SmsService } from '../../services/sms.service';
//import { ChatService } from '../../services/chat.service';
//import { FcmService } from '../../services/fcm.service';
import { config,myEnv, userPicArr } from '../app.angularfireconfig';
import * as firebase from 'firebase/app';
firebase.initializeApp(config);

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {

  firebuddychats = firebase.database().ref('/buddychats');
  firedata = firebase.database().ref('/chatusers');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  fireInvitationSent = firebase.database().ref('/invitationsent');
  firePhones = firebase.database().ref('/phones');

  lastMsgReader:any = {};
  blockRequest: any = [];
  subscriptionAddedObj = {};
  subscriptionAddedFriendsObj = {};

  notifications: any;
  userId;
  myfriends;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
		private platform: Platform,
		public loadingProvider: LoadingService,
    public alertCtrl: AlertController,
		private sqlite: SQLite,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		public tpStorageService: TpstorageProvider
    ) {

   }

   getmyfriends_ (){
       console.log ("getmyfriends_");
       this.firefriends.child(this.userId).on('value', (snapshot) => {
            this.myfriends = [];
            let allfriends = snapshot.val();
            if (allfriends != null) {
                  // I have some friends
                 ////console.log ("I have friends " );
                  let CounterFriends_ = 0;
                  for (let i in allfriends) {
                     ////console.log ("iiii ")
                     let lastMsgActual__ = {
                       message: "",
                       type: "",
                       dateofmsg: "",
                       timeofmsg: "",
                       isRead: "",
                       isStarred: "",
                       referral_type : "",
                       selectCatId : ""
                     };
                     if(!allfriends[i].isBlock) {
                       allfriends[i].isBlock = false;
                     }
                     allfriends[i].unreadmessage=lastMsgActual__;
                     allfriends[i].lastMessage=lastMsgActual__;
                     allfriends[i].business='';
                     allfriends[i].read=false;
                     this.myfriends.push(allfriends[i]);
                     CounterFriends_++;
                  }//end for
                  //////console.log ("this.cnter "+ this.cnter);

                 //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
                 let friendsSoFar = 0;
                 for (let y in this.myfriends) {

                   if(true) {
                    this.firebuddychats.child(this.userId).child(this.myfriends[y].uid).limitToLast(1).once('value', (snapshot_last_msg) => {
                           let lastMsg__ = snapshot_last_msg.val();
                           if(lastMsg__){
                             for(var key in lastMsg__){
                               let lastMsgActual__ = {
                                 message: lastMsg__[key].message,
                                 type: lastMsg__[key].type,
                                 dateofmsg: lastMsg__[key].dateofmsg,
                                 timeofmsg: lastMsg__[key].timeofmsg,
                                 referral_type : lastMsg__[key].referral_type,
                                 selectCatId : lastMsg__[key].selectCatId,
                                 isRead: false,
                                 isStarred: false
                               };
                               this.myfriends[y].unreadmessage = lastMsgActual__;
                               this.myfriends[y].lastMessage = lastMsgActual__;
                               break;
                             }//end for
                           }//endif

                           if(++friendsSoFar >= CounterFriends_){
                              console.log ("1. doneLoadingFriendsFromFirebase");
                              this.doneLoadingFriendsFromFirebase ();
                           }
                    });
                  } else {
                    //this friend has already been subscripted
                  }//endif
                 }//end for

            } else {
               // I dont have any friend (:
                 console.log ("2. doneLoadingFriendsFromFirebase");
                 this.doneLoadingFriendsFromFirebase ();
            }//endif


       });//get all friends
   }

   doneLoadingFriendsFromFirebase () {
     console.log ("Done Loading Friends ");
     console.log ( this.myfriends );
   }

   ionViewDidEnter (){
     console.log ("ionViewDidLoad");
     this.tpStorageService.get('userUID').then((myUserId: any) => {
       this.userId = myUserId;
       this.getmyfriends_ ();
     }).catch(e => {

       //mytodo : redirect to login page
     });

   }


  ngOnInit(): void {
  }

}
