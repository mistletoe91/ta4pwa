import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, MenuController } from '@ionic/angular';

import { TermsOfServicePage } from '../terms-of-service/terms-of-service.page';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy.page';
import { PasswordValidator } from '../validators/password.validator';
import { AuthenticationService } from '../authentication.service';
import { ToastController } from '@ionic/angular';

import { TpstorageProvider } from '../tpstorage.service';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: [
    './styles/signup.page.scss'
  ]
})
export class SignupPage implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required' }
    ],
    'matching_passwords': [
      { type: 'areNotEqual', message: 'Password mismatch' }
    ]
  };

  constructor(
    public router: Router,
    public modalController: ModalController,
    public menu: MenuController,
    private authService: AuthenticationService,
    public toastController: ToastController,
    public tpStorageService: TpstorageProvider,
    public userService:UserService
  ) {
    this.matching_passwords_group = new FormGroup({
      'password': new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      'confirm_password': new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areNotEqual(formGroup);
    });

    this.signupForm = new FormGroup({
      'email': new FormControl('test@test.com', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      'matching_passwords': this.matching_passwords_group
    });
  }

  ngOnInit(): void {
    this.menu.enable(false);
  }

  async showTermsModal() {
    const modal = await this.modalController.create({
      component: TermsOfServicePage
    });
    return await modal.present();
  }

  async showPrivacyModal() {
    const modal = await this.modalController.create({
      component: PrivacyPolicyPage
    });
    return await modal.present();
  }

  doSignup(value): void {
    console.log('do sign up');
    this.tryRegister(value);
  }

  async presentToast(msg ,in_color ) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color : in_color
    });
    toast.present();
  }

  tryRegister(value){
    this.userService.getWebsiteUserDetails(value.email)
     .then(res_1 => {
              if (res_1) {

                  let i_userId = '';
                  let i_paid = 0;
                  for(var i in res_1) {
                     if(i == "userId"){
                        //userId found
                        i_userId = res_1 [i];
                        continue;
                     }
                     if(i == "paid"){
                        //paid found
                        i_paid = res_1 [i];
                        continue;
                     }
                  }

                  console.log ("i_userId : " + i_userId);
                  console.log ("i_paid : " + i_paid);

                  //User registered on website
                  if(i_userId){
                        //user already registerd in past
                        this.registerFirebaseAuth (value, true , i_userId , i_paid );
                  } else {
                        //user does not have uid so its Fresh User
                        this.registerFirebaseAuth (value, false , null , i_paid);
                  }
              } else {
                 //User not registerd so its Fresh User
                 this.registerFirebaseAuth (value, false, null , 0 );
              }
     }, err => {
              this.registerFirebaseAuth (value, false, null , 0 );
     });
  }

  registerFirebaseAuth (value, userExistOnWebsiteUsers, oldUid , isPaid ){
    this.authService.registerUser(value)
     .then(res => {
       if( true) {
          if(oldUid){
            //mytodo_critical  : this is conflict : What should we do if there is old uid
          }
          this.presentToast( "Welcome Aboard ! Your account has been created", "dark");
          this.router.navigate(['forms-and-validations']);
          this.tpStorageService.setItem('userUID', res.user.uid);
          this.tpStorageService.setItem('app_username', value.email);
          this.tpStorageService.setItem('app_password', value.matching_passwords.password);
          this.userService.updateWebsiteUserDetails(value.email, res.user.uid, isPaid);
       }

     }, err => {
       //User already exists so try to login
       //mytodo : try to login
       this.presentToast( err.message, "danger");
       /*console.log ("userExistOnWebsiteUsers");
       console.log (userExistOnWebsiteUsers);
       if( !userExistOnWebsiteUsers ) {
          //it does not exist on real-time database
          this.userService.updateWebsiteUserDetails(value.email, '', 0);
       }
       */
     })
  }//end function

  doFacebookSignup(): void {
    console.log('facebook signup');
    this.router.navigate(['forms-and-validations']);
  }

  doGoogleSignup(): void {
    console.log('google signup');
    this.router.navigate(['forms-and-validations']);
  }

  doTwitterSignup(): void {
    console.log('twitter signup');
    this.router.navigate(['forms-and-validations']);
  }
}
