import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-referral-modal',
  templateUrl: './referral-modal.page.html',
  styleUrls: ['./referral-modal.page.scss'],
})
export class ReferralModalPage implements OnInit {
  validationsForm: FormGroup;
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private router: Router,
    public toastController:ToastController
  ) { }

  onSubmit (){
      this.presentToast ();

      let navigationExtras: NavigationExtras = {
            queryParams: {
              cat_id: 1
            }
      };
      this.router.navigate([ '/app/categories'], navigationExtras );//HomeAfterINeedTodayPage
      this.modalController.dismiss(); // close modal
  }

  async presentToast() {
      const toast = await this.toastController.create({
        message: 'Your request has been posted in your network. It may take some time before your contacts send you a referral back. Meanwhile, feel free to browse nearby businesses',
        duration: 7000,
        color: "dark",
        position : "top"
      });
      toast.present();
    }



  ngOnInit() {
      console.table(this.navParams);

      this.validationsForm = new FormGroup({
        'name': new FormControl('', Validators.required)
      });

  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }


    fnGoToHomeAfterINeedTodayPage(item_id) {

      let navigationExtras: NavigationExtras = {
            queryParams: {
              cat_id: item_id
            }
      };
      console.log (item_id);
      this.router.navigate([ '/contactimportprogress'], navigationExtras );//HomeAfterINeedTodayPage
    }
}
