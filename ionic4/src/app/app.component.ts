import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

//For testing only
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from './const';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    './side-menu/styles/side-menu.scss',
    './side-menu/styles/side-menu.shell.scss',
    './side-menu/styles/side-menu.responsive.scss'
  ]
})
export class AppComponent {
  appPages = [
    {
      title: 'Home',
      url: '/app/home',
      icon: 'home'
    },
    {
      title: 'Profile Settings',
      url: '/app/user',
      icon: 'profile'
    },
    {
      title: 'Business Settings',
      url: '/registerbusiness',
      icon: 'map'
    },
    {
      title: 'How it works',
      url: '/howitworks',
      icon: 'help-circle'
    }
  ];
  accountPages = [
    {
      title: 'Log In',
      url: '/auth/login',
      icon: './assets/sample-icons/side-menu/login.svg'
    },
    {
      title: 'Sign Up',
      url: '/auth/signup',
      icon: './assets/sample-icons/side-menu/signup.svg'
    },
    {
      title: 'Tutorial',
      url: '/walkthrough',
      icon: './assets/sample-icons/side-menu/tutorial.svg'
    },
    {
      title: 'Getting Started',
      url: '/getting-started',
      icon: './assets/sample-icons/side-menu/getting-started.svg'
    },
    {
      title: '404 page',
      url: '/page-not-found',
      icon: './assets/sample-icons/side-menu/warning.svg'
    }
  ];

  textDir = 'ltr';
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private sqlite: SQLite
  ) {
    //this.dropTable ('cache_tapally_friends');
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  dropAllStorage (){
    ////console.log("DROP Everything and start fresh ");
    this.dropTable ('cache_users_local');
    this.dropTable ('cache_tapally_contacts');
    this.dropTable ('cache_tapally_friends');
    this.dropTable ('key_val');
    //this.tpStorageService.clear();
  }

  dropTable(tbl) {
      this.sqlite.create({
        name: 'gr.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        console.log ("DROPPED TABLE "+tbl);
        db.executeSql( "DROP TABLE "+tbl, []);
      });//create database
 }//end function

}
