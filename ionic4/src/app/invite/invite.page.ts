import { Component, OnInit } from '@angular/core';
import { Router,NavigationExtras  } from '@angular/router';
import { ContactService } from '../../services/contact.service';
@Component({
  selector: 'app-invite',
  templateUrl: './invite.page.html',
  styleUrls: ['./invite.page.scss'],
})
export class InvitePage implements OnInit {
  redirectTo:string = '/contactimportprogress';
  constructor(private router: Router,public contactProvider: ContactService) { }

  ngOnInit() {
    this.contactProvider.ContactsCount()
    .then(res_count => {
          console.log ("1> res_count : ");
          console.log (res_count);
          if(res_count>0){
              this.redirectTo = '/contact';
          }
    }).catch(e => {
    });
  }//end function

  openContacts (){
    let navigationExtras: NavigationExtras = {
          queryParams: {
            source: "invitepage"
          }
    };
    //console.log ("2> res_count : "+res_count);
    this.router.navigate([ this.redirectTo], navigationExtras );
  }//end function

}
