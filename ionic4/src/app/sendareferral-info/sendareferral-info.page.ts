import { Component, OnInit } from '@angular/core';
import {   Router   } from '@angular/router';
@Component({
  selector: 'app-sendareferral-info',
  templateUrl: './sendareferral-info.page.html',
  styleUrls: ['./sendareferral-info.page.scss'],
})
export class SendareferralInfoPage implements OnInit {
  counter=0;
  items = [];
  constructor(private router: Router) {

  }

  requestservice (){

  }

  sendareferral (){
    this.router.navigateByUrl('/sendareferral-options');
  }

  ngOnInit() {
  }

  loadItems (){
    console.log ("loadItems");

    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Gary Standingonthewall",
          date : "31 June 2019",
          status : 1,
          note_from_business : "A gift card of $50 is being send to your address, please allow 5-10 business days to deliver"
       };
       this.items.push ( record );
    }//end for

    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Myself",
          date : "29 June 2019",
          status : 1,
          note_from_business : "A gift card of $50 is being send to your address, please allow 5-10 business days to deliver"
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Colin Paul",
          date : "18 June 2019",
          status : 0,
          note_from_business : "Tried calling Colin Paul several times, phone goes VM. Please follow with him & contact me"
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Gary Gobin",
          date : "15 June 2019",
          status : 2,
          note_from_business : ""
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "Michakel Cobrin",
          date : "15 June 2019",
          status : 1,
          note_from_business : ""
       };
       this.items.push ( record );
    }//end for
    for(let b=0;b<1;b++){
      let record = {
          uid : "jd9oijoij12oi3j123",
          name : "John Martin",
          date : "13 June 2019",
          status : 1,
          note_from_business : "A gift card of $50 is being send to your address, please allow 5-10 business days to deliver"
       };
       this.items.push ( record );
    }//end for

  }
}
