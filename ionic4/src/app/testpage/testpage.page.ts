import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-testpage',
  templateUrl: './testpage.page.html',
  styleUrls: ['./testpage.page.scss'],
})
export class TestpagePage implements OnInit {

  constructor(private router: Router,public modalController: ModalController) { }

  goto (pg){
    this.router.navigateByUrl(pg);// "/referral"
  }
  ngOnInit() {
  }

}
