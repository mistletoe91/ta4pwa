import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowitworksPage } from './howitworks.page';

describe('HowitworksPage', () => {
  let component: HowitworksPage;
  let fixture: ComponentFixture<HowitworksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowitworksPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowitworksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
