import { Component, OnInit } from '@angular/core';
import {   Router   } from '@angular/router';
import { Location } from "@angular/common";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-loyaltydetail',
  templateUrl: './loyaltydetail.page.html',
  styleUrls: ['./loyaltydetail.page.scss'],
})
export class LoyaltydetailPage implements OnInit {

  constructor(private location: Location,private router: Router,public toastController: ToastController) { }

  sendTo (){
    this.presentToast ();
    this.location.back();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Free Ice Cream Has Been Marked Redeemed.',
      duration: 2000,
      color : "dark",
      position : "top"
    });
    toast.present();
  }

  ngOnInit() {
  }

}
