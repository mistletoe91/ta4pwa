import { Component, ViewChild,OnInit,ElementRef } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { ModalController, MenuController, Events,AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import {  Platform } from '@ionic/angular';
import { connreq } from '../../models/interfaces/request';
import { TpstorageProvider } from '../tpstorage.service';
import { LoadingService } from '../../services/loading.service';
import { CatsService } from '../../services/cats.service';
import { ViewphotoModalPage } from '../viewphoto-modal/viewphoto-modal.page';
import { config,myEnv } from '../app.angularfireconfig';
import { UserService } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';
import * as firebase from 'firebase/app';
if (!firebase.apps.length) {firebase.initializeApp(config);}

@Component({
  selector: 'app-buddychat',
  templateUrl: './buddychat.page.html',
  styleUrls: ['./buddychat.page.scss'],
})
export class BuddychatPage implements OnInit {
  firebuddychats = firebase.database().ref('/buddychats');
  /*firedata = firebase.database().ref('/chatusers');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  fireInvitationSent = firebase.database().ref('/invitationsent');
  firePhones = firebase.database().ref('/phones');
  */
  userId;

  newmessage = '';
  newmessageTmp = '';
  referralRequestPrefix;
  referralRequestPostfix;
  isRefresher:boolean = false;
  allmessages = [];
  filterallmessages = [];
  tempFilterallmessages = [];
  photoURL;
  imgornot;
  todaydate;
  myfriends;
  buddy = {uid:""};
  buddymessages = [];
  myfriendsList;
  tempmyfriendsList;
  allmessagesGroups = [];
  isData: boolean = false;
  buddyStatus: any;
  datacounter = 50;
  headercopyicon = true;
  catItems;
  showheader:boolean = true;
  selectAllMessage = [];
  selectCounter = 0;
  UnreadMSG: number;
  isuserBlock: boolean = false;
  msgstatus: boolean = false;
  backbuttonstatus: boolean = false;
  DeleteMsg;
  private mutationObserver: MutationObserver;



    @ViewChild('content') private content: any;

    buddy_uid;
    scrollToBottomOnInit() {
      console.log ("first Time scroll");
      this.content.scrollToBottom(300);
    }
    callFunctionScroll() {
      if(this.content._scroll)  {
        console.log ("Scrolled callFunctionScroll");
        setTimeout(function(){ this.content.scrollToBottom(0); }, 3000);
      }
    }
  constructor(
    private route: ActivatedRoute,
    public tpStorageService: TpstorageProvider,
    public events: Events,
    public router: Router,
    public userservice: UserService,
    public chatservice: ChatService,
    private modalController: ModalController,
    public catservice: CatsService,
    public actionSheetController: ActionSheetController
  ) {
  }
  async initializeItems() {
      this.catItems = this.catservice.getCats();
  }
  async openAllImg(url, $event) {
    console.log (url);
    const modal = await this.modalController.create({
      component: ViewphotoModalPage,
      componentProps: {
        'url': url
      }
    });
    return await modal.present();
  }
  ionViewDidEnter (){

    this.tpStorageService.get('userUID').then((myUserId: any) => {
      this.userId = myUserId;
      console.log ("my USERID");
      console.log (this.userId);

      this.initializeItems ();
      this.todaydate = this.formatDate(new Date());
      this.getbuddymessages(this.datacounter);

    }).catch(e => {
      //mytodo : redirect to login page
    });

  }
  formatDate(date) {
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var yyyy = date.getFullYear();
      if (dd < 10)
          dd = '0' + dd;
      if (mm < 10)
          mm = '0' + mm;
      return dd + '/' + mm + '/' + yyyy;
  }
  sortdata(data) {
      return data.sort(function (a, b) {
          var keyA = new Date(a.date),
              keyB = new Date(b.date);
          // Compare the 2 dates
          if (keyA < keyB) return -1;
          if (keyA > keyB) return 1;
          return 0;
      });
  }
  getbuddymessages (limit){
    this.buddymessages = [];
    console.log ("firebuddychats : "+this.userId+"/"+this.buddy_uid);
    this.firebuddychats.child(this.userId).child(this.buddy_uid).limitToLast(limit).on('child_added', (snapshot) => {
      //Check for unique value
      let tmpObj = snapshot.val();

      console.log ("Got new message from Firebase");

      //rememberme assign all keys
      /*
      Object.keys(tmpObj).forEach(ele => {
        console.log (ele);
      });
      */

      let isMatch  = false;
      for (var myKey in this.buddymessages) {
          if( this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
          this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
          this.buddymessages[myKey]['message'] == tmpObj.message &&
          this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
          this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
          this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
          this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
          this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
          this.buddymessages[myKey]['type'] == tmpObj.type
        ){
            //This is redundant message
            isMatch = true;
            break;
        }
       }//end for
       if(!isMatch){
         this.buddymessages.push(tmpObj);
         this.MessageSubscription();
         //console.log ("publishing event :"+tmpObj.messageId);
       }
    })
  }

  addmessage_async (){
    //this.newmessageTmp = this.newmessage;
    //this.newmessage = '';
    //////////console.log ("addmessage_async");
    setTimeout(() => {
          this.addmessage ();
    }, 1);
  }

  async addmessage() {
      //this.closeButtonClick()
      this.newmessageTmp = this.newmessage;
      //this.newmessage = '';
      this.UnreadMSG = 0;
      this.selectAllMessage = [];
      this.selectCounter = 0;
      this.headercopyicon = true;
      this.userservice.getstatusblock(this.buddy).then((res) => {
          //////////console.log ("-getstatus");
          if (!res) {
              this.newmessageTmp = this.newmessageTmp.trim();
              if (this.newmessageTmp != '') {
                    //////////console.log ("-if message is not empty");
                    //this.fcm.sendNotification(this.buddy, 'You have received a message', 'chatpage');

                    //this.events.unsubscribe('newmessage');//to avoid triggring multiple calls
                    //mytodo : above fix doesn`t work. Please try something else
                    this.chatservice.addnewmessage(this.newmessageTmp, 'message', this.buddyStatus).then(() => {
                      this.newmessage = '';
                      this.scrollto();
                      this.msgstatus = false;
                    });

              } else {
                  this.newmessage = this.newmessageTmp;
                  //this.loading.presentToast('Please enter valid message...');
              }
          } else {
              //user might be blocked
              this.newmessage = this.newmessageTmp;
              this.openOptions();

          }
      })
      //////////console.log ("-End Function");
  }//end function

  async presentActionSheet() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Unblock this person to send a message',
        buttons: [{
          text: 'UNBLOCK',
          icon: 'contact',
          handler: () => {
            console.log('Delete clicked');
          }
        }, {
          text: 'Share',
          icon: 'share',
          handler: () => {
            console.log('Share clicked');
          }
        },{
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }

  async openOptions() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Unblock this person to send a message.',
        buttons: [{
          text: 'UNBLOCK',
          icon: 'contact',
          handler: () => {
            this.userservice.unblockUser(this.buddy).then((res) => {
                if (res) {
                    this.addmessage();
                }
            });
          }
        },{
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }

  scrollto() {
    /*
      setTimeout(() => {
          if(this.content._scroll)  {
            this.content.scrollToBottom();
          }
      }, 1000);
      */
  }

  scrollToBottom_() {
      setTimeout(() => {
          if(this.content._scroll)  {
            this.content.scrollToBottom(1);
          }
      }, 1);
  }
  async openAttachment() {
      const actionSheet = await this.actionSheetController.create({
        header: 'Share Image',
        buttons: [{
          text: 'Camera',
          icon: 'camera',
          handler: () => {
            console.log('camera clicked');
          }
        }, {
          text: 'Photo Gallery',
          icon: 'image',
          handler: () => {
            console.log('Photo Gallery clicked');
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
      });
      await actionSheet.present();
    }

  MessageSubscription (){
      console.log ("MessageSubscription");
      this.allmessages = [];
      this.allmessagesGroups = [];
      for (var msgKey in this.buddymessages) {
        if(null == this.buddymessages[msgKey].dateofmsg){
          continue;
        }
        this.buddymessages[msgKey].selection = false;
        this.buddymessages[msgKey].selectCatName = '';

        if (this.buddymessages[msgKey].referral_type == null){
          this.buddymessages[msgKey].referral_type = "";
        }

        if (this.buddymessages[msgKey].selectCatId != null){
          for(let g=0;g<this.catItems.length;g++){
            if(this.catItems[g]['id']==this.buddymessages[msgKey].selectCatId){
              this.buddymessages[msgKey].selectCatName = this.catItems[g]['name'];
              break;
            }
          }
        }

        let tmpArr = {
          'dateofmsg' : this.buddymessages[msgKey].dateofmsg,
          'message_item' : [this.buddymessages[msgKey]]
        }

        //Check if data exists
        let foundKey:boolean = false;
        for (var myKey in this.allmessagesGroups) {
            if(this.allmessagesGroups[myKey].dateofmsg == this.buddymessages[msgKey].dateofmsg){
              this.allmessagesGroups [myKey].message_item.push (this.buddymessages[msgKey]);
              foundKey = true;
              break;
            }
        }//end for

        if(!foundKey){
          this.allmessagesGroups.push (tmpArr);
        }

        this.allmessages.push(this.buddymessages[msgKey]);//this is used for search the chats
      }//end for
      console.log ("Done ji");
      console.log (this.allmessagesGroups);
      this.scrollToBottomOnInit ();
  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
          if (params && params.buddy_uid) {
            this.buddy_uid  = params.buddy_uid;
            this.buddy = {"uid": this.buddy_uid};
            console.log ("buddy_uid");
            console.log (this.buddy_uid);
          }
    });

     this.scrollToBottomOnInit();
  }

}
