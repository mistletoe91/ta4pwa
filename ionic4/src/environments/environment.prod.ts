import { config,myEnv } from '../app/app.angularfireconfig';
export const environment = {
  production: false,
  firebase: config
};
if(myEnv){
  environment.production = true;
}


/*
export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyC5QK355uuknu0_ldVxFqqNqgp9oJi_eLc',
    authDomain: 'ion4fullpwa.firebaseapp.com',
    databaseURL: 'https://ion4fullpwa.firebaseio.com',
    projectId: 'ion4fullpwa',
    storageBucket: 'ion4fullpwa.appspot.com',
    messagingSenderId: '813357714189'
  }
};
*/
