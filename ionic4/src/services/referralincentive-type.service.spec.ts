import { TestBed } from '@angular/core/testing';

import { ReferralincentiveTypeService } from './referralincentive-type.service';

describe('ReferralincentiveTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReferralincentiveTypeService = TestBed.get(ReferralincentiveTypeService);
    expect(service).toBeTruthy();
  });
});
