import { Injectable } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Events,Platform} from '@ionic/angular';
import { userPicArr } from '../app/app.angularfireconfig';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../app/const';
import { TpstorageProvider } from '../app/tpstorage.service';
import { connreq } from '../models/interfaces/request';
import { UserService } from './user.service';
//import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {
  firebuddychats = firebase.database().ref('/buddychats');

  firedata = firebase.database().ref('/chatusers');
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  fireInvitationSent = firebase.database().ref('/invitationsent');
  firePhones = firebase.database().ref('/phones');
  //friendsPublished:boolean = false;
  userdetails;
  myfriends;
  userId;
  //new_chat_message_flag:boolean = false;
  lastMsgReader:any = {};
  blockRequest: any = [];
  subscriptionAddedObj = {};
  subscriptionAddedFriendsObj = {};
  messages;
  constructor(
    public userservice: UserService,
    public events: Events, 
    public platform: Platform,
    public tpStorageService: TpstorageProvider
  ) {
    this.platform.ready().then((readySource) => {
            //////console.log ("RequestsProvider constructor");
            this.tpStorageService.getItem('userUID').then((userID: any) => {
               //////console.log ("Getting userUIDuserUIDuserUIDuserUIDuserUIDuserUID");
               if (userID != undefined) {
                 //////console.log ("SETTING this.userId +++++++++++++++++++++++");
                 this.userId = userID;
               } else {
                 ////////console.log ("This is culprit");
                 ////////console.log (firebase.auth().currentUser);
                 //////console.log ("SETTING this.userId ++++++++++--+++++++++++++");
                 this.userId = firebase.auth().currentUser.uid;
               }
            }).catch(e => { //////console.log ("Error 38912"); //////console.log(e);
            });
    });
  }

/*
Sample : of "requests"

{
  "1bqhG3I2U5e6tMzkhTrIw8Ehozb2" : {
    "-Ld5sBkuU-eXYZ_LfbQS" : {
      "displayName" : "Kaztaaa",
      "isBlock" : false,
      "phone": "6471111212",
      "photoURL" : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
      "sender" : "46u6XlnVuIeBnofPrxkUZ8sRxem1"
    }
  }
}


*/

checkPhones(phoneNumber)  {
  return new Promise((resolve, reject) => {

    this.firePhones.child(phoneNumber).once('value', (snapshot) => {
      resolve(snapshot.val());
    }).catch((err) => {
      reject(err);
    });

  }).catch((error) => {
    ////console.log ("Error 3652");////console.log (error);
  });
}

  sendrequest(req: connreq) {
    var promise = new Promise((resolve, reject) => {
      this.userservice.getuserdetails().then((myDetails: any) => {
        this.firereq.child(req.recipient).push(
          {
            sender: req.sender,
            isBlock: false,
            phone: myDetails.phone,
            mobile: myDetails.phone,
            displayName:myDetails.displayName,
            photoURL:myDetails.photoURL
          }
        ).then(() => {
          resolve({ success: true });
        })
      });
    }).catch((error) => {
      ////console.log ("Error 6422");////console.log (error);
    });
    return promise;
  }

  setInvitedBy(phone) {
    return new Promise((resolve, reject) => {
      this.firePhones.child(phone).set({
        invited_by : this.userId,
        timestamp: firebase.database.ServerValue.TIMESTAMP
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        resolve(false);
      })
    }).catch((error) => {
      ////console.log ("Error 8426");////console.log (error);
    });

   }

  logInvitation(phone) {
    return new Promise((resolve, reject) => {
      this.fireInvitationSent.child(this.userId).child(phone).set({
        timestamp: firebase.database.ServerValue.TIMESTAMP
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        resolve(false);
      })
    }).catch((error) => {
      ////console.log ("Error 8456");////console.log (error);
    });

   }

  getmyrequests_promise() {
    var promise = new Promise((resolve, reject) => {

      this.tpStorageService.getItem('userUID').then((userId__: any) => {
            this.userId = userId__;

            this.firereq.child(this.userId).once('value', (snapshot) => {
              //check if user not blocked
              this.userdetails = [];
              let myrequests = snapshot.val();
              for (var j in myrequests){
                if (!myrequests[j].isBlock) {
                  this.userdetails.push(myrequests[j]);
                }
              }//end for
              resolve(this.userdetails);
           });

      }).catch(e => { });

  }).catch((error) => {
    ////console.log ("Error 6234");////console.log (error)
  });
  return promise;
}//end function


  getmyrequests (){
    this.tpStorageService.getItem('userUID').then((userId__: any) => {
          this.userId = userId__;
          this.getmyrequests_ ();
    }).catch(e => { });
  }

  getmyrequests_() {
    let allmyrequests;
    var myrequests = [];
    ////////console.log ("GET MY REQUESTS");
    ////////console.log (this.userId);
    this.firereq.child(this.userId).once('value', (snapshot) => {
      ////////console.log ("Found requests");
      ////////console.log (snapshot.val());
      allmyrequests = snapshot.val();
      myrequests = [];
      for (var i in allmyrequests) {
        myrequests.push(allmyrequests[i]);
      }

      //Check if user is not blocked by this user
      this.userdetails = [];
      for (var j in myrequests){
        if (!myrequests[j].isBlock) {
          this.userdetails.push(myrequests[j]);
        }
      }

      //////console.log ("<<<>>>>");
      //////console.log (this.userdetails);
      this.events.publish('gotrequests');

      /*
      this.userservice.getallusers_modified(myrequests).then((res) => {
        var allusers = res;
        this.userdetails = [];
        for (var j in myrequests)
          for (var key in allusers) {
            if (myrequests[j].sender === allusers[key].uid && myrequests[j].isBlock == false) {
              this.userdetails.push(allusers[key]);
            }
          }
        this.events.publish('gotrequests');
      })
      */


    })
  }
  //duplicate function in userst.ts  but not entirly same
  acceptrequest(buddy, deleteRequest = true) {
    var promise = new Promise((resolve, reject) => {
      this.userservice.getuserdetails().then((aboutMe: any) => {
        this.firefriends.child(this.userId).child(buddy.uid).set({
          uid: buddy.uid,
          displayName: buddy.displayName,
          photoURL: buddy.photoURL,
          isActive: 1,
          isBlock: false,
          deviceToken: buddy.deviceToken,
          countryCode: buddy.countryCode,
          disc: buddy.disc,
          mobile: buddy.mobile,
          timestamp: buddy.timestamp
        }).then(() => {
          this.firefriends.child(buddy.uid).child(this.userId).set({
            uid: this.userId,
            displayName: aboutMe.displayName,
            photoURL: aboutMe.photoURL,
            isActive: 1,
            isBlock: false,
            deviceToken: aboutMe.deviceToken,
            countryCode: aboutMe.countryCode,
            disc: aboutMe.disc,
            mobile: aboutMe.mobile,
            timestamp: aboutMe.timestamp
          }).then(() => {

            if(deleteRequest){
              this.deleterequest(buddy).then(() => {
                resolve(true);
              }).catch((err) => {
                //////console.log ("Error 3259");
                reject(err);
              }) ;
            }//endif

         });
      });
    });
  }).catch((error) => {
    ////console.log ("Error 4563");////console.log (error)
  });
      return promise;
  }//end function

  deleterequest(buddy) {
    var promise = new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('sender').equalTo(buddy.uid).once('value', (snapshot) => {
        let somekey;
        for (var key in snapshot.val())
          somekey = key;
        this.firereq.child(this.userId).child(somekey).remove().then(() => {
          resolve(true);
        })
      })
        .then(() => {

        }).catch((err) => {
          reject(err);
        })
    }).catch((error) => {
      ////console.log ("Error 1243");////console.log (error);
    });
    return promise;
  }

  getmyfriends (){
    //console.log ( "getmyfriends ()");
    this.tpStorageService.getItem('userUID').then((userId__: any) => {
          this.userId = userId__;
          //////console.log ("LLLove ");
          this.getmyfriends_ ();
    }).catch(e => { });
  }

  getmyfriends_ (){

    this.firefriends.child(this.userId).on('value', (snapshot) => {
         this.myfriends = [];
         //console.log ("FIREFRIEND EVENT TRIGGERED");
         //console.log (snapshot.val());
         let allfriends = snapshot.val();
         if (allfriends != null) {
               // I have some friends
              ////console.log ("I have friends " );
               let CounterFriends_ = 0;
               for (let i in allfriends) {
                  ////console.log ("iiii ")     ;
                  let lastMsgActual__ = {
                    message: "",
                    type: "",
                    dateofmsg: "",
                    timeofmsg: "",
                    isRead: "",
                    isStarred: "",
                    referral_type : "",
                    selectCatId : ""
                  };
                  if(!allfriends[i].isBlock) {
                    allfriends[i].isBlock = false;
                  }
                  allfriends[i].unreadmessage=lastMsgActual__;
                  allfriends[i].lastMessage=lastMsgActual__;
                  this.myfriends.push(allfriends[i]);
                  CounterFriends_++;
               }//end for
               //////console.log ("this.cnter "+ this.cnter);

              //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
              let friendsSoFar = 0;
              for (let y in this.myfriends) {

                if(true || null == this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid]) {
                 this.firebuddychats.child(this.userId).child(this.myfriends[y].uid).limitToLast(1).once('value', (snapshot_last_msg) => {
                        let lastMsg__ = snapshot_last_msg.val();
                        if(lastMsg__){
                          for(var key in lastMsg__){
                            let lastMsgActual__ = {
                              message: lastMsg__[key].message,
                              type: lastMsg__[key].type,
                              dateofmsg: lastMsg__[key].dateofmsg,
                              timeofmsg: lastMsg__[key].timeofmsg,
                              referral_type : lastMsg__[key].referral_type,
                              selectCatId : lastMsg__[key].selectCatId,
                              isRead: false,
                              isStarred: false
                            };
                            this.myfriends[y].unreadmessage = lastMsgActual__;
                            this.myfriends[y].lastMessage = lastMsgActual__;
                            break;
                          }//end for
                        }//endif

                        if(++friendsSoFar >= CounterFriends_){
                          //Make sure it run only on last one and only once
                          //console.log ("Publish Friends Event in Chat.ts ");
                          this.events.publish('friends');//jaswinder
                        }
                 });
                 this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid] = true;
               } else {
                 //this friend has already been subscripted
               }//endif
              }//end for

         } else {
            // I dont have any friend (:
            //if(null == this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid]) {
              this.events.publish('friends');
            //  this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid] = true;
          //  }//endif
         }//endif


    });//get all friends
  }


  subscribeToReadingLastMsg (){
    this.lastMsgReader = {};
    //console.log ("subscribeToReadingLastMsg");
    for (let y in this.myfriends) {
       //console.log ("Friend " + y);
      if(null == this.subscriptionAddedObj [this.userId + "/" + this.myfriends[y].uid]) {
            //console.log ("->Add Subscription To : " +this.userId + "/" + this.myfriends[y].uid);
            this.firebuddychats.child(this.userId).child(this.myfriends[y].uid).limitToLast(1).on('child_added', (snapshot_last_msg) => {
              //console.log ("Inside firebuddychats ()");
              let lastMsg__ = snapshot_last_msg.val();
              //console.log (lastMsg__);
              //if(lastMsg__){
                  this.lastMsgReader [this.myfriends[y].uid] = ({
                    message: lastMsg__.message,
                    type: lastMsg__.type,
                    sentby: lastMsg__.sentby,
                    dateofmsg: lastMsg__.dateofmsg,
                    timeofmsg: lastMsg__.timeofmsg,
                    request_to_release_incentive: lastMsg__.request_to_release_incentive,
                    isRead: false,
                    isStarred: false
                  });

                  //if(!this.new_chat_message_flag){
                    this.events.publish('new_chat_message');
                    //console.log ("Publishing event new_chat_message");
                    //this.new_chat_message_flag = true;
                  //}

              //}//endif
            });
            this.subscriptionAddedObj [this.userId + "/" + this.myfriends[y].uid] = true;
      }//endif


    }//end for
  }


/*
  getmyfriends_NOTEUSED() {
    let friendsuid = [];
    this.firefriends.child(this.userId).once('value', (snapshot) => {
      let allfriends = snapshot.val();
      if (allfriends != null) {
        for (var i in allfriends) {
          friendsuid.push(allfriends[i]);
        }
      }

      this.userservice.getallusers_modified(friendsuid).then((users) => {
        this.myfriends = [];
        let counter = 0;
        if (friendsuid.length > 0) {
          for (var j in friendsuid) {
            for (var key in users) {
              if (friendsuid[j].uid === users[key].uid) {
                users[key].isActive = friendsuid[j].isActive;
                users[key].userprio = friendsuid[j].userprio ? friendsuid[j].userprio : null;
                let userKey;
                userKey = users[key];
                let flag1 = false;
                let flag2 = false;
                let flag3 = false;
                this.userservice.getuserblock(users[key]).then((res) => {
                  userKey.isBlock = res;
                  flag1 = true;
                })
                this.get_last_unreadmessage(users[key]).then(responce => {
                  userKey.unreadmessage = responce;
                  flag2 = true;
                })
                this.get_last_msg(users[key]).then(resp => {
                  userKey.lastMessage = resp;
                  flag3 = true;
                  this.myfriends.push(userKey);
                  counter++;
                  if (counter == friendsuid.length) {
                  }
                })
              }
            }
          }
        } else {
          if (friendsuid.length == 0) {
          }
        }
      }).catch((err) => {
      })

    })
  }
*/

    get_last_msg(buddy) {
      return new Promise((resolve, reject) => {
        this.firebuddychats.child(this.userId).child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
          let allmsg = snapshot.val();
          if (allmsg != null) {
            let lastmsg = allmsg[Object.keys(allmsg)[Object.keys(allmsg).length - 1]]
            resolve(lastmsg)
          } else {
            resolve('')
          }
        })
      }).catch((error) => {
        ////console.log ("Error 7423");////console.log (error);
      });
    }



  get_last_unreadmessage(buddy) {
    return new Promise((resolve, reject) => {
      let allunreadmessage = [];
      this.firebuddychats.child(buddy.uid).child(this.userId).orderByChild('uid').on('value', (snapshot) => {
        let allmsg = snapshot.val();
        if (allmsg != null) {
          for (let tmpkey in allmsg) {
            if (allmsg[tmpkey].isRead == false) {
              allunreadmessage.push(allmsg);
            }
          }
        }
        if (allunreadmessage.length > 0) {
          resolve(allunreadmessage.length)
        } else {
          resolve(allunreadmessage.length)
        }
      })
    }).catch((error) => {
      ////console.log ("Error 7842");////console.log (error);
    });
  }

   pendingetmy__requests(userd) {
    return new Promise((resolve, reject) => {
      this.firereq.child(userd).once('value', (snapshot) => {
        resolve(snapshot.val());
      })
    }).catch((error) => {
      ////console.log ("Error 7234");////console.log (error);
    });
  }


  pendingetmyrequests(userd) {
    //////console.log ("pendingetmyrequests");
    //////console.log (userd);
    //////console.log (this.userId);
    //////console.log ("pendingetmyrequests END");

    return new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((userId__: any) => {
            this.userId = userId__;
            if(!userd || userd == undefined) {
                       userd = this.userId;
            }

            let allmyrequests;
            var myrequests = [];
            this.firereq.child(userd).once('value', (snapshot) => {
              allmyrequests = snapshot.val();
              myrequests = [];
              for (var i in allmyrequests) {
                myrequests.push(allmyrequests[i].sender);
              }
              this.userservice.getallusers_modified(myrequests).then((allusers) => {
                let pendinguserdetails = [];
                for (var j in myrequests)
                  for (var key in allusers) {
                    if (myrequests[j] == allusers[key].uid) {
                      pendinguserdetails.push(allusers[key]);
                    }
                  }
                resolve(pendinguserdetails);
              })
            })

      }).catch(e => { });
    }).catch((error) => {
      ////console.log ("Error 1652");////console.log (error);
    });
  }
  blockuserprof(buddy) {
    return new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let requestFriends = snapshot.val();
        for (let tmpkey in requestFriends) {
          if (requestFriends[tmpkey].sender == buddy.uid) {
            this.firereq.child(this.userId).child(tmpkey).update({ isBlock: true })
            resolve(true);
          }
        }
      })
    }).catch((error) => {
      ////console.log ("Error 9645");////console.log (error);
    });
  }


  getAllBlockRequest (){
    this.tpStorageService.getItem('userUID').then((userId__: any) => {
          this.userId = userId__;
          this.getAllBlockRequest_ ();
    }).catch(e => { });
  }

  getAllBlockRequest_() {
    this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allrequest = snapshot.val();
      let blockuser = []
      for (var key in allrequest) {
        if (allrequest[key].isBlock) {
          this.firedata.child(allrequest[key].sender).once('value', (snapsho) => {
            blockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.blockRequest = [];
        this.blockRequest = blockuser;
      }
      this.events.publish('block-request');
    })
  }
  unblockRequest(buddy) {
    return new Promise((resolve, reject) => {
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].sender == buddy.uid) {
            this.firereq.child(this.userId).child(key).update({ isBlock: false });
            resolve(true);
          }
        }
      })
    }).catch((error) => {
      ////console.log ("Error 1745");////console.log (error);
    });
  }

}
