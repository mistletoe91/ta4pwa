import { Injectable } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Events,Platform} from '@ionic/angular';
import { Firebase } from '@ionic-native/firebase/ngx';
import { userPicArr,server_key } from '../app/app.angularfireconfig';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../app/const';
import { TpstorageProvider } from '../app/tpstorage.service';
import {RequestOptions, Headers } from '@angular/http';
import { UserService } from './user.service';
import { HTTP } from '@ionic-native/http/ngx';
@Injectable({
  providedIn: 'root'
})
export class FcmService {
  userId;
  fireuserStatus = firebase.database().ref('/userstatus');

  constructor(
    public userservice: UserService,
    private http: HTTP,
    public platform: Platform,
    public firebaseNative: Firebase,
    public tpStorageService: TpstorageProvider
  ) {
    this.platform.ready().then((readySource) => {
      ////console.log ("Trying to call tpStorageService.getItem('userUID') FROM FcmProvider");
      this.tpStorageService.getItem('userUID').then((res: any) => {
        ////console.log (">>>>>>>>>>>>>>Inside getItem()");
        if(res){
          this.userId = res;
        }
        if (!this.userId) {
          this.userId = firebase.auth().currentUser.uid;
        }
      }).catch(e => {
        if(firebase && firebase.auth().currentUser){
          this.userId = firebase.auth().currentUser.uid;
        }
        ////console.log ("Tapally_Error_382731 FcmProvider");
      });
    });
  }

  // Get permission from the user
  async getToken() {
       let token;
       //mytodo : make it bit more intelligent in case userId is not there
       //await this.tpStorageService.getItem('userUID').then((res: any) => {
           if (this.platform.is('android')) {
             token = await this.firebaseNative.getToken();

             this.firebaseNative.onTokenRefresh()
               .subscribe((token: string) => {

                 if(!this.userId){
                   if(firebase && firebase.auth().currentUser){
                     this.userId = firebase.auth().currentUser.uid;
                   }
                 }
                 if(!this.userId){
                    this.tpStorageService.getItem('userUID').then((res: any) => {
                       this.userservice.updateDeviceToken(token, this.userId);
                    }).catch(e => { });
                 } else {
                    this.userservice.updateDeviceToken(token, this.userId);
                 }

               });
           }
           if (this.platform.is('ios')) {
             token = await this.firebaseNative.getToken();
             await this.firebaseNative.grantPermission();
             await this.firebaseNative.onTokenRefresh()
               .subscribe((token: string) => {

                 if(!this.userId){
                   if(firebase && firebase.auth().currentUser){
                     this.userId = firebase.auth().currentUser.uid;
                   }
                 }
                 if(!this.userId){
                    this.tpStorageService.getItem('userUID').then((res: any) => {
                       this.userservice.updateDeviceToken(token, this.userId);
                    }).catch(e => { });
                 } else {
                    this.userservice.updateDeviceToken(token, this.userId);
                 }

               });
           }
       //}).catch(e => { });
  }
  // Listen to incoming FCM messages
  listenToNotifications() {
    //this.firebaseNative.grantPermission();
    return this.firebaseNative.onNotificationOpen();
  }

  setstatusUser() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());
    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).set({
        status: 1,
        data: 'online',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  setStatusOffline() {
    let time = this.formatAMPM(new Date());
    let date = this.formatDate(new Date());

    var promise = new Promise((resolve, reject) => {
      this.fireuserStatus.child(this.userId).update({
        status: 0,
        data: 'offline',
        timestamp: date + ' at ' + time
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        reject(err);
      })

    })
    return promise;
  }

  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }
  formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10)
      dd = '0' + dd;
    if (mm < 10)
      mm = '0' + mm;
    return dd + '/' + mm + '/' + yyyy;
  }

  sendNotification(buddy, msg, pagename) {
    let body = {};
    if (pagename == 'chatpage') {
      body = {
        "notification": {
          "title": buddy.displayName,
          "body": msg,
          "sound": "default",
          "forceStart": "1",
          "click_action" : "FCM_PLUGIN_ACTIVITY",
          "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    } else if (pagename == 'Grouppage') {
      body = {
        "notification": {
          "title": buddy.displayName,
          "body": msg,
          "sound": "default",
          "forceStart": "1",
          "click_action" : "FCM_PLUGIN_ACTIVITY",
          "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    } else if (pagename == 'sendreq') {
      body = {
        "notification": {
          "title": '',
          "body": msg,
          "forceStart": "1",
          "sound": "default",
          "click_action" : "FCM_PLUGIN_ACTIVITY",
          "icon": "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/common%2Ficon.png?alt=media&token=35726d75-69f6-4f50-bfb2-07079e28d67a"
        },
        "to": buddy.deviceToken,
        "priority": "high"
      }
    }
    let headers = new Headers({ 'Authorization': 'key=' + server_key.key, 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    //console.log ("FCM Sending");
    //console.log (body);
    //console.log (options);
    this.http.post("https://fcm.googleapis.com/fcm/send", body, options).then(res => {
        //console.log ("RESPONSE");
        //console.log (res);
    }).catch(e => {
        //console.log ("FCM ERROR ");
        //console.log (error);
    });
  }
}
