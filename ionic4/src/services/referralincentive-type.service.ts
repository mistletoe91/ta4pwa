import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReferralincentiveTypeService {

  constructor() { }

  getList (){
  return [
    {"id":	0	,"name":"	None", "sort":	0	},
    {"id":	1	,"name":"	Cash", "sort":	1	},
    {"id":	2	,"name":"	Gift Card","sort":	2	},
    {"id":	3	,"name":"	Extra service","sort":	3	},
    {"id":	4	,"name":"	Other","sort":	4	},
  ]}  
}
