(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~buddychat-buddychat-module~contact-contact-module"],{

/***/ "./src/services/chat.service.ts":
/*!**************************************!*\
  !*** ./src/services/chat.service.ts ***!
  \**************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.service */ "./src/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatService = /** @class */ (function () {
    function ChatService(events, tpStorageService, userservice) {
        var _this = this;
        this.events = events;
        this.tpStorageService = tpStorageService;
        this.userservice = userservice;
        this.firedata = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/chatusers');
        this.firebuddychats = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/buddychats');
        this.firebuddymessagecounter = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/buddychats');
        this.fireuserStatus = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/userstatus');
        this.fireStar = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/starredmessage');
        this.firefriends = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/friends');
        this.fireReferral_sent = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/referral_sent');
        this.fireReferral_received = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/referral_received');
        this.fireBusiness = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/business');
        this.buddymessages = [];
        this.msgcount = 0;
        this.tpStorageService.getItem('userUID').then(function (userID) {
            if (userID != undefined) {
                _this.userId = userID;
            }
            else {
                _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_1__["auth"]().currentUser.uid;
            }
        }).catch(function (e) { });
    }
    //jaswinder probbaly not used
    ChatService.prototype.buddymessageRead = function (limit) {
        var _this = this;
        this.firebuddychats.child(this.userId).child(this.buddy.uid)
            .limitToLast(limit).once('value', function (snapshot) {
            var allmessahes = snapshot.val();
            for (var key in allmessahes) {
                if (allmessahes[key].isRead == false) {
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(key).update({ isRead: true });
                }
            }
        });
    };
    ChatService.prototype.initializebuddy = function (buddy) {
        var _this = this;
        if (this.userId) {
            this.initializebuddy_(buddy);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.initializebuddy_(buddy);
            }).catch(function (e) { });
        } //endif
    };
    ChatService.prototype.initializebuddy_ = function (buddy) {
        this.buddy = buddy;
        ////console.log ("initializebuddy_ Done");
        ////console.log (this.buddy);
        //this.buddymessageRead(10);
    };
    ChatService.prototype.formatAMPM = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    };
    ChatService.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    ChatService.prototype.addnewmessageDirectReferral = function (msg, type, buddy, referral, referralType) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.tpStorageService.getItem('userUID').then(function (userId__) {
                    _this.userId = userId__;
                    _this.firebuddychats.child(_this.userId).child(buddy.uid).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        selectCatId: 0,
                        multiMessage: false,
                        request_to_release_incentive: '',
                        isStarred: false,
                        referral_type: referralType,
                        referral: referral,
                        referralSavedOnPhone: false,
                        userprio: ''
                    })
                        .then(function (item) {
                        //Log info
                        _this.fireReferral_sent.child(_this.userId).child(buddy.uid).push({
                            sent_by: _this.userId,
                            received_by: buddy.uid,
                            referral_type: referralType,
                            referral: referral,
                            timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                            timeofmsg: time,
                            dateofmsg: date,
                            redeemStatus: 0,
                            incentiveType: "",
                            incentiveDetail: "",
                            business_referral_detail: "",
                            business_referral_type_id: ""
                        }).then(function (referral_sent) {
                            _this.fireReferral_received.child(buddy.uid).child(_this.userId).child(referral_sent.key).set({
                                sent_by: _this.userId,
                                received_by: buddy.uid,
                                referral_type: referralType,
                                referral: referral,
                                timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                redeemStatus: 0,
                                incentiveType: "",
                                incentiveDetail: "",
                                business_referral_detail: "",
                                business_referral_type_id: ""
                            }).then(function (referral_received) {
                                //Now check and see if the guy who recieved referral is actually busienss owner at that time
                                _this.fireBusiness.child(buddy.uid).once('value', function (snapshot_business_of_referral_rec) {
                                    var businessGuy = snapshot_business_of_referral_rec.val();
                                    if (businessGuy) {
                                        _this.fireReferral_sent.child(_this.userId).child(buddy.uid).child(referral_sent.key).update({
                                            business_referral_detail: businessGuy.referral_detail,
                                            business_referral_type_id: businessGuy.referral_type_id
                                        });
                                        _this.fireReferral_received.child(buddy.uid).child(_this.userId).child(referral_sent.key).update({
                                            business_referral_detail: businessGuy.referral_detail,
                                            business_referral_type_id: businessGuy.referral_type_id
                                        });
                                    } //endif
                                }).catch(function (err) {
                                });
                            });
                        });
                        _this.firefriends.child(_this.userId).child(buddy.uid).update({ isActive: 1, userprio: new Date() });
                        _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                        _this.userservice.getstatusblock(buddy).then(function (res) {
                            //////console.log ("going_to_call gotstatusblock ");
                            if (res == false) {
                                //mytodo : check if buddystatus is online
                                //if (buddystatus == "online") {
                                if (false) {}
                                else {
                                    //////console.log ("going_to_call firebuddychats 1");
                                    _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                                }
                                //Also add to buddy
                                _this.firebuddychats.child(buddy.uid).child(_this.userId).push({
                                    sentby: _this.userId,
                                    message: msg,
                                    type: type,
                                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                                    timeofmsg: time,
                                    dateofmsg: date,
                                    messageId: '',
                                    isRead: true,
                                    isStarred: false,
                                    selectCatId: 0,
                                    request_to_release_incentive: '',
                                    referral_type: referralType,
                                    referral: referral,
                                    referralSavedOnPhone: false,
                                    userprio: ''
                                }).then(function (items) {
                                    _this.firebuddychats.child(buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                        _this.firefriends.child(buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                                        resolve(true);
                                    });
                                });
                            }
                            else {
                                //Buddy has blocked us
                                //////console.log ("going_to_call firebuddychats 2");
                                _this.firebuddychats.child(_this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                                resolve(true);
                            }
                        });
                    });
                }).catch(function (e) { });
            });
            return promise;
        }
    };
    //buddy is receipient
    ChatService.prototype.addnewmessageBroadcast = function (msg, type, buddy, selectCatId, bothways) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.tpStorageService.getItem('userUID').then(function (userId__) {
                    _this.userId = userId__;
                    _this.userservice.getstatusblock(buddy).then(function (res) {
                        if (res == false) {
                            //user has not blocked buddy
                            _this.firebuddychats.child(buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                request_to_release_incentive: '',
                                messageId: '',
                                selectCatId: selectCatId,
                                multiMessage: false,
                                isStarred: false,
                                userprio: ''
                            })
                                .then(function (item) {
                                if (bothways) {
                                    //This is both way broadcast so include me as whitelabeled
                                    _this.firebuddychats.child(_this.userId).child(buddy.uid).push({
                                        sentby: _this.userId,
                                        message: msg,
                                        type: type,
                                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                                        timeofmsg: time,
                                        dateofmsg: date,
                                        request_to_release_incentive: '',
                                        messageId: '',
                                        selectCatId: selectCatId,
                                        multiMessage: false,
                                        isStarred: false,
                                        userprio: ''
                                    }).then(function (item) { });
                                    //mytodo : broadcast in groups too
                                }
                                else {
                                    //A referral request has been sent
                                    //somebody who posted referral request
                                }
                                _this.firefriends.child(buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                                _this.firebuddychats.child(buddy.uid).child(_this.userId).child(item.key).update({ messageId: item.key, userprio: new Date() });
                                resolve(true);
                                //mytodo : check if buddystatus is online
                                //if (buddystatus == "online") {
                                if (false) {}
                                else {
                                    //Need this because we did not send push notification
                                    _this.firebuddychats.child(buddy.uid).child(_this.userId).child(item.key).update({ isRead: false });
                                }
                            });
                        } //endif
                    }); //getstatusblock
                }).catch(function (e) { });
            });
            return promise;
        }
    };
    //under dev : not used
    ChatService.prototype.addnewmessageAskForIncentives = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    request_to_release_incentive: referral_id,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        request_to_release_incentive: referral_id,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatService.prototype.addnewmessageRedeemDone = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    referral_redeemed: 1,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        referral_redeemed: 1,
                        referral_send_by: referral_send_by,
                        referral_received_by: referral_received_by,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatService.prototype.addnewmessageRedeemRequest = function (msg, type, buddystatus, referral_id, referral_send_by, referral_received_by) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    request_to_release_incentive: referral_id,
                    referral_send_by: referral_send_by,
                    referral_received_by: referral_received_by,
                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        messageId: '',
                        request_to_release_incentive: referral_id,
                        referral_send_by: referral_send_by,
                        referral_received_by: referral_received_by,
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatService.prototype.addnewmessage = function (msg, type, buddystatus) {
        var _this = this;
        //console.log ("addnewmessageaddnewmessage");
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                //console.log ("GGGGGGGGGGGGG1");
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive: '',
                    messageId: '',
                    multiMessage: false,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    ////console.log ("GGGGGGGGGGGGG2");
                    _this.firefriends.child(_this.userId).child(_this.buddy.uid).update({ isActive: 1, userprio: new Date() });
                    ////console.log ("GGGGGGGGGGGGG3");
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    ////console.log ("GGGGGGGGGGGGG4");
                    _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                        sentby: _this.userId,
                        message: msg,
                        type: type,
                        timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                        timeofmsg: time,
                        dateofmsg: date,
                        request_to_release_incentive: '',
                        messageId: '',
                        isRead: true,
                        isStarred: false,
                        userprio: ''
                    }).then(function (items) {
                        ////console.log ("GGGGGGGGGGGGG5");
                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() });
                        ////console.log ("GGGGGGGGGGGGG6");
                        _this.firefriends.child(_this.buddy.uid).child(_this.userId).update({ isActive: 1, userprio: new Date() });
                        resolve(true);
                    });
                });
            });
            return promise;
        }
    };
    ChatService.prototype.addnewmessagemultiple = function (msg, type, buddystatus) {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        if (this.buddy) {
            var promise = new Promise(function (resolve, reject) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).push({
                    sentby: _this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive: '',
                    messageId: '',
                    isRead: true,
                    multiMessage: true,
                    isStarred: false,
                    userprio: ''
                })
                    .then(function (item) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                resolve(true);
                            }
                        }
                    });
                    _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ messageId: item.key, userprio: new Date() });
                    if (buddystatus == "online") {
                        _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                    }
                    else {
                        _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                            if (res == false) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: false });
                            }
                            else {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(item.key).update({ isRead: true });
                            }
                        });
                    }
                    _this.userservice.getstatusblock(_this.buddy).then(function (res) {
                        if (res == false) {
                            _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).push({
                                sentby: _this.userId,
                                message: msg,
                                type: type,
                                timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP,
                                timeofmsg: time,
                                dateofmsg: date,
                                request_to_release_incentive: '',
                                messageId: '',
                                multiMessage: true,
                                isStarred: false,
                                userprio: ''
                            }).then(function (items) {
                                _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ messageId: items.key, userprio: new Date() }).then(function () {
                                    if (buddystatus == "online") {
                                        _this.firebuddychats.child(_this.buddy.uid).child(_this.userId).child(items.key).update({ isRead: true });
                                    }
                                    else {
                                    }
                                    _this.getfirendlist(_this.buddy.uid).then(function (res) {
                                        var friends = res;
                                        for (var tmpkey in friends) {
                                            if (friends[tmpkey].uid == _this.userId) {
                                                _this.firefriends.child(_this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                                                resolve(true);
                                            }
                                        }
                                    });
                                });
                            });
                        }
                        else {
                            resolve(true);
                        }
                    });
                });
            });
            return promise;
        }
    };
    ChatService.prototype.deleteMessages = function (items) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < items.length; i++) {
                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(items[i].messageId).remove()
                    .then(function (res) {
                    _this.getfirendlist(_this.userId).then(function (res) {
                        var friends = res;
                        for (var tmpkey in friends) {
                            if (friends[tmpkey].uid == _this.buddy.uid) {
                                _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).once('value', function (snapshot) {
                                    var tempdata = _this.converanobj(snapshot.val());
                                    if (tempdata.length > 0) {
                                        var lastMsg = tempdata[tempdata.length - 1];
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: lastMsg.userprio });
                                        resolve(true);
                                    }
                                    else {
                                        _this.firefriends.child(_this.userId).child(tmpkey).update({ userprio: '' });
                                        resolve(true);
                                    }
                                });
                            }
                        }
                    });
                }).catch(function (err) {
                    reject(false);
                });
            }
        });
    };
    ChatService.prototype.deleteUserMessages = function (allbuddyuser) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (allbuddyuser.length > 0) {
                var _loop_1 = function (i) {
                    _this.firebuddychats.child(_this.userId).child(allbuddyuser[i].uid).remove()
                        .then(function (res) {
                        _this.getfirendlist(_this.userId).then(function (res) {
                            var friends = res;
                            for (var tmpkey in friends) {
                                if (friends[tmpkey].uid == allbuddyuser[i].uid) {
                                    _this.firefriends.child(_this.userId).child(tmpkey).update({ isActive: 0 }).then(function (res) {
                                        resolve(true);
                                        _this.events.publish('friends');
                                    });
                                }
                            }
                        });
                    });
                };
                for (var i = 0; i < allbuddyuser.length; i++) {
                    _loop_1(i);
                }
            }
        });
    };
    ChatService.prototype.getfirendlist = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //////console.log ("<<<<<<<<Finding Frenids in DB>>>>>>>>");
            _this.firefriends.child(uid).on('value', function (snapshot) {
                var friendsAll = snapshot.val();
                resolve(friendsAll);
            });
        });
    };
    ChatService.prototype.updateContactSaved = function (msgId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firebuddychats.child(_this.userId).child(_this.buddy.uid).child(msgId).update({
                referralSavedOnPhone: true,
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatService.prototype.getbuddymessagesForSecondaPage = function (limit) {
        var _this = this;
        if (this.userId) {
            this.getbuddymessagesForSecondaPage_(limit);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.getbuddymessagesForSecondaPage_(limit);
            }).catch(function (e) { });
        } //endif
    };
    ChatService.prototype.getbuddymessagesForSecondaPage_ = function (limit) {
        var _this = this;
        this.buddymessages = [];
        this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', function (snapshot) {
            //Check for unique value
            var tmpObj = snapshot.val();
            var isMatch = false;
            for (var myKey in _this.buddymessages) {
                if (_this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
                    _this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
                    _this.buddymessages[myKey]['message'] == tmpObj.message &&
                    _this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
                    _this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
                    _this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
                    _this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
                    _this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
                    _this.buddymessages[myKey]['type'] == tmpObj.type) {
                    //This is redundant message
                    isMatch = true;
                    break;
                }
            } //end for
            if (!isMatch) {
                _this.buddymessages.push(tmpObj);
                //console.log ("publishing event :"+tmpObj.messageId);
                //this.events.publish('newmessage_secondpage', tmpObj);
            }
        });
    };
    ChatService.prototype.getbuddymessages = function (limit) {
        var _this = this;
        if (this.userId) {
            this.getbuddymessages_(limit);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.getbuddymessages_(limit);
            }).catch(function (e) { });
        } //endif
    };
    ChatService.prototype.getbuddymessages_ = function (limit) {
        var _this = this;
        this.buddymessages = [];
        this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', function (snapshot) {
            //Check for unique value
            var tmpObj = snapshot.val();
            var isMatch = false;
            for (var myKey in _this.buddymessages) {
                if (_this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
                    _this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
                    _this.buddymessages[myKey]['message'] == tmpObj.message &&
                    _this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
                    _this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
                    _this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
                    _this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
                    _this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
                    _this.buddymessages[myKey]['type'] == tmpObj.type) {
                    //This is redundant message
                    isMatch = true;
                    break;
                }
            } //end for
            if (!isMatch) {
                _this.buddymessages.push(tmpObj);
                _this.events.publish('newmessage');
            }
        });
    };
    ChatService.prototype.getbuddyStatus = function () {
        var _this = this;
        var tmpStatus;
        this.fireuserStatus.child(this.buddy.uid).on('value', function (statuss) {
            tmpStatus = statuss.val();
            if (tmpStatus.status == 1) {
                _this.buddyStatus = tmpStatus.data;
            }
            else {
                var date = tmpStatus.timestamp;
                _this.buddyStatus = date;
            }
            _this.events.publish('onlieStatus');
        });
    };
    ChatService.prototype.setstatusUser = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).set({
                status: 1,
                data: 'online',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatService.prototype.setStatusOffline = function () {
        var _this = this;
        var time = this.formatAMPM(new Date());
        var date = this.formatDate(new Date());
        var promise = new Promise(function (resolve, reject) {
            _this.fireuserStatus.child(_this.userId).update({
                status: 0,
                data: 'offline',
                timestamp: date + ' at ' + time
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    ChatService.prototype.converanobj = function (obj) {
        var tmp = [];
        for (var key in obj) {
            tmp.push(obj[key]);
        }
        return tmp;
    };
    ChatService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"], _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_3__["TpstorageProvider"], _user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]])
    ], ChatService);
    return ChatService;
}());



/***/ })

}]);
//# sourceMappingURL=default~buddychat-buddychat-module~contact-contact-module.js.map