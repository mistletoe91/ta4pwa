(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~firebase-firebase-integration-module~user-details-firebase-user-details-module"],{

/***/ "./src/app/firebase/user/update/firebase-update-user.modal.html":
/*!**********************************************************************!*\
  !*** ./src/app/firebase/user/update/firebase-update-user.modal.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- This is the proper way if you have submit button outside your ion-content form (typically in the ion-header or ion-footer) -->\n<!-- (ref: https://github.com/ionic-team/ionic/issues/16661) -->\n<form class=\"update-user-form ion-page\" [formGroup]=\"updateUserForm\" (ngSubmit)=\"updateUser()\">\n  <ion-header>\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"end\">\n        <ion-button (click)=\"dismissModal()\">\n          <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n      <ion-title>Update User</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content class=\"update-user-content\">\n    <ion-row class=\"select-user-image-row\">\n      <ion-col class=\"user-image-col\" size=\"5\">\n        <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n          <app-image-shell [mode]=\"'cover'\" class=\"user-image\" animation=\"spinner\" [src]=\"selectedAvatar\" [alt]=\"'user image'\"></app-image-shell>\n        </app-aspect-ratio>\n        <ion-button class=\"change-image-btn\" color=\"secondary\" (click)=\"changeUserImage()\">\n          <ion-icon class=\"btn-icon\" slot=\"icon-only\" name=\"camera\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n\n    <section class=\"user-details-fields fields-section\">\n      <ion-list class=\"inputs-list\" lines=\"full\">\n        <ion-item class=\"input-item\">\n          <ion-label color=\"secondary\" position=\"floating\">Name</ion-label>\n          <ion-input type=\"text\" formControlName=\"name\" required></ion-input>\n        </ion-item>\n        <ion-item class=\"input-item\">\n          <ion-label color=\"secondary\" position=\"floating\">Last name</ion-label>\n          <ion-input type=\"text\" formControlName=\"lastname\" required></ion-input>\n        </ion-item>\n        <ion-item class=\"input-item\">\n          <ion-label color=\"secondary\" position=\"floating\">Date of Birth</ion-label>\n          <ion-datetime display-format=\"DD/MM/YYYY\" picker-format=\"DD MMMM YYYY\" formControlName=\"birthdate\" required></ion-datetime>\n        </ion-item>\n        <ion-item class=\"input-item\">\n          <ion-label color=\"secondary\" position=\"floating\">Phone number</ion-label>\n          <ion-input type=\"tel\" formControlName=\"phone\" required></ion-input>\n        </ion-item>\n        <ion-item class=\"input-item\">\n          <ion-label color=\"secondary\" position=\"floating\">Email</ion-label>\n          <ion-input type=\"email\" formControlName=\"email\" required></ion-input>\n        </ion-item>\n      </ion-list>\n    </section>\n\n    <section class=\"user-experience-fields fields-section\">\n      <h5 class=\"section-header\">Experience in</h5>\n      <ion-row class=\"checkbox-tags rounded-checkbox-tags\">\n        <ion-item formArrayName=\"skills\" lines=\"none\" class=\"checkbox-tag rounded-tag\" *ngFor=\"let skill of skillsFormArray.controls; let i = index;\">\n          <ion-label class=\"tag-label\">{{skills[i].name}}</ion-label>\n          <ion-checkbox [formControlName]=\"i\"></ion-checkbox>\n        </ion-item>\n      </ion-row>\n    </section>\n\n    <section class=\"user-languages-fields fields-section\">\n      <h5 class=\"section-header\">Languages</h5>\n      <ion-row class=\"range-item-row\">\n        <ion-col size=\"12\">\n          <div class=\"range-header\">\n            <span class=\"range-label\">English</span>\n            <span class=\"range-value\">{{ changeLangValue(updateUserForm.controls.english.value) }}</span>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-range class=\"range-control\" formControlName=\"english\" color=\"secondary\" min=\"1\" max=\"10\" step=\"1\"></ion-range>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"range-item-row\">\n        <ion-col size=\"12\">\n          <div class=\"range-header\">\n            <span class=\"range-label\">Spanish</span>\n            <span class=\"range-value\">{{ changeLangValue(updateUserForm.controls.spanish.value) }}</span>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-range class=\"range-control\" formControlName=\"spanish\" color=\"secondary\" min=\"1\" max=\"10\" step=\"1\"></ion-range>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"range-item-row\">\n        <ion-col size=\"12\">\n          <div class=\"range-header\">\n            <span class=\"range-label\">French</span>\n            <span class=\"range-value\">{{ changeLangValue(updateUserForm.controls.french.value) }}</span>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-range class=\"range-control\" formControlName=\"french\" color=\"secondary\" min=\"1\" max=\"10\" step=\"1\"></ion-range>\n        </ion-col>\n      </ion-row>\n    </section>\n  </ion-content>\n\n  <ion-footer>\n    <ion-row class=\"form-actions-wrapper\">\n      <ion-col>\n        <ion-button class=\"delete-btn\" expand=\"block\" color=\"primary\" fill=\"outline\" (click)=\"deleteUser()\">DELETE</ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button class=\"submit-btn\" expand=\"block\" color=\"secondary\" type=\"submit\" fill=\"solid\" [disabled]=\"!updateUserForm.valid\">UPDATE</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-footer>\n</form>\n"

/***/ }),

/***/ "./src/app/firebase/user/update/firebase-update-user.modal.ts":
/*!********************************************************************!*\
  !*** ./src/app/firebase/user/update/firebase-update-user.modal.ts ***!
  \********************************************************************/
/*! exports provided: FirebaseUpdateUserModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseUpdateUserModal", function() { return FirebaseUpdateUserModal; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _validators_checkbox_checked_validator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../validators/checkbox-checked.validator */ "./src/app/validators/checkbox-checked.validator.ts");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _firebase_user_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../firebase-user.model */ "./src/app/firebase/user/firebase-user.model.ts");
/* harmony import */ var _select_image_select_user_image_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../select-image/select-user-image.modal */ "./src/app/firebase/user/select-image/select-user-image.modal.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};









var FirebaseUpdateUserModal = /** @class */ (function () {
    function FirebaseUpdateUserModal(modalController, alertController, firebaseService, router) {
        this.modalController = modalController;
        this.alertController = alertController;
        this.firebaseService = firebaseService;
        this.router = router;
        this.skills = [];
    }
    FirebaseUpdateUserModal.prototype.ngOnInit = function () {
        var _this = this;
        this.selectedAvatar = this.user.avatar;
        this.updateUserForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.name, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.phone, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            birthdate: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](dayjs__WEBPACK_IMPORTED_MODULE_4__["unix"](this.user.birthdate).format('DD/MMMM/YYYY'), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            skills: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([], _validators_checkbox_checked_validator__WEBPACK_IMPORTED_MODULE_5__["CheckboxCheckedValidator"].minSelectedCheckboxes(1)),
            spanish: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.languages.spanish),
            english: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.languages.english),
            french: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](this.user.languages.french)
        });
        this.firebaseService.getSkills().subscribe(function (skills) {
            _this.skills = skills;
            // create skill checkboxes
            _this.skills.map(function (skill) {
                var userSkillsIds = [];
                if (_this.user.skills) {
                    userSkillsIds = _this.user.skills.map(function (skillId) {
                        return skillId['id'];
                    });
                }
                // set the control value to 'true' if the user already has this skill
                var control = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userSkillsIds.includes(skill.id));
                _this.updateUserForm.controls.skills.push(control);
            });
        });
    };
    Object.defineProperty(FirebaseUpdateUserModal.prototype, "skillsFormArray", {
        get: function () { return this.updateUserForm.get('skills'); },
        enumerable: true,
        configurable: true
    });
    FirebaseUpdateUserModal.prototype.changeLangValue = function (value) {
        switch (true) {
            case (value <= 3):
                return 'Novice';
            case (value > 3 && value <= 6):
                return 'Competent';
            case (value > 6):
                return 'Expert';
        }
    };
    FirebaseUpdateUserModal.prototype.dismissModal = function () {
        this.modalController.dismiss();
    };
    FirebaseUpdateUserModal.prototype.deleteUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Confirm',
                            message: 'Do you want to delete ' + this.user.name + '?',
                            buttons: [
                                {
                                    text: 'No',
                                    role: 'cancel',
                                    handler: function () { }
                                },
                                {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.firebaseService.deleteUser(_this.user.id)
                                            .then(function () {
                                            _this.dismissModal();
                                            _this.router.navigate(['firebase/listing']);
                                        }, function (err) { return console.log(err); });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FirebaseUpdateUserModal.prototype.updateUser = function () {
        var _this = this;
        this.user.avatar = this.selectedAvatar;
        this.user.name = this.updateUserForm.value.name;
        this.user.lastname = this.updateUserForm.value.lastname;
        this.user.birthdate = dayjs__WEBPACK_IMPORTED_MODULE_4__(this.updateUserForm.value.birthdate).unix(); // save it in timestamp
        this.user.phone = this.updateUserForm.value.phone;
        this.user.email = this.updateUserForm.value.email;
        this.user.languages.spanish = this.updateUserForm.value.spanish;
        this.user.languages.english = this.updateUserForm.value.english;
        this.user.languages.french = this.updateUserForm.value.french;
        // get the ids of the selected skills
        var selectedSkills = [];
        this.updateUserForm.value.skills
            .map(function (value, index) {
            if (value) {
                selectedSkills.push(_this.skills[index].id);
            }
        });
        this.user.skills = selectedSkills;
        var _a = this.user, age = _a.age, userData = __rest(_a, ["age"]); // we don't want to save the age in the DB because is something that changes over time
        this.firebaseService.updateUser(userData)
            .then(function () { return _this.modalController.dismiss(); }, function (err) { return console.log(err); });
    };
    FirebaseUpdateUserModal.prototype.changeUserImage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _select_image_select_user_image_modal__WEBPACK_IMPORTED_MODULE_8__["SelectUserImageModal"]
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (avatar) {
                            if (avatar.data != null) {
                                _this.selectedAvatar = avatar.data.link;
                            }
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", _firebase_user_model__WEBPACK_IMPORTED_MODULE_7__["FirebaseUserModel"])
    ], FirebaseUpdateUserModal.prototype, "user", void 0);
    FirebaseUpdateUserModal = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-firebase-update-user',
            template: __webpack_require__(/*! ./firebase-update-user.modal.html */ "./src/app/firebase/user/update/firebase-update-user.modal.html"),
            styles: [__webpack_require__(/*! ./styles/firebase-update-user.modal.scss */ "./src/app/firebase/user/update/styles/firebase-update-user.modal.scss"), __webpack_require__(/*! ./styles/firebase-update-user.shell.scss */ "./src/app/firebase/user/update/styles/firebase-update-user.shell.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _firebase_integration_service__WEBPACK_IMPORTED_MODULE_6__["FirebaseService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], FirebaseUpdateUserModal);
    return FirebaseUpdateUserModal;
}());



/***/ }),

/***/ "./src/app/firebase/user/update/styles/firebase-update-user.modal.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/firebase/user/update/styles/firebase-update-user.modal.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-background-shade: var(--app-background-shade);\n  --page-tags-gutter: 5px; }\n\n.update-user-content {\n  --background: var(--page-background); }\n\n.update-user-content .select-user-image-row {\n    --ion-grid-column-padding: 0px;\n    padding: var(--page-margin);\n    background-color: var(--page-background-shade); }\n\n.update-user-content .select-user-image-row .user-image-col {\n      margin: 0px auto;\n      position: relative; }\n\n.update-user-content .select-user-image-row .user-image-col .user-image {\n        border-radius: 50%;\n        border: solid 3px var(--ion-color-lightest); }\n\n.update-user-content .select-user-image-row .change-image-btn {\n      --border-radius: 50%;\n      --padding-start: 0px;\n      --padding-end: 0px;\n      position: absolute;\n      right: 0px;\n      bottom: var(--page-margin);\n      z-index: 3;\n      width: 4ch;\n      height: 4ch;\n      margin: 0px; }\n\n.update-user-content .select-user-image-row .change-image-btn .btn-icon {\n        font-size: 26px; }\n\n.update-user-content .fields-section {\n    padding: var(--page-margin) 0px;\n    border-bottom: calc(var(--page-margin) * 2) solid var(--page-background-shade); }\n\n.update-user-content .fields-section .section-header {\n      padding: 0px var(--page-margin);\n      margin-top: 0px;\n      margin-bottom: var(--page-margin); }\n\n.update-user-content .user-details-fields .inputs-list {\n    padding: 0px var(--page-margin); }\n\n.update-user-content .user-details-fields .input-item {\n    --padding-start: 0px;\n    --padding-end: 0px;\n    --inner-padding-start: 0px;\n    --inner-padding-end: 0px; }\n\n.update-user-content .user-experience-fields .checkbox-tags {\n    padding: 0px calc(var(--page-margin) - var(--page-tags-gutter));\n    --checkbox-tag-color: #000;\n    --checkbox-tag-background: #FFF;\n    --checkbox-tag-active-color: #FFF;\n    --checkbox-tag-active-background: #000; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag {\n      --padding-start: 0px;\n      --inner-padding-end: 8px;\n      --inner-padding-start: 8px;\n      --ion-item-background: var(--checkbox-tag-background);\n      --ion-item-color: var(--checkbox-tag-color); }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag.rounded-tag {\n        --border-radius: 2.2rem; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag.item-checkbox-checked {\n        --ion-item-background: var(--checkbox-tag-active-background);\n        --ion-item-color: var(--checkbox-tag-active-color); }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag.item-interactive-disabled {\n        opacity: 0.5; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag.item-interactive-disabled .tag-label {\n          opacity: 1; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag .tag-label {\n        margin: 5px;\n        font-size: 14px;\n        font-weight: 500;\n        letter-spacing: 0.2px;\n        text-align: center; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag ion-checkbox {\n        margin: 0px;\n        width: 0px;\n        --border-width: 0px;\n        height: 0px;\n        --color-checked: transparent; }\n\n.update-user-content .user-experience-fields .checkbox-tags .checkbox-tag {\n      padding: 0px var(--page-tags-gutter);\n      margin: var(--page-tags-gutter) 0px; }\n\n.update-user-content .user-experience-fields .checkbox-tags.rounded-checkbox-tags {\n      background-color: var(--ion-color-lightest); }\n\n.update-user-content .user-experience-fields .checkbox-tags.rounded-checkbox-tags .checkbox-tag {\n        --border-width: 2px;\n        --border-style: solid;\n        --border-color: var(--ion-color-medium-shade);\n        --checkbox-tag-color: var(--ion-color-medium-shade);\n        --checkbox-tag-background: var(--ion-color-lightest);\n        --checkbox-tag-active-color: var(--ion-color-lightest);\n        --checkbox-tag-active-background: var(--ion-color-darkest); }\n\n.update-user-content .user-experience-fields .checkbox-tags.rounded-checkbox-tags .checkbox-tag.item-checkbox-checked {\n          --border-color: var(--ion-color-darkest); }\n\n.update-user-content .user-languages-fields {\n    padding-bottom: 0px; }\n\n.update-user-content .user-languages-fields .range-item-row {\n      --ion-grid-column-padding: 0px;\n      padding-top: var(--page-margin);\n      -webkit-padding-start: var(--page-margin);\n              padding-inline-start: var(--page-margin);\n      -webkit-padding-end: var(--page-margin);\n              padding-inline-end: var(--page-margin);\n      box-shadow: inset 0 6px 3px -8px var(--ion-color-darkest); }\n\n.update-user-content .user-languages-fields .range-item-row .range-header {\n        display: -webkit-box;\n        display: flex;\n        -webkit-box-pack: justify;\n                justify-content: space-between;\n        padding-bottom: calc(var(--page-margin) / 2); }\n\n.update-user-content .user-languages-fields .range-item-row .range-header .range-value {\n          font-size: 12px;\n          font-weight: 600;\n          letter-spacing: 0.2px;\n          color: var(--ion-color-medium-shade); }\n\n.update-user-content .user-languages-fields .range-item-row .range-header .range-label {\n          font-size: 14px;\n          font-weight: 500;\n          letter-spacing: 0.2px;\n          color: var(--ion-color-medium-shade); }\n\n.update-user-content .user-languages-fields .range-item-row .range-control {\n        --ion-text-color: var(--ion-color-medium-shade);\n        padding-top: 0px;\n        padding-bottom: 0px; }\n\n.form-actions-wrapper {\n  --ion-grid-column-padding: calc(var(--page-margin) / 2);\n  padding: calc(var(--page-margin) / 2); }\n\n.form-actions-wrapper .submit-btn {\n    margin: 0px; }\n\n.form-actions-wrapper .delete-btn {\n    margin: 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci91cGRhdGUvc3R5bGVzL2ZpcmViYXNlLXVwZGF0ZS11c2VyLm1vZGFsLnNjc3MiLCIvVXNlcnMvbWFjYm9va3Byby9Eb2N1bWVudHMvY29kZS9hcHAvdGE0cHdhL2lvbmljNC9zcmMvdGhlbWUvbWl4aW5zL2lucHV0cy9jaGVja2JveC10YWcuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHFDQUFjO0VBQ2Qsd0NBQWtCO0VBRWxCLG9EQUF3QjtFQUN4Qix1QkFBbUIsRUFBQTs7QUFHckI7RUFDRSxvQ0FBYSxFQUFBOztBQURmO0lBSUksOEJBQTBCO0lBRTFCLDJCQUEyQjtJQUMzQiw4Q0FBOEMsRUFBQTs7QUFQbEQ7TUFVTSxnQkFBZ0I7TUFDaEIsa0JBQWtCLEVBQUE7O0FBWHhCO1FBY1Esa0JBQWtCO1FBQ2xCLDJDQUEyQyxFQUFBOztBQWZuRDtNQW9CTSxvQkFBZ0I7TUFDaEIsb0JBQWdCO01BQ2hCLGtCQUFjO01BRWQsa0JBQWtCO01BQ2xCLFVBQVU7TUFDViwwQkFBMEI7TUFDMUIsVUFBVTtNQUNWLFVBQVU7TUFDVixXQUFXO01BQ1gsV0FBVyxFQUFBOztBQTlCakI7UUFpQ1EsZUFBZSxFQUFBOztBQWpDdkI7SUF1Q0ksK0JBQStCO0lBQy9CLDhFQUE4RSxFQUFBOztBQXhDbEY7TUEyQ00sK0JBQStCO01BQy9CLGVBQWU7TUFDZixpQ0FBaUMsRUFBQTs7QUE3Q3ZDO0lBbURNLCtCQUErQixFQUFBOztBQW5EckM7SUF1RE0sb0JBQWdCO0lBQ2hCLGtCQUFjO0lBQ2QsMEJBQXNCO0lBQ3RCLHdCQUFvQixFQUFBOztBQTFEMUI7SUFnRU0sK0RBQStEO0lDeEVuRSwwQkFBcUI7SUFDckIsK0JBQTBCO0lBQzFCLGlDQUE0QjtJQUM1QixzQ0FBaUMsRUFBQTs7QURLbkM7TUNERSxvQkFBZ0I7TUFDZCx3QkFBb0I7TUFDdEIsMEJBQXNCO01BQ3RCLHFEQUFzQjtNQUNwQiwyQ0FBaUIsRUFBQTs7QURIckI7UUNNRyx1QkFBZ0IsRUFBQTs7QURObkI7UUNVTSw0REFBc0I7UUFDdEIsa0RBQWlCLEVBQUE7O0FEWHZCO1FDZU0sWUFBWSxFQUFBOztBRGZsQjtVQ21CUSxVQUFVLEVBQUE7O0FEbkJsQjtRQ3dCRyxXQUFXO1FBQ1IsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixxQkFBcUI7UUFDckIsa0JBQWtCLEVBQUE7O0FENUJ4QjtRQ2dDRyxXQUFXO1FBRVgsVUFBVTtRQUNWLG1CQUFlO1FBQ2YsV0FBVztRQUVYLDRCQUFnQixFQUFBOztBRHRDbkI7TUFxRVEsb0NBQW9DO01BQ3BDLG1DQUFtQyxFQUFBOztBQXRFM0M7TUEyRVEsMkNBQTJDLEVBQUE7O0FBM0VuRDtRQThFVSxtQkFBZTtRQUNmLHFCQUFlO1FBQ2YsNkNBQWU7UUFDZixtREFBcUI7UUFDckIsb0RBQTBCO1FBQzFCLHNEQUE0QjtRQUM1QiwwREFBaUMsRUFBQTs7QUFwRjNDO1VBdUZZLHdDQUFlLEVBQUE7O0FBdkYzQjtJQStGSSxtQkFBbUIsRUFBQTs7QUEvRnZCO01Ba0dNLDhCQUEwQjtNQUUxQiwrQkFBK0I7TUFDL0IseUNBQXdDO2NBQXhDLHdDQUF3QztNQUN4Qyx1Q0FBc0M7Y0FBdEMsc0NBQXNDO01BRXRDLHlEQUEwRCxFQUFBOztBQXhHaEU7UUEyR1Esb0JBQWE7UUFBYixhQUFhO1FBQ2IseUJBQThCO2dCQUE5Qiw4QkFBOEI7UUFDOUIsNENBQTRDLEVBQUE7O0FBN0dwRDtVQWdIVSxlQUFlO1VBQ2YsZ0JBQWdCO1VBQ2hCLHFCQUFxQjtVQUNyQixvQ0FBb0MsRUFBQTs7QUFuSDlDO1VBdUhVLGVBQWU7VUFDZixnQkFBZ0I7VUFDaEIscUJBQXFCO1VBQ3JCLG9DQUFvQyxFQUFBOztBQTFIOUM7UUFnSVEsK0NBQWlCO1FBRWpCLGdCQUFnQjtRQUNoQixtQkFBbUIsRUFBQTs7QUFNM0I7RUFDRSx1REFBMEI7RUFFMUIscUNBQXFDLEVBQUE7O0FBSHZDO0lBTUksV0FBVyxFQUFBOztBQU5mO0lBVUksV0FBVyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmlyZWJhc2UvdXNlci91cGRhdGUvc3R5bGVzL2ZpcmViYXNlLXVwZGF0ZS11c2VyLm1vZGFsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vLi4vdGhlbWUvbWl4aW5zL2lucHV0cy9jaGVja2JveC10YWdcIjtcblxuOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuXG4gIC0tcGFnZS1iYWNrZ3JvdW5kLXNoYWRlOiB2YXIoLS1hcHAtYmFja2dyb3VuZC1zaGFkZSk7XG4gIC0tcGFnZS10YWdzLWd1dHRlcjogNXB4O1xufVxuXG4udXBkYXRlLXVzZXItY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcblxuICAuc2VsZWN0LXVzZXItaW1hZ2Utcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kLXNoYWRlKTtcblxuICAgIC51c2VyLWltYWdlLWNvbCB7XG4gICAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuXG4gICAgICAudXNlci1pbWFnZSB7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgYm9yZGVyOiBzb2xpZCAzcHggdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuY2hhbmdlLWltYWdlLWJ0biB7XG4gICAgICAtLWJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgLS1wYWRkaW5nLWVuZDogMHB4O1xuXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICByaWdodDogMHB4O1xuICAgICAgYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICB6LWluZGV4OiAzO1xuICAgICAgd2lkdGg6IDRjaDtcbiAgICAgIGhlaWdodDogNGNoO1xuICAgICAgbWFyZ2luOiAwcHg7XG5cbiAgICAgIC5idG4taWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuZmllbGRzLXNlY3Rpb24ge1xuICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKSAwcHg7XG4gICAgYm9yZGVyLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAyKSBzb2xpZCB2YXIoLS1wYWdlLWJhY2tncm91bmQtc2hhZGUpO1xuXG4gICAgLnNlY3Rpb24taGVhZGVyIHtcbiAgICAgIHBhZGRpbmc6IDBweCB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICBtYXJnaW4tYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgfVxuICB9XG5cbiAgLnVzZXItZGV0YWlscy1maWVsZHMge1xuICAgIC5pbnB1dHMtbGlzdCB7XG4gICAgICBwYWRkaW5nOiAwcHggdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIH1cblxuICAgIC5pbnB1dC1pdGVtIHtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgLS1wYWRkaW5nLWVuZDogMHB4O1xuICAgICAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG4gICAgfVxuICB9XG5cbiAgLnVzZXItZXhwZXJpZW5jZS1maWVsZHMge1xuICAgIC5jaGVja2JveC10YWdzIHtcbiAgICAgIHBhZGRpbmc6IDBweCBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAtIHZhcigtLXBhZ2UtdGFncy1ndXR0ZXIpKTtcblxuICAgICAgQGluY2x1ZGUgY2hlY2tib3gtdGFnKCk7XG5cbiAgICAgIC5jaGVja2JveC10YWcge1xuICAgICAgICBwYWRkaW5nOiAwcHggdmFyKC0tcGFnZS10YWdzLWd1dHRlcik7XG4gICAgICAgIG1hcmdpbjogdmFyKC0tcGFnZS10YWdzLWd1dHRlcikgMHB4O1xuICAgICAgfVxuXG4gICAgICAvLyBBZGQgYSBkZWVwZXIgc2VsZWN0b3IgdG8gb3ZlcnJpZGUgZGVmYXVsdCBjb2xvcnNcbiAgICAgICYucm91bmRlZC1jaGVja2JveC10YWdzIHtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcblxuICAgICAgICAuY2hlY2tib3gtdGFnIHtcbiAgICAgICAgICAtLWJvcmRlci13aWR0aDogMnB4O1xuICAgICAgICAgIC0tYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICAgICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSk7XG4gICAgICAgICAgLS1jaGVja2JveC10YWctY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xuICAgICAgICAgIC0tY2hlY2tib3gtdGFnLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgICAgICAgLS1jaGVja2JveC10YWctYWN0aXZlLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgICAgICAgIC0tY2hlY2tib3gtdGFnLWFjdGl2ZS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItZGFya2VzdCk7XG5cbiAgICAgICAgICAmLml0ZW0tY2hlY2tib3gtY2hlY2tlZCB7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmtlc3QpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC51c2VyLWxhbmd1YWdlcy1maWVsZHMge1xuICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG5cbiAgICAucmFuZ2UtaXRlbS1yb3cge1xuICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuXG4gICAgICBwYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgICAgIHBhZGRpbmctaW5saW5lLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgICAgLy8gYm94LXNoYWRvdyBhdCB0aGUgdG9wXG4gICAgICBib3gtc2hhZG93OiBpbnNldCAwIDZweCAzcHggLThweCAgdmFyKC0taW9uLWNvbG9yLWRhcmtlc3QpO1xuXG4gICAgICAucmFuZ2UtaGVhZGVyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAgICAgICAucmFuZ2UtdmFsdWUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSk7XG4gICAgICAgIH1cblxuICAgICAgICAucmFuZ2UtbGFiZWwge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcbiAgICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLnJhbmdlLWNvbnRyb2wge1xuICAgICAgICAvLyBvdmVycmlkZSB0aGUgcGluIGNvbG9yXG4gICAgICAgIC0taW9uLXRleHQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xuXG4gICAgICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi5mb3JtLWFjdGlvbnMtd3JhcHBlciB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG5cbiAgcGFkZGluZzogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAuc3VibWl0LWJ0biB7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cblxuICAuZGVsZXRlLWJ0biB7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbn1cbiIsIkBtaXhpbiBjaGVja2JveC10YWcoKSB7XG4gIC8vIERlZmF1bHQgdmFsdWVzXG4gIC0tY2hlY2tib3gtdGFnLWNvbG9yOiAjMDAwO1xuICAtLWNoZWNrYm94LXRhZy1iYWNrZ3JvdW5kOiAjRkZGO1xuICAtLWNoZWNrYm94LXRhZy1hY3RpdmUtY29sb3I6ICNGRkY7XG4gIC0tY2hlY2tib3gtdGFnLWFjdGl2ZS1iYWNrZ3JvdW5kOiAjMDAwO1xuXG5cdC5jaGVja2JveC10YWcge1xuICAgIC8vIFJlc2V0IHZhbHVlcyBmcm9tIElvbmljIChpb24taXRlbSkgc3R5bGVzXG5cdFx0LS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogOHB4O1xuXHRcdC0taW5uZXItcGFkZGluZy1zdGFydDogOHB4O1xuXHRcdC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdmFyKC0tY2hlY2tib3gtdGFnLWJhY2tncm91bmQpO1xuICAgIC0taW9uLWl0ZW0tY29sb3I6IHZhcigtLWNoZWNrYm94LXRhZy1jb2xvcik7XG5cbiAgICAmLnJvdW5kZWQtdGFnIHtcblx0XHRcdC0tYm9yZGVyLXJhZGl1czogMi4ycmVtO1xuXHRcdH1cblxuXHRcdCYuaXRlbS1jaGVja2JveC1jaGVja2VkIHtcbiAgICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdmFyKC0tY2hlY2tib3gtdGFnLWFjdGl2ZS1iYWNrZ3JvdW5kKTtcbiAgICAgIC0taW9uLWl0ZW0tY29sb3I6IHZhcigtLWNoZWNrYm94LXRhZy1hY3RpdmUtY29sb3IpO1xuXHRcdH1cblxuICAgICYuaXRlbS1pbnRlcmFjdGl2ZS1kaXNhYmxlZCB7XG4gICAgICBvcGFjaXR5OiAwLjU7XG5cbiAgICAgIC50YWctbGFiZWwge1xuICAgICAgICAvLyBPdmVycmlkZSBJb25pYyBkZWZhdWx0IHN0eWxlXG4gICAgICAgIG9wYWNpdHk6IDE7XG4gICAgICB9XG4gICAgfVxuXG5cdFx0LnRhZy1sYWJlbCB7XG5cdFx0XHRtYXJnaW46IDVweDtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICBsZXR0ZXItc3BhY2luZzogMC4ycHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cdFx0fVxuXG5cdFx0aW9uLWNoZWNrYm94IHtcblx0XHRcdG1hcmdpbjogMHB4O1xuXHRcdFx0Ly8gVG8gaGlkZSB0aGUgLmNoZWNrYm94LWljb25cblx0XHRcdHdpZHRoOiAwcHg7XG5cdFx0XHQtLWJvcmRlci13aWR0aDogMHB4O1xuXHRcdFx0aGVpZ2h0OiAwcHg7XG5cdFx0XHQvLyBXZSBjYW50IHNldCB3aWR0aCBhbmQgaGVpZ2h0IGZvciAuY2hlY2tib3gtaWNvbiAuY2hlY2tib3gtaW5uZXIsIHNvIGxldHMgaGlkZSBpdCBjaGFuZ2luZyBpdHMgY29sb3Jcblx0XHRcdC0tY29sb3ItY2hlY2tlZDogdHJhbnNwYXJlbnQ7XG5cdFx0fVxuXHR9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/firebase/user/update/styles/firebase-update-user.shell.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/firebase/user/update/styles/firebase-update-user.shell.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.user-image {\n  --image-shell-border-radius: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci91cGRhdGUvc3R5bGVzL2ZpcmViYXNlLXVwZGF0ZS11c2VyLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQ0FBNEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZpcmViYXNlL3VzZXIvdXBkYXRlL3N0eWxlcy9maXJlYmFzZS11cGRhdGUtdXNlci5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYXBwLWltYWdlLXNoZWxsLnVzZXItaW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IDUwJTtcbn1cbiJdfQ== */"

/***/ })

}]);
//# sourceMappingURL=default~firebase-firebase-integration-module~user-details-firebase-user-details-module.js.map