(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["referral-referral-module"],{

/***/ "./src/app/referral/referral.module.ts":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.module.ts ***!
  \*********************************************/
/*! exports provided: ReferralPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPageModule", function() { return ReferralPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _referral_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./referral.page */ "./src/app/referral/referral.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _referral_page__WEBPACK_IMPORTED_MODULE_5__["ReferralPage"]
    }
];
var ReferralPageModule = /** @class */ (function () {
    function ReferralPageModule() {
    }
    ReferralPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_referral_page__WEBPACK_IMPORTED_MODULE_5__["ReferralPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], ReferralPageModule);
    return ReferralPageModule;
}());



/***/ }),

/***/ "./src/app/referral/referral.page.html":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Get Referral</ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\" (click)=\"askForReferral()\"  name=\"arrow-dropleft-circle\"></ion-icon>\n  </ion-toolbar>\n\n</ion-header>\n\n\n<ion-content  padding  >\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Get Referral</ion-card-subtitle>\n      <ion-card-title>Ask For A Referral</ion-card-title>\n    </ion-card-header>\n\n    <ion-card-content>\n      Post a request to get referral in your network. Get trusted referral with ease\n    </ion-card-content>\n  </ion-card>\n\n\n  <!-- List of Text Items -->\n  <ion-list detail=\"true\" lines=\"full\" class=\"users-list\">\n    <!-- Searchbar with a placeholder -->\n    <ion-list no-lines>\n      <ion-item  class=\"itemlistref mainitemlist\" >\n          <ion-searchbar (ionInput)=\"getItems($event)\" placeholder=\"Filter categories\"></ion-searchbar>\n      </ion-item>\n    </ion-list>\n    <ion-list no-lines>\n      <ion-item detail=\"true\" tappable  class=\"itemlistref mainitemlist\"  *ngFor=\"let item of items\" (click)=\"selCategory(item.id)\">\n           <ion-label class=\"shelllayer\">\n             {{item.name}}\n           </ion-label>\n      </ion-item>\n    </ion-list>\n\n  </ion-list>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/referral/referral.page.scss":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZmVycmFsL3JlZmVycmFsLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/referral/referral.page.ts":
/*!*******************************************!*\
  !*** ./src/app/referral/referral.page.ts ***!
  \*******************************************/
/*! exports provided: ReferralPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPage", function() { return ReferralPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _referral_modal_referral_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../referral-modal/referral-modal.page */ "./src/app/referral-modal/referral-modal.page.ts");
/* harmony import */ var _services_cats_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cats.service */ "./src/services/cats.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var ReferralPage = /** @class */ (function () {
    function ReferralPage(platform, catservice, modalController, router) {
        this.catservice = catservice;
        this.modalController = modalController;
        this.router = router;
        this.myrequests = [];
        this.requestcounter = null;
        this.showheader = true;
        this.isVirgin = false;
        this.timestamp = new Date();
        this.timestamp = new Date();
        console.log("ReferralPage : Referral Page Construction " + this.timestamp.getTime());
    }
    ReferralPage.prototype.initializeItems = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.timestamp = new Date();
                console.log("ReferralPage : initializeItems " + this.timestamp.getTime());
                this.catservice.getCats().then(function (res) {
                    _this.timestamp = new Date();
                    setTimeout(function () {
                        _this.items = res;
                        console.log("ReferralPage : this.items = res " + _this.timestamp.getTime());
                    }, 1);
                }).catch(function (e) {
                });
                return [2 /*return*/];
            });
        });
    };
    ReferralPage.prototype.ngOnInit = function () {
    };
    ReferralPage.prototype.ionViewDidEnter = function () {
        this.timestamp = new Date();
        console.log("ReferralPage : ngOnInit" + this.timestamp.getTime());
        this.timestamp = new Date();
        console.log("ReferralPage : Platform Ready " + this.timestamp.getTime());
        this.initializeItems();
    };
    ReferralPage.prototype.askForReferral = function () {
        this.router.navigateByUrl('/referral');
    };
    ReferralPage.prototype.selCategory = function (item_id) {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _referral_modal_referral_modal_page__WEBPACK_IMPORTED_MODULE_3__["ReferralModalPage"],
                            componentProps: {
                                'item_id': item_id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ReferralPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ReferralPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-referral',
            template: __webpack_require__(/*! ./referral.page.html */ "./src/app/referral/referral.page.html"),
            styles: [__webpack_require__(/*! ./referral.page.scss */ "./src/app/referral/referral.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"], _services_cats_service__WEBPACK_IMPORTED_MODULE_4__["CatsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ReferralPage);
    return ReferralPage;
}());



/***/ })

}]);
//# sourceMappingURL=referral-referral-module.js.map