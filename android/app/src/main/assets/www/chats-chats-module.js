(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chats-chats-module"],{

/***/ "./src/app/chats/chats.module.ts":
/*!***************************************!*\
  !*** ./src/app/chats/chats.module.ts ***!
  \***************************************/
/*! exports provided: ChatsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPageModule", function() { return ChatsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chats_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./chats.page */ "./src/app/chats/chats.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _chats_page__WEBPACK_IMPORTED_MODULE_5__["ChatsPage"]
    }
];
var ChatsPageModule = /** @class */ (function () {
    function ChatsPageModule() {
    }
    ChatsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_chats_page__WEBPACK_IMPORTED_MODULE_5__["ChatsPage"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], ChatsPageModule);
    return ChatsPageModule;
}());



/***/ }),

/***/ "./src/app/chats/chats.page.html":
/*!***************************************!*\
  !*** ./src/app/chats/chats.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Contacts\n    </ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\" (click)=\"askForReferral()\"  name=\"add-circle\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content   class=\"notifications-content\">\n\n  <ion-list>\n    <ion-item  *ngFor=\"let friend of myfriends\">\n      <ion-label>{{friend.displayName}} < </ion-label>\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/chats/chats.page.scss":
/*!***************************************!*\
  !*** ./src/app/chats/chats.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NoYXRzL2NoYXRzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/chats/chats.page.ts":
/*!*************************************!*\
  !*** ./src/app/chats/chats.page.ts ***!
  \*************************************/
/*! exports provided: ChatsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatsPage", function() { return ChatsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _tpstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/loading.service */ "./src/services/loading.service.ts");
/* harmony import */ var _app_angularfireconfig__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app.angularfireconfig */ "./src/app/app.angularfireconfig.ts");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import { UserService } from '../../services/user.service';

//import { ContactService } from '../../services/contact.service';
//import { RequestsService } from '../../services/requests.service';
//import { SmsService } from '../../services/sms.service';
//import { ChatService } from '../../services/chat.service';
//import { FcmService } from '../../services/fcm.service';


firebase_app__WEBPACK_IMPORTED_MODULE_7__["initializeApp"](_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_6__["config"]);
var ChatsPage = /** @class */ (function () {
    function ChatsPage(route, router, platform, loadingProvider, alertCtrl, sqlite, modalCtrl, toastCtrl, tpStorageService) {
        this.route = route;
        this.router = router;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.tpStorageService = tpStorageService;
        this.firebuddychats = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/buddychats');
        this.firedata = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/chatusers');
        this.firereq = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/requests');
        this.firefriends = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/friends');
        this.fireInvitationSent = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/invitationsent');
        this.firePhones = firebase_app__WEBPACK_IMPORTED_MODULE_7__["database"]().ref('/phones');
        this.lastMsgReader = {};
        this.blockRequest = [];
        this.subscriptionAddedObj = {};
        this.subscriptionAddedFriendsObj = {};
    }
    ChatsPage.prototype.getmyfriends_ = function () {
        var _this = this;
        console.log("getmyfriends_");
        this.firefriends.child(this.userId).on('value', function (snapshot) {
            _this.myfriends = [];
            var allfriends = snapshot.val();
            if (allfriends != null) {
                // I have some friends
                ////console.log ("I have friends " );
                var CounterFriends_1 = 0;
                for (var i in allfriends) {
                    ////console.log ("iiii ")
                    var lastMsgActual__ = {
                        message: "",
                        type: "",
                        dateofmsg: "",
                        timeofmsg: "",
                        isRead: "",
                        isStarred: "",
                        referral_type: "",
                        selectCatId: ""
                    };
                    if (!allfriends[i].isBlock) {
                        allfriends[i].isBlock = false;
                    }
                    allfriends[i].unreadmessage = lastMsgActual__;
                    allfriends[i].lastMessage = lastMsgActual__;
                    allfriends[i].business = '';
                    allfriends[i].read = false;
                    _this.myfriends.push(allfriends[i]);
                    CounterFriends_1++;
                } //end for
                //////console.log ("this.cnter "+ this.cnter);
                //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
                var friendsSoFar_1 = 0;
                var _loop_1 = function (y) {
                    if (true) {
                        _this.firebuddychats.child(_this.userId).child(_this.myfriends[y].uid).limitToLast(1).once('value', function (snapshot_last_msg) {
                            var lastMsg__ = snapshot_last_msg.val();
                            if (lastMsg__) {
                                for (var key in lastMsg__) {
                                    var lastMsgActual__ = {
                                        message: lastMsg__[key].message,
                                        type: lastMsg__[key].type,
                                        dateofmsg: lastMsg__[key].dateofmsg,
                                        timeofmsg: lastMsg__[key].timeofmsg,
                                        referral_type: lastMsg__[key].referral_type,
                                        selectCatId: lastMsg__[key].selectCatId,
                                        isRead: false,
                                        isStarred: false
                                    };
                                    _this.myfriends[y].unreadmessage = lastMsgActual__;
                                    _this.myfriends[y].lastMessage = lastMsgActual__;
                                    break;
                                } //end for
                            } //endif
                            if (++friendsSoFar_1 >= CounterFriends_1) {
                                console.log("1. doneLoadingFriendsFromFirebase");
                                _this.doneLoadingFriendsFromFirebase();
                            }
                        });
                    }
                    else {} //endif
                };
                for (var y in _this.myfriends) {
                    _loop_1(y);
                } //end for
            }
            else {
                // I dont have any friend (:
                console.log("2. doneLoadingFriendsFromFirebase");
                _this.doneLoadingFriendsFromFirebase();
            } //endif
        }); //get all friends
    };
    ChatsPage.prototype.doneLoadingFriendsFromFirebase = function () {
        console.log("Done Loading Friends ");
        console.log(this.myfriends);
    };
    ChatsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        console.log("ionViewDidLoad");
        this.tpStorageService.get('userUID').then(function (myUserId) {
            _this.userId = myUserId;
            _this.getmyfriends_();
        }).catch(function (e) {
            //mytodo : redirect to login page
        });
    };
    ChatsPage.prototype.ngOnInit = function () {
    };
    ChatsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chats',
            template: __webpack_require__(/*! ./chats.page.html */ "./src/app/chats/chats.page.html"),
            styles: [__webpack_require__(/*! ./chats.page.scss */ "./src/app/chats/chats.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _services_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_3__["SQLite"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _tpstorage_service__WEBPACK_IMPORTED_MODULE_4__["TpstorageProvider"]])
    ], ChatsPage);
    return ChatsPage;
}());



/***/ })

}]);
//# sourceMappingURL=chats-chats-module.js.map