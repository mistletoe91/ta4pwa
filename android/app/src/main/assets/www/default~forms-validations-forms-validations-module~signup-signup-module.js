(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~forms-validations-forms-validations-module~signup-signup-module"],{

/***/ "./node_modules/@ionic-native/http/ngx/index.js":
/*!******************************************************!*\
  !*** ./node_modules/@ionic-native/http/ngx/index.js ***!
  \******************************************************/
/*! exports provided: HTTP */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HTTP", function() { return HTTP; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var HTTP = /** @class */ (function (_super) {
    __extends(HTTP, _super);
    function HTTP() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HTTP.prototype.getBasicAuthHeader = function (username, password) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "getBasicAuthHeader", { "sync": true }, arguments); };
    HTTP.prototype.useBasicAuth = function (username, password) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "useBasicAuth", { "sync": true }, arguments); };
    HTTP.prototype.getHeaders = function (host) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "getHeaders", { "sync": true }, arguments); };
    HTTP.prototype.setHeader = function (host, header, value) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "setHeader", { "sync": true }, arguments); };
    HTTP.prototype.getDataSerializer = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "getDataSerializer", { "sync": true }, arguments); };
    HTTP.prototype.setDataSerializer = function (serializer) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "setDataSerializer", { "sync": true }, arguments); };
    HTTP.prototype.setCookie = function (url, cookie) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "setCookie", { "sync": true }, arguments); };
    HTTP.prototype.clearCookies = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "clearCookies", { "sync": true }, arguments); };
    HTTP.prototype.removeCookies = function (url, cb) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "removeCookies", { "sync": true }, arguments); };
    HTTP.prototype.getCookieString = function (url) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "getCookieString", { "sync": true }, arguments); };
    HTTP.prototype.getRequestTimeout = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "getRequestTimeout", { "sync": true }, arguments); };
    HTTP.prototype.setRequestTimeout = function (timeout) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "setRequestTimeout", { "sync": true }, arguments); };
    HTTP.prototype.setSSLCertMode = function (mode) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "setSSLCertMode", {}, arguments); };
    HTTP.prototype.disableRedirect = function (disable) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "disableRedirect", {}, arguments); };
    HTTP.prototype.post = function (url, body, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "post", {}, arguments); };
    HTTP.prototype.get = function (url, parameters, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "get", {}, arguments); };
    HTTP.prototype.put = function (url, body, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "put", {}, arguments); };
    HTTP.prototype.patch = function (url, body, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "patch", {}, arguments); };
    HTTP.prototype.delete = function (url, parameters, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "delete", {}, arguments); };
    HTTP.prototype.head = function (url, parameters, headers) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "head", {}, arguments); };
    HTTP.prototype.uploadFile = function (url, body, headers, filePath, name) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "uploadFile", {}, arguments); };
    HTTP.prototype.downloadFile = function (url, body, headers, filePath) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "downloadFile", {}, arguments); };
    HTTP.prototype.sendRequest = function (url, options) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["cordova"])(this, "sendRequest", {}, arguments); };
    HTTP.pluginName = "HTTP";
    HTTP.plugin = "cordova-plugin-advanced-http";
    HTTP.pluginRef = "cordova.plugin.http";
    HTTP.repo = "https://github.com/silkimen/cordova-plugin-advanced-http";
    HTTP.platforms = ["Android", "iOS"];
    HTTP = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], HTTP);
    return HTTP;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2h0dHAvbmd4L2luZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sOEJBQXNDLE1BQU0sb0JBQW9CLENBQUM7O0lBc0U5Qyx3QkFBaUI7Ozs7SUFRekMsaUNBQWtCLGFBQUMsUUFBZ0IsRUFBRSxRQUFnQjtJQVVyRCwyQkFBWSxhQUFDLFFBQWdCLEVBQUUsUUFBZ0I7SUFRL0MseUJBQVUsYUFBQyxJQUFZO0lBV3ZCLHdCQUFTLGFBQUMsSUFBWSxFQUFFLE1BQWMsRUFBRSxLQUFhO0lBT3JELGdDQUFpQjtJQVNqQixnQ0FBaUIsYUFBQyxVQUFrQjtJQVFwQyx3QkFBUyxhQUFDLEdBQVcsRUFBRSxNQUFjO0lBTXJDLDJCQUFZO0lBUVosNEJBQWEsYUFBQyxHQUFXLEVBQUUsRUFBYztJQU96Qyw4QkFBZSxhQUFDLEdBQVc7SUFTM0IsZ0NBQWlCO0lBU2pCLGdDQUFpQixhQUFDLE9BQWU7SUFZakMsNkJBQWMsYUFBQyxJQUFpRDtJQVVoRSw4QkFBZSxhQUFDLE9BQWdCO0lBWWhDLG1CQUFJLGFBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxPQUFZO0lBWXpDLGtCQUFHLGFBQUMsR0FBVyxFQUFFLFVBQWUsRUFBRSxPQUFZO0lBWTlDLGtCQUFHLGFBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxPQUFZO0lBWXhDLG9CQUFLLGFBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxPQUFZO0lBWTFDLHFCQUFNLGFBQUMsR0FBVyxFQUFFLFVBQWUsRUFBRSxPQUFZO0lBWWpELG1CQUFJLGFBQUMsR0FBVyxFQUFFLFVBQWUsRUFBRSxPQUFZO0lBYy9DLHlCQUFVLGFBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxPQUFZLEVBQUUsUUFBZ0IsRUFBRSxJQUFZO0lBYS9FLDJCQUFZLGFBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxPQUFZLEVBQUUsUUFBZ0I7SUFvQm5FLDBCQUFXLGFBQ1QsR0FBVyxFQUNYLE9BU0M7Ozs7OztJQTVQUSxJQUFJO1FBRGhCLFVBQVUsRUFBRTtPQUNBLElBQUk7ZUF2RWpCO0VBdUUwQixpQkFBaUI7U0FBOUIsSUFBSSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIElvbmljTmF0aXZlUGx1Z2luLCBQbHVnaW4gfSBmcm9tICdAaW9uaWMtbmF0aXZlL2NvcmUnO1xuXG5leHBvcnQgaW50ZXJmYWNlIEhUVFBSZXNwb25zZSB7XG4gIC8qKlxuICAgKiBUaGUgc3RhdHVzIG51bWJlciBvZiB0aGUgcmVzcG9uc2VcbiAgICovXG4gIHN0YXR1czogbnVtYmVyO1xuICAvKipcbiAgICogVGhlIGhlYWRlcnMgb2YgdGhlIHJlc3BvbnNlXG4gICAqL1xuICBoZWFkZXJzOiBhbnk7XG4gIC8qKlxuICAgKiBUaGUgVVJMIG9mIHRoZSByZXNwb25zZS4gVGhpcyBwcm9wZXJ0eSB3aWxsIGJlIHRoZSBmaW5hbCBVUkwgb2J0YWluZWQgYWZ0ZXIgYW55IHJlZGlyZWN0cy5cbiAgICovXG4gIHVybDogc3RyaW5nO1xuICAvKipcbiAgICogVGhlIGRhdGEgdGhhdCBpcyBpbiB0aGUgcmVzcG9uc2UuIFRoaXMgcHJvcGVydHkgdXN1YWxseSBleGlzdHMgd2hlbiBhIHByb21pc2UgcmV0dXJuZWQgYnkgYSByZXF1ZXN0IG1ldGhvZCByZXNvbHZlcy5cbiAgICovXG4gIGRhdGE/OiBhbnk7XG4gIC8qKlxuICAgKiBFcnJvciByZXNwb25zZSBmcm9tIHRoZSBzZXJ2ZXIuIFRoaXMgcHJvcGVydHkgdXN1YWxseSBleGlzdHMgd2hlbiBhIHByb21pc2UgcmV0dXJuZWQgYnkgYSByZXF1ZXN0IG1ldGhvZCByZWplY3RzLlxuICAgKi9cbiAgZXJyb3I/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogQG5hbWUgSFRUUFxuICogQGRlc2NyaXB0aW9uXG4gKiBDb3Jkb3ZhIC8gUGhvbmVnYXAgcGx1Z2luIGZvciBjb21tdW5pY2F0aW5nIHdpdGggSFRUUCBzZXJ2ZXJzLiBTdXBwb3J0cyBpT1MgYW5kIEFuZHJvaWQuXG4gKlxuICogQWR2YW50YWdlcyBvdmVyIEphdmFzY3JpcHQgcmVxdWVzdHM6XG4gKiAtIEJhY2tncm91bmQgdGhyZWFkaW5nIC0gYWxsIHJlcXVlc3RzIGFyZSBkb25lIGluIGEgYmFja2dyb3VuZCB0aHJlYWRcbiAqIC0gU1NMIFBpbm5pbmdcbiAqXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IEhUVFAgfSBmcm9tICdAaW9uaWMtbmF0aXZlL2h0dHAvbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEhUVFApIHt9XG4gKlxuICogLi4uXG4gKlxuICogdGhpcy5odHRwLmdldCgnaHR0cDovL2lvbmljLmlvJywge30sIHt9KVxuICogICAudGhlbihkYXRhID0+IHtcbiAqXG4gKiAgICAgY29uc29sZS5sb2coZGF0YS5zdGF0dXMpO1xuICogICAgIGNvbnNvbGUubG9nKGRhdGEuZGF0YSk7IC8vIGRhdGEgcmVjZWl2ZWQgYnkgc2VydmVyXG4gKiAgICAgY29uc29sZS5sb2coZGF0YS5oZWFkZXJzKTtcbiAqXG4gKiAgIH0pXG4gKiAgIC5jYXRjaChlcnJvciA9PiB7XG4gKlxuICogICAgIGNvbnNvbGUubG9nKGVycm9yLnN0YXR1cyk7XG4gKiAgICAgY29uc29sZS5sb2coZXJyb3IuZXJyb3IpOyAvLyBlcnJvciBtZXNzYWdlIGFzIHN0cmluZ1xuICogICAgIGNvbnNvbGUubG9nKGVycm9yLmhlYWRlcnMpO1xuICpcbiAqICAgfSk7XG4gKlxuICogYGBgXG4gKiBAaW50ZXJmYWNlc1xuICogSFRUUFJlc3BvbnNlXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnSFRUUCcsXG4gIHBsdWdpbjogJ2NvcmRvdmEtcGx1Z2luLWFkdmFuY2VkLWh0dHAnLFxuICBwbHVnaW5SZWY6ICdjb3Jkb3ZhLnBsdWdpbi5odHRwJyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9zaWxraW1lbi9jb3Jkb3ZhLXBsdWdpbi1hZHZhbmNlZC1odHRwJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnaU9TJ11cbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSFRUUCBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqIFRoaXMgcmV0dXJucyBhbiBvYmplY3QgcmVwcmVzZW50aW5nIGEgYmFzaWMgSFRUUCBBdXRob3JpemF0aW9uIGhlYWRlciBvZiB0aGUgZm9ybS5cbiAgICogQHBhcmFtIHVzZXJuYW1lIHtzdHJpbmd9IFVzZXJuYW1lXG4gICAqIEBwYXJhbSBwYXNzd29yZCB7c3RyaW5nfSBQYXNzd29yZFxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBhbiBvYmplY3QgcmVwcmVzZW50aW5nIGEgYmFzaWMgSFRUUCBBdXRob3JpemF0aW9uIGhlYWRlciBvZiB0aGUgZm9ybSB7J0F1dGhvcml6YXRpb24nOiAnQmFzaWMgYmFzZTY0RW5jb2RlZFVzZXJuYW1lQW5kUGFzc3dvcmQnfVxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIGdldEJhc2ljQXV0aEhlYWRlcih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogeyBBdXRob3JpemF0aW9uOiBzdHJpbmcgfSB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgc2V0cyB1cCBhbGwgZnV0dXJlIHJlcXVlc3RzIHRvIHVzZSBCYXNpYyBIVFRQIGF1dGhlbnRpY2F0aW9uIHdpdGggdGhlIGdpdmVuIHVzZXJuYW1lIGFuZCBwYXNzd29yZC5cbiAgICogQHBhcmFtIHVzZXJuYW1lIHtzdHJpbmd9IFVzZXJuYW1lXG4gICAqIEBwYXJhbSBwYXNzd29yZCB7c3RyaW5nfSBQYXNzd29yZFxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIHVzZUJhc2ljQXV0aCh1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBHZXQgYWxsIGhlYWRlcnMgZGVmaW5lZCBmb3IgYSBnaXZlbiBob3N0bmFtZS5cbiAgICogQHBhcmFtIGhvc3Qge3N0cmluZ30gVGhlIGhvc3RuYW1lXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9IHJldHVybiBhbGwgaGVhZGVycyBkZWZpbmVkIGZvciB0aGUgaG9zdG5hbWVcbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBnZXRIZWFkZXJzKGhvc3Q6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNldCBhIGhlYWRlciBmb3IgYWxsIGZ1dHVyZSByZXF1ZXN0cy4gVGFrZXMgYSBob3N0bmFtZSwgYSBoZWFkZXIgYW5kIGEgdmFsdWUuXG4gICAqIEBwYXJhbSBob3N0IHtzdHJpbmd9IFRoZSBob3N0bmFtZSB0byBiZSB1c2VkIGZvciBzY29waW5nIHRoaXMgaGVhZGVyXG4gICAqIEBwYXJhbSBoZWFkZXIge3N0cmluZ30gVGhlIG5hbWUgb2YgdGhlIGhlYWRlclxuICAgKiBAcGFyYW0gdmFsdWUge3N0cmluZ30gVGhlIHZhbHVlIG9mIHRoZSBoZWFkZXJcbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBzZXRIZWFkZXIoaG9zdDogc3RyaW5nLCBoZWFkZXI6IHN0cmluZywgdmFsdWU6IHN0cmluZyk6IHZvaWQge31cblxuICAvKipcbiAgICogR2V0IHRoZSBuYW1lIG9mIHRoZSBkYXRhIHNlcmlhbGl6ZXIgd2hpY2ggd2lsbCBiZSB1c2VkIGZvciBhbGwgZnV0dXJlIFBPU1QgYW5kIFBVVCByZXF1ZXN0cy5cbiAgICogQHJldHVybnMge3N0cmluZ30gcmV0dXJucyB0aGUgbmFtZSBvZiB0aGUgY29uZmlndXJlZCBkYXRhIHNlcmlhbGl6ZXJcbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBnZXREYXRhU2VyaWFsaXplcigpOiBzdHJpbmcge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgdGhlIGRhdGEgc2VyaWFsaXplciB3aGljaCB3aWxsIGJlIHVzZWQgZm9yIGFsbCBmdXR1cmUgUE9TVCBhbmQgUFVUIHJlcXVlc3RzLiBUYWtlcyBhIHN0cmluZyByZXByZXNlbnRpbmcgdGhlIG5hbWUgb2YgdGhlIHNlcmlhbGl6ZXIuXG4gICAqIEBwYXJhbSBzZXJpYWxpemVyIHtzdHJpbmd9IFRoZSBuYW1lIG9mIHRoZSBzZXJpYWxpemVyLiBDYW4gYmUgdXJsZW5jb2RlZCwgdXRmOCBvciBqc29uXG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgc2V0RGF0YVNlcmlhbGl6ZXIoc2VyaWFsaXplcjogc3RyaW5nKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBBZGQgYSBjdXN0b20gY29va2llLlxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9IFNjb3BlIG9mIHRoZSBjb29raWVcbiAgICogQHBhcmFtIGNvb2tpZSB7c3RyaW5nfSBSRkMgY29tcGxpYW50IGNvb2tpZSBzdHJpbmdcbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBzZXRDb29raWUodXJsOiBzdHJpbmcsIGNvb2tpZTogc3RyaW5nKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBDbGVhciBhbGwgY29va2llcy5cbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBjbGVhckNvb2tpZXMoKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBSZW1vdmUgY29va2llcyBmb3IgZ2l2ZW4gVVJMLlxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBjYlxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIHJlbW92ZUNvb2tpZXModXJsOiBzdHJpbmcsIGNiOiAoKSA9PiB2b2lkKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBSZXNvbHZlIGNvb2tpZSBzdHJpbmcgZm9yIGdpdmVuIFVSTC5cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfVxuICAgKi9cbiAgQENvcmRvdmEoeyBzeW5jOiB0cnVlIH0pXG4gIGdldENvb2tpZVN0cmluZyh1cmw6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldCBnbG9iYWwgcmVxdWVzdCB0aW1lb3V0IHZhbHVlIGluIHNlY29uZHMuXG4gICAqIEByZXR1cm5zIHtudW1iZXJ9IHJldHVybnMgdGhlIGdsb2JhbCByZXF1ZXN0IHRpbWVvdXQgdmFsdWVcbiAgICovXG4gIEBDb3Jkb3ZhKHsgc3luYzogdHJ1ZSB9KVxuICBnZXRSZXF1ZXN0VGltZW91dCgpOiBudW1iZXIge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXQgZ2xvYmFsIHJlcXVlc3QgdGltZW91dCB2YWx1ZSBpbiBzZWNvbmRzLlxuICAgKiBAcGFyYW0gdGltZW91dCB7bnVtYmVyfSBUaGUgdGltZW91dCBpbiBzZWNvbmRzLiBEZWZhdWx0IDYwXG4gICAqL1xuICBAQ29yZG92YSh7IHN5bmM6IHRydWUgfSlcbiAgc2V0UmVxdWVzdFRpbWVvdXQodGltZW91dDogbnVtYmVyKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBTZXQgU1NMIENlcnQgaGFuZGxpbmcgbW9kZSwgYmVpbmcgb25lIG9mIHRoZSBmb2xsb3dpbmcgdmFsdWVzXG4gICAqIGRlZmF1bHQ6IGRlZmF1bHQgU1NMIGNlcnQgaGFuZGxpbmcgdXNpbmcgc3lzdGVtJ3MgQ0EgY2VydHNcbiAgICogbGVnYWN5OiB1c2UgbGVnYWN5IGRlZmF1bHQgYmVoYXZpb3IgKDwgMi4wLjMpLCBleGNsdWRpbmcgdXNlciBpbnN0YWxsZWQgQ0EgY2VydHMgKG9ubHkgZm9yIEFuZHJvaWQpXG4gICAqIG5vY2hlY2s6IGRpc2FibGUgU1NMIGNlcnQgY2hlY2tpbmcsIHRydXN0aW5nIGFsbCBjZXJ0cyAobWVhbnQgdG8gYmUgdXNlZCBvbmx5IGZvciB0ZXN0aW5nIHB1cnBvc2VzKVxuICAgKiBwaW5uZWQ6IHRydXN0IG9ubHkgcHJvdmlkZWQgY2VydHNcbiAgICogQHNlZSBodHRwczovL2dpdGh1Yi5jb20vc2lsa2ltZW4vY29yZG92YS1wbHVnaW4tYWR2YW5jZWQtaHR0cCNzZXRzc2xjZXJ0bW9kZVxuICAgKiBAcGFyYW0geydkZWZhdWx0JyB8ICdsZWdhY3knIHwgJ25vY2hlY2snIHwgJ3Bpbm5lZCd9IG1vZGUgU1NMIENlcnQgaGFuZGxpbmcgbW9kZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBzZXRTU0xDZXJ0TW9kZShtb2RlOiAnZGVmYXVsdCcgfCAnbGVnYWN5JyB8ICdub2NoZWNrJyB8ICdwaW5uZWQnKTogUHJvbWlzZTx2b2lkPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc2FibGUgZm9sbG93aW5nIHJlZGlyZWN0cyBhdXRvbWF0aWNhbGx5LlxuICAgKiBAcGFyYW0gZGlzYWJsZSB7Ym9vbGVhbn0gU2V0IHRvIHRydWUgdG8gZGlzYWJsZSBmb2xsb3dpbmcgcmVkaXJlY3RzIGF1dG9tYXRpY2FsbHlcbiAgICogQHJldHVybnMge1Byb21pc2U8dm9pZD59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgd2lsbCByZXNvbHZlIG9uIHN1Y2Nlc3MsIGFuZCByZWplY3Qgb24gZmFpbHVyZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBkaXNhYmxlUmVkaXJlY3QoZGlzYWJsZTogYm9vbGVhbik6IFByb21pc2U8dm9pZD4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYWtlIGEgUE9TVCByZXF1ZXN0XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gVGhlIHVybCB0byBzZW5kIHRoZSByZXF1ZXN0IHRvXG4gICAqIEBwYXJhbSBib2R5IHtPYmplY3R9IFRoZSBib2R5IG9mIHRoZSByZXF1ZXN0XG4gICAqIEBwYXJhbSBoZWFkZXJzIHtPYmplY3R9IFRoZSBoZWFkZXJzIHRvIHNldCBmb3IgdGhpcyByZXF1ZXN0XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEhUVFBSZXNwb25zZT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZSBvbiBzdWNjZXNzLCBhbmQgcmVqZWN0IG9uIGZhaWx1cmVcbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgcG9zdCh1cmw6IHN0cmluZywgYm9keTogYW55LCBoZWFkZXJzOiBhbnkpOiBQcm9taXNlPEhUVFBSZXNwb25zZT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBNYWtlIGEgR0VUIHJlcXVlc3RcbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfSBUaGUgdXJsIHRvIHNlbmQgdGhlIHJlcXVlc3QgdG9cbiAgICogQHBhcmFtIHBhcmFtZXRlcnMge09iamVjdH0gUGFyYW1ldGVycyB0byBzZW5kIHdpdGggdGhlIHJlcXVlc3RcbiAgICogQHBhcmFtIGhlYWRlcnMge09iamVjdH0gVGhlIGhlYWRlcnMgdG8gc2V0IGZvciB0aGlzIHJlcXVlc3RcbiAgICogQHJldHVybnMge1Byb21pc2U8SFRUUFJlc3BvbnNlPn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlIG9uIHN1Y2Nlc3MsIGFuZCByZWplY3Qgb24gZmFpbHVyZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBnZXQodXJsOiBzdHJpbmcsIHBhcmFtZXRlcnM6IGFueSwgaGVhZGVyczogYW55KTogUHJvbWlzZTxIVFRQUmVzcG9uc2U+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogTWFrZSBhIFBVVCByZXF1ZXN0XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gVGhlIHVybCB0byBzZW5kIHRoZSByZXF1ZXN0IHRvXG4gICAqIEBwYXJhbSBib2R5IHtPYmplY3R9IFRoZSBib2R5IG9mIHRoZSByZXF1ZXN0XG4gICAqIEBwYXJhbSBoZWFkZXJzIHtPYmplY3R9IFRoZSBoZWFkZXJzIHRvIHNldCBmb3IgdGhpcyByZXF1ZXN0XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEhUVFBSZXNwb25zZT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZSBvbiBzdWNjZXNzLCBhbmQgcmVqZWN0IG9uIGZhaWx1cmVcbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgcHV0KHVybDogc3RyaW5nLCBib2R5OiBhbnksIGhlYWRlcnM6IGFueSk6IFByb21pc2U8SFRUUFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIE1ha2UgYSBQQVRDSCByZXF1ZXN0XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gVGhlIHVybCB0byBzZW5kIHRoZSByZXF1ZXN0IHRvXG4gICAqIEBwYXJhbSBib2R5IHtPYmplY3R9IFRoZSBib2R5IG9mIHRoZSByZXF1ZXN0XG4gICAqIEBwYXJhbSBoZWFkZXJzIHtPYmplY3R9IFRoZSBoZWFkZXJzIHRvIHNldCBmb3IgdGhpcyByZXF1ZXN0XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEhUVFBSZXNwb25zZT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZSBvbiBzdWNjZXNzLCBhbmQgcmVqZWN0IG9uIGZhaWx1cmVcbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgcGF0Y2godXJsOiBzdHJpbmcsIGJvZHk6IGFueSwgaGVhZGVyczogYW55KTogUHJvbWlzZTxIVFRQUmVzcG9uc2U+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogTWFrZSBhIERFTEVURSByZXF1ZXN0XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gVGhlIHVybCB0byBzZW5kIHRoZSByZXF1ZXN0IHRvXG4gICAqIEBwYXJhbSBwYXJhbWV0ZXJzIHtPYmplY3R9IFBhcmFtZXRlcnMgdG8gc2VuZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAqIEBwYXJhbSBoZWFkZXJzIHtPYmplY3R9IFRoZSBoZWFkZXJzIHRvIHNldCBmb3IgdGhpcyByZXF1ZXN0XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPEhUVFBSZXNwb25zZT59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZSBvbiBzdWNjZXNzLCBhbmQgcmVqZWN0IG9uIGZhaWx1cmVcbiAgICovXG4gIEBDb3Jkb3ZhKClcbiAgZGVsZXRlKHVybDogc3RyaW5nLCBwYXJhbWV0ZXJzOiBhbnksIGhlYWRlcnM6IGFueSk6IFByb21pc2U8SFRUUFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIE1ha2UgYSBIRUFEIHJlcXVlc3RcbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfSBUaGUgdXJsIHRvIHNlbmQgdGhlIHJlcXVlc3QgdG9cbiAgICogQHBhcmFtIHBhcmFtZXRlcnMge09iamVjdH0gUGFyYW1ldGVycyB0byBzZW5kIHdpdGggdGhlIHJlcXVlc3RcbiAgICogQHBhcmFtIGhlYWRlcnMge09iamVjdH0gVGhlIGhlYWRlcnMgdG8gc2V0IGZvciB0aGlzIHJlcXVlc3RcbiAgICogQHJldHVybnMge1Byb21pc2U8SFRUUFJlc3BvbnNlPn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlIG9uIHN1Y2Nlc3MsIGFuZCByZWplY3Qgb24gZmFpbHVyZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBoZWFkKHVybDogc3RyaW5nLCBwYXJhbWV0ZXJzOiBhbnksIGhlYWRlcnM6IGFueSk6IFByb21pc2U8SFRUUFJlc3BvbnNlPiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gVGhlIHVybCB0byBzZW5kIHRoZSByZXF1ZXN0IHRvXG4gICAqIEBwYXJhbSBib2R5IHtPYmplY3R9IFRoZSBib2R5IG9mIHRoZSByZXF1ZXN0XG4gICAqIEBwYXJhbSBoZWFkZXJzIHtPYmplY3R9IFRoZSBoZWFkZXJzIHRvIHNldCBmb3IgdGhpcyByZXF1ZXN0XG4gICAqIEBwYXJhbSBmaWxlUGF0aCB7c3RyaW5nfSBUaGUgbG9jYWwgcGF0aCBvZiB0aGUgZmlsZSB0byB1cGxvYWRcbiAgICogQHBhcmFtIG5hbWUge3N0cmluZ30gVGhlIG5hbWUgb2YgdGhlIHBhcmFtZXRlciB0byBwYXNzIHRoZSBmaWxlIGFsb25nIGFzXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59IHJldHVybnMgYSBGaWxlRW50cnkgcHJvbWlzZSB0aGF0IHJlc29sdmUgb24gc3VjY2VzcywgYW5kIHJlamVjdCBvbiBmYWlsdXJlXG4gICAqL1xuICBAQ29yZG92YSgpXG4gIHVwbG9hZEZpbGUodXJsOiBzdHJpbmcsIGJvZHk6IGFueSwgaGVhZGVyczogYW55LCBmaWxlUGF0aDogc3RyaW5nLCBuYW1lOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKlxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9IFRoZSB1cmwgdG8gc2VuZCB0aGUgcmVxdWVzdCB0b1xuICAgKiBAcGFyYW0gYm9keSB7T2JqZWN0fSBUaGUgYm9keSBvZiB0aGUgcmVxdWVzdFxuICAgKiBAcGFyYW0gaGVhZGVycyB7T2JqZWN0fSBUaGUgaGVhZGVycyB0byBzZXQgZm9yIHRoaXMgcmVxdWVzdFxuICAgKiBAcGFyYW0gZmlsZVBhdGgge3N0cmluZ30gVGhlIHBhdGggdG8gZG93bmxvYWQgdGhlIGZpbGUgdG8sIGluY2x1ZGluZyB0aGUgZmlsZSBuYW1lLlxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fSByZXR1cm5zIGEgRmlsZUVudHJ5IHByb21pc2UgdGhhdCByZXNvbHZlIG9uIHN1Y2Nlc3MsIGFuZCByZWplY3Qgb24gZmFpbHVyZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBkb3dubG9hZEZpbGUodXJsOiBzdHJpbmcsIGJvZHk6IGFueSwgaGVhZGVyczogYW55LCBmaWxlUGF0aDogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICpcbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfSBUaGUgdXJsIHRvIHNlbmQgdGhlIHJlcXVlc3QgdG9cbiAgICogQHBhcmFtIG9wdGlvbnMge09iamVjdH0gb3B0aW9ucyBmb3IgaW5kaXZpZHVhbCByZXF1ZXN0XG4gICAqIEBwYXJhbSBvcHRpb25zLm1ldGhvZCB7c3RyaW5nfSByZXF1ZXN0IG1ldGhvZFxuICAgKiBAcGFyYW0gb3B0aW9ucy5kYXRhIHtPYmplY3R9IHBheWxvYWQgdG8gYmUgc2VuZCB0byB0aGUgc2VydmVyIChvbmx5IGFwcGxpY2FibGUgb24gcG9zdCwgcHV0IG9yIHBhdGNoIG1ldGhvZHMpXG4gICAqIEBwYXJhbSBvcHRpb25zLnBhcmFtcyB7T2JqZWN0fSBxdWVyeSBwYXJhbXMgdG8gYmUgYXBwZW5kZWQgdG8gdGhlIFVSTCAob25seSBhcHBsaWNhYmxlIG9uIGdldCwgaGVhZCwgZGVsZXRlLCB1cGxvYWQgb3IgZG93bmxvYWQgbWV0aG9kcylcbiAgICogQHBhcmFtIG9wdGlvbnMuc2VyaWFsaXplciB7c3RyaW5nfSBkYXRhIHNlcmlhbGl6ZXIgdG8gYmUgdXNlZCAob25seSBhcHBsaWNhYmxlIG9uIHBvc3QsIHB1dCBvciBwYXRjaCBtZXRob2RzKSwgZGVmYXVsdHMgdG8gZ2xvYmFsIHNlcmlhbGl6ZXIgdmFsdWUsIHNlZSBzZXREYXRhU2VyaWFsaXplciBmb3Igc3VwcG9ydGVkIHZhbHVlc1xuICAgKiBAcGFyYW0gb3B0aW9ucy50aW1lb3V0IHtudW1iZXJ9IHRpbWVvdXQgdmFsdWUgZm9yIHRoZSByZXF1ZXN0IGluIHNlY29uZHMsIGRlZmF1bHRzIHRvIGdsb2JhbCB0aW1lb3V0IHZhbHVlXG4gICAqIEBwYXJhbSBvcHRpb25zLmhlYWRlcnMge09iamVjdH0gaGVhZGVycyBvYmplY3QgKGtleSB2YWx1ZSBwYWlyKSwgd2lsbCBiZSBtZXJnZWQgd2l0aCBnbG9iYWwgdmFsdWVzXG4gICAqIEBwYXJhbSBvcHRpb25zLmZpbGVQYXRoIHtzdHJpbmd9IGZpbGVQYXRoIHRvIGJlIHVzZWQgZHVyaW5nIHVwbG9hZCBhbmQgZG93bmxvYWQgc2VlIHVwbG9hZEZpbGUgYW5kIGRvd25sb2FkRmlsZSBmb3IgZGV0YWlsZWQgaW5mb3JtYXRpb25cbiAgICogQHBhcmFtIG9wdGlvbnMubmFtZSB7c3RyaW5nfSBuYW1lIHRvIGJlIHVzZWQgZHVyaW5nIHVwbG9hZCBzZWUgdXBsb2FkRmlsZSBmb3IgZGV0YWlsZWQgaW5mb3JtYXRpb25cbiAgICpcbiAgICogQHJldHVybnMge1Byb21pc2U8SFRUUFJlc3BvbnNlPn0gcmV0dXJucyBhIHByb21pc2UgdGhhdCByZXNvbHZlIG9uIHN1Y2Nlc3MsIGFuZCByZWplY3Qgb24gZmFpbHVyZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBzZW5kUmVxdWVzdChcbiAgICB1cmw6IHN0cmluZyxcbiAgICBvcHRpb25zOiB7XG4gICAgICBtZXRob2Q6ICdnZXQnIHwgJ3Bvc3QnIHwgJ3B1dCcgfCAncGF0Y2gnIHwgJ2hlYWQnIHwgJ2RlbGV0ZScgfCAndXBsb2FkJyB8ICdkb3dubG9hZCc7XG4gICAgICBkYXRhPzogeyBbaW5kZXg6IHN0cmluZ106IGFueSB9O1xuICAgICAgcGFyYW1zPzogeyBbaW5kZXg6IHN0cmluZ106IHN0cmluZyB8IG51bWJlciB9O1xuICAgICAgc2VyaWFsaXplcj86ICdqc29uJyB8ICd1cmxlbmNvZGVkJyB8ICd1dGY4JztcbiAgICAgIHRpbWVvdXQ/OiBudW1iZXI7XG4gICAgICBoZWFkZXJzPzogeyBbaW5kZXg6IHN0cmluZ106IHN0cmluZyB9O1xuICAgICAgZmlsZVBhdGg/OiBzdHJpbmc7XG4gICAgICBuYW1lPzogc3RyaW5nO1xuICAgIH1cbiAgKTogUHJvbWlzZTxIVFRQUmVzcG9uc2U+IHtcbiAgICByZXR1cm47XG4gIH1cbn1cbiJdfQ==

/***/ }),

/***/ "./src/app/validators/password.validator.ts":
/*!**************************************************!*\
  !*** ./src/app/validators/password.validator.ts ***!
  \**************************************************/
/*! exports provided: PasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PasswordValidator", function() { return PasswordValidator; });
var PasswordValidator = /** @class */ (function () {
    function PasswordValidator() {
    }
    // If our validation fails, we return an object with a key for the error name and a value of true.
    // Otherwise, if the validation passes, we simply return null because there is no error.
    PasswordValidator.areNotEqual = function (formGroup) {
        var val;
        var valid = true;
        for (var key in formGroup.controls) {
            if (formGroup.controls.hasOwnProperty(key)) {
                var control = formGroup.controls[key];
                if (val === undefined) {
                    val = control.value;
                }
                else {
                    if (val !== control.value) {
                        valid = false;
                        break;
                    }
                }
            }
        }
        if (valid) {
            return null;
        }
        return {
            areNotEqual: true
        };
    };
    return PasswordValidator;
}());



/***/ }),

/***/ "./src/services/user.service.ts":
/*!**************************************!*\
  !*** ./src/services/user.service.ts ***!
  \**************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app/app.angularfireconfig */ "./src/app/app.angularfireconfig.ts");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _app_const__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app/const */ "./src/app/const.ts");
/* harmony import */ var _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../app/tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UserService = /** @class */ (function () {
    function UserService(sqlite, afireAuth, 
    //public storage: Storage,
    platform, events, http, tpStorageService) {
        var _this = this;
        this.sqlite = sqlite;
        this.afireAuth = afireAuth;
        this.platform = platform;
        this.events = events;
        this.http = http;
        this.tpStorageService = tpStorageService;
        this.firedata = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/chatusers');
        this.firefriend = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/friends');
        this.firebuddychat = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/buddychats');
        this.users = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/users');
        this.userstatus = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/userstatus');
        this.firereq = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/requests');
        this.firenotify = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/notification');
        this.fireBusiness = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/business');
        //fireInvitationSent = firebase.database().ref('/invitationsent');
        this.firePhones = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/phones');
        this.fireWebsiteUsers = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/website_users');
        this.fireReferral_sent = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/referral_sent');
        this.fireReferral_received = firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/referral_received');
        this.isUserExits = true;
        this.blockUsers = [];
        this.unblockUsers = [];
        this.isuserBlock = false;
        this.blockUsersCounter = 0;
        this.platform.ready().then(function (readySource) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                //////console.log ("this.userIdthis.userIdthis.userIdthis.userId:" + this.userId);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
            });
            _this.tpStorageService.getItem('userName').then(function (res) {
                if (res) {
                    _this.userName = res;
                }
                else {
                    _this.userName = _this.afireAuth.auth.currentUser.displayName;
                }
            }).catch(function (e) {
                if (_this.afireAuth && _this.afireAuth.auth && _this.afireAuth.auth.currentUser) {
                    _this.userName = _this.afireAuth.auth.currentUser.displayName;
                }
            });
            _this.tpStorageService.getItem('userPic').then(function (res) {
                if (res) {
                    _this.userPic = res;
                }
                else {
                    _this.userPic = _this.afireAuth.auth.currentUser.photoURL;
                }
            }).catch(function (e) {
                if (_this.afireAuth && _this.afireAuth.auth && _this.afireAuth.auth.currentUser) {
                    _this.userPic = _this.afireAuth.auth.currentUser.photoURL;
                }
            });
        });
    }
    UserService.prototype.alphaNumericEmail = function (email_) {
        email_ = email_.toLowerCase();
        return email_.replace(/[^0-9a-z]+/gi, "");
    };
    UserService.prototype.getWebsiteUserDetails = function (email_) {
        var _this = this;
        var cleanEmail = this.alphaNumericEmail(email_);
        console.log("cleanEmail:");
        console.log(cleanEmail);
        var promise = new Promise(function (resolve, reject) {
            _this.fireWebsiteUsers.child(cleanEmail).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserService.prototype.updateWebsiteUserDetails = function (email_, userid_, paid_) {
        var myuserPic = _app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"][Math.floor(Math.random() * Object.keys(_app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"]).length - 1)];
        var cleanEmail = this.alphaNumericEmail(email_);
        console.log("email_" + email_);
        console.log("DO THIS NOW");
        console.log(cleanEmail);
        this.fireWebsiteUsers.child(cleanEmail).update({
            paid: paid_,
            userId: userid_
        }).then(function () {
        }).catch(function (err) {
        });
        this.tpStorageService.setItem('myuserPic', myuserPic);
        this.firedata.child(userid_).update({
            photoURL: myuserPic,
            timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"].ServerValue.TIMESTAMP,
            uid: userid_,
            useremail: email_
        }).then(function () {
            console.log("::Done ");
        }).catch(function (err) {
        });
    }; //end function
    UserService.prototype.getuserdetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                _this.firedata.child(_this.userId).once('value', function (snapshot) {
                    var res = snapshot.val();
                    _this.tpStorageService.setItem('mymobile', res['mobile']);
                    _this.tpStorageService.setItem('displayName', res['displayName']);
                    _this.tpStorageService.setItem('photoURL', res['photoURL']);
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) {
            });
        });
        return promise;
    };
    //this is good
    UserService.prototype.updatedisplayname = function (newname) {
        var _this = this;
        //////console.log ("trying to updaet display name");
        this.tpStorageService.setItem('myDisplayName', newname);
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (res) {
                _this.userId = res;
                _this.firedata.child(_this.userId).update({
                    displayName: newname
                }).then(function () {
                    //this.updatedisplayname_DB(newname);//do it in parellel
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) {
                if (_this.userId) {
                }
                else {
                    if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                        _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                    }
                } //endif
                _this.tpStorageService.setItem('userUID', _this.userId);
                _this.firedata.child(_this.userId).update({
                    displayName: newname
                }).then(function () {
                    //this.updatedisplayname_DB(newname);//do it in parellel
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    UserService.prototype.initializeItem = function (userID) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(userID).child('isNotify').once('value', function (snapshot) {
                _this.isNotify = snapshot.val();
                resolve(_this.isNotify);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    UserService.prototype.adduser = function (newuser) {
        var _this = this;
        if (this.userId) {
            return this.adduser_(newuser);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.adduser_(newuser);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.adduser_(newuser);
            });
        }
    };
    //mytodo : is this being used anywhere ?
    UserService.prototype.adduser_ = function (newuser) {
        var _this = this;
        var myuserPic = _app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"][Math.floor(Math.random() * Object.keys(_app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"]).length - 1)];
        var promise = new Promise(function (resolve, reject) {
            _this.afireAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(function () {
                _this.afireAuth.auth.currentUser.updateProfile({
                    displayName: newuser.username,
                    photoURL: myuserPic
                }).then(function () {
                    _this.firedata.child(_this.userId).set({
                        uid: _this.userId,
                        displayName: newuser.username,
                        photoURL: myuserPic,
                        invited_by: "none"
                    }).then(function () {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    //This doesnt look like being used
    UserService.prototype.addmobileUser = function (verificationCredential, code, mobile) {
        var _this = this;
        var myuserPic = _app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"][Math.floor(Math.random() * Object.keys(_app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"]).length - 1)];
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.setItem('verificationCredential', verificationCredential);
            _this.tpStorageService.setItem('code', code);
            var signInData = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"].PhoneAuthProvider.credential(verificationCredential, code);
            //////console.log ("signInData");
            //////console.log (signInData);
            _this.afireAuth.auth.signInWithCredential(signInData).then(function (info) {
                var profileImage;
                if (_this.afireAuth.auth.currentUser.photoURL != null) {
                    profileImage = _this.afireAuth.auth.currentUser.photoURL;
                }
                else {
                    profileImage = myuserPic;
                }
                _this.afireAuth.auth.currentUser.updateProfile({ displayName: _this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(function () {
                    _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                        uid: _this.afireAuth.auth.currentUser.uid,
                        mobile: mobile,
                        countryCode: verificationCredential,
                        displayName: _this.afireAuth.auth.currentUser.displayName,
                        disc: '',
                        photoURL: profileImage,
                        deviceToken: ''
                    }).then(function () {
                        resolve({ success: true });
                    }).catch(function (err) {
                        reject(err);
                    });
                }).catch(function (err) {
                });
            }).catch(function (err) {
                resolve({ success: false, msg: JSON.stringify(err.message) });
            });
        });
    };
    UserService.prototype.addmobileUserIOS = function (countryCode, mobile) {
        var _this = this;
        var myuserPic = _app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"][Math.floor(Math.random() * Object.keys(_app_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_4__["userPicArr"]).length - 1)];
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.setItem('verificationCredential', countryCode);
            _this.tpStorageService.setItem('code', mobile);
            _this.tpStorageService.setItem('fullPhoneNumber', countryCode + mobile);
            //this.firedata.once('value', (snep) => {
            /*let allData = snep.val();
            for (let tmpkey in allData) {
              if (allData[tmpkey].mobile == mobile) {
                this.isUserExits = false;
                this.tpStorageService.setItem('userUID', allData[tmpkey].uid);
              }
            }*/
            if (_this.isUserExits) {
                _this.afireAuth.auth.signInAnonymously().then(function (info) {
                    var profileImage;
                    if (_this.afireAuth.auth.currentUser.photoURL != null) {
                        profileImage = _this.afireAuth.auth.currentUser.photoURL;
                    }
                    else {
                        profileImage = myuserPic;
                    }
                    _this.tpStorageService.setItem('userUID', _this.afireAuth.auth.currentUser.uid);
                    _this.afireAuth.auth.currentUser.updateProfile({
                        displayName: _this.afireAuth.auth.currentUser.displayName,
                        photoURL: profileImage
                    }).then(function () {
                        //mytodo_critical  : test this
                        _this.http.get('https://tapally.com/wp-json/log/v1/createuser?phone=' + mobile + '&countrycode=' + countryCode + '&uid=' + _this.afireAuth.auth.currentUser.uid, {}, {})
                            .then(function (data) {
                        })
                            .catch(function (error) {
                        });
                        _this.firedata.child(_this.afireAuth.auth.currentUser.uid).set({
                            uid: _this.afireAuth.auth.currentUser.uid,
                            mobile: mobile,
                            countryCode: countryCode,
                            displayName: _this.afireAuth.auth.currentUser.displayName,
                            disc: '',
                            photoURL: profileImage,
                            deviceToken: '',
                            timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"].ServerValue.TIMESTAMP
                        }).then(function () {
                            //Check if user is invited by someone else
                            _this.firePhones.child(mobile).once('value', function (snapshot) {
                                var phoneNumberOb = snapshot.val();
                                if (phoneNumberOb) {
                                    if (phoneNumberOb.uid) {
                                        //it already have uid so this means that this user is trying to register again perhaps he/she uninstalled the app
                                        //Lets remove some pieces of his old profile
                                        _this.removeOldProfile(phoneNumberOb.uid, _this.afireAuth.auth.currentUser.uid);
                                        //update business profile with new value
                                        _this.fireBusiness.child(phoneNumberOb.uid).once('value', function (snapshot_business) {
                                            _this.fireBusiness.child(_this.afireAuth.auth.currentUser.uid).set(snapshot_business.val());
                                            _this.fireBusiness.child(phoneNumberOb.uid).remove();
                                        });
                                    }
                                    else {
                                        if (phoneNumberOb.invited_by) {
                                            //I was actually invited by someone so update both cache and database
                                            _this.tpStorageService.setItem('invited_by', phoneNumberOb.invited_by);
                                            _this.firedata.child(_this.afireAuth.auth.currentUser.uid).update({
                                                invited_by: phoneNumberOb.invited_by
                                            });
                                            //if i am invieted. lets make friends
                                            _this.acceptrequest(_this.afireAuth.auth.currentUser.uid, phoneNumberOb.invited_by, _this.afireAuth.auth.currentUser.displayName, mobile, countryCode, profileImage, firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"].ServerValue.TIMESTAMP);
                                        } //endif
                                    }
                                    //Object is not properly set so set it
                                    _this.firePhones.child(mobile).update({
                                        uid: _this.afireAuth.auth.currentUser.uid,
                                    });
                                }
                                else {
                                    //Object does not exist
                                    _this.firePhones.child(mobile).update({
                                        uid: _this.afireAuth.auth.currentUser.uid,
                                    });
                                }
                            }).catch(function (err) {
                                //Object does not exist
                                _this.firePhones.child(mobile).update({
                                    uid: _this.afireAuth.auth.currentUser.uid,
                                });
                            });
                            resolve({ success: true });
                        }).catch(function (err) {
                            reject(err);
                        });
                    }).catch(function (err) {
                    });
                }).catch(function (err) {
                    resolve({ success: false, msg: JSON.stringify(err.message) });
                });
            }
            else {
                resolve({ success: true });
            }
            //});
        });
    };
    //duplicate function in requests.ts  but not entirly same
    UserService.prototype.acceptrequest = function (me_uid, buddy_uid, displayName, mobile, countryCode, profileImage, timestamp_) {
        var _this = this;
        this.userId = me_uid;
        this.getbuddydetails(buddy_uid).then(function (buddy) {
            //mytodo : update  deviceToken
            //this.getbuddydetails(me_uid).then((aboutMe: any) => {
            _this.firefriend.child(_this.userId).child(buddy.uid).set({
                uid: buddy.uid,
                displayName: buddy.displayName,
                photoURL: buddy.photoURL,
                isActive: 1,
                isBlock: false,
                deviceToken: buddy.deviceToken,
                countryCode: buddy.countryCode,
                disc: buddy.disc,
                mobile: buddy.mobile,
                timestamp: buddy.timestamp
            }).then(function () {
                _this.firefriend.child(buddy.uid).child(_this.userId).set({
                    uid: _this.userId,
                    displayName: displayName,
                    photoURL: profileImage,
                    isActive: 1,
                    isBlock: false,
                    deviceToken: "",
                    countryCode: countryCode,
                    disc: '',
                    mobile: mobile,
                    timestamp: timestamp_
                }).then(function () {
                });
            });
            //});
        });
    }; //end function
    //it remove and re-attach
    UserService.prototype.removeOldProfile = function (oldUid, newuid) {
        /*
         //This function works great but is it still relevent ? We need similar function because we are now using email instead of phone as primary key
        
            this.firefriend.child(oldUid).once('value', (snapshot) => {
                let allFriendsWithOldUid = snapshot.val();
                for (var key in allFriendsWithOldUid) {
                   this.firefriend.child(key).once('value', (snapshot_friends) => {
                        let allFriendsWithOldUid_friends = snapshot_friends.val();
                        for (var key_friends in allFriendsWithOldUid_friends) {
                            if(key_friends == oldUid) {
                               var pathToRemove = snapshot_friends.ref.path.toString();
                               pathToRemove = pathToRemove.replace ('/friends/', '')
                               ////console.log("Adding : "+"(pathToRemove"+"/"+this.afireAuth.auth.currentUser.uid);
                               ////console.log (allFriendsWithOldUid_friends [oldUid]);
                               allFriendsWithOldUid_friends [oldUid].uid = this.afireAuth.auth.currentUser.uid;
                               this.firefriend.child(pathToRemove+"/"+this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid_friends [oldUid]);
                               this.firefriend.child(pathToRemove+"/"+oldUid).remove();
                               break;
                            }
                        }//end for
                   });
                }//end for
        
                //now change the old friend record to new record
                this.firefriend.child(this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid);
                this.firefriend.child(oldUid).remove();
              })
              */
    };
    UserService.prototype.updateimage = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updateimage_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updateimage_(args);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updateimage_(args);
            });
        }
    };
    UserService.prototype.updateimage_ = function (imageurl) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({ photoURL: imageurl }).then(function () {
                firebase_app__WEBPACK_IMPORTED_MODULE_2__["database"]().ref('/users/' + _this.userId).update({
                    photoURL: imageurl
                }).then(function () {
                    resolve({ success: true });
                }).catch(function (err) {
                    reject(err);
                });
            });
        });
        return promise;
    };
    /*getuserdetails (){
       if(this.userId){
         return this.getuserdetails_();
       } else {
         this.tpStorageService.getItem('userUID').then((res: any) => {
           if(res){
             this.userId = res;
           }
           if (!this.userId) {
             this.userId = firebase.auth().currentUser.uid;
           }
           return this.getuserdetails_();
         }).catch(e => {
           if(firebase && firebase.auth().currentUser){
             this.userId = firebase.auth().currentUser.uid;
           }
           return this.getuserdetails_();
         });
       }
    }
    */
    UserService.prototype.getbuddydetails = function (buddyID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(buddyID).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    /*
      updatedisplayname (args){
         if(this.userId){
           return this.updatedisplayname_(args);
         } else {
           this.tpStorageService.getItem('userUID').then((res: any) => {
             if(res){
               this.userId = res;
             }
             if (!this.userId) {
               this.userId = firebase.auth().currentUser.uid;
             }
             return this.updatedisplayname_(args);
           }).catch(e => {
             if(firebase && firebase.auth().currentUser){
               this.userId = firebase.auth().currentUser.uid;
             }
             return this.updatedisplayname_(args);
           });
         }
      }
      */
    UserService.prototype.updatedisplayname_DB = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updatedisplayname_DB_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updatedisplayname_DB_(args);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updatedisplayname_DB_(args);
            });
        }
    };
    UserService.prototype.saveUserIdInSqlLite = function (userId) {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_6__["CONST"].create_table_statement, [])
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_users_local", [])
                    .then(function (res) {
                    if (res.rows.length > 0) {
                    }
                    else {
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid)   VALUES('" + _this.userId + "')", [])
                            .then(function (res1) {
                            //////console.log ("executeSql INSERT");//testa
                        }).catch(function (e) {
                        });
                    } //endif
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    UserService.prototype.updatedisplayname_DB_ = function (newname) {
        var _this = this;
        //////console.log ("Querying DB");
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_6__["CONST"].create_table_statement, [])
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_users_local", [])
                    .then(function (res) {
                    if (res.rows.length > 0) {
                        //////console.log ("user.ts record exists");
                        //UPDATE THE RECORD
                        db.executeSql("UPDATE cache_users_local SET uid='" + _this.userId + "',user_name='" + newname + "',user_email='" + res.rows.item(0).user_email + "'", [])
                            .then(function (res1) {
                            //////console.log ("executeSql UPDATE");
                        }).catch(function (e) {
                        });
                    }
                    else {
                        //////console.log ("user.ts record does not exists");
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid,user_name)   VALUES('" + _this.userId + "','" + newname + "')", [])
                            .then(function (res1) {
                            //////console.log ("executeSql INSERT");//testa
                        }).catch(function (e) {
                        });
                    } //endif
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
                //Error in operation
            }); //create table
        }); //create database
    }; //end function
    UserService.prototype.updateDeviceToken = function (token, userID) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            //Update the one who invited me
            //The google cloud function should do this job but sometime its lazy and take lot of time to run. So doing here quickly now
            //Do not run additional call to google cloud or something. That should happen through google cloud triggers function on cloud
            _this.tpStorageService.getItem('invited_by').then(function (invited_by_res) {
                _this.tpStorageService.getItem('myDisplayName').then(function (myDisplayName_res) {
                    _this.firefriend.child(invited_by_res).child(userID).update({
                        deviceToken: token,
                        displayName: myDisplayName_res
                    });
                }).catch(function (e) {
                });
            }).catch(function (e) {
            });
            _this.firedata.child(userID).update({
                deviceToken: token
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserService.prototype.updatedisc = function (args) {
        var _this = this;
        if (this.userId) {
            return this.updatedisc_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updatedisc_(args);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.updatedisc_(args);
            });
        }
    };
    UserService.prototype.updatedisc_ = function (disc) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                disc: disc
            }).then(function () {
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    //We need to remove getallusers_modified and getallusers . (mytodo) This is just for placeholder
    UserService.prototype.getallusers_modified = function (obj) {
        var promise = new Promise(function (resolve, reject) {
            resolve(obj);
        });
        return promise;
    };
    UserService.prototype.getallusers = function () {
        var _this = this;
        //////console.log ("[[[[[[[[[[[[[[[[[GET ALL USERS]]]]]]]]]]]]]]]]]");
        var promise = new Promise(function (resolve, reject) {
            _this.firedata.orderByChild('uid').once('value', function (snapshot) {
                //////console.log (snapshot.val());
                //mytodo immediate : Why its getting all users. it should only get me
                var userdata = snapshot.val();
                var temparr = [];
                for (var key in userdata) {
                    temparr.push(userdata[key]);
                }
                resolve(temparr);
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise;
    };
    UserService.prototype.directLogin = function () {
        return new Promise(function (resolve, reject) {
            resolve({ success: true });
        });
    };
    UserService.prototype.deleteUser = function () {
        var _this = this;
        if (this.userId) {
            return this.deleteUser_();
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.deleteUser_();
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.deleteUser_();
            });
        }
    };
    UserService.prototype.deleteUser_ = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend;
            _this.firedata.child(_this.userId).remove().then(function () {
                _this.deletefriend(_this.userId);
                _this.deletefirebuddychat(_this.userId);
                _this.deleteusers(_this.userId);
                _this.deleteuserstatus(_this.userId);
                _this.tpStorageService.clear();
                resolve({ success: true });
            }).catch(function (err) {
                reject({ success: true });
            });
        });
    };
    UserService.prototype.deletefriend = function (id) {
        this.firedata.child(id).remove().then(function (res) {
        });
    };
    UserService.prototype.deletefirebuddychat = function (id) {
        this.firebuddychat.child(id).remove().then(function (res) {
        });
    };
    UserService.prototype.deleteusers = function (id) {
        this.users.child(id).remove().then(function (res) {
        });
    };
    UserService.prototype.deleteuserstatus = function (id) {
        this.userstatus.child(id).remove().then(function (res) {
        });
    };
    UserService.prototype.blockUser = function (args) {
        var _this = this;
        if (this.userId) {
            return this.blockUser_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.blockUser_(args);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.blockUser_(args);
            });
        }
    };
    UserService.prototype.blockUser_ = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        });
    };
    /*This is proxy function for unblockUser_ */
    UserService.prototype.unblockUser = function (args) {
        var _this = this;
        if (this.userId) {
            return this.unblockUser_(args);
        }
        else {
            this.tpStorageService.getItem('userUID').then(function (res) {
                if (res) {
                    _this.userId = res;
                }
                if (!_this.userId) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.unblockUser_(args);
            }).catch(function (e) {
                if (firebase_app__WEBPACK_IMPORTED_MODULE_2__ && firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser) {
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_2__["auth"]().currentUser.uid;
                }
                return _this.unblockUser_(args);
            });
        }
    };
    UserService.prototype.unblockUser_ = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == buddy.uid) {
                        _this.firefriend.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        });
    };
    //mytodo : create proxy functions for all the following (The one has this.userId arggument)
    //The reason for theses proxy functions is to make sure this.userId exists before they are called
    //Chances are rare but it still happens. if happen it throw error on user screen and its ugly error
    UserService.prototype.getstatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                    var allfriends = snapshot.val();
                    for (var key in allfriends) {
                        if (allfriends[key].uid == buddy.uid) {
                            resolve(allfriends[key].isBlock);
                        }
                    }
                });
            }).catch(function (e) { });
        });
    };
    UserService.prototype.getstatusblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                //console.log ("firefriend/"+buddy.uid+"/"+ this.userId);
                _this.firefriend.child(buddy.uid).child(_this.userId).once('value', function (snapshot) {
                    var buddyS = snapshot.val();
                    resolve(buddyS.isBlock);
                });
            }).catch(function (e) { });
        });
    };
    UserService.prototype.getuserblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserService.prototype.getmsgblock = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firefriend.child(buddy).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].uid == _this.userId) {
                        resolve(allfriends[key].isBlock);
                    }
                }
            });
        });
    };
    UserService.prototype.getstatusblockuser = function (buddy) {
        var _this = this;
        this.firefriend.child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
            var allfriends = snapshot.val();
            for (var key in allfriends) {
                if (allfriends[key].uid == _this.userId) {
                    _this.isuserBlock = allfriends[key].isBlock;
                }
            }
            _this.events.publish('isblock-user');
        });
    };
    UserService.prototype.getAllBlockUsers = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var blockuser = [];
                for (var key in allfriends) {
                    if (allfriends[key].isBlock) {
                        _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                            blockuser.push(snapsho.val());
                        }).catch(function (err) {
                        });
                    }
                    _this.blockUsers = [];
                    _this.blockUsers = blockuser;
                }
                _this.events.publish('block-users');
            });
        }).catch(function (e) { });
    };
    UserService.prototype.getAllunBlockUsers = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var unblockuser = [];
                for (var key in allfriends) {
                    if (allfriends[key].isBlock == false) {
                        _this.firedata.child(allfriends[key].uid).once('value', function (snapsho) {
                            unblockuser.push(snapsho.val());
                        }).catch(function (err) {
                        });
                    }
                    _this.unblockUsers = [];
                    _this.unblockUsers = unblockuser;
                }
                _this.events.publish('unblock-users');
            });
        }).catch(function (e) { });
    };
    UserService.prototype.getAllBlockUsersCounter = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            // block-users-counter
            _this.firefriend.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                var blockalluser = [];
                for (var tmpkey in allfriends) {
                    if (allfriends[tmpkey].isBlock) {
                        blockalluser.push(allfriends[tmpkey]);
                    }
                }
                _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                    var allrequest = snapshot.val();
                    for (var tmp in allrequest) {
                        if (allrequest[tmp].isBlock) {
                            blockalluser.push(allrequest[tmp]);
                        }
                    }
                    _this.blockUsersCounter = 0;
                    _this.blockUsersCounter = blockalluser.length;
                    _this.events.publish('block-users-counter');
                });
            });
        }).catch(function (e) { });
    };
    UserService.prototype.notifyUser = function (isnotify) {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.firenotify.child(_this.userId).set({
                isNotify: isnotify
            });
        }).catch(function (e) { });
    };
    UserService.prototype.getNotifyStatus = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firenotify.child(buddy.uid).child('isNotify').once('value', function (snapshot) {
                var isNotify = snapshot.val();
                if (isNotify == true)
                    resolve(true);
                else
                    resolve(false);
            });
        });
    };
    UserService.prototype.registerBusiness = function (newname, useremail, bcats, rtlist, referral_incentive_detail_txt) {
        var _this = this;
        //////console.log (">registerBusiness");
        //update email
        this.tpStorageService.setItem('useremail', useremail);
        new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                useremail: useremail
            }).then(function () {
                //////console.log ("Done1");
            }).catch(function (err) {
                //////console.log ("Tapally Error 9122");
                //////console.log(err);
            });
        });
        var timeStamp = String(Date.now()); // get timestamp
        //update/create business name
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                displayName: newname,
                timestamp_user_local_time_updated: timeStamp,
                bcats: bcats,
                referral_type_id: rtlist,
                referral_detail: referral_incentive_detail_txt
            }).then(function () {
                _this.tpStorageService.setItem('businessName', newname);
                _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                }).catch(function (e) {
                    _this.tpStorageService.setItem('business_created', timeStamp);
                });
                _this.tpStorageService.setItem('bcats', bcats);
                _this.tpStorageService.setItem('business_my_referral_type_id', rtlist);
                _this.tpStorageService.setItem('business_my_referral_detail', referral_incentive_detail_txt);
                //Check for paid flag and update it with userid
                var clean_email = useremail.replace(/[^0-9A-Za-z]/gi, '');
                //////console.log ("clean_email:"+clean_email);
                new Promise(function (resolve, reject) {
                    clean_email = clean_email.toLowerCase(); //mytodo : please check on website if its lower case as well
                    _this.fireWebsiteUsers.child(clean_email).once('value', function (snapshot_websiteUser) {
                        //////console.log (snapshot_websiteUser.val());
                        //User register on website yet
                        var websiteData = snapshot_websiteUser.val();
                        var paidvalue = 0;
                        if (websiteData && websiteData.paid > 0) {
                            // User has paid business account
                            _this.tpStorageService.setItem('paid', "1");
                            paidvalue = 1;
                        }
                        else {
                            _this.tpStorageService.setItem('paid', "0");
                            paidvalue = 0;
                        } //endif
                        //////console.log ("websiteData.paid");
                        //////console.log (websiteData.paid);
                        _this.fireBusiness.child(_this.userId).update({
                            paid: paidvalue,
                        });
                        //now update websiteuserdata  it with our id
                        _this.fireWebsiteUsers.child(clean_email).update({
                            userId: _this.userId
                        }).then(function () {
                        }).catch(function (err) {
                        });
                    });
                });
                // end website_user
                //////console.log ("Promise Resolved 1");
                resolve(true);
            }).catch(function (err) {
                //////console.log ("Tapally Error 29832");
                //////console.log(err);
                resolve(false);
            });
        });
        return promise;
    }; //end function
    UserService.prototype.registerBusiness_old = function (newname, useremail, bcats) {
        var _this = this;
        //update email
        this.tpStorageService.setItem('useremail', useremail);
        new Promise(function (resolve, reject) {
            _this.firedata.child(_this.userId).update({
                useremail: useremail
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        });
        var timeStamp = String(Date.now()); //
        //update/create business name
        var promise = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                displayName: newname,
                timestamp_user_local_time_updated: timeStamp
            }).then(function () {
                if (_this.tpStorageService.getItem('businessName') || _this.tpStorageService.getItem('businessName') != newname) {
                    _this.tpStorageService.setItem('businessName', "");
                }
                if (!_this.tpStorageService.getItem('businessName')) {
                    //Business Just got registered
                    //var timestamp = firebase.database.ServerValue.TIMESTAMP;
                    new Promise(function (resolve, reject) {
                        _this.fireBusiness.child(_this.userId).update({
                            timestamp_user_local_time_created: timeStamp
                        }).then(function () {
                            resolve({ success: true });
                        }).catch(function (err) {
                        });
                    });
                    _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                    }).catch(function (e) {
                        _this.tpStorageService.setItem('business_created', timeStamp);
                    });
                } //endif
                _this.tpStorageService.setItem('businessName', newname);
                resolve({ success: true });
            }).catch(function (err) {
                reject(err);
            });
        });
        //update cat
        new Promise(function (resolve, reject) {
            _this.fireBusiness.child(_this.userId).update({
                bcats: bcats
            }).then(function () {
                _this.tpStorageService.setItem('bcats', bcats);
            }).catch(function (err) {
            });
        });
        return promise;
    }; //end function
    UserService.prototype.checkIfBusinessIsPaid = function (businessId) {
        var _this = this;
        var promise_cibp = new Promise(function (resolve, reject) {
            _this.fireBusiness.child(businessId).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        });
        return promise_cibp;
    };
    UserService.prototype.getBusinessDetailsOther = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fireBusiness.child(uid).once('value', function (snapshot) {
                var res = snapshot.val();
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    }; //end function
    UserService.prototype.getAnotherUser = function (uid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firedata.child(uid).once('value', function (snapshot) {
                var res = snapshot.val();
                resolve(res);
            }).catch(function (err) {
                reject(err);
            });
        });
    }; //end function
    UserService.prototype.getBusinessDetails = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.fireBusiness.child(_this.userId).once('value', function (snapshot) {
                    var res = snapshot.val();
                    if (res) {
                        if (Object.keys(res).length > 0) {
                            _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                            }).catch(function (e) {
                                _this.tpStorageService.setItem('business_created', res['timestamp_user_local_time_created']);
                            });
                            _this.tpStorageService.setItem('businessName', res['displayName']);
                            _this.tpStorageService.setItem('bcats', res['bcats']);
                            if (res['paid']) {
                                _this.tpStorageService.setItem('paid', "1");
                            }
                            else {
                                _this.tpStorageService.setItem('paid', "-1");
                            }
                        }
                        else {
                            _this.tpStorageService.setItem('paid', "-1");
                        }
                    } //endif   .
                    resolve(res);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserService.prototype.fnMarkIncentiveRedeemed_ = function (referral_send_by, referral_received_by, requestId) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.fireReferral_sent.child(referral_send_by).child(referral_received_by).child(requestId).update({
                redeemStatus: 2,
            });
            _this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(requestId).update({
                redeemStatus: 2,
            });
            resolve(true);
        });
        return promise;
    }; //end function
    UserService.prototype.getReferralRecieved = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.fireReferral_sent.child(_this.userId).once('value', function (snapshot) {
                    resolve(snapshot.val());
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserService.prototype.getReferralRecievedByID = function (referral_send_by, referral_received_by, Id) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                if (referral_received_by == _this.userId) {
                    //i am reciever of this referral
                    _this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(Id).once('value', function (snapshot) {
                        resolve(snapshot.val());
                    }).catch(function (err) {
                        reject(err);
                    });
                }
                else {
                    //following is not used right now
                    //somebody else is reciever ; I probably (might not necessarily) sent
                    _this.fireReferral_received.child(referral_send_by).child(referral_received_by).child(Id).once('value', function (snapshot) {
                        resolve(snapshot.val());
                    }).catch(function (err) {
                        reject(err);
                    });
                }
            }).catch(function (e) { });
        });
        return promise;
    }; //end function
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_5__["SQLite"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_1__["AngularFireAuth"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"],
            _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_8__["HTTP"],
            _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_7__["TpstorageProvider"]])
    ], UserService);
    return UserService;
}());



/***/ })

}]);
//# sourceMappingURL=default~forms-validations-forms-validations-module~signup-signup-module.js.map