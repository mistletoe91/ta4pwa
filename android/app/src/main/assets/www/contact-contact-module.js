(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-contact-module"],{

/***/ "./src/app/contact/contact.module.ts":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.module.ts ***!
  \*******************************************/
/*! exports provided: ContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact.page */ "./src/app/contact/contact.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _contact_page__WEBPACK_IMPORTED_MODULE_5__["ContactPage"]
    }
];
var ContactPageModule = /** @class */ (function () {
    function ContactPageModule() {
    }
    ContactPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_contact_page__WEBPACK_IMPORTED_MODULE_5__["ContactPage"]]
        })
    ], ContactPageModule);
    return ContactPageModule;
}());



/***/ }),

/***/ "./src/app/contact/contact.page.html":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Invite Contacts</ion-title>\n    <ion-buttons tappable (click)=\"backBtn()\" slot=\"start\">\n      <ion-icon class=\"leftmargin_topbtn\"  name=\"arrow-round-back\"></ion-icon>\n    </ion-buttons>\n    <ion-icon slot=\"end\" class=\"iconright\"  (click)=\"refreshPage()\" name=\"refresh-circle\"></ion-icon>\n  </ion-toolbar>\n\n          <!-- Searchbar with a placeholder -->\n          <ion-searchbar tappable   placeholder=\"Search\" (ionInput)=\"searchuser($event)\"></ion-searchbar>\n</ion-header>\n\n<ion-content>\n  <ion-row>\n    <ion-col size=\"12\">\n\n      <ion-list detail=\"false\">\n        <ion-list-header lines=\"false\">\n          Invite Businesses, Friends & Build Meaningful Network\n        </ion-list-header>\n\n      <ion-virtual-scroll [items]=\"mainArray\" class=\"contactslistion \" approxItemHeight=\"320px\">\n        <ion-item *virtualItem=\"let item; let itemBounds = bounds;\"   >\n\n          <ion-label *ngIf=\"item.displayName\">{{item.displayName}}\n              <ion-note  *ngIf=\"item.mobile_formatted\" > {{item.mobile_formatted}} </ion-note>\n          </ion-label>\n\n          <ion-button fill=\"clear\" shape=\"round\" class=\"iconbtn\" tappable  (click)=\"sendSMS(item)\" >\n            <ion-icon class=\"iconbtninsidebtn\" *ngIf=\"!item.isdisable\" name=\"add-circle\"></ion-icon>\n          </ion-button>\n\n        </ion-item>\n      </ion-virtual-scroll>\n\n      </ion-list>\n\n    </ion-col>\n\n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/contact/contact.page.scss":
/*!*******************************************!*\
  !*** ./src/app/contact/contact.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3QvY29udGFjdC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/contact/contact.page.ts":
/*!*****************************************!*\
  !*** ./src/app/contact/contact.page.ts ***!
  \*****************************************/
/*! exports provided: ContactPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPage", function() { return ContactPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tpstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/loading.service */ "./src/services/loading.service.ts");
/* harmony import */ var _services_contact_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/contact.service */ "./src/services/contact.service.ts");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/requests.service */ "./src/services/requests.service.ts");
/* harmony import */ var _services_sms_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/sms.service */ "./src/services/sms.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/chat.service */ "./src/services/chat.service.ts");
/* harmony import */ var _app_angularfireconfig__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../app.angularfireconfig */ "./src/app/app.angularfireconfig.ts");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};












//import { FcmService } from '../../services/fcm.service';


var ContactPage = /** @class */ (function () {
    function ContactPage(route, events, router, platform, loadingProvider, contactProvider, alertCtrl, sqlite, userservice, requestservice, chatservice, SmsProvider, modalCtrl, toastCtrl, tpStorageService) {
        var _this = this;
        this.route = route;
        this.events = events;
        this.router = router;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.contactProvider = contactProvider;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.userservice = userservice;
        this.requestservice = requestservice;
        this.chatservice = chatservice;
        this.SmsProvider = SmsProvider;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.tpStorageService = tpStorageService;
        this.contactList = [];
        this.allregisteUserData = [];
        this.metchContact = [];
        this.mainArray = [];
        this.tempArray = [];
        this.newrequest = {};
        this.OnIsSearchBarActive = false;
        this.sourceImportpPage_ = false;
        this.userpic = _app_angularfireconfig__WEBPACK_IMPORTED_MODULE_10__["userPicArr"];
        this.showheader = true;
        this.readContactsFromCache = false;
        this.cssClassChecking = 'hide';
        this.cssClassContacts = 'show';
        this.isData = false;
        this.reqStatus = false;
        this.isInviteArray = [];
        this.temparr = [];
        this.filteredusers = [];
        this.contactCounter = 0;
        this.route.queryParams.subscribe(function (params) {
            if (params && params.source) {
                _this.sourcePage = params.source;
                console.log("paramData");
                console.log(_this.sourcePage);
            }
        });
    }
    ContactPage.prototype.ngOnInit = function () {
    };
    ContactPage.prototype.backBtn = function () {
        console.log("BTN back clickd");
        this.router.navigate(['/app/notifications']);
        /*if(this.sourcePage == "contactimportprogresspage"){
    
        } else {
    
        }
        */
    };
    //public fcm: FcmService,
    ContactPage.prototype.ionViewDidEnter = function () {
        console.log("Page loadi...");
        this.ionViewDidLoad_();
    };
    ContactPage.prototype.searchuser = function (ev) {
        return this.searchuser_android(ev);
    };
    ContactPage.prototype.searchuser_android = function (ev) {
        //initize the array
        if (this.tempArray.length <= 0) {
            this.tempArray = this.mainArray;
        }
        else {
            this.mainArray = this.tempArray;
        } //endif
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.mainArray = this.mainArray.filter(function (item) {
                if (item.displayName != undefined) {
                    return ((item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
                }
            });
        }
        else {
            //nothing in search
        } //endif
    }; //end function
    ContactPage.prototype.tpInitilizeFromStorage = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('isInvitedContact').then(function (res) {
            if (res) {
                _this.isInviteArray = JSON.parse(res);
            }
        }).catch(function (e) { });
        this.tpStorageService.getItem('updated').then(function (res) {
            if (res) {
                _this.updatedRef = res;
            }
        }).catch(function (e) { });
    };
    ContactPage.prototype.sendSMS = function (item) {
        this.SmsProvider.sendSms(item.mobile);
    };
    ContactPage.prototype.ionViewDidLoad_ = function () {
        var _this = this;
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage();
        this.contactProvider.getSimJsonContacts()
            .then(function (res) {
            console.log("Done");
            console.log(res);
            if (res) {
                /*--Start*/
                Object.keys(res).forEach(function (ele) {
                    if (ele == "fromcache") {
                        if (!res["fromcache"]) {
                            //came from device
                            _this.readContactsFromCache = true;
                            _this.cssClassChecking = 'show';
                            _this.cssClassContacts = 'hide';
                            _this.events.subscribe('contactInsertingDone', function () {
                                _this.events.unsubscribe('contactInsertingDone');
                                if (!_this.contactProvider.contactInsertingDone) {
                                    //still inserting
                                    _this.readContactsFromCache = true;
                                    _this.cssClassChecking = 'show';
                                    _this.cssClassContacts = 'hide';
                                }
                                else {
                                    //done inserting
                                    //this.router.navigateByUrl('/app/notifications');
                                }
                            });
                        }
                        else {
                            //came from database
                            _this.readContactsFromCache = false;
                            _this.cssClassChecking = 'hide';
                            _this.cssClassContacts = 'show';
                        }
                    }
                    if (ele == "contacts") {
                        _this.mainArray = [];
                        _this.contactCounter = 0;
                        Object.keys(res["contacts"]).forEach(function (key) {
                            _this.mainArray.push({
                                "displayName": res["contacts"][key].displayName,
                                "mobile": res["contacts"][key].mobile,
                                "mobile_formatted": res["contacts"][key].mobile_formatted,
                                "status": "",
                                "isBlock": false,
                                "isUser": 0,
                                "isdisable": false,
                                "dim": false,
                                "photoURL": res["contacts"][key].photoURL,
                                "uid": res["contacts"][key].uid
                            });
                        });
                        _this.loadingProvider.dismissMyLoading();
                        //check and fix the invited one
                        _this.tpStorageService.getItem('isInvitedContact').then(function (res) {
                            if (res) {
                                _this.isInviteArray = JSON.parse(res);
                                for (var g = 0; g < _this.isInviteArray.length; g++) {
                                    for (var x = 0; x < _this.mainArray.length; x++) {
                                        if (_this.mainArray[x].mobile == _this.isInviteArray[g] || _this.mainArray[x].mobile_formatted == _this.isInviteArray[g]) {
                                            _this.mainArray[x].isdisable = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }).catch(function (e) { });
                    } //endif
                });
                /*--End */
            } //endif
        }).catch(function (e) { });
    }; //end function
    ContactPage.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    ContactPage.prototype.refreshPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var confirm;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Sync All Contacts',
                            message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
                            buttons: [
                                {
                                    text: 'Cancel',
                                    handler: function () {
                                        //console.log('Disagree clicked');
                                    }
                                },
                                {
                                    text: 'Agree',
                                    handler: function () {
                                        //console.log('Agree clicked');
                                        //this.navCtrl.setRoot('ContactimportprogressPage', {source : 'ContactPage'});
                                        _this.openContacts();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        confirm = _a.sent();
                        return [4 /*yield*/, confirm.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactPage.prototype.openContacts = function () {
        var navigationExtras = {
            queryParams: {
                source: "contactspage"
            }
        };
        this.router.navigate(['/contactimportprogress'], navigationExtras);
    }; //end function
    ContactPage.prototype.inviteReq = function (recipient) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Invitation',
                            subHeader: 'Invitation already sent to ' + recipient.displayName + '.',
                            buttons: ['Ok']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ContactPage.prototype.sendreq = function (recipient) {
        return __awaiter(this, void 0, void 0, function () {
            var userName_1;
            var _this = this;
            return __generator(this, function (_a) {
                this.reqStatus = true;
                this.newrequest.sender = this.userId;
                this.newrequest.recipient = recipient.uid;
                if (this.newrequest.sender === this.newrequest.recipient)
                    alert('You are your friend always');
                else {
                    this.userservice.getuserdetails().then(function (res) {
                        userName_1 = res.displayName;
                        var newMessage = userName_1 + " has sent you friend request.";
                        //this.fcm.sendNotification(recipient, newMessage, 'sendreq');
                    });
                    this.requestservice.sendrequest(this.newrequest).then(function (res) {
                        if (res.success) {
                            _this.reqStatus = false;
                            /*
                            let successalert = await this.alertCtrl.create({
                              title: 'Request sent',
                              subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
                              buttons: ['ok']
                            });
                            await successalert.present();
                            */
                            var sentuser = _this.mainArray.indexOf(recipient);
                            _this.mainArray[sentuser].status = "pending";
                        }
                    }).catch(function (err) {
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    //href="sms:{{item.mobile}}?body=Hi, I'm offering referral incentives. Please download my app at tapally.com and start earning"
    ContactPage.prototype.sanitizeAndSms = function (mobile) {
        console.log('sms:' + mobile);
        return 'sms:' + mobile;
    };
    ContactPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__(/*! ./contact.page.html */ "./src/app/contact/contact.page.html"),
            styles: [__webpack_require__(/*! ./contact.page.scss */ "./src/app/contact/contact.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _services_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"],
            _services_contact_service__WEBPACK_IMPORTED_MODULE_6__["ContactService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_11__["SQLite"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_7__["RequestsService"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_9__["ChatService"],
            _services_sms_service__WEBPACK_IMPORTED_MODULE_8__["SmsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _tpstorage_service__WEBPACK_IMPORTED_MODULE_3__["TpstorageProvider"]])
    ], ContactPage);
    return ContactPage;
}());



/***/ }),

/***/ "./src/services/requests.service.ts":
/*!******************************************!*\
  !*** ./src/services/requests.service.ts ***!
  \******************************************/
/*! exports provided: RequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsService", function() { return RequestsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app/tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user.service */ "./src/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//import 'rxjs/add/operator/map';
var RequestsService = /** @class */ (function () {
    function RequestsService(userservice, events, platform, tpStorageService) {
        var _this = this;
        this.userservice = userservice;
        this.events = events;
        this.platform = platform;
        this.tpStorageService = tpStorageService;
        this.firebuddychats = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/buddychats');
        this.firedata = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/chatusers');
        this.firereq = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/requests');
        this.firefriends = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/friends');
        this.fireInvitationSent = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/invitationsent');
        this.firePhones = firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"]().ref('/phones');
        //new_chat_message_flag:boolean = false;
        this.lastMsgReader = {};
        this.blockRequest = [];
        this.subscriptionAddedObj = {};
        this.subscriptionAddedFriendsObj = {};
        this.platform.ready().then(function (readySource) {
            //////console.log ("RequestsProvider constructor");
            _this.tpStorageService.getItem('userUID').then(function (userID) {
                //////console.log ("Getting userUIDuserUIDuserUIDuserUIDuserUIDuserUID");
                if (userID != undefined) {
                    //////console.log ("SETTING this.userId +++++++++++++++++++++++");
                    _this.userId = userID;
                }
                else {
                    ////////console.log ("This is culprit");
                    ////////console.log (firebase.auth().currentUser);
                    //////console.log ("SETTING this.userId ++++++++++--+++++++++++++");
                    _this.userId = firebase_app__WEBPACK_IMPORTED_MODULE_1__["auth"]().currentUser.uid;
                }
            }).catch(function (e) {
            });
        });
    }
    /*
    Sample : of "requests"
    
    {
      "1bqhG3I2U5e6tMzkhTrIw8Ehozb2" : {
        "-Ld5sBkuU-eXYZ_LfbQS" : {
          "displayName" : "Kaztaaa",
          "isBlock" : false,
          "phone": "6471111212",
          "photoURL" : "https://firebasestorage.googleapis.com/v0/b/tapallyionic3.appspot.com/o/profileimages%2F0.png?alt=media&token=2c3594e5-a0ce-42a8-83af-c98a7a7827b9",
          "sender" : "46u6XlnVuIeBnofPrxkUZ8sRxem1"
        }
      }
    }
    
    
    */
    RequestsService.prototype.checkPhones = function (phoneNumber) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firePhones.child(phoneNumber).once('value', function (snapshot) {
                resolve(snapshot.val());
            }).catch(function (err) {
                reject(err);
            });
        }).catch(function (error) {
            ////console.log ("Error 3652");////console.log (error);
        });
    };
    RequestsService.prototype.sendrequest = function (req) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.userservice.getuserdetails().then(function (myDetails) {
                _this.firereq.child(req.recipient).push({
                    sender: req.sender,
                    isBlock: false,
                    phone: myDetails.phone,
                    mobile: myDetails.phone,
                    displayName: myDetails.displayName,
                    photoURL: myDetails.photoURL
                }).then(function () {
                    resolve({ success: true });
                });
            });
        }).catch(function (error) {
            ////console.log ("Error 6422");////console.log (error);
        });
        return promise;
    };
    RequestsService.prototype.setInvitedBy = function (phone) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firePhones.child(phone).set({
                invited_by: _this.userId,
                timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        }).catch(function (error) {
            ////console.log ("Error 8426");////console.log (error);
        });
    };
    RequestsService.prototype.logInvitation = function (phone) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fireInvitationSent.child(_this.userId).child(phone).set({
                timestamp: firebase_app__WEBPACK_IMPORTED_MODULE_1__["database"].ServerValue.TIMESTAMP
            }).then(function () {
                resolve(true);
            }).catch(function (err) {
                resolve(false);
            });
        }).catch(function (error) {
            ////console.log ("Error 8456");////console.log (error);
        });
    };
    RequestsService.prototype.getmyrequests_promise = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                _this.firereq.child(_this.userId).once('value', function (snapshot) {
                    //check if user not blocked
                    _this.userdetails = [];
                    var myrequests = snapshot.val();
                    for (var j in myrequests) {
                        if (!myrequests[j].isBlock) {
                            _this.userdetails.push(myrequests[j]);
                        }
                    } //end for
                    resolve(_this.userdetails);
                });
            }).catch(function (e) { });
        }).catch(function (error) {
            ////console.log ("Error 6234");////console.log (error)
        });
        return promise;
    }; //end function
    RequestsService.prototype.getmyrequests = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.getmyrequests_();
        }).catch(function (e) { });
    };
    RequestsService.prototype.getmyrequests_ = function () {
        var _this = this;
        var allmyrequests;
        var myrequests = [];
        ////////console.log ("GET MY REQUESTS");
        ////////console.log (this.userId);
        this.firereq.child(this.userId).once('value', function (snapshot) {
            ////////console.log ("Found requests");
            ////////console.log (snapshot.val());
            allmyrequests = snapshot.val();
            myrequests = [];
            for (var i in allmyrequests) {
                myrequests.push(allmyrequests[i]);
            }
            //Check if user is not blocked by this user
            _this.userdetails = [];
            for (var j in myrequests) {
                if (!myrequests[j].isBlock) {
                    _this.userdetails.push(myrequests[j]);
                }
            }
            //////console.log ("<<<>>>>");
            //////console.log (this.userdetails);
            _this.events.publish('gotrequests');
            /*
            this.userservice.getallusers_modified(myrequests).then((res) => {
              var allusers = res;
              this.userdetails = [];
              for (var j in myrequests)
                for (var key in allusers) {
                  if (myrequests[j].sender === allusers[key].uid && myrequests[j].isBlock == false) {
                    this.userdetails.push(allusers[key]);
                  }
                }
              this.events.publish('gotrequests');
            })
            */
        });
    };
    //duplicate function in userst.ts  but not entirly same
    RequestsService.prototype.acceptrequest = function (buddy, deleteRequest) {
        var _this = this;
        if (deleteRequest === void 0) { deleteRequest = true; }
        var promise = new Promise(function (resolve, reject) {
            _this.userservice.getuserdetails().then(function (aboutMe) {
                _this.firefriends.child(_this.userId).child(buddy.uid).set({
                    uid: buddy.uid,
                    displayName: buddy.displayName,
                    photoURL: buddy.photoURL,
                    isActive: 1,
                    isBlock: false,
                    deviceToken: buddy.deviceToken,
                    countryCode: buddy.countryCode,
                    disc: buddy.disc,
                    mobile: buddy.mobile,
                    timestamp: buddy.timestamp
                }).then(function () {
                    _this.firefriends.child(buddy.uid).child(_this.userId).set({
                        uid: _this.userId,
                        displayName: aboutMe.displayName,
                        photoURL: aboutMe.photoURL,
                        isActive: 1,
                        isBlock: false,
                        deviceToken: aboutMe.deviceToken,
                        countryCode: aboutMe.countryCode,
                        disc: aboutMe.disc,
                        mobile: aboutMe.mobile,
                        timestamp: aboutMe.timestamp
                    }).then(function () {
                        if (deleteRequest) {
                            _this.deleterequest(buddy).then(function () {
                                resolve(true);
                            }).catch(function (err) {
                                //////console.log ("Error 3259");
                                reject(err);
                            });
                        } //endif
                    });
                });
            });
        }).catch(function (error) {
            ////console.log ("Error 4563");////console.log (error)
        });
        return promise;
    }; //end function
    RequestsService.prototype.deleterequest = function (buddy) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('sender').equalTo(buddy.uid).once('value', function (snapshot) {
                var somekey;
                for (var key in snapshot.val())
                    somekey = key;
                _this.firereq.child(_this.userId).child(somekey).remove().then(function () {
                    resolve(true);
                });
            })
                .then(function () {
            }).catch(function (err) {
                reject(err);
            });
        }).catch(function (error) {
            ////console.log ("Error 1243");////console.log (error);
        });
        return promise;
    };
    RequestsService.prototype.getmyfriends = function () {
        var _this = this;
        //console.log ( "getmyfriends ()");
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            //////console.log ("LLLove ");
            _this.getmyfriends_();
        }).catch(function (e) { });
    };
    RequestsService.prototype.getmyfriends_ = function () {
        var _this = this;
        this.firefriends.child(this.userId).on('value', function (snapshot) {
            _this.myfriends = [];
            //console.log ("FIREFRIEND EVENT TRIGGERED");
            //console.log (snapshot.val());
            var allfriends = snapshot.val();
            if (allfriends != null) {
                // I have some friends
                ////console.log ("I have friends " );
                var CounterFriends_1 = 0;
                for (var i in allfriends) {
                    ////console.log ("iiii ")     ;
                    var lastMsgActual__ = {
                        message: "",
                        type: "",
                        dateofmsg: "",
                        timeofmsg: "",
                        isRead: "",
                        isStarred: "",
                        referral_type: "",
                        selectCatId: ""
                    };
                    if (!allfriends[i].isBlock) {
                        allfriends[i].isBlock = false;
                    }
                    allfriends[i].unreadmessage = lastMsgActual__;
                    allfriends[i].lastMessage = lastMsgActual__;
                    _this.myfriends.push(allfriends[i]);
                    CounterFriends_1++;
                } //end for
                //////console.log ("this.cnter "+ this.cnter);
                //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
                var friendsSoFar_1 = 0;
                var _loop_1 = function (y) {
                    if (true) {
                        _this.firebuddychats.child(_this.userId).child(_this.myfriends[y].uid).limitToLast(1).once('value', function (snapshot_last_msg) {
                            var lastMsg__ = snapshot_last_msg.val();
                            if (lastMsg__) {
                                for (var key in lastMsg__) {
                                    var lastMsgActual__ = {
                                        message: lastMsg__[key].message,
                                        type: lastMsg__[key].type,
                                        dateofmsg: lastMsg__[key].dateofmsg,
                                        timeofmsg: lastMsg__[key].timeofmsg,
                                        referral_type: lastMsg__[key].referral_type,
                                        selectCatId: lastMsg__[key].selectCatId,
                                        isRead: false,
                                        isStarred: false
                                    };
                                    _this.myfriends[y].unreadmessage = lastMsgActual__;
                                    _this.myfriends[y].lastMessage = lastMsgActual__;
                                    break;
                                } //end for
                            } //endif
                            if (++friendsSoFar_1 >= CounterFriends_1) {
                                //Make sure it run only on last one and only once
                                //console.log ("Publish Friends Event in Chat.ts ");
                                _this.events.publish('friends'); //jaswinder
                            }
                        });
                        _this.subscriptionAddedFriendsObj[_this.userId + "/" + _this.myfriends[y].uid] = true;
                    }
                    else {} //endif
                };
                for (var y in _this.myfriends) {
                    _loop_1(y);
                } //end for
            }
            else {
                // I dont have any friend (:
                //if(null == this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid]) {
                _this.events.publish('friends');
                //  this.subscriptionAddedFriendsObj [this.userId + "/" + this.myfriends[y].uid] = true;
                //  }//endif
            } //endif
        }); //get all friends
    };
    RequestsService.prototype.subscribeToReadingLastMsg = function () {
        var _this = this;
        this.lastMsgReader = {};
        var _loop_2 = function (y) {
            //console.log ("Friend " + y);
            if (null == this_1.subscriptionAddedObj[this_1.userId + "/" + this_1.myfriends[y].uid]) {
                //console.log ("->Add Subscription To : " +this.userId + "/" + this.myfriends[y].uid);
                this_1.firebuddychats.child(this_1.userId).child(this_1.myfriends[y].uid).limitToLast(1).on('child_added', function (snapshot_last_msg) {
                    //console.log ("Inside firebuddychats ()");
                    var lastMsg__ = snapshot_last_msg.val();
                    //console.log (lastMsg__);
                    //if(lastMsg__){
                    _this.lastMsgReader[_this.myfriends[y].uid] = ({
                        message: lastMsg__.message,
                        type: lastMsg__.type,
                        sentby: lastMsg__.sentby,
                        dateofmsg: lastMsg__.dateofmsg,
                        timeofmsg: lastMsg__.timeofmsg,
                        request_to_release_incentive: lastMsg__.request_to_release_incentive,
                        isRead: false,
                        isStarred: false
                    });
                    //if(!this.new_chat_message_flag){
                    _this.events.publish('new_chat_message');
                    //console.log ("Publishing event new_chat_message");
                    //this.new_chat_message_flag = true;
                    //}
                    //}//endif
                });
                this_1.subscriptionAddedObj[this_1.userId + "/" + this_1.myfriends[y].uid] = true;
            } //endif
        };
        var this_1 = this;
        //console.log ("subscribeToReadingLastMsg");
        for (var y in this.myfriends) {
            _loop_2(y);
        } //end for
    };
    /*
      getmyfriends_NOTEUSED() {
        let friendsuid = [];
        this.firefriends.child(this.userId).once('value', (snapshot) => {
          let allfriends = snapshot.val();
          if (allfriends != null) {
            for (var i in allfriends) {
              friendsuid.push(allfriends[i]);
            }
          }
    
          this.userservice.getallusers_modified(friendsuid).then((users) => {
            this.myfriends = [];
            let counter = 0;
            if (friendsuid.length > 0) {
              for (var j in friendsuid) {
                for (var key in users) {
                  if (friendsuid[j].uid === users[key].uid) {
                    users[key].isActive = friendsuid[j].isActive;
                    users[key].userprio = friendsuid[j].userprio ? friendsuid[j].userprio : null;
                    let userKey;
                    userKey = users[key];
                    let flag1 = false;
                    let flag2 = false;
                    let flag3 = false;
                    this.userservice.getuserblock(users[key]).then((res) => {
                      userKey.isBlock = res;
                      flag1 = true;
                    })
                    this.get_last_unreadmessage(users[key]).then(responce => {
                      userKey.unreadmessage = responce;
                      flag2 = true;
                    })
                    this.get_last_msg(users[key]).then(resp => {
                      userKey.lastMessage = resp;
                      flag3 = true;
                      this.myfriends.push(userKey);
                      counter++;
                      if (counter == friendsuid.length) {
                      }
                    })
                  }
                }
              }
            } else {
              if (friendsuid.length == 0) {
              }
            }
          }).catch((err) => {
          })
    
        })
      }
    */
    RequestsService.prototype.get_last_msg = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firebuddychats.child(_this.userId).child(buddy.uid).orderByChild('uid').once('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    var lastmsg = allmsg[Object.keys(allmsg)[Object.keys(allmsg).length - 1]];
                    resolve(lastmsg);
                }
                else {
                    resolve('');
                }
            });
        }).catch(function (error) {
            ////console.log ("Error 7423");////console.log (error);
        });
    };
    RequestsService.prototype.get_last_unreadmessage = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var allunreadmessage = [];
            _this.firebuddychats.child(buddy.uid).child(_this.userId).orderByChild('uid').on('value', function (snapshot) {
                var allmsg = snapshot.val();
                if (allmsg != null) {
                    for (var tmpkey in allmsg) {
                        if (allmsg[tmpkey].isRead == false) {
                            allunreadmessage.push(allmsg);
                        }
                    }
                }
                if (allunreadmessage.length > 0) {
                    resolve(allunreadmessage.length);
                }
                else {
                    resolve(allunreadmessage.length);
                }
            });
        }).catch(function (error) {
            ////console.log ("Error 7842");////console.log (error);
        });
    };
    RequestsService.prototype.pendingetmy__requests = function (userd) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(userd).once('value', function (snapshot) {
                resolve(snapshot.val());
            });
        }).catch(function (error) {
            ////console.log ("Error 7234");////console.log (error);
        });
    };
    RequestsService.prototype.pendingetmyrequests = function (userd) {
        //////console.log ("pendingetmyrequests");
        //////console.log (userd);
        //////console.log (this.userId);
        //////console.log ("pendingetmyrequests END");
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('userUID').then(function (userId__) {
                _this.userId = userId__;
                if (!userd || userd == undefined) {
                    userd = _this.userId;
                }
                var allmyrequests;
                var myrequests = [];
                _this.firereq.child(userd).once('value', function (snapshot) {
                    allmyrequests = snapshot.val();
                    myrequests = [];
                    for (var i in allmyrequests) {
                        myrequests.push(allmyrequests[i].sender);
                    }
                    _this.userservice.getallusers_modified(myrequests).then(function (allusers) {
                        var pendinguserdetails = [];
                        for (var j in myrequests)
                            for (var key in allusers) {
                                if (myrequests[j] == allusers[key].uid) {
                                    pendinguserdetails.push(allusers[key]);
                                }
                            }
                        resolve(pendinguserdetails);
                    });
                });
            }).catch(function (e) { });
        }).catch(function (error) {
            ////console.log ("Error 1652");////console.log (error);
        });
    };
    RequestsService.prototype.blockuserprof = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var requestFriends = snapshot.val();
                for (var tmpkey in requestFriends) {
                    if (requestFriends[tmpkey].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(tmpkey).update({ isBlock: true });
                        resolve(true);
                    }
                }
            });
        }).catch(function (error) {
            ////console.log ("Error 9645");////console.log (error);
        });
    };
    RequestsService.prototype.getAllBlockRequest = function () {
        var _this = this;
        this.tpStorageService.getItem('userUID').then(function (userId__) {
            _this.userId = userId__;
            _this.getAllBlockRequest_();
        }).catch(function (e) { });
    };
    RequestsService.prototype.getAllBlockRequest_ = function () {
        var _this = this;
        this.firereq.child(this.userId).orderByChild('uid').once('value', function (snapshot) {
            var allrequest = snapshot.val();
            var blockuser = [];
            for (var key in allrequest) {
                if (allrequest[key].isBlock) {
                    _this.firedata.child(allrequest[key].sender).once('value', function (snapsho) {
                        blockuser.push(snapsho.val());
                    }).catch(function (err) {
                    });
                }
                _this.blockRequest = [];
                _this.blockRequest = blockuser;
            }
            _this.events.publish('block-request');
        });
    };
    RequestsService.prototype.unblockRequest = function (buddy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.firereq.child(_this.userId).orderByChild('uid').once('value', function (snapshot) {
                var allfriends = snapshot.val();
                for (var key in allfriends) {
                    if (allfriends[key].sender == buddy.uid) {
                        _this.firereq.child(_this.userId).child(key).update({ isBlock: false });
                        resolve(true);
                    }
                }
            });
        }).catch(function (error) {
            ////console.log ("Error 1745");////console.log (error);
        });
    };
    RequestsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_3__["TpstorageProvider"]])
    ], RequestsService);
    return RequestsService;
}());



/***/ }),

/***/ "./src/services/sms.service.ts":
/*!*************************************!*\
  !*** ./src/services/sms.service.ts ***!
  \*************************************/
/*! exports provided: SmsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmsService", function() { return SmsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app/tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _ionic_native_sms_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/sms/ngx */ "./node_modules/@ionic-native/sms/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SmsService = /** @class */ (function () {
    function SmsService(platform, sms, tpStorageService) {
        this.platform = platform;
        this.sms = sms;
        this.tpStorageService = tpStorageService;
        this.iosDelay = 300;
        this.simulateSMS = false;
        this.options = {
            replaceLineBreaks: false,
            android: {
                intent: 'INTENT' // send SMS with the native android SMS messaging
                //intent: '' // send SMS without opening any other app
            }
        };
    }
    SmsService.prototype.sendSmsCustomMsg = function (mobile, msg) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.simulateSMS) {
                resolve(true);
            }
            else {
                try {
                    if (_this.platform.is('ios')) {
                        window.location.href = 'sms:' + mobile + '?&body=' + encodeURIComponent(msg);
                        resolve(true);
                    }
                    else {
                        _this.sms.send(mobile, msg, _this.options).then(function (res) {
                            resolve(true);
                        }).catch(function (err) {
                            reject(err);
                        });
                    } //endif
                }
                catch (e) {
                }
            } //endif
        });
    }; //end function
    SmsService.prototype.sendSms = function (mobileIn) {
        var _this = this;
        //let mobile = mobileIn.toString();
        var mobile = mobileIn;
        return new Promise(function (resolve, reject) {
            _this.tpStorageService.getItem('business_created').then(function (business_created_raw) {
                //business created
                var msg = "Hi, I'm offering referral incentives. Please download my app at tapally.com and start earning";
                if (_this.platform.is('ios')) {
                    window.location.href = 'sms:' + mobile + '?&body=' + encodeURIComponent(msg);
                    resolve(true);
                }
                else {
                    _this.sms.send(mobile, msg, _this.options).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                } //endif
            }).catch(function (e) {
                //no business registered for this user
                var msg = "Hi, please download app at TapAlly.com and start earning";
                //timeout to prevent issue we are getting on IOS device. Keyboard dissapear : https://github.com/cordova-sms/cordova-sms-plugin/issues/22
                if (_this.platform.is('ios')) {
                    window.location.href = 'sms:' + mobile + '?&body=' + encodeURIComponent(msg);
                    resolve(true);
                }
                else {
                    _this.sms.send(mobile, msg, _this.options).then(function (res) {
                        resolve(true);
                    }).catch(function (err) {
                        reject(err);
                    });
                } //endif
            });
        });
    }; //end function
    SmsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"], _ionic_native_sms_ngx__WEBPACK_IMPORTED_MODULE_3__["SMS"], _app_tpstorage_service__WEBPACK_IMPORTED_MODULE_2__["TpstorageProvider"]])
    ], SmsService);
    return SmsService;
}());



/***/ })

}]);
//# sourceMappingURL=contact-contact-module.js.map