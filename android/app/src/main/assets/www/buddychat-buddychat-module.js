(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["buddychat-buddychat-module"],{

/***/ "./src/app/buddychat/buddychat.module.ts":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.module.ts ***!
  \***********************************************/
/*! exports provided: BuddychatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPageModule", function() { return BuddychatPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _buddychat_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./buddychat.page */ "./src/app/buddychat/buddychat.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _buddychat_page__WEBPACK_IMPORTED_MODULE_5__["BuddychatPage"]
    }
];
var BuddychatPageModule = /** @class */ (function () {
    function BuddychatPageModule() {
    }
    BuddychatPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_buddychat_page__WEBPACK_IMPORTED_MODULE_5__["BuddychatPage"]]
        })
    ], BuddychatPageModule);
    return BuddychatPageModule;
}());



/***/ }),

/***/ "./src/app/buddychat/buddychat.page.html":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Anguls Dev</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content #content padding class=\"buddychat-content\">\n\n  <ng-container *ngFor=\"let items  of allmessagesGroups ; let i = index;let last = last;\">\n     {{last ? callFunctionScroll() : ''}}\n      <ion-list-header class=\"dayheader\" *ngIf=\"items.dateofmsg == todaydate\" align=\"center\">Today</ion-list-header>\n      <ion-list-header class=\"dayheader\" *ngIf=\"items.dateofmsg != todaydate\" align=\"center\">{{items.dateofmsg}}</ion-list-header>\n      <div *ngFor=\"let item of items.message_item; \">\n\n\n          <div  [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\"\n          *ngIf=\"item.type == 'message' && (!item.selectCatId || item.selectCatId<=0) &&\n          !item.referral_type && !item.request_to_release_incentive && !item.referral_redeemed\">\n            <p>{{item.message}}</p>\n          </div>\n\n          <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.type == 'message' && item.referral_redeemed\">\n              <h2 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Incentives Redeemed</span></h2>\n              <p class=\"typeofreferral\">Your request to redeem incentives has been approved</p>\n              <div><ion-badge (click)=\"fnSendToEarningsPage()\"  color=\"dark\" class=\"savecnt\" text-wrap >View Details</ion-badge></div>\n         </div>\n\n\n              <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.type == 'message' && item.request_to_release_incentive\">\n                  <h2 (click)=\"sendReferral (buddy, item.selectCatId)\" class=\"headinginchat\"><span>Redeem Incentives</span></h2>\n                  <p class=\"typeofreferral\">Redeem Incentives For Referral Request</p>\n                  <div><ion-badge (click)=\"fnviewincentiverequst(item)\"  color=\"dark\" class=\"savecnt\" text-wrap >View Redeem Request</ion-badge></div>\n             </div>\n\n           <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.type == 'message' && item.referral_type && item.referral_type == 'backward'\">\n               <h2   class=\"headinginchat\"><span>Referral Sent</span></h2>\n               <p class=\"typeofreferral\">Hi, I am sending a referral to you</p>\n               <div *ngIf=\"item.referral.mobile\">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n               <div *ngIf=\"!item.referralSavedOnPhone\"><ion-badge (click)=\"saveContactInPhone(item)\"  color=\"primary\" class=\"savecnt\" text-wrap >Save Contact</ion-badge></div>\n               <div *ngIf=\"item.referralSavedOnPhone\"><ion-badge   color=\"secondary\" class=\"savecnt\" text-wrap >Contact Saved</ion-badge></div>\n           </div>\n\n           <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.type == 'message' && item.referral_type && item.referral_type == 'forward'\">\n               <h2   class=\"headinginchat\"><span> Referral Forwarded</span></h2>\n               <p class=\"typeofreferral\">Hi, I forwarded your referral to</p>\n               <div *ngIf=\"item.referral.mobile\">{{item.referral.displayName}} ({{item.referral.mobile}})</div>\n               <div *ngIf=\"!item.referralSavedOnPhone\"><ion-badge (click)=\"saveContactInPhone(item)\"  color=\"primary\" class=\"savecnt\" text-wrap >Save Contact</ion-badge></div>\n               <div *ngIf=\"item.referralSavedOnPhone\"><ion-badge   color=\"secondary\" class=\"savecnt\" text-wrap >Contact Saved</ion-badge></div>\n           </div>\n\n           <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.type == 'message' && item.selectCatId && item.selectCatId>0\">\n               <h2 (click)=\"sendReferral (buddy, item.selectCatId)\" class=\"headinginchat\"><span>Referral Request</span> <ion-icon class=\"referralicon\"    item-end name='arrow-dropright-circle'></ion-icon></h2>\n               <p class=\"typeofreferral\">Hi, Can you send me a reliable referral for <ion-badge  class=\"catname\" text-wrap >{{item.selectCatName}}</ion-badge></p>\n               <div *ngIf=\"item.message && !item.request_to_release_incentive && !item.referral_redeemed\">\"{{item.message}}\"</div>\n           </div>\n\n           <div [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"item.multiMessage && item.type=='image'\">\n               <div (click)=\"openAllImg(item.message,$event)\" class=\"selection-data\">\n                   <img class=\"msg-partision\" *ngFor=\"let pic of item.message | slice:0:4; let ii = index \"\n                       src=\"{{pic}}\" />\n                   <span class=\"img-number overlay\">+ {{item.message.length - 4}}</span>\n               </div>\n           </div>\n\n           <div (click)=\"openAllImg(item.message,$event)\" [ngClass]=\"(item.sentby==buddy_uid) ? 'buddychat' : 'mychat'\" *ngIf=\"!item.multiMessage && item.type == 'image'\">\n               <img src=\"{{item.message}}\" *ngIf=\"item.type == 'image'\" class=\"msg\" #mychatImage\n                   />\n           </div>\n\n      </div>\n   </ng-container>\n</ion-content>\n\n<ion-footer ion-fixed>\n        <ion-toolbar class=\"no-border\" color=\"white\">\n          <!-- Textarea in an item with a placeholder -->\n          <ion-item lines=\"none\">\n            <ion-textarea [(ngModel)]=\"newmessage\" placeholder=\"Type to send message\"></ion-textarea>\n            <ion-buttons end>\n              <a ion-button (click)=\"openAttachment($event)\">\n                  <ion-icon class=\"ion-image iconmediumsize\" ios=\"md-attach\" md=\"md-attach\"></ion-icon>\n              </a>\n              <ion-button (click)=\"addmessage_async()\" color=\"primary\"><ion-icon class=\"iconmediumsize\" name=\"send\"></ion-icon></ion-button>\n            </ion-buttons>\n          </ion-item>\n\n        </ion-toolbar>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.scss":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background-shade);\n  --page-tags-gutter: 5px;\n  --page-color-radio-items-per-row: 5;\n  --page-color-radio-gutter: 2%;\n  --page-color-radio-width: calc((100% - (2 * var(--page-color-radio-gutter) * var(--page-color-radio-items-per-row))) / var(--page-color-radio-items-per-row)); }\n\n.buddychat-content {\n  --background: var(--page-background); }\n\n.buddychat-content ion-item-divider {\n    --background: var(--page-background);\n    --padding-bottom: calc(var(--page-margin) / 2);\n    --padding-top: calc(var(--page-margin) / 2);\n    --padding-start: var(--page-margin);\n    --padding-end: var(--page-margin);\n    border: none; }\n\n.headinginchat {\n  border-bottom: 1px solid #ccc; }\n\n.messages-container {\n  min-height: 100%;\n  width: 100%;\n  position: relative;\n  display: -webkit-box;\n  display: flex;\n  overflow: hidden;\n  margin: 0 auto; }\n\n.buddychat, .mychat {\n  padding: 0px 5px;\n  margin: 5px;\n  background-color: #fff;\n  width: 80%;\n  font-size: 15px;\n  clear: both; }\n\n.buddychat {\n  border-radius: 0px 10px 10px 10px;\n  border: 1px solid #444;\n  color: #fff;\n  background-color: #444;\n  float: left; }\n\n.mychat {\n  border-radius: 10px 10px 0px 10px;\n  border: 1px solid #fff;\n  float: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvYnVkZHljaGF0L2J1ZGR5Y2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxxQ0FBYztFQUNkLDhDQUFrQjtFQUVsQix1QkFBbUI7RUFFbkIsbUNBQWlDO0VBQ2pDLDZCQUEwQjtFQUMxQiw2SkFBeUIsRUFBQTs7QUFHM0I7RUFDRSxvQ0FBYSxFQUFBOztBQURmO0lBSUUsb0NBQWE7SUFDYiw4Q0FBaUI7SUFDakIsMkNBQWM7SUFDZCxtQ0FBZ0I7SUFDaEIsaUNBQWM7SUFFZCxZQUFZLEVBQUE7O0FBR2Q7RUFDRSw2QkFBNkIsRUFBQTs7QUFFL0I7RUFDSSxnQkFBZ0I7RUFDaEIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixvQkFBYTtFQUFiLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsY0FBYyxFQUFBOztBQUVsQjtFQUNFLGdCQUFnQjtFQUNoQixXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLFVBQVU7RUFDVixlQUFlO0VBQ2YsV0FBVSxFQUFBOztBQUVaO0VBQ0UsaUNBQWlDO0VBQ2pDLHNCQUFzQjtFQUN0QixXQUFVO0VBQ1Ysc0JBQXNCO0VBQ3RCLFdBQVUsRUFBQTs7QUFFWjtFQUNFLGlDQUFpQztFQUNqQyxzQkFBc0I7RUFDdEIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYnVkZHljaGF0L2J1ZGR5Y2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLWZhaXItbWFyZ2luKTtcbiAgLS1wYWdlLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kLXNoYWRlKTtcblxuICAtLXBhZ2UtdGFncy1ndXR0ZXI6IDVweDtcblxuICAtLXBhZ2UtY29sb3ItcmFkaW8taXRlbXMtcGVyLXJvdzogNTtcbiAgLS1wYWdlLWNvbG9yLXJhZGlvLWd1dHRlcjogMiU7XG4gIC0tcGFnZS1jb2xvci1yYWRpby13aWR0aDogY2FsYygoMTAwJSAtICgyICogdmFyKC0tcGFnZS1jb2xvci1yYWRpby1ndXR0ZXIpICogdmFyKC0tcGFnZS1jb2xvci1yYWRpby1pdGVtcy1wZXItcm93KSkpIC8gdmFyKC0tcGFnZS1jb2xvci1yYWRpby1pdGVtcy1wZXItcm93KSk7XG59XG5cbi5idWRkeWNoYXQtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcblxuXHRpb24taXRlbS1kaXZpZGVyIHtcblx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XG5cdFx0LS1wYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblx0XHQtLXBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuXHRcdC0tcGFkZGluZy1zdGFydDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuXHRcdC0tcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcblxuXHRcdGJvcmRlcjogbm9uZTtcblx0fVxufVxuLmhlYWRpbmdpbmNoYXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbn1cbi5tZXNzYWdlcy1jb250YWluZXIge1xuICAgIG1pbi1oZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBtYXJnaW46IDAgYXV0bztcbn1cbi5idWRkeWNoYXQsLm15Y2hhdCB7XG4gIHBhZGRpbmc6IDBweCA1cHg7XG4gIG1hcmdpbjogNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICB3aWR0aDogODAlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGNsZWFyOmJvdGg7XG59XG4uYnVkZHljaGF0IHtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDEwcHggMTBweCAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjNDQ0O1xuICBjb2xvcjojZmZmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDQ0O1xuICBmbG9hdDpsZWZ0O1xufVxuLm15Y2hhdCB7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHggMTBweCAwcHggMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.ts":
/*!*********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.ts ***!
  \*********************************************/
/*! exports provided: BuddychatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPage", function() { return BuddychatPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tpstorage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _services_cats_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/cats.service */ "./src/services/cats.service.ts");
/* harmony import */ var _viewphoto_modal_viewphoto_modal_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../viewphoto-modal/viewphoto-modal.page */ "./src/app/viewphoto-modal/viewphoto-modal.page.ts");
/* harmony import */ var _app_angularfireconfig__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app.angularfireconfig */ "./src/app/app.angularfireconfig.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _services_chat_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/chat.service */ "./src/services/chat.service.ts");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_9__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











if (!firebase_app__WEBPACK_IMPORTED_MODULE_9__["apps"].length) {
    firebase_app__WEBPACK_IMPORTED_MODULE_9__["initializeApp"](_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_6__["config"]);
}
var BuddychatPage = /** @class */ (function () {
    function BuddychatPage(route, tpStorageService, events, router, userservice, chatservice, modalController, catservice, actionSheetController) {
        this.route = route;
        this.tpStorageService = tpStorageService;
        this.events = events;
        this.router = router;
        this.userservice = userservice;
        this.chatservice = chatservice;
        this.modalController = modalController;
        this.catservice = catservice;
        this.actionSheetController = actionSheetController;
        this.firebuddychats = firebase_app__WEBPACK_IMPORTED_MODULE_9__["database"]().ref('/buddychats');
        this.newmessage = '';
        this.newmessageTmp = '';
        this.isRefresher = false;
        this.allmessages = [];
        this.filterallmessages = [];
        this.tempFilterallmessages = [];
        this.buddy = { uid: "" };
        this.buddymessages = [];
        this.allmessagesGroups = [];
        this.isData = false;
        this.datacounter = 50;
        this.headercopyicon = true;
        this.showheader = true;
        this.selectAllMessage = [];
        this.selectCounter = 0;
        this.isuserBlock = false;
        this.msgstatus = false;
        this.backbuttonstatus = false;
    }
    BuddychatPage.prototype.scrollToBottomOnInit = function () {
        console.log("first Time scroll");
        this.content.scrollToBottom(300);
    };
    BuddychatPage.prototype.callFunctionScroll = function () {
        if (this.content._scroll) {
            console.log("Scrolled callFunctionScroll");
            setTimeout(function () { this.content.scrollToBottom(0); }, 3000);
        }
    };
    BuddychatPage.prototype.initializeItems = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.catItems = this.catservice.getCats();
                return [2 /*return*/];
            });
        });
    };
    BuddychatPage.prototype.openAllImg = function (url, $event) {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(url);
                        return [4 /*yield*/, this.modalController.create({
                                component: _viewphoto_modal_viewphoto_modal_page__WEBPACK_IMPORTED_MODULE_5__["ViewphotoModalPage"],
                                componentProps: {
                                    'url': url
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    BuddychatPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.tpStorageService.get('userUID').then(function (myUserId) {
            _this.userId = myUserId;
            console.log("my USERID");
            console.log(_this.userId);
            _this.initializeItems();
            _this.todaydate = _this.formatDate(new Date());
            _this.getbuddymessages(_this.datacounter);
        }).catch(function (e) {
            //mytodo : redirect to login page
        });
    };
    BuddychatPage.prototype.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();
        if (dd < 10)
            dd = '0' + dd;
        if (mm < 10)
            mm = '0' + mm;
        return dd + '/' + mm + '/' + yyyy;
    };
    BuddychatPage.prototype.sortdata = function (data) {
        return data.sort(function (a, b) {
            var keyA = new Date(a.date), keyB = new Date(b.date);
            // Compare the 2 dates
            if (keyA < keyB)
                return -1;
            if (keyA > keyB)
                return 1;
            return 0;
        });
    };
    BuddychatPage.prototype.getbuddymessages = function (limit) {
        var _this = this;
        this.buddymessages = [];
        console.log("firebuddychats : " + this.userId + "/" + this.buddy_uid);
        this.firebuddychats.child(this.userId).child(this.buddy_uid).limitToLast(limit).on('child_added', function (snapshot) {
            //Check for unique value
            var tmpObj = snapshot.val();
            console.log("Got new message from Firebase");
            //rememberme assign all keys
            /*
            Object.keys(tmpObj).forEach(ele => {
              console.log (ele);
            });
            */
            var isMatch = false;
            for (var myKey in _this.buddymessages) {
                if (_this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
                    _this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
                    _this.buddymessages[myKey]['message'] == tmpObj.message &&
                    _this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
                    _this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
                    _this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
                    _this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
                    _this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
                    _this.buddymessages[myKey]['type'] == tmpObj.type) {
                    //This is redundant message
                    isMatch = true;
                    break;
                }
            } //end for
            if (!isMatch) {
                _this.buddymessages.push(tmpObj);
                _this.MessageSubscription();
                //console.log ("publishing event :"+tmpObj.messageId);
            }
        });
    };
    BuddychatPage.prototype.addmessage_async = function () {
        var _this = this;
        //this.newmessageTmp = this.newmessage;
        //this.newmessage = '';
        //////////console.log ("addmessage_async");
        setTimeout(function () {
            _this.addmessage();
        }, 1);
    };
    BuddychatPage.prototype.addmessage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                //this.closeButtonClick()
                this.newmessageTmp = this.newmessage;
                //this.newmessage = '';
                this.UnreadMSG = 0;
                this.selectAllMessage = [];
                this.selectCounter = 0;
                this.headercopyicon = true;
                this.userservice.getstatusblock(this.buddy).then(function (res) {
                    //////////console.log ("-getstatus");
                    if (!res) {
                        _this.newmessageTmp = _this.newmessageTmp.trim();
                        if (_this.newmessageTmp != '') {
                            //////////console.log ("-if message is not empty");
                            //this.fcm.sendNotification(this.buddy, 'You have received a message', 'chatpage');
                            //this.events.unsubscribe('newmessage');//to avoid triggring multiple calls
                            //mytodo : above fix doesn`t work. Please try something else
                            _this.chatservice.addnewmessage(_this.newmessageTmp, 'message', _this.buddyStatus).then(function () {
                                _this.newmessage = '';
                                _this.scrollto();
                                _this.msgstatus = false;
                            });
                        }
                        else {
                            _this.newmessage = _this.newmessageTmp;
                            //this.loading.presentToast('Please enter valid message...');
                        }
                    }
                    else {
                        //user might be blocked
                        _this.newmessage = _this.newmessageTmp;
                        _this.openOptions();
                    }
                });
                return [2 /*return*/];
            });
        });
    }; //end function
    BuddychatPage.prototype.presentActionSheet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Unblock this person to send a message',
                            buttons: [{
                                    text: 'UNBLOCK',
                                    icon: 'contact',
                                    handler: function () {
                                        console.log('Delete clicked');
                                    }
                                }, {
                                    text: 'Share',
                                    icon: 'share',
                                    handler: function () {
                                        console.log('Share clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BuddychatPage.prototype.openOptions = function () {
        return __awaiter(this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Unblock this person to send a message.',
                            buttons: [{
                                    text: 'UNBLOCK',
                                    icon: 'contact',
                                    handler: function () {
                                        _this.userservice.unblockUser(_this.buddy).then(function (res) {
                                            if (res) {
                                                _this.addmessage();
                                            }
                                        });
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BuddychatPage.prototype.scrollto = function () {
        /*
          setTimeout(() => {
              if(this.content._scroll)  {
                this.content.scrollToBottom();
              }
          }, 1000);
          */
    };
    BuddychatPage.prototype.scrollToBottom_ = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToBottom(1);
            }
        }, 1);
    };
    BuddychatPage.prototype.openAttachment = function () {
        return __awaiter(this, void 0, void 0, function () {
            var actionSheet;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.actionSheetController.create({
                            header: 'Share Image',
                            buttons: [{
                                    text: 'Camera',
                                    icon: 'camera',
                                    handler: function () {
                                        console.log('camera clicked');
                                    }
                                }, {
                                    text: 'Photo Gallery',
                                    icon: 'image',
                                    handler: function () {
                                        console.log('Photo Gallery clicked');
                                    }
                                }, {
                                    text: 'Cancel',
                                    icon: 'close',
                                    role: 'cancel',
                                    handler: function () {
                                        console.log('Cancel clicked');
                                    }
                                }]
                        })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    BuddychatPage.prototype.MessageSubscription = function () {
        console.log("MessageSubscription");
        this.allmessages = [];
        this.allmessagesGroups = [];
        for (var msgKey in this.buddymessages) {
            if (null == this.buddymessages[msgKey].dateofmsg) {
                continue;
            }
            this.buddymessages[msgKey].selection = false;
            this.buddymessages[msgKey].selectCatName = '';
            if (this.buddymessages[msgKey].referral_type == null) {
                this.buddymessages[msgKey].referral_type = "";
            }
            if (this.buddymessages[msgKey].selectCatId != null) {
                for (var g = 0; g < this.catItems.length; g++) {
                    if (this.catItems[g]['id'] == this.buddymessages[msgKey].selectCatId) {
                        this.buddymessages[msgKey].selectCatName = this.catItems[g]['name'];
                        break;
                    }
                }
            }
            var tmpArr = {
                'dateofmsg': this.buddymessages[msgKey].dateofmsg,
                'message_item': [this.buddymessages[msgKey]]
            };
            //Check if data exists
            var foundKey = false;
            for (var myKey in this.allmessagesGroups) {
                if (this.allmessagesGroups[myKey].dateofmsg == this.buddymessages[msgKey].dateofmsg) {
                    this.allmessagesGroups[myKey].message_item.push(this.buddymessages[msgKey]);
                    foundKey = true;
                    break;
                }
            } //end for
            if (!foundKey) {
                this.allmessagesGroups.push(tmpArr);
            }
            this.allmessages.push(this.buddymessages[msgKey]); //this is used for search the chats
        } //end for
        console.log("Done ji");
        console.log(this.allmessagesGroups);
        this.scrollToBottomOnInit();
    };
    BuddychatPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            if (params && params.buddy_uid) {
                _this.buddy_uid = params.buddy_uid;
                _this.buddy = { "uid": _this.buddy_uid };
                console.log("buddy_uid");
                console.log(_this.buddy_uid);
            }
        });
        this.scrollToBottomOnInit();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('content'),
        __metadata("design:type", Object)
    ], BuddychatPage.prototype, "content", void 0);
    BuddychatPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-buddychat',
            template: __webpack_require__(/*! ./buddychat.page.html */ "./src/app/buddychat/buddychat.page.html"),
            styles: [__webpack_require__(/*! ./buddychat.page.scss */ "./src/app/buddychat/buddychat.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _tpstorage_service__WEBPACK_IMPORTED_MODULE_3__["TpstorageProvider"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
            _services_chat_service__WEBPACK_IMPORTED_MODULE_8__["ChatService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _services_cats_service__WEBPACK_IMPORTED_MODULE_4__["CatsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]])
    ], BuddychatPage);
    return BuddychatPage;
}());



/***/ })

}]);
//# sourceMappingURL=buddychat-buddychat-module.js.map