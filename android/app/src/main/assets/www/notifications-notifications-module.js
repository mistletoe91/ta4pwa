(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notifications-notifications-module"],{

/***/ "./src/app/notifications/notifications.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/notifications/notifications.module.ts ***!
  \*******************************************************/
/*! exports provided: NotificationsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsPageModule", function() { return NotificationsPageModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _notifications_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notifications.page */ "./src/app/notifications/notifications.page.ts");
/* harmony import */ var _notifications_notifications_resolver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../notifications/notifications.resolver */ "./src/app/notifications/notifications.resolver.ts");
/* harmony import */ var _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../notifications/notifications.service */ "./src/app/notifications/notifications.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var NotificationsPageModule = /** @class */ (function () {
    function NotificationsPageModule() {
    }
    NotificationsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _notifications_page__WEBPACK_IMPORTED_MODULE_6__["NotificationsPage"],
                        resolve: {
                            data: _notifications_notifications_resolver__WEBPACK_IMPORTED_MODULE_7__["NotificationsResolver"]
                        }
                    }
                ])
            ],
            declarations: [_notifications_page__WEBPACK_IMPORTED_MODULE_6__["NotificationsPage"]],
            providers: [
                _notifications_notifications_resolver__WEBPACK_IMPORTED_MODULE_7__["NotificationsResolver"],
                _notifications_notifications_service__WEBPACK_IMPORTED_MODULE_8__["NotificationsService"]
            ]
        })
    ], NotificationsPageModule);
    return NotificationsPageModule;
}());



/***/ }),

/***/ "./src/app/notifications/notifications.page.html":
/*!*******************************************************!*\
  !*** ./src/app/notifications/notifications.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Contacts\n    </ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\" (click)=\"askForReferral()\"  name=\"add-circle\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content   class=\"notifications-content\">\n\n\n  <ng-container >\n    <ion-item-group>\n      <!-- Searchbar with a placeholder -->\n      <ion-searchbar (ionInput)=\"getItems($event)\" placeholder=\"Filter Contacts\"></ion-searchbar>\n      <ion-item  detail=\"false\"  class=\"notification-item\" lines=\"none\" *ngFor=\"let friend of myfriends\">\n        <ion-row class=\"notification-item-wrapper\">\n          <ion-col size=\"2\"  (click)=\"gotobuddypage(friend)\">\n            <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n              <app-image-shell class=\"notification-image\" [src]=\"friend.photoURL\" [alt]=\"'user image'\"></app-image-shell>\n            </app-aspect-ratio>\n          </ion-col>\n          <ion-col class=\"details-wrapper\" (click)=\"gotobuddypage(friend)\">\n            <h2 class=\"details-name\">\n              <app-text-shell animation=\"bouncing\" [data]=\"friend.displayName\"></app-text-shell>\n              <ion-note text-wrap *ngIf=\"friend.business\">\n                <app-text-shell   [data]=\"friend.business\"></app-text-shell>\n              </ion-note>\n            </h2>\n            <p  [ngClass]=\"(friend.read) ? 'cssread' : 'cssnotread'\"    class=\"details-description\">\n              <span *ngIf=\"friend.lastMessage.type == 'message' && !friend.lastMessage.message && ( friend.lastMessage.selectCatId || friend.lastMessage.referral_type)\" class=\"lastMSG\">Referral Request</span>\n              <span *ngIf=\"friend.lastMessage.type == 'message' && friend.lastMessage.message\" class=\"lastMSG\">{{friend.lastMessage.message}}</span>\n              <span *ngIf=\"friend.lastMessage.type == 'image'\" class=\"lastMSG\">\n                <ion-icon name=\"camera\" class=\"list\"></ion-icon> Photo\n              </span>\n              <span *ngIf=\"friend.lastMessage.type == 'contact'\" class=\"lastMSG\">\n                <ion-icon name=\"contact\" class=\"list\"></ion-icon> Contact\n              </span>\n              <span *ngIf=\"friend.lastMessage.type == 'audio'\" class=\"lastMSG\">\n                <ion-icon name=\"musical-note\" class=\"list\"></ion-icon> Audio\n              </span>\n              <span *ngIf=\"friend.lastMessage.type == 'document'\" class=\"lastMSG\">\n                <ion-icon name=\"document\" class=\"list\"></ion-icon> document\n              </span>\n              <span *ngIf=\"friend.lastMessage.type == 'location'\" class=\"lastMSG\">\n                <ion-icon name=\"locate\" class=\"list\"></ion-icon> location\n              </span>\n            </p>\n          </ion-col>\n\n          <ion-col size=\"2\" style=\"text-align:right;\"  (click)=\"sendReferral(friend.uid)\" >\n            <ion-icon  color=\"dark\"\n            class=\"iconrightfont\" name=\"arrow-dropright-circle\"></ion-icon>\n          </ion-col>\n\n        </ion-row>\n      </ion-item>\n    </ion-item-group>\n\n\n    <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button (click)=\"addbuddy()\">\n        <ion-icon name=\"person-add\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n\n  </ng-container>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/notifications/notifications.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/notifications/notifications.page.ts ***!
  \*****************************************************/
/*! exports provided: NotificationsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsPage", function() { return NotificationsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _const__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../const */ "./src/app/const.ts");
/* harmony import */ var _tpstorage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/loading.service */ "./src/services/loading.service.ts");
/* harmony import */ var _app_angularfireconfig__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../app.angularfireconfig */ "./src/app/app.angularfireconfig.ts");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_8__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { FcmService } from '../../services/fcm.service';


if (!firebase_app__WEBPACK_IMPORTED_MODULE_8__["apps"].length) {
    firebase_app__WEBPACK_IMPORTED_MODULE_8__["initializeApp"](_app_angularfireconfig__WEBPACK_IMPORTED_MODULE_7__["config"]);
}
var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(route, router, platform, loadingProvider, alertCtrl, sqlite, modalCtrl, toastCtrl, tpStorageService) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.platform = platform;
        this.loadingProvider = loadingProvider;
        this.alertCtrl = alertCtrl;
        this.sqlite = sqlite;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.tpStorageService = tpStorageService;
        this.firebuddychats = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/buddychats');
        this.firedata = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/chatusers');
        this.firereq = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/requests');
        this.firefriends = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/friends');
        this.fireInvitationSent = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/invitationsent');
        this.firePhones = firebase_app__WEBPACK_IMPORTED_MODULE_8__["database"]().ref('/phones');
        this.lastMsgReader = {};
        this.blockRequest = [];
        this.subscriptionAddedObj = {};
        this.subscriptionAddedFriendsObj = {};
        this.isNoRecord = false;
        this.tpStorageService.get('userUID').then(function (myUserId) {
            _this.userId = myUserId;
            setTimeout(function () {
                //This should be in constructor
                _this.getmyfriends_(false);
            }, 1);
        }).catch(function (e) {
            //mytodo : redirect to login page
        });
    }
    NotificationsPage.prototype.ionViewDidEnter = function () {
        this.fnGetLocalCache();
    };
    NotificationsPage.prototype.sendReferral = function (buddy_uid) {
        console.log("buddy_uid:" + buddy_uid);
    };
    NotificationsPage.prototype.initializeItems = function () {
        if (!this.myfriends_backup) {
            //init it first time
            this.myfriends_backup = this.myfriends;
        }
        else {
            //subsequent time pick from backup
            this.myfriends = this.myfriends_backup;
        }
    };
    NotificationsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.myfriends = this.myfriends.filter(function (item) {
                return (item.displayName.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    NotificationsPage.prototype.fnGetLocalCache = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            //Get Friends
            db.executeSql(_const__WEBPACK_IMPORTED_MODULE_4__["CONST"].create_table_statement_tapally_friends, [])
                .then(function (res) {
                db.executeSql("SELECT * FROM cache_tapally_friends", [])
                    .then(function (res) {
                    ////////console.log ("Going to read from  cache_tapally_friends <--------<<<<<-------------");
                    ////////console.log (res.rows.length + " Records");
                    if (res.rows.length > 0) {
                        //if row exists
                        ////////console.log ("RESETTING myfriends = []");
                        _this.myfriends = [];
                        var uniqueFriends = {};
                        _this.isNoRecord = false;
                        for (var i = 0; i < res.rows.length; i++) {
                            ////////////console.log (res.rows.item(i));
                            var isBlock_ = false;
                            if (res.rows.item(i).isBlock) {
                                isBlock_ = true;
                            }
                            var isActive_ = true;
                            if (!res.rows.item(i).isactive) {
                                isActive_ = false;
                            }
                            var record = {};
                            if (res.rows.item(i).gpflag > 0) {
                                //Group
                            }
                            else {
                                //////////////console.log ("gpflaggpflaggpflaggpflag= 0");
                                //Individual
                                var lastMssg = '';
                                if (res.rows.item(i).lastMessage_message && res.rows.item(i).lastMessage_message != "undefined") {
                                    lastMssg = res.rows.item(i).lastMessage_message;
                                }
                                var tmofmsg = '';
                                if (res.rows.item(i).timeofmsg) {
                                    tmofmsg = res.rows.item(i).timeofmsg;
                                }
                                record = {
                                    uid: res.rows.item(i).uid,
                                    displayName: res.rows.item(i).displayName,
                                    gpflag: res.rows.item(i).gpflag,
                                    isActive: isActive_,
                                    isBlock: isBlock_,
                                    lastMessage: {
                                        message: lastMssg,
                                        type: res.rows.item(i).lastMessage_type,
                                        dateofmsg: res.rows.item(i).lastMessage_dateofmsg,
                                        selectCatId: res.rows.item(i).selectCatId,
                                        referral_type: res.rows.item(i).referral_type,
                                        timeofmsg: tmofmsg,
                                        isRead: false,
                                        isStarred: false
                                    },
                                    mobile: res.rows.item(i).mobile,
                                    selection: res.rows.item(i).selection,
                                    unreadmessage: 0,
                                    photoURL: res.rows.item(i).photoURL,
                                    localphotoURL: '',
                                    deviceToken: res.rows.item(i).deviceToken
                                };
                            }
                            ////////console.log ("Check1 ");
                            if (res.rows.item(i).gpflag > 1) {
                                //group
                            }
                            else {
                                //individual
                                if (!uniqueFriends["individual" + res.rows.item(i).uid]) {
                                    var lengthOfArr = _this.myfriends.push(record);
                                    uniqueFriends["individual" + res.rows.item(i).uid] = true;
                                }
                            }
                        } //end for
                        console.log("From cache ");
                        console.log(_this.myfriends);
                        if (_this.myfriends.length == 0) {
                            _this.isNoRecord = true;
                        }
                    }
                    else {
                        //nothing in cache
                        //get from firebase
                        _this.getmyfriends_(true);
                    }
                })
                    .catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
            }); //create table
        }); //create database
    }; //end function
    //no need to send insert statement because contacts page will do that
    NotificationsPage.prototype.updateContactsTable = function (db, allChatListing__) {
        if (!allChatListing__.mobile || allChatListing__.mobile == "undefined") {
            return;
        }
        if (allChatListing__.photoURL == null) {
            allChatListing__.photoURL = ""; //mytodo set default pic
        }
        db.executeSql("UPDATE cache_tapally_contacts SET uid='" + allChatListing__.uid + "',photoURL='" + allChatListing__.photoURL + "' where mobile = '" + allChatListing__.mobile + "'", [])
            .then(function (res) { }).catch(function (e) {
            console.log("ERROR UPDATE cache_tapally_contacts");
            console.log(e);
        });
    }; //end function
    NotificationsPage.prototype.fnDeleteAndCreateCacheOfList = function () {
        var _this = this;
        this.sqlite.create({
            name: 'gr.db',
            location: 'default'
        }).then(function (db) {
            //Update  Local Cahce
            db.executeSql(_const__WEBPACK_IMPORTED_MODULE_4__["CONST"].create_table_statement_tapally_friends, [])
                .then(function (res) {
                db.executeSql("DELETE  FROM cache_tapally_friends", []).then(function (res) {
                    _this.isNoRecord = false;
                    var uniquemyFriends = {};
                    for (var i = 0; i < _this.myfriends_FromFirebase.length; i++) {
                        var recGotInserted = false;
                        if (_this.myfriends_FromFirebase[i].gpflag > 0) {
                            //Group
                        }
                        else {
                            //Individual
                            //do not insert duplicate
                            if (uniquemyFriends["indivial" + _this.myfriends_FromFirebase[i].uid]) {
                                console.log("SKipping Record");
                                continue;
                            }
                            else {
                                uniquemyFriends["indivial" + _this.myfriends_FromFirebase[i].uid] = true;
                            }
                            var isactive_ = 0;
                            if (_this.myfriends_FromFirebase[i].isActive) {
                                isactive_ = 1;
                            }
                            var isBlock_ = 0;
                            if (_this.myfriends_FromFirebase[i].isBlock) {
                                isBlock_ = 1;
                            }
                            var selection_ = 0;
                            if (_this.myfriends_FromFirebase[i].selection) {
                                selection_ = 1;
                            }
                            console.log("INSERTING INTO cache_tapally_friends");
                            console.log("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES (" + isactive_ + ",'" + _this.myfriends_FromFirebase[i].uid + "','" + _this.myfriends_FromFirebase[i].gpflag + "','" + selection_ + "'," + isBlock_ + ",0,'" + _this.myfriends_FromFirebase[i].displayName + "','" + _this.myfriends_FromFirebase[i].mobile + "','" + _this.myfriends_FromFirebase[i].photoURL + "','" + _this.myfriends_FromFirebase[i].lastMessage.message + "','" + _this.myfriends_FromFirebase[i].lastMessage.type + "','" + _this.myfriends_FromFirebase[i].lastMessage.dateofmsg + "','" + _this.myfriends_FromFirebase[i].groupimage + "','" + _this.myfriends_FromFirebase[i].groupName + "','" + _this.myfriends_FromFirebase[i].lastmsg + "','" + _this.myfriends_FromFirebase[i].dateofmsg + "','" + _this.myfriends_FromFirebase[i].timeofmsg + "','" + _this.myfriends_FromFirebase[i].selectCatId + "','" + _this.myfriends_FromFirebase[i].referral_type + "','" + _this.myfriends_FromFirebase[i].deviceToken + "')");
                            db.executeSql("INSERT INTO cache_tapally_friends (isactive,uid,gpflag,selection,isBlock, unreadmessage  , displayName  ,mobile  ,photoURL   ,                                      lastMessage_message  ,lastMessage_type  ,lastMessage_dateofmsg  ,groupimage  ,groupName  , lastmsg  ,dateofmsg,timeofmsg,selectCatId,referral_type,deviceToken  ) VALUES (" + isactive_ + ",'" + _this.myfriends_FromFirebase[i].uid + "','" + _this.myfriends_FromFirebase[i].gpflag + "','" + selection_ + "'," + isBlock_ + ",0,'" + _this.myfriends_FromFirebase[i].displayName + "','" + _this.myfriends_FromFirebase[i].mobile + "','" + _this.myfriends_FromFirebase[i].photoURL + "','" + _this.myfriends_FromFirebase[i].lastMessage.message + "','" + _this.myfriends_FromFirebase[i].lastMessage.type + "','" + _this.myfriends_FromFirebase[i].lastMessage.dateofmsg + "','" + _this.myfriends_FromFirebase[i].groupimage + "','" + _this.myfriends_FromFirebase[i].groupName + "','" + _this.myfriends_FromFirebase[i].lastmsg + "','" + _this.myfriends_FromFirebase[i].dateofmsg + "','" + _this.myfriends_FromFirebase[i].timeofmsg + "','" + _this.myfriends_FromFirebase[i].selectCatId + "','" + _this.myfriends_FromFirebase[i].referral_type + "','" + _this.myfriends_FromFirebase[i].deviceToken + "')", [])
                                .catch(function (e) {
                                console.log("ERROR inserting in cache_tapally_friends");
                                console.log(e);
                            });
                            console.log("1 ");
                            //Also update cache_tapally_contacts
                            _this.updateContactsTable(db, _this.myfriends_FromFirebase[i]);
                            console.log("2 ");
                            //Update in ARray as well
                            console.log(_this.myfriends_FromFirebase);
                            for (var g = 0; g < _this.myfriends.length; g++) {
                                console.log(_this.myfriends_FromFirebase[i].uid + ":" + _this.myfriends[g].uid);
                                if (_this.myfriends_FromFirebase[i].uid == _this.myfriends[g].uid) {
                                    console.log("3 ");
                                    //update photo url
                                    if (_this.myfriends[g].photoURL != _this.myfriends_FromFirebase[i].photoURL) {
                                        _this.myfriends[g].photoURL = _this.myfriends_FromFirebase[i].photoURL;
                                    } //endif
                                    console.log("4 ");
                                    _this.myfriends[g].displayName = _this.myfriends_FromFirebase[i].displayName;
                                    _this.myfriends[g].lastMessage = _this.myfriends_FromFirebase[i].lastMessage;
                                    console.log("5 ");
                                    recGotInserted = true;
                                    break;
                                }
                            } //endfor
                            console.log("FOR DONE ");
                        } //endif
                        if (!recGotInserted) {
                            _this.myfriends.push(_this.myfriends_FromFirebase[i]);
                        }
                    } //end for
                    if (_this.myfriends_FromFirebase.length == 0) {
                        _this.isNoRecord = true;
                    }
                    //Subscribe to new msg  jaswinder
                    //mytodo subscribe to last message
                }).catch(function (e) {
                    //Error in operation
                });
            })
                .catch(function (e) {
            }); //create table
        }); //create database
    }; //end function
    NotificationsPage.prototype.getmyfriends_ = function (updateFriends) {
        var _this = this;
        this.firefriends.child(this.userId).on('value', function (snapshot) {
            _this.myfriends_FromFirebase = [];
            var allfriends = snapshot.val();
            if (allfriends != null) {
                // I have some friends
                ////console.log ("I have friends " );
                var CounterFriends_1 = 0;
                for (var i in allfriends) {
                    ////console.log ("iiii ")     ;
                    var lastMsgActual__ = {
                        message: "",
                        type: "",
                        dateofmsg: "",
                        timeofmsg: "",
                        isRead: "",
                        isStarred: "",
                        referral_type: "",
                        selectCatId: ""
                    };
                    if (!allfriends[i].isBlock) {
                        allfriends[i].isBlock = false;
                    }
                    allfriends[i].unreadmessage = lastMsgActual__;
                    allfriends[i].lastMessage = lastMsgActual__;
                    allfriends[i].business = '';
                    allfriends[i].gpflag = 0;
                    allfriends[i].read = false;
                    _this.myfriends_FromFirebase.push(allfriends[i]);
                    CounterFriends_1++;
                } //end for
                //////console.log ("this.cnter "+ this.cnter);
                //console.log ("Now Check last message one by one for each friend. CounterFriends_:"+CounterFriends_);
                var friendsSoFar_1 = 0;
                var _loop_1 = function (y) {
                    if (true) {
                        _this.firebuddychats.child(_this.userId).child(_this.myfriends_FromFirebase[y].uid).limitToLast(1).once('value', function (snapshot_last_msg) {
                            var lastMsg__ = snapshot_last_msg.val();
                            if (lastMsg__) {
                                for (var key in lastMsg__) {
                                    var lastMsgActual__ = {
                                        message: lastMsg__[key].message,
                                        type: lastMsg__[key].type,
                                        dateofmsg: lastMsg__[key].dateofmsg,
                                        timeofmsg: lastMsg__[key].timeofmsg,
                                        referral_type: lastMsg__[key].referral_type,
                                        selectCatId: lastMsg__[key].selectCatId,
                                        isRead: false,
                                        isStarred: false
                                    };
                                    _this.myfriends_FromFirebase[y].unreadmessage = lastMsgActual__;
                                    _this.myfriends_FromFirebase[y].lastMessage = lastMsgActual__;
                                    break;
                                } //end for
                            } //endif
                            if (++friendsSoFar_1 >= CounterFriends_1) {
                                console.log("I have Friends and calling done");
                                _this.doneLoadingFriendsFromFirebase(updateFriends);
                            }
                        });
                        _this.subscriptionAddedFriendsObj[_this.userId + "/" + _this.myfriends_FromFirebase[y].uid] = true;
                    }
                    else {} //endif
                };
                for (var y in _this.myfriends_FromFirebase) {
                    _loop_1(y);
                } //end for
            }
            else {
                console.log("I dont have any friend (:");
                _this.doneLoadingFriendsFromFirebase(updateFriends);
            } //endif
        }); //get all friends
    };
    NotificationsPage.prototype.doneLoadingFriendsFromFirebase = function (updateFriends) {
        console.log("Done Loading Friends from Firebase  ");
        console.log(this.myfriends_FromFirebase);
        if (updateFriends) {
            console.log("this.myfriends UPDATED");
            this.myfriends = this.myfriends_FromFirebase;
        }
        this.fnDeleteAndCreateCacheOfList();
    };
    NotificationsPage.prototype.addbuddy = function () {
        this.router.navigateByUrl('/invite');
    };
    NotificationsPage.prototype.gotobuddypage = function (friend) {
        var navigationExtras = {
            queryParams: {
                buddy_uid: friend.uid
            }
        };
        this.router.navigate(['/buddychat'], navigationExtras);
    };
    NotificationsPage.prototype.askForReferral = function () {
        this.router.navigateByUrl('/referral');
    };
    NotificationsPage.prototype.ngOnInit = function () {
        /*
        if (this.route && this.route.data) {
          this.route.data.subscribe(resolvedData => {
            const dataSource = resolvedData['data'];
            if (dataSource) {
              dataSource.source.subscribe(pageData => {
                if (pageData) {
                  this.notifications = pageData;
                }
              });
            }
          });
        }
        */
    };
    NotificationsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__(/*! ./notifications.page.html */ "./src/app/notifications/notifications.page.html"),
            styles: [__webpack_require__(/*! ./styles/notifications.page.scss */ "./src/app/notifications/styles/notifications.page.scss"), __webpack_require__(/*! ./styles/notifications.shell.scss */ "./src/app/notifications/styles/notifications.shell.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _services_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_3__["SQLite"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _tpstorage_service__WEBPACK_IMPORTED_MODULE_5__["TpstorageProvider"]])
    ], NotificationsPage);
    return NotificationsPage;
}());



/***/ }),

/***/ "./src/app/notifications/notifications.resolver.ts":
/*!*********************************************************!*\
  !*** ./src/app/notifications/notifications.resolver.ts ***!
  \*********************************************************/
/*! exports provided: NotificationsResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsResolver", function() { return NotificationsResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _notifications_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notifications.service */ "./src/app/notifications/notifications.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsResolver = /** @class */ (function () {
    function NotificationsResolver(notificationsService) {
        this.notificationsService = notificationsService;
    }
    NotificationsResolver.prototype.resolve = function () {
        // Base Observable (where we get data from)
        var dataObservable = this.notificationsService.getData();
        return { source: dataObservable };
    };
    NotificationsResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_notifications_service__WEBPACK_IMPORTED_MODULE_1__["NotificationsService"]])
    ], NotificationsResolver);
    return NotificationsResolver;
}());



/***/ }),

/***/ "./src/app/notifications/notifications.service.ts":
/*!********************************************************!*\
  !*** ./src/app/notifications/notifications.service.ts ***!
  \********************************************************/
/*! exports provided: NotificationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsService", function() { return NotificationsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsService = /** @class */ (function () {
    function NotificationsService(http) {
        this.http = http;
    }
    NotificationsService.prototype.getData = function () {
        return this.http.get('./assets/sample-data/notifications.json');
    };
    NotificationsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], NotificationsService);
    return NotificationsService;
}());



/***/ }),

/***/ "./src/app/notifications/styles/notifications.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/notifications/styles/notifications.page.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-narrow-margin); }\n\n.notifications-content .cssnotread {\n  font-weight: bold; }\n\n.notifications-content ion-item-divider {\n  --background: var(--ion-color-light);\n  --padding-start: var(--page-margin); }\n\n.notifications-content .notification-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  padding: var(--page-margin);\n  color: var(--ion-color-medium);\n  box-shadow: inset 0 8px 2px -9px var(--ion-color-darkest);\n  /*\n    .referral-wrapper {\n      border: 1px solid var(--ion-color-dark);\n      text-align: center;\n      border-radius: 45px;\n    }\n    */ }\n\n.notifications-content .notification-item .notification-item-wrapper {\n    --ion-grid-column-padding: 0px;\n    width: 100%;\n    -webkit-box-align: center;\n            align-items: center; }\n\n.notifications-content .notification-item .refferalicon {\n    font-size: 30px;\n    color: var(--ion-color-dark); }\n\n.notifications-content .notification-item .details-wrapper {\n    display: -webkit-box;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n            flex-direction: column;\n    justify-content: space-around;\n    padding-left: var(--page-margin); }\n\n.notifications-content .notification-item .details-wrapper .details-name {\n      margin-top: 0px;\n      margin-bottom: 5px;\n      font-size: 16px;\n      font-weight: 400;\n      letter-spacing: 0.2px;\n      color: var(--ion-color-dark); }\n\n.notifications-content .notification-item .details-wrapper .details-description {\n      font-size: 12px;\n      margin: 0px; }\n\n.notifications-content .notification-item .date-wrapper {\n    align-self: flex-start; }\n\n.notifications-content .notification-item .date-wrapper .notification-date {\n      margin: 0px;\n      font-size: 12px;\n      text-align: end; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvbm90aWZpY2F0aW9ucy9zdHlsZXMvbm90aWZpY2F0aW9ucy5wYWdlLnNjc3MiLCJzcmMvYXBwL25vdGlmaWNhdGlvbnMvc3R5bGVzL25vdGlmaWNhdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsdUNBQWMsRUFBQTs7QUFJaEI7RUFNSSxpQkFBaUIsRUFBQTs7QUFOckI7RUFVSSxvQ0FBYTtFQUNiLG1DQUFnQixFQUFBOztBQVhwQjtFQWVJLG9CQUFnQjtFQUNoQix3QkFBb0I7RUFFcEIsMkJBQTJCO0VBQzNCLDhCQUE4QjtFQUM5Qix5REFBeUQ7RUFZekQ7Ozs7OztLQ2pCQyxFRHVCQzs7QUF0Q047SUF1Qk0sOEJBQTBCO0lBRTFCLFdBQVc7SUFDWCx5QkFBbUI7WUFBbkIsbUJBQW1CLEVBQUE7O0FBMUJ6QjtJQTZCTSxlQUFlO0lBQ2YsNEJBQTRCLEVBQUE7O0FBOUJsQztJQXdDTSxvQkFBYTtJQUFiLGFBQWE7SUFDYiw0QkFBc0I7SUFBdEIsNkJBQXNCO1lBQXRCLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsZ0NBQWdDLEVBQUE7O0FBM0N0QztNQThDUSxlQUFlO01BQ2Ysa0JBQWtCO01BQ2xCLGVBQWU7TUFDaEIsZ0JBQWdCO01BQ2hCLHFCQUFxQjtNQUNyQiw0QkFBNEIsRUFBQTs7QUFuRG5DO01BdURRLGVBQWU7TUFDZixXQUFXLEVBQUE7O0FBeERuQjtJQTZETSxzQkFBc0IsRUFBQTs7QUE3RDVCO01BZ0VRLFdBQVc7TUFDWCxlQUFlO01BQ2YsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvbm90aWZpY2F0aW9ucy9zdHlsZXMvbm90aWZpY2F0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLW5hcnJvdy1tYXJnaW4pO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5ub3RpZmljYXRpb25zLWNvbnRlbnQge1xuXG4gIC5jc3NyZWFkIHtcblxuICB9XG4gIC5jc3Nub3RyZWFkIHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuXG4gIGlvbi1pdGVtLWRpdmlkZXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgfVxuXG4gIC5ub3RpZmljYXRpb24taXRlbSB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xuXG4gICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcbiAgICBib3gtc2hhZG93OiBpbnNldCAwIDhweCAycHggLTlweCB2YXIoLS1pb24tY29sb3ItZGFya2VzdCk7XG5cbiAgICAubm90aWZpY2F0aW9uLWl0ZW0td3JhcHBlciB7XG4gICAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG4gICAgLnJlZmZlcmFsaWNvbiB7XG4gICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xuICAgIH1cbiAgICAvKlxuICAgIC5yZWZlcnJhbC13cmFwcGVyIHtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1kYXJrKTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDQ1cHg7XG4gICAgfVxuICAgICovXG4gICAgLmRldGFpbHMtd3JhcHBlciB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG5cbiAgICAgIC5kZXRhaWxzLW5hbWUge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgXHRmb250LXdlaWdodDogNDAwO1xuICAgICAgXHRsZXR0ZXItc3BhY2luZzogMC4ycHg7XG4gICAgICBcdGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XG4gICAgICB9XG5cbiAgICAgIC5kZXRhaWxzLWRlc2NyaXB0aW9uIHtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuZGF0ZS13cmFwcGVyIHtcbiAgICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XG5cbiAgICAgIC5ub3RpZmljYXRpb24tZGF0ZSB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLW5hcnJvdy1tYXJnaW4pOyB9XG5cbi5ub3RpZmljYXRpb25zLWNvbnRlbnQgLmNzc25vdHJlYWQge1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG4ubm90aWZpY2F0aW9ucy1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gIC0tcGFkZGluZy1zdGFydDogdmFyKC0tcGFnZS1tYXJnaW4pOyB9XG5cbi5ub3RpZmljYXRpb25zLWNvbnRlbnQgLm5vdGlmaWNhdGlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcbiAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIGJveC1zaGFkb3c6IGluc2V0IDAgOHB4IDJweCAtOXB4IHZhcigtLWlvbi1jb2xvci1kYXJrZXN0KTtcbiAgLypcbiAgICAucmVmZXJyYWwtd3JhcHBlciB7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItZGFyayk7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBib3JkZXItcmFkaXVzOiA0NXB4O1xuICAgIH1cbiAgICAqLyB9XG4gIC5ub3RpZmljYXRpb25zLWNvbnRlbnQgLm5vdGlmaWNhdGlvbi1pdGVtIC5ub3RpZmljYXRpb24taXRlbS13cmFwcGVyIHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgfVxuICAubm90aWZpY2F0aW9ucy1jb250ZW50IC5ub3RpZmljYXRpb24taXRlbSAucmVmZmVyYWxpY29uIHtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrKTsgfVxuICAubm90aWZpY2F0aW9ucy1jb250ZW50IC5ub3RpZmljYXRpb24taXRlbSAuZGV0YWlscy13cmFwcGVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7IH1cbiAgICAubm90aWZpY2F0aW9ucy1jb250ZW50IC5ub3RpZmljYXRpb24taXRlbSAuZGV0YWlscy13cmFwcGVyIC5kZXRhaWxzLW5hbWUge1xuICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwLjJweDtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7IH1cbiAgICAubm90aWZpY2F0aW9ucy1jb250ZW50IC5ub3RpZmljYXRpb24taXRlbSAuZGV0YWlscy13cmFwcGVyIC5kZXRhaWxzLWRlc2NyaXB0aW9uIHtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIG1hcmdpbjogMHB4OyB9XG4gIC5ub3RpZmljYXRpb25zLWNvbnRlbnQgLm5vdGlmaWNhdGlvbi1pdGVtIC5kYXRlLXdyYXBwZXIge1xuICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7IH1cbiAgICAubm90aWZpY2F0aW9ucy1jb250ZW50IC5ub3RpZmljYXRpb24taXRlbSAuZGF0ZS13cmFwcGVyIC5ub3RpZmljYXRpb24tZGF0ZSB7XG4gICAgICBtYXJnaW46IDBweDtcbiAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgIHRleHQtYWxpZ246IGVuZDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/notifications/styles/notifications.shell.scss":
/*!***************************************************************!*\
  !*** ./src/app/notifications/styles/notifications.shell.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.notification-image {\n  --image-shell-border-radius: 50%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvbm90aWZpY2F0aW9ucy9zdHlsZXMvbm90aWZpY2F0aW9ucy5zaGVsbC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0NBQTRCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9ub3RpZmljYXRpb25zL3N0eWxlcy9ub3RpZmljYXRpb25zLnNoZWxsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtaW1hZ2Utc2hlbGwubm90aWZpY2F0aW9uLWltYWdlIHtcbiAgLS1pbWFnZS1zaGVsbC1ib3JkZXItcmFkaXVzOiA1MCU7XG59XG4iXX0= */"

/***/ })

}]);
//# sourceMappingURL=notifications-notifications-module.js.map