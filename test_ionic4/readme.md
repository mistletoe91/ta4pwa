
===============================
Android platform files which need to be updated after platform is being added :
diff -rq --suppress-common-lines test_ionic4/platforms/android test_ionic4/tmp/android-WORKING


Files test_ionic4/platforms/android/app/build.gradle and test_ionic4/tmp/android-WORKING/app/build.gradle differ
Files test_ionic4/platforms/android/app/src/main/res/xml/config.xml and test_ionic4/tmp/android-WORKING/app/src/main/res/xml/config.xml differ
Files test_ionic4/platforms/android/project.properties and test_ionic4/tmp/android-WORKING/project.properties differ
