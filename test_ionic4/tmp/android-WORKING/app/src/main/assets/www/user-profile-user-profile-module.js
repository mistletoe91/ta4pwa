(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-profile-user-profile-module"],{

/***/ "./src/app/language/language.service.ts":
/*!**********************************************!*\
  !*** ./src/app/language/language.service.ts ***!
  \**********************************************/
/*! exports provided: LanguageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LanguageService", function() { return LanguageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LanguageService = /** @class */ (function () {
    function LanguageService() {
        this.languages = new Array();
        this.languages.push({ name: 'English', code: 'en' }, { name: 'Spanish', code: 'es' }, { name: 'French', code: 'fr' });
    }
    LanguageService.prototype.getLanguages = function () {
        return this.languages;
    };
    LanguageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LanguageService);
    return LanguageService;
}());



/***/ }),

/***/ "./src/app/shell/config/app-shell.config.ts":
/*!**************************************************!*\
  !*** ./src/app/shell/config/app-shell.config.ts ***!
  \**************************************************/
/*! exports provided: AppShellConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppShellConfig", function() { return AppShellConfig; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
// Inspired in: https://devblogs.microsoft.com/premier-developer/angular-how-to-editable-config-files/




var AppShellConfig = /** @class */ (function () {
    function AppShellConfig(http) {
        this.http = http;
    }
    AppShellConfig_1 = AppShellConfig;
    // Simplified version from: https://stackoverflow.com/a/49707898/1116959
    AppShellConfig.prototype.load = function () {
        var configFile = './assets/config/app-shell.config' + ((!Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["isDevMode"])()) ? '.prod' : '') + '.json';
        return this.http.get(configFile).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (configSettings) {
            AppShellConfig_1.settings = configSettings;
        }))
            .toPromise()
            .catch(function (error) {
            console.log("Could not load file '" + configFile + "'", error);
        });
    };
    var AppShellConfig_1;
    AppShellConfig = AppShellConfig_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AppShellConfig);
    return AppShellConfig;
}());



/***/ }),

/***/ "./src/app/shell/data-store.ts":
/*!*************************************!*\
  !*** ./src/app/shell/data-store.ts ***!
  \*************************************/
/*! exports provided: ShellModel, DataStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShellModel", function() { return ShellModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataStore", function() { return DataStore; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config/app-shell.config */ "./src/app/shell/config/app-shell.config.ts");



var ShellModel = /** @class */ (function () {
    function ShellModel() {
        this.isShell = false;
    }
    return ShellModel;
}());

var DataStore = /** @class */ (function () {
    function DataStore(shellModel) {
        this.shellModel = shellModel;
        // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length
        this.networkDelay = (_config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay) ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
    }
    // Static function with generics
    // (ref: https://stackoverflow.com/a/24293088/1116959)
    // Append a shell (T & ShellModel) to every value (T) emmited to the timeline
    DataStore.AppendShell = function (dataObservable, shellModel, networkDelay) {
        if (networkDelay === void 0) { networkDelay = 400; }
        var delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay));
        // Assign shell flag accordingly
        // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([
            delayObservable,
            dataObservable
        ]).pipe(
        // Dismiss unnecessary delayValue
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_a) {
            var delayValue = _a[0], dataValue = _a[1];
            return Object.assign(dataValue, { isShell: false });
        }), 
        // Set the shell model as the initial value
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, { isShell: true })));
    };
    DataStore.prototype.load = function (dataSourceObservable) {
        var _this = this;
        var dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
        dataSourceWithShellObservable
            .subscribe(function (dataValue) {
            _this.timeline.next(dataValue);
        });
    };
    Object.defineProperty(DataStore.prototype, "state", {
        get: function () {
            return this.timeline.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    return DataStore;
}());



/***/ }),

/***/ "./src/app/user/profile/styles/user-profile.ios.scss":
/*!***********************************************************!*\
  !*** ./src/app/user/profile/styles/user-profile.ios.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host-context(.plt-mobile.ios) .user-details-section .user-image-wrapper {\n  max-width: 30%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC91c2VyL3Byb2ZpbGUvc3R5bGVzL3VzZXItcHJvZmlsZS5pb3Muc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUdNLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9zdHlsZXMvdXNlci1wcm9maWxlLmlvcy5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3QtY29udGV4dCgucGx0LW1vYmlsZS5pb3MpIHtcbiAgLnVzZXItZGV0YWlscy1zZWN0aW9uIHtcbiAgICAudXNlci1pbWFnZS13cmFwcGVyIHtcbiAgICAgIG1heC13aWR0aDogMzAlO1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/user/profile/styles/user-profile.md.scss":
/*!**********************************************************!*\
  !*** ./src/app/user/profile/styles/user-profile.md.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host-context(.plt-mobile.md) .user-details-section .user-image-wrapper {\n  max-width: 28%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC91c2VyL3Byb2ZpbGUvc3R5bGVzL3VzZXItcHJvZmlsZS5tZC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR00sY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdXNlci9wcm9maWxlL3N0eWxlcy91c2VyLXByb2ZpbGUubWQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0LWNvbnRleHQoLnBsdC1tb2JpbGUubWQpIHtcbiAgLnVzZXItZGV0YWlscy1zZWN0aW9uIHtcbiAgICAudXNlci1pbWFnZS13cmFwcGVyIHtcbiAgICAgIG1heC13aWR0aDogMjglO1xuICAgIH1cbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/user/profile/styles/user-profile.page.scss":
/*!************************************************************!*\
  !*** ./src/app/user/profile/styles/user-profile.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-narrow-margin);\n  --page-border-radius: var(--app-fair-radius);\n  --page-actions-padding: 5px;\n  --page-friends-gutter: calc(var(--page-margin) / 2);\n  --page-pictures-gutter: calc(var(--page-margin) / 2); }\n\n.user-details-section {\n  --ion-grid-column-padding: 0px;\n  margin: var(--page-margin); }\n\n.user-details-section .user-image-wrapper {\n    max-width: 26%; }\n\n.user-details-section .user-info-wrapper {\n    padding-left: var(--page-margin);\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    flex-wrap: nowrap; }\n\n.user-details-section .user-info-wrapper .user-data-row {\n      padding-bottom: var(--page-margin);\n      flex-wrap: nowrap;\n      justify-content: space-between; }\n\n.user-details-section .user-info-wrapper .user-data-row .user-name {\n        margin: 0px 0px 5px;\n        font-size: 20px;\n        letter-spacing: 0.5px; }\n\n.user-details-section .user-info-wrapper .user-data-row .user-title {\n        margin: 0px;\n        color: var(--ion-color-medium);\n        font-size: 16px; }\n\n.user-details-section .user-info-wrapper .user-data-row .membership-col {\n        padding-left: var(--page-margin);\n        flex-grow: 0; }\n\n.user-details-section .user-info-wrapper .user-data-row .user-membership {\n        display: block;\n        background-color: var(--ion-color-secondary);\n        color: var(--ion-color-lightest);\n        border-radius: var(--app-narrow-radius);\n        padding: 4px 8px;\n        text-align: center;\n        font-weight: 500;\n        font-size: 14px; }\n\n.user-details-section .user-info-wrapper .actions-row {\n      --ion-grid-columns: 10;\n      --ion-grid-column-padding: var(--page-actions-padding);\n      justify-content: space-between;\n      flex-wrap: nowrap;\n      margin-left: calc(var(--page-actions-padding) * -1);\n      margin-right: calc(var(--page-actions-padding) * -1); }\n\n.user-details-section .user-info-wrapper .actions-row .main-actions {\n        flex-grow: 0;\n        padding-top: 0px;\n        padding-bottom: 0px;\n        margin-left: calc(var(--page-actions-padding) * -1);\n        margin-right: calc(var(--page-actions-padding) * -1);\n        display: flex; }\n\n.user-details-section .user-info-wrapper .actions-row .main-actions .call-to-action-btn {\n          padding: 0px var(--page-actions-padding);\n          margin: 0px; }\n\n.user-details-section .user-info-wrapper .actions-row .secondary-actions {\n        flex-grow: 0;\n        padding-top: 0px;\n        padding-bottom: 0px; }\n\n.user-details-section .user-info-wrapper .actions-row .secondary-actions .more-btn {\n          --padding-start: 4px;\n          --padding-end: 4px;\n          margin: 0px; }\n\n.user-stats-section {\n  --ion-grid-column-padding: 0px;\n  margin: calc(var(--page-margin) * 2) var(--page-margin) var(--page-margin);\n  padding-bottom: var(--page-margin);\n  border-bottom: 1px solid var(--ion-color-light-shade); }\n\n.user-stats-section .user-stats-wrapper {\n    text-align: center; }\n\n.user-stats-section .user-stats-wrapper .stat-value {\n      display: block;\n      padding-bottom: 5px;\n      color: var(--ion-color-dark-shade);\n      font-weight: 500;\n      font-size: 18px; }\n\n.user-stats-section .user-stats-wrapper .stat-name {\n      font-size: 16px;\n      color: var(--ion-color-medium); }\n\n.details-section-title {\n  font-size: 18px;\n  font-weight: 500;\n  color: var(--ion-color-dark-tint);\n  margin: 0px 0px var(--page-margin); }\n\n.user-about-section {\n  margin: calc(var(--page-margin) * 2) var(--page-margin) var(--page-margin);\n  padding-bottom: var(--page-margin);\n  border-bottom: 1px solid var(--ion-color-light-shade); }\n\n.user-about-section .user-description {\n    color: var(--ion-color-dark-shade);\n    text-align: justify;\n    margin: var(--page-margin) 0px;\n    font-size: 14px;\n    line-height: 1.3; }\n\n.user-friends-section {\n  margin: calc(var(--page-margin) * 2) var(--page-margin) var(--page-margin);\n  padding-bottom: var(--page-margin);\n  border-bottom: 1px solid var(--ion-color-light-shade); }\n\n.user-friends-section .heading-row {\n    margin-bottom: var(--page-margin);\n    justify-content: space-between;\n    align-items: center; }\n\n.user-friends-section .heading-row .details-section-title {\n      margin: 0px; }\n\n.user-friends-section .heading-row .heading-call-to-action {\n      padding: calc(var(--page-margin) / 2) calc(var(--page-margin) / 4);\n      text-decoration: none;\n      color: var(--ion-color-secondary); }\n\n.user-friends-section .friends-row {\n    --ion-grid-columns: 7;\n    --ion-grid-column-padding: 0px;\n    flex-wrap: nowrap;\n    overflow-x: scroll;\n    will-change: scroll-position;\n    margin-left: calc(var(--page-margin) * -1);\n    margin-right: calc(var(--page-margin) * -1);\n    -ms-overflow-style: none;\n    overflow: -moz-scrollbars-none;\n    scrollbar-width: none; }\n\n.user-friends-section .friends-row::-webkit-scrollbar {\n      display: none; }\n\n.user-friends-section .friends-row::before, .user-friends-section .friends-row::after {\n      content: '';\n      flex: 0 0 calc(var(--page-margin) - var(--page-friends-gutter)); }\n\n.user-friends-section .friends-row .friend-item {\n      padding: 0px var(--page-friends-gutter); }\n\n.user-friends-section .friends-row .friend-item .friend-name {\n        display: block;\n        text-align: center;\n        margin: var(--page-margin) 0px;\n        font-size: 14px; }\n\n.user-photos-section {\n  margin: calc(var(--page-margin) * 2) var(--page-margin) var(--page-margin);\n  padding-bottom: calc(var(--page-margin) * 2); }\n\n.user-photos-section .heading-row {\n    margin-bottom: var(--page-margin);\n    justify-content: space-between;\n    align-items: center; }\n\n.user-photos-section .heading-row .details-section-title {\n      margin: 0px; }\n\n.user-photos-section .heading-row .heading-call-to-action {\n      padding: calc(var(--page-margin) / 2) calc(var(--page-margin) / 4);\n      text-decoration: none;\n      color: var(--ion-color-secondary); }\n\n.user-photos-section .pictures-row {\n    --ion-grid-columns: 4;\n    --ion-grid-column-padding: 0px;\n    margin: 0px calc(var(--page-pictures-gutter) * -1); }\n\n.user-photos-section .pictures-row .picture-item {\n      padding: 0px var(--page-pictures-gutter) calc(var(--page-pictures-gutter) * 2); }\n\n::ng-deep .language-alert {\n  --select-alert-color: #000;\n  --select-alert-background: #FFF;\n  --select-alert-margin: 16px;\n  --select-alert-color: var(--ion-color-lightest);\n  --select-alert-background: var(--ion-color-primary);\n  --select-alert-margin: var(--app-fair-margin); }\n\n::ng-deep .language-alert .alert-head {\n    padding-top: calc((var(--select-alert-margin) / 4) * 3);\n    padding-bottom: calc((var(--select-alert-margin) / 4) * 3);\n    -webkit-padding-start: var(--select-alert-margin);\n            padding-inline-start: var(--select-alert-margin);\n    -webkit-padding-end: var(--select-alert-margin);\n            padding-inline-end: var(--select-alert-margin); }\n\n::ng-deep .language-alert .alert-title {\n    color: var(--select-alert-color); }\n\n::ng-deep .language-alert .alert-head,\n  ::ng-deep .language-alert .alert-message {\n    background-color: var(--select-alert-background); }\n\n::ng-deep .language-alert .alert-wrapper.sc-ion-alert-ios .alert-title {\n    margin: 0px; }\n\n::ng-deep .language-alert .alert-wrapper.sc-ion-alert-md .alert-title {\n    font-size: 18px;\n    font-weight: 400; }\n\n::ng-deep .language-alert .alert-wrapper.sc-ion-alert-md .alert-button {\n    --padding-top: 0;\n    --padding-start: 0.9em;\n    --padding-end: 0.9em;\n    --padding-bottom: 0;\n    height: 2.1em;\n    font-size: 13px; }\n\n::ng-deep .language-alert .alert-message {\n    display: none; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC91c2VyL3Byb2ZpbGUvc3R5bGVzL3VzZXItcHJvZmlsZS5wYWdlLnNjc3MiLCIvVXNlcnMvbWFjYm9va3Byby9Eb2N1bWVudHMvY29kZS9hcHAvdGE0cHdhL3Rlc3RfaW9uaWM0L3NyYy90aGVtZS9taXhpbnMvc2Nyb2xsYmFycy5zY3NzIiwiL1VzZXJzL21hY2Jvb2twcm8vRG9jdW1lbnRzL2NvZGUvYXBwL3RhNHB3YS90ZXN0X2lvbmljNC9zcmMvdGhlbWUvbWl4aW5zL2lucHV0cy9zZWxlY3QtYWxlcnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQTtFQUNFLHVDQUFjO0VBQ2QsNENBQXFCO0VBRXJCLDJCQUF1QjtFQUN2QixtREFBc0I7RUFDdEIsb0RBQXVCLEVBQUE7O0FBSXpCO0VBQ0UsOEJBQTBCO0VBRTFCLDBCQUEwQixFQUFBOztBQUg1QjtJQU1JLGNBQWMsRUFBQTs7QUFObEI7SUFVSSxnQ0FBZ0M7SUFDaEMsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsaUJBQWlCLEVBQUE7O0FBZHJCO01BaUJNLGtDQUFrQztNQUNsQyxpQkFBaUI7TUFDakIsOEJBQThCLEVBQUE7O0FBbkJwQztRQXNCUSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLHFCQUFxQixFQUFBOztBQXhCN0I7UUE0QlEsV0FBVztRQUNYLDhCQUE4QjtRQUM5QixlQUFlLEVBQUE7O0FBOUJ2QjtRQWtDUSxnQ0FBZ0M7UUFDaEMsWUFBWSxFQUFBOztBQW5DcEI7UUF1Q1EsY0FBYztRQUNkLDRDQUE0QztRQUM1QyxnQ0FBZ0M7UUFDaEMsdUNBQXVDO1FBQ3ZDLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLGVBQWUsRUFBQTs7QUE5Q3ZCO01BbURNLHNCQUFtQjtNQUNuQixzREFBMEI7TUFFMUIsOEJBQThCO01BQzlCLGlCQUFpQjtNQUNqQixtREFBbUQ7TUFDbkQsb0RBQW9ELEVBQUE7O0FBekQxRDtRQTREUSxZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixtREFBbUQ7UUFDbkQsb0RBQW9EO1FBQ3BELGFBQWEsRUFBQTs7QUFqRXJCO1VBb0VVLHdDQUF3QztVQUN4QyxXQUFXLEVBQUE7O0FBckVyQjtRQTBFUSxZQUFZO1FBQ1osZ0JBQWdCO1FBQ2hCLG1CQUFtQixFQUFBOztBQTVFM0I7VUErRVUsb0JBQWdCO1VBQ2hCLGtCQUFjO1VBRWQsV0FBVyxFQUFBOztBQU9yQjtFQUNFLDhCQUEwQjtFQUUxQiwwRUFBMEU7RUFDMUUsa0NBQWtDO0VBQ2xDLHFEQUFxRCxFQUFBOztBQUx2RDtJQVFJLGtCQUFrQixFQUFBOztBQVJ0QjtNQVdNLGNBQWM7TUFDZCxtQkFBbUI7TUFDbkIsa0NBQWtDO01BQ2xDLGdCQUFnQjtNQUNoQixlQUFlLEVBQUE7O0FBZnJCO01BbUJNLGVBQWU7TUFDZiw4QkFBOEIsRUFBQTs7QUFLcEM7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGlDQUFpQztFQUNqQyxrQ0FBa0MsRUFBQTs7QUFHcEM7RUFDRSwwRUFBMEU7RUFDMUUsa0NBQWtDO0VBQ2xDLHFEQUFxRCxFQUFBOztBQUh2RDtJQU1JLGtDQUFrQztJQUNsQyxtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUFJcEI7RUFDRSwwRUFBMEU7RUFDMUUsa0NBQWtDO0VBQ2xDLHFEQUFxRCxFQUFBOztBQUh2RDtJQU1JLGlDQUFpQztJQUNqQyw4QkFBOEI7SUFDOUIsbUJBQW1CLEVBQUE7O0FBUnZCO01BV00sV0FBVyxFQUFBOztBQVhqQjtNQWVNLGtFQUFrRTtNQUNsRSxxQkFBcUI7TUFDckIsaUNBQWlDLEVBQUE7O0FBakJ2QztJQXNCSSxxQkFBbUI7SUFDbkIsOEJBQTBCO0lBRTFCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsNEJBQTRCO0lBQzVCLDBDQUEwQztJQUMxQywyQ0FBMkM7SUNoTDdDLHdCQUF3QjtJQUd4Qiw4QkFBOEI7SUFDOUIscUJBQXFCLEVBQUE7O0FEK0l2QjtNQzNJSSxhQUFhLEVBQUE7O0FEMklqQjtNQW1DTSxXQUFXO01BRVgsK0RBQStELEVBQUE7O0FBckNyRTtNQXlDTSx1Q0FBdUMsRUFBQTs7QUF6QzdDO1FBNENRLGNBQWM7UUFDZCxrQkFBa0I7UUFDbEIsOEJBQThCO1FBQzlCLGVBQWUsRUFBQTs7QUFNdkI7RUFDRSwwRUFBMEU7RUFDMUUsNENBQTRDLEVBQUE7O0FBRjlDO0lBS0ksaUNBQWlDO0lBQ2pDLDhCQUE4QjtJQUM5QixtQkFBbUIsRUFBQTs7QUFQdkI7TUFVTSxXQUFXLEVBQUE7O0FBVmpCO01BY00sa0VBQWtFO01BQ2xFLHFCQUFxQjtNQUNyQixpQ0FBaUMsRUFBQTs7QUFoQnZDO0lBcUJJLHFCQUFtQjtJQUNuQiw4QkFBMEI7SUFFMUIsa0RBQWtELEVBQUE7O0FBeEJ0RDtNQTJCTSw4RUFBOEUsRUFBQTs7QUFNcEY7RUUxT0UsMEJBQXFCO0VBQ3JCLCtCQUEwQjtFQUMxQiwyQkFBc0I7RUY0T3RCLCtDQUFxQjtFQUNyQixtREFBMEI7RUFDMUIsNkNBQXNCLEVBQUE7O0FBTnhCO0lFck9JLHVEQUF1RDtJQUN2RCwwREFBMEQ7SUFDMUQsaURBQWdEO1lBQWhELGdEQUFnRDtJQUNoRCwrQ0FBOEM7WUFBOUMsOENBQThDLEVBQUE7O0FGa09sRDtJRTlOSSxnQ0FBZ0MsRUFBQTs7QUY4TnBDOztJRXpOSSxnREFBZ0QsRUFBQTs7QUZ5TnBEO0lFbk5NLFdBQVcsRUFBQTs7QUZtTmpCO0lFNU1NLGVBQWU7SUFDZixnQkFBZ0IsRUFBQTs7QUYyTXRCO0lFdE1NLGdCQUFjO0lBQ2Qsc0JBQWdCO0lBQ2hCLG9CQUFjO0lBQ2QsbUJBQWlCO0lBRWpCLGFBQWE7SUFDYixlQUFlLEVBQUE7O0FGZ01yQjtJQVNJLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9zdHlsZXMvdXNlci1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi90aGVtZS9taXhpbnMvc2Nyb2xsYmFyc1wiO1xuQGltcG9ydCBcIi4uLy4uLy4uLy4uL3RoZW1lL21peGlucy9pbnB1dHMvc2VsZWN0LWFsZXJ0XCI7XG5cbi8vIEN1c3RvbSB2YXJpYWJsZXNcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xuOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtbmFycm93LW1hcmdpbik7XG4gIC0tcGFnZS1ib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtZmFpci1yYWRpdXMpO1xuXG4gIC0tcGFnZS1hY3Rpb25zLXBhZGRpbmc6IDVweDtcbiAgLS1wYWdlLWZyaWVuZHMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAtLXBhZ2UtcGljdHVyZXMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi51c2VyLWRldGFpbHMtc2VjdGlvbiB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICBtYXJnaW46IHZhcigtLXBhZ2UtbWFyZ2luKTtcblxuICAudXNlci1pbWFnZS13cmFwcGVyIHtcbiAgICBtYXgtd2lkdGg6IDI2JTtcbiAgfVxuXG4gIC51c2VyLWluZm8td3JhcHBlciB7XG4gICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBmbGV4LXdyYXA6IG5vd3JhcDtcblxuICAgIC51c2VyLWRhdGEtcm93IHtcbiAgICAgIHBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICBmbGV4LXdyYXA6IG5vd3JhcDtcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgICAgLnVzZXItbmFtZSB7XG4gICAgICAgIG1hcmdpbjogMHB4IDBweCA1cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xuICAgICAgfVxuXG4gICAgICAudXNlci10aXRsZSB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgIH1cblxuICAgICAgLm1lbWJlcnNoaXAtY29sIHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICAgIGZsZXgtZ3JvdzogMDtcbiAgICAgIH1cblxuICAgICAgLnVzZXItbWVtYmVyc2hpcCB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IHZhcigtLWFwcC1uYXJyb3ctcmFkaXVzKTtcbiAgICAgICAgcGFkZGluZzogNHB4IDhweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLmFjdGlvbnMtcm93IHtcbiAgICAgIC0taW9uLWdyaWQtY29sdW1uczogMTA7XG4gICAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiB2YXIoLS1wYWdlLWFjdGlvbnMtcGFkZGluZyk7XG5cbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgICAgbWFyZ2luLWxlZnQ6IGNhbGModmFyKC0tcGFnZS1hY3Rpb25zLXBhZGRpbmcpICogLTEpO1xuICAgICAgbWFyZ2luLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtYWN0aW9ucy1wYWRkaW5nKSAqIC0xKTtcblxuICAgICAgLm1haW4tYWN0aW9ucyB7XG4gICAgICAgIGZsZXgtZ3JvdzogMDtcbiAgICAgICAgcGFkZGluZy10b3A6IDBweDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGNhbGModmFyKC0tcGFnZS1hY3Rpb25zLXBhZGRpbmcpICogLTEpO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGNhbGModmFyKC0tcGFnZS1hY3Rpb25zLXBhZGRpbmcpICogLTEpO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuXG4gICAgICAgIC5jYWxsLXRvLWFjdGlvbi1idG4ge1xuICAgICAgICAgIHBhZGRpbmc6IDBweCB2YXIoLS1wYWdlLWFjdGlvbnMtcGFkZGluZyk7XG4gICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLnNlY29uZGFyeS1hY3Rpb25zIHtcbiAgICAgICAgZmxleC1ncm93OiAwO1xuICAgICAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuXG4gICAgICAgIC5tb3JlLWJ0biB7XG4gICAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiA0cHg7XG4gICAgICAgICAgLS1wYWRkaW5nLWVuZDogNHB4O1xuXG4gICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLnVzZXItc3RhdHMtc2VjdGlvbiB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMikgdmFyKC0tcGFnZS1tYXJnaW4pIHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG5cbiAgLnVzZXItc3RhdHMtd3JhcHBlciB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgLnN0YXQtdmFsdWUge1xuICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlKTtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgfVxuXG4gICAgLnN0YXQtbmFtZSB7XG4gICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gICAgfVxuICB9XG59XG5cbi5kZXRhaWxzLXNlY3Rpb24tdGl0bGUge1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyay10aW50KTtcbiAgbWFyZ2luOiAwcHggMHB4IHZhcigtLXBhZ2UtbWFyZ2luKTtcbn1cblxuLnVzZXItYWJvdXQtc2VjdGlvbiB7XG4gIG1hcmdpbjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAyKSB2YXIoLS1wYWdlLW1hcmdpbikgdmFyKC0tcGFnZS1tYXJnaW4pO1xuICBwYWRkaW5nLWJvdHRvbTogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcblxuICAudXNlci1kZXNjcmlwdGlvbiB7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlKTtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xuICAgIG1hcmdpbjogdmFyKC0tcGFnZS1tYXJnaW4pIDBweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuMztcbiAgfVxufVxuXG4udXNlci1mcmllbmRzLXNlY3Rpb24ge1xuICBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMikgdmFyKC0tcGFnZS1tYXJnaW4pIHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG5cbiAgLmhlYWRpbmctcm93IHtcbiAgICBtYXJnaW4tYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAuZGV0YWlscy1zZWN0aW9uLXRpdGxlIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cblxuICAgIC5oZWFkaW5nLWNhbGwtdG8tYWN0aW9uIHtcbiAgICAgIHBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMikgY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyA0KTtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICB9XG4gIH1cblxuICAuZnJpZW5kcy1yb3cge1xuICAgIC0taW9uLWdyaWQtY29sdW1uczogNzsgLy8gV2Ugd2FudCB0byBzaG93IHRocmVlIGZyaWVuZHMgYW5kIGEgaGFsZi4gRWFjaCBmcmllbmQgZmlsbHMgMiBjb2xzID0+ICgzLjUgKiAyID0gNyBjb2xzKVxuICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcbiAgICB3aWxsLWNoYW5nZTogc2Nyb2xsLXBvc2l0aW9uO1xuICAgIG1hcmdpbi1sZWZ0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIC0xKTtcbiAgICBtYXJnaW4tcmlnaHQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogLTEpO1xuXG4gICAgQGluY2x1ZGUgaGlkZS1zY3JvbGxiYXJzKCk7XG5cbiAgICAmOjpiZWZvcmUsXG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogJyc7XG4gICAgICAvLyAuZnJpZW5kLWl0ZW0gaGFzIDhweCBvZiBzaWRlIHBhZGRpbmcsIHdlIG5lZWQgYW4gZXh0cmEgNHB4IGF0IHRoZSBiZWdpbm5pbmcgYW5kIGVuZCB0byBmaWxsIHRoZSAxMnB4IHNpZGUgbWFyZ2luIG9mIHRoZSB2aWV3XG4gICAgICBmbGV4OiAwIDAgY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLSB2YXIoLS1wYWdlLWZyaWVuZHMtZ3V0dGVyKSk7XG4gICAgfVxuXG4gICAgLmZyaWVuZC1pdGVtIHtcbiAgICAgIHBhZGRpbmc6IDBweCB2YXIoLS1wYWdlLWZyaWVuZHMtZ3V0dGVyKTtcblxuICAgICAgLmZyaWVuZC1uYW1lIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luOiB2YXIoLS1wYWdlLW1hcmdpbikgMHB4O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi51c2VyLXBob3Rvcy1zZWN0aW9uIHtcbiAgbWFyZ2luOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpIHZhcigtLXBhZ2UtbWFyZ2luKSB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIHBhZGRpbmctYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xuXG4gIC5oZWFkaW5nLXJvdyB7XG4gICAgbWFyZ2luLWJvdHRvbTogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgLmRldGFpbHMtc2VjdGlvbi10aXRsZSB7XG4gICAgICBtYXJnaW46IDBweDtcbiAgICB9XG5cbiAgICAuaGVhZGluZy1jYWxsLXRvLWFjdGlvbiB7XG4gICAgICBwYWRkaW5nOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpIGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XG4gICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgfVxuICB9XG5cbiAgLnBpY3R1cmVzLXJvdyB7XG4gICAgLS1pb24tZ3JpZC1jb2x1bW5zOiA0O1xuICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICAgIG1hcmdpbjogMHB4IGNhbGModmFyKC0tcGFnZS1waWN0dXJlcy1ndXR0ZXIpICogLTEpO1xuXG4gICAgLnBpY3R1cmUtaXRlbSB7XG4gICAgICBwYWRkaW5nOiAwcHggdmFyKC0tcGFnZS1waWN0dXJlcy1ndXR0ZXIpIGNhbGModmFyKC0tcGFnZS1waWN0dXJlcy1ndXR0ZXIpICogMik7XG4gICAgfVxuICB9XG59XG4vLyBBbGVydHMgYW5kIGluIGdlbmVyYWwgYWxsIG92ZXJsYXlzIGFyZSBhdHRhY2hlZCB0byB0aGUgYm9keSBvciBpb24tYXBwIGRpcmVjdGx5XG4vLyBXZSBuZWVkIHRvIHVzZSA6Om5nLWRlZXAgdG8gYWNjZXNzIGl0IGZyb20gaGVyZVxuOjpuZy1kZWVwIC5sYW5ndWFnZS1hbGVydCB7XG4gIEBpbmNsdWRlIHNlbGVjdC1hbGVydCgpO1xuXG4gIC8vIFZhcmlhYmxlcyBzaG91bGQgYmUgaW4gYSBkZWVwZXIgc2VsZWN0b3Igb3IgYWZ0ZXIgdGhlIG1peGluIGluY2x1ZGUgdG8gb3ZlcnJpZGUgZGVmYXVsdCB2YWx1ZXNcbiAgLS1zZWxlY3QtYWxlcnQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gIC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgLS1zZWxlY3QtYWxlcnQtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuXG4gIC5hbGVydC1tZXNzYWdlIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG4iLCIvLyBIaWRlIHNjcm9sbGJhcnM6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zODk5NDgzNy8xMTE2OTU5XG5AbWl4aW4gaGlkZS1zY3JvbGxiYXJzKCkge1xuICAvLyBJRSAxMCtcbiAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lO1xuXG4gIC8vIEZpcmVmb3hcbiAgb3ZlcmZsb3c6IC1tb3otc2Nyb2xsYmFycy1ub25lO1xuICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7XG5cbiAgLy8gU2FmYXJpIGFuZCBDaHJvbWVcbiAgJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbiIsIkBtaXhpbiBzZWxlY3QtYWxlcnQoKSB7XG4gIC8vIERlZmF1bHQgdmFsdWVzXG4gIC0tc2VsZWN0LWFsZXJ0LWNvbG9yOiAjMDAwO1xuICAtLXNlbGVjdC1hbGVydC1iYWNrZ3JvdW5kOiAjRkZGO1xuICAtLXNlbGVjdC1hbGVydC1tYXJnaW46IDE2cHg7XG5cbiAgLmFsZXJ0LWhlYWQge1xuICAgIHBhZGRpbmctdG9wOiBjYWxjKCh2YXIoLS1zZWxlY3QtYWxlcnQtbWFyZ2luKSAvIDQpICogMyk7XG4gICAgcGFkZGluZy1ib3R0b206IGNhbGMoKHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pIC8gNCkgKiAzKTtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogdmFyKC0tc2VsZWN0LWFsZXJ0LW1hcmdpbik7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiB2YXIoLS1zZWxlY3QtYWxlcnQtbWFyZ2luKTtcbiAgfVxuXG4gIC5hbGVydC10aXRsZSB7XG4gICAgY29sb3I6IHZhcigtLXNlbGVjdC1hbGVydC1jb2xvcik7XG4gIH1cblxuICAuYWxlcnQtaGVhZCxcbiAgLmFsZXJ0LW1lc3NhZ2Uge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXNlbGVjdC1hbGVydC1iYWNrZ3JvdW5kKTtcbiAgfVxuXG4gIC8vIGlPUyBzdHlsZXNcbiAgLmFsZXJ0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LWlvcyB7XG4gICAgLmFsZXJ0LXRpdGxlIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbiAgfVxuXG4gIC8vIE1hdGVyaWFsIHN0eWxlc1xuICAuYWxlcnQtd3JhcHBlci5zYy1pb24tYWxlcnQtbWQge1xuICAgIC5hbGVydC10aXRsZSB7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuICAgIH1cblxuICAgIC5hbGVydC1idXR0b24ge1xuICAgICAgLy8gVmFsdWVzIHRha2VuIGZyb20gSW9uaWMgc21hbGwgYnV0dG9uIHByZXNldFxuICAgICAgLS1wYWRkaW5nLXRvcDogMDtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMC45ZW07XG4gICAgICAtLXBhZGRpbmctZW5kOiAwLjllbTtcbiAgICAgIC0tcGFkZGluZy1ib3R0b206IDA7XG5cbiAgICAgIGhlaWdodDogMi4xZW07XG4gICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgfVxuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/user/profile/styles/user-profile.shell.scss":
/*!*************************************************************!*\
  !*** ./src/app/user/profile/styles/user-profile.shell.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.user-image {\n  --image-shell-border-radius: var(--page-border-radius); }\n\n.user-name > app-text-shell {\n  --text-shell-line-height: 20px;\n  max-width: 50%; }\n\n.user-name > app-text-shell.text-loaded {\n    max-width: unset; }\n\n.user-title > app-text-shell {\n  --text-shell-line-height: 16px;\n  max-width: 70%; }\n\n.user-title > app-text-shell.text-loaded {\n    max-width: unset; }\n\n.user-membership > app-text-shell {\n  --text-shell-line-color: rgba(var(--ion-color-secondary-rgb), 0.4);\n  --text-shell-line-height: 14px;\n  min-width: 30px; }\n\n.user-membership > app-text-shell.text-loaded {\n    min-width: 0px; }\n\n.stat-value > app-text-shell {\n  --text-shell-line-height: 18px;\n  max-width: 50%;\n  margin: 0px auto; }\n\n.stat-value > app-text-shell.text-loaded {\n    max-width: unset;\n    margin: initial; }\n\n.user-description > app-text-shell {\n  --text-shell-line-height: 14px; }\n\napp-image-shell.friend-picture {\n  --image-shell-border-radius: var(--page-border-radius); }\n\n.friend-name > app-text-shell {\n  --text-shell-line-height: 14px; }\n\napp-image-shell.user-photo-image {\n  --image-shell-border-radius: var(--page-border-radius); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC91c2VyL3Byb2ZpbGUvc3R5bGVzL3VzZXItcHJvZmlsZS5zaGVsbC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0RBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsOEJBQXlCO0VBQ3pCLGNBQWMsRUFBQTs7QUFGaEI7SUFLSSxnQkFBZ0IsRUFBQTs7QUFJcEI7RUFDRSw4QkFBeUI7RUFDekIsY0FBYyxFQUFBOztBQUZoQjtJQUtJLGdCQUFnQixFQUFBOztBQUlwQjtFQUNFLGtFQUF3QjtFQUN4Qiw4QkFBeUI7RUFDekIsZUFBZSxFQUFBOztBQUhqQjtJQU1JLGNBQWMsRUFBQTs7QUFJbEI7RUFDRSw4QkFBeUI7RUFDekIsY0FBYztFQUNkLGdCQUFnQixFQUFBOztBQUhsQjtJQU1JLGdCQUFnQjtJQUNoQixlQUFlLEVBQUE7O0FBSW5CO0VBQ0UsOEJBQXlCLEVBQUE7O0FBRzNCO0VBQ0Usc0RBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsOEJBQXlCLEVBQUE7O0FBRzNCO0VBQ0Usc0RBQTRCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC91c2VyL3Byb2ZpbGUvc3R5bGVzL3VzZXItcHJvZmlsZS5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYXBwLWltYWdlLXNoZWxsLnVzZXItaW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IHZhcigtLXBhZ2UtYm9yZGVyLXJhZGl1cyk7XG59XG5cbi51c2VyLW5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMjBweDtcbiAgbWF4LXdpZHRoOiA1MCU7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiB1bnNldDtcbiAgfVxufVxuXG4udXNlci10aXRsZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xuICBtYXgtd2lkdGg6IDcwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IHVuc2V0O1xuICB9XG59XG5cbi51c2VyLW1lbWJlcnNoaXAgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktcmdiKSwgMC40KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xuICBtaW4td2lkdGg6IDMwcHg7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWluLXdpZHRoOiAwcHg7XG4gIH1cbn1cblxuLnN0YXQtdmFsdWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMThweDtcbiAgbWF4LXdpZHRoOiA1MCU7XG4gIG1hcmdpbjogMHB4IGF1dG87XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiB1bnNldDtcbiAgICBtYXJnaW46IGluaXRpYWw7XG4gIH1cbn1cblxuLnVzZXItZGVzY3JpcHRpb24gPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbn1cblxuYXBwLWltYWdlLXNoZWxsLmZyaWVuZC1waWN0dXJlIHtcbiAgLS1pbWFnZS1zaGVsbC1ib3JkZXItcmFkaXVzOiB2YXIoLS1wYWdlLWJvcmRlci1yYWRpdXMpO1xufVxuXG4uZnJpZW5kLW5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbn1cblxuYXBwLWltYWdlLXNoZWxsLnVzZXItcGhvdG8taW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IHZhcigtLXBhZ2UtYm9yZGVyLXJhZGl1cyk7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/user/profile/user-profile.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/user/profile/user-profile.module.ts ***!
  \*****************************************************/
/*! exports provided: UserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageModule", function() { return UserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-profile.page */ "./src/app/user/profile/user-profile.page.ts");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../user.service */ "./src/app/user/user.service.ts");
/* harmony import */ var _user_profile_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user-profile.resolver */ "./src/app/user/profile/user-profile.resolver.ts");
/* harmony import */ var _language_language_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../language/language.service */ "./src/app/language/language.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");











var routes = [
    {
        path: '',
        component: _user_profile_page__WEBPACK_IMPORTED_MODULE_6__["UserProfilePage"],
        resolve: {
            data: _user_profile_resolver__WEBPACK_IMPORTED_MODULE_8__["UserProfileResolver"]
        }
    }
];
var UserProfilePageModule = /** @class */ (function () {
    function UserProfilePageModule() {
    }
    UserProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_10__["TranslateModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            declarations: [_user_profile_page__WEBPACK_IMPORTED_MODULE_6__["UserProfilePage"]],
            providers: [
                _user_profile_resolver__WEBPACK_IMPORTED_MODULE_8__["UserProfileResolver"],
                _user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"],
                _language_language_service__WEBPACK_IMPORTED_MODULE_9__["LanguageService"]
            ]
        })
    ], UserProfilePageModule);
    return UserProfilePageModule;
}());



/***/ }),

/***/ "./src/app/user/profile/user-profile.page.html":
/*!*****************************************************!*\
  !*** ./src/app/user/profile/user-profile.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"user-profile-content\">\n  <ion-row class=\"user-details-section\">\n    <ion-col class=\"user-image-wrapper\">\n      <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n        <app-image-shell class=\"user-image\" animation=\"spinner\" [src]=\"profile.userImage\"></app-image-shell>\n      </app-aspect-ratio>\n    </ion-col>\n    <ion-col class=\"user-info-wrapper\">\n      <ion-row class=\"user-data-row\">\n        <ion-col size=\"9\">\n          <h3 class=\"user-name\">\n            <app-text-shell [data]=\"profile.name\"></app-text-shell>\n          </h3>\n          <h5 class=\"user-title\">\n            <app-text-shell [data]=\"profile.job\"></app-text-shell>\n          </h5>\n        </ion-col>\n        <ion-col class=\"membership-col\">\n          <span class=\"user-membership\">\n            <app-text-shell [data]=\"profile.membership\"></app-text-shell>\n          </span>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"actions-row\">\n        <ion-col class=\"main-actions\">\n          <ion-button class=\"call-to-action-btn\" size=\"small\" color=\"primary\">{{ 'FOLLOW' | translate }}</ion-button>\n          <ion-button class=\"call-to-action-btn\" size=\"small\" color=\"medium\">{{ 'MESSAGE' | translate }}</ion-button>\n        </ion-col>\n        <ion-col class=\"secondary-actions\">\n          <ion-button class=\"more-btn\" size=\"small\" fill=\"clear\" color=\"medium\" (click)=\"openLanguageChooser()\">\n            <ion-icon slot=\"icon-only\" name=\"more\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n  </ion-row>\n  <ion-row class=\"user-stats-section\">\n    <ion-col class=\"user-stats-wrapper\" size=\"4\">\n      <span class=\"stat-value\">\n        <app-text-shell [data]=\"profile.likes\"></app-text-shell>\n      </span>\n      <span class=\"stat-name\">{{ 'LIKES' | translate }}</span>\n    </ion-col>\n    <ion-col class=\"user-stats-wrapper\" size=\"4\">\n      <span class=\"stat-value\">\n        <app-text-shell [data]=\"profile.followers\"></app-text-shell>\n      </span>\n      <span class=\"stat-name\">{{ 'FOLLOWERS' | translate }}</span>\n    </ion-col>\n    <ion-col class=\"user-stats-wrapper\" size=\"4\">\n      <span class=\"stat-value\">\n        <app-text-shell [data]=\"profile.following\"></app-text-shell>\n      </span>\n      <span class=\"stat-name\">{{ 'FOLLOWING' | translate }}</span>\n    </ion-col>\n  </ion-row>\n  <div class=\"user-about-section\">\n    <h3 class=\"details-section-title\">{{ 'ABOUT' | translate }}</h3>\n    <p class=\"user-description\">\n      <app-text-shell animation=\"bouncing\" lines=\"4\" [data]=\"profile.about\"></app-text-shell>\n    </p>\n  </div>\n  <div class=\"user-friends-section\">\n    <ion-row class=\"heading-row\">\n      <h3 class=\"details-section-title\">{{ 'FRIENDS' | translate }}</h3>\n      <a class=\"heading-call-to-action\" [routerLink]=\"['/app/user/friends']\">{{ 'SEE_ALL' | translate }}</a>\n    </ion-row>\n    <ion-row class=\"friends-row\">\n      <ion-col class=\"friend-item\" size=\"2\" *ngFor=\"let friend of profile.friends\">\n        <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n          <app-image-shell class=\"friend-picture\" [src]=\"friend.image\"></app-image-shell>\n        </app-aspect-ratio>\n        <span class=\"friend-name\">\n          <app-text-shell [data]=\"friend.name\"></app-text-shell>\n        </span>\n      </ion-col>\n    </ion-row>\n  </div>\n\n  <div class=\"user-photos-section\">\n    <ion-row class=\"heading-row\">\n      <h3 class=\"details-section-title\">{{ 'PHOTOS' | translate }}</h3>\n      <a class=\"heading-call-to-action\">{{ 'SEE_ALL' | translate }}</a>\n    </ion-row>\n    <ion-row class=\"pictures-row\">\n      <ion-col class=\"picture-item\" size=\"2\" *ngFor=\"let photo of profile.photos\">\n        <app-image-shell [mode]=\"'cover'\" [src]=\"photo.image\" class=\"user-photo-image\">\n          <app-aspect-ratio [ratio]=\"{w:1, h:1}\">\n          </app-aspect-ratio>\n        </app-image-shell>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/user/profile/user-profile.page.ts":
/*!***************************************************!*\
  !*** ./src/app/user/profile/user-profile.page.ts ***!
  \***************************************************/
/*! exports provided: UserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePage", function() { return UserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _language_language_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../language/language.service */ "./src/app/language/language.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");






var UserProfilePage = /** @class */ (function () {
    function UserProfilePage(route, translate, languageService, alertController) {
        this.route = route;
        this.translate = translate;
        this.languageService = languageService;
        this.alertController = alertController;
        this.available_languages = [];
    }
    Object.defineProperty(UserProfilePage.prototype, "isShell", {
        get: function () {
            return (this.profile && this.profile.isShell) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    UserProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (resolvedRouteData) {
            var profileDataStore = resolvedRouteData['data'];
            profileDataStore.state.subscribe(function (state) {
                _this.profile = state;
                // get translations for this page to use in the Language Chooser Alert
                _this.getTranslations();
                _this.translate.onLangChange.subscribe(function () {
                    _this.getTranslations();
                });
            }, function (error) { });
        }, function (error) { });
    };
    UserProfilePage.prototype.getTranslations = function () {
        var _this = this;
        // get translations for this page to use in the Language Chooser Alert
        this.translate.getTranslation(this.translate.currentLang)
            .subscribe(function (translations) {
            _this.translations = translations;
        });
    };
    UserProfilePage.prototype.openLanguageChooser = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.available_languages = this.languageService.getLanguages()
                            .map(function (item) {
                            return ({
                                name: item.name,
                                type: 'radio',
                                label: item.name,
                                value: item.code,
                                checked: item.code === _this.translate.currentLang
                            });
                        });
                        return [4 /*yield*/, this.alertController.create({
                                header: this.translations.SELECT_LANGUAGE,
                                inputs: this.available_languages,
                                cssClass: 'language-alert',
                                buttons: [
                                    {
                                        text: this.translations.CANCEL,
                                        role: 'cancel',
                                        cssClass: 'secondary',
                                        handler: function () { }
                                    }, {
                                        text: this.translations.OK,
                                        handler: function (data) {
                                            if (data) {
                                                _this.translate.use(data);
                                            }
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.is-shell'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserProfilePage.prototype, "isShell", null);
    UserProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-profile',
            template: __webpack_require__(/*! ./user-profile.page.html */ "./src/app/user/profile/user-profile.page.html"),
            styles: [__webpack_require__(/*! ./styles/user-profile.page.scss */ "./src/app/user/profile/styles/user-profile.page.scss"), __webpack_require__(/*! ./styles/user-profile.shell.scss */ "./src/app/user/profile/styles/user-profile.shell.scss"), __webpack_require__(/*! ./styles/user-profile.ios.scss */ "./src/app/user/profile/styles/user-profile.ios.scss"), __webpack_require__(/*! ./styles/user-profile.md.scss */ "./src/app/user/profile/styles/user-profile.md.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"],
            _language_language_service__WEBPACK_IMPORTED_MODULE_4__["LanguageService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]])
    ], UserProfilePage);
    return UserProfilePage;
}());



/***/ }),

/***/ "./src/app/user/profile/user-profile.resolver.ts":
/*!*******************************************************!*\
  !*** ./src/app/user/profile/user-profile.resolver.ts ***!
  \*******************************************************/
/*! exports provided: UserProfileResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfileResolver", function() { return UserProfileResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user.service */ "./src/app/user/user.service.ts");



var UserProfileResolver = /** @class */ (function () {
    function UserProfileResolver(userService) {
        this.userService = userService;
    }
    UserProfileResolver.prototype.resolve = function () {
        var dataSource = this.userService.getProfileDataSource();
        var dataStore = this.userService.getProfileStore(dataSource);
        return dataStore;
    };
    UserProfileResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]])
    ], UserProfileResolver);
    return UserProfileResolver;
}());



/***/ })

}]);
//# sourceMappingURL=user-profile-user-profile-module.js.map