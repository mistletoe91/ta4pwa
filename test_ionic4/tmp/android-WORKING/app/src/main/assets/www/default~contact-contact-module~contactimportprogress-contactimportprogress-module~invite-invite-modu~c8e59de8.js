(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~contact-contact-module~contactimportprogress-contactimportprogress-module~invite-invite-modu~c8e59de8"],{

/***/ "./src/services/contact.service.ts":
/*!*****************************************!*\
  !*** ./src/services/contact.service.ts ***!
  \*****************************************/
/*! exports provided: ContactService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactService", function() { return ContactService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/contacts/ngx */ "./node_modules/@ionic-native/contacts/ngx/index.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _app_const__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app/const */ "./src/app/const.ts");






var ContactService = /** @class */ (function () {
    function ContactService(events, contacts, sqlite) {
        this.events = events;
        this.contacts = contacts;
        this.sqlite = sqlite;
        this.contactInsertCounter = 0;
        this.contactErrorCounter = 0;
        this.contactTotalCounter = 0;
        this.contactInsertingDone = false;
        this.ContactCounterMargin = 40; //margin of error when calculating insert/error and total counter
        //this.deleteFromTable ();//mytodocritical_removeme_before_prod
    }
    ContactService.prototype.ContactsCount = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_5__["CONST"].create_table_statement_tapally_contacts, [])
                    .then(function (res) {
                    db.executeSql("SELECT count(*) as total_ctn  FROM cache_tapally_contacts", [])
                        .then(function (res) {
                        if (res.rows.length > 0) {
                            resolve(res.rows.item(0).total_ctn);
                        }
                        else {
                            resolve(0);
                        }
                    }).catch(function (e) { resolve(0); });
                }).catch(function (e) { resolve(0); });
            }).catch(function (e) { resolve(0); });
        });
    }; //end function
    ContactService.prototype.deleteFromTable = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_5__["CONST"].create_table_statement_tapally_contacts, [])
                    .then(function (res) {
                    db.executeSql("DELETE FROM cache_tapally_contacts", [])
                        .then(function (res) {
                        console.log("table cache_tapally_contacts dropped ");
                        resolve(true);
                    })
                        .catch(function (e) {
                        //Error in operation
                        console.log("ERROR 101");
                        console.log(e);
                        reject(false);
                    });
                })
                    .catch(function (e) {
                    //Error in operation
                    console.log("ERROR 102");
                    console.log(e);
                    reject(false);
                }); //create table
            }); //create database
        }).catch(function (e) { });
    }; //end function
    ContactService.prototype.getSimJsonContacts = function () {
        var _this = this;
        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_5__["CONST"].create_table_statement_tapally_contacts, [])
                    .then(function (res) {
                    console.log("Going to run query");
                    db.executeSql("SELECT *  FROM cache_tapally_contacts order by displayName", [])
                        .then(function (res) {
                        if (res.rows.length > 0) {
                            console.log("Record Found");
                            var mainArray = {};
                            for (var i = 0; i < res.rows.length; i++) {
                                mainArray[res.rows.item(i).displayName + '_' + res.rows.item(i).mobile] = res.rows.item(i);
                            } //end for
                            resolve({ contacts: mainArray, fromcache: true });
                        }
                        else {
                            console.log("No Contact found from sqlite. Lets get from contact list ");
                            //No contact found. LEts get from contact list
                            _this.contacts.find(["phoneNumbers", "displayName"], options).then(function (phone_contacts) {
                                if (phone_contacts != null) {
                                    resolve(_this.onSuccessSqlLite(phone_contacts, db));
                                }
                            }).catch(function (err) {
                                reject(err);
                            });
                        } //endif
                    }).catch(function (e) {
                        console.log(e);
                        reject();
                    });
                }).catch(function (e) {
                    console.log(e);
                    reject();
                });
            }).catch(function (e) {
                console.log(e);
                reject();
            });
        }).catch(function (error) {
            console.log("Error 81273 ");
        });
    }; //end function
    ContactService.prototype.CheckForUpdatedContacts = function () {
        var _this = this;
        var options = {
            filter: "",
            multiple: true,
            hasPhoneNumber: true
        };
        return new Promise(function (resolve, reject) {
            _this.sqlite.create({
                name: 'gr.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql(_app_const__WEBPACK_IMPORTED_MODULE_5__["CONST"].create_table_statement_tapally_contacts, [])
                    .then(function (res) {
                    db.executeSql("SELECT *  FROM cache_tapally_contacts", [])
                        .then(function (res) {
                        if (res.rows.length > 0) {
                            //////console.log ("Finding contacts in phone ")
                            _this.contacts.find(["phoneNumbers", "displayName"], options).then(function (phone_contacts) {
                                if (phone_contacts.length > res.rows.length) {
                                    //If contact on phone are more than what in our DB
                                    ////console.log (">>>>contact on phone are more than what in our DB");
                                    ////console.log (phone_contacts.length+"::::::"+res.rows.length);
                                    _this.onSuccessSqlLite(phone_contacts, db);
                                    //Uncommend following if returning value
                                    //resolve(this.onSuccessSqlLite(phone_contacts,db));
                                }
                                else {
                                    //console.log ("Resolve : No new contacts found  ")
                                    resolve(true);
                                    //Uncommend following if returning value
                                    //resolve({contacts: phoneContactsJson});
                                }
                            }).catch(function (err) {
                                resolve(true);
                                //Uncommend following if returning value
                                //resolve({contacts: phoneContactsJson});
                            });
                        } //endif
                    }).catch(function (e) { });
                }).catch(function (e) { });
            }).catch(function (e) { });
        }).catch(function (error) {
            //console.log ("Error 2371");
        });
    }; //end function
    ContactService.prototype.formatPhoneNumber = function (phoneNumberString) {
        var cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
        if (match) {
            var intlCode = (match[1] ? '+1 ' : '');
            return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
        }
        return "";
    };
    // Call   contact res
    ContactService.prototype.onSuccessSqlLite = function (contacts, db) {
        var _this = this;
        ////console.log ("Came hereee : " + contacts.length);
        var phoneContactsJson = {};
        //sort by name
        /*
        contacts = contacts.sort(function (a, b) {
          if (a.displayName != undefined) {
            var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (b.displayName != undefined) {
            var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase
          }
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          // names must be equal
          return 0;
        });
        */
        for (var i = 0; i < contacts.length; i++) {
            //////console.log ("Itetrating "+i);
            var no = "";
            if (contacts[i].name && contacts[i].name != null && contacts[i].name != undefined) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {
                for (var x = 0; x < phonenumber.length; x++) {
                    if (true) {
                        var phone = phonenumber[x].value;
                        var tempMobile = void 0;
                        tempMobile = phone.replace(/[^0-9]/g, "");
                        var mobile = void 0;
                        if (tempMobile.length > 10) {
                            mobile = tempMobile.substr(tempMobile.length - 10);
                        }
                        else {
                            mobile = tempMobile;
                        }
                        var contactData = {
                            "_id": mobile,
                            "displayName": no,
                            "mobile": mobile,
                            "mobile_formatted": this.formatPhoneNumber(mobile),
                            "status": "",
                            "isBlock": false,
                            "isUser": 0,
                            "isdisable": false,
                            "photoURL": "",
                            "uid": ""
                        };
                        if (contactData.displayName && contactData.displayName != "undefined" && contactData.mobile && contactData.mobile != "undefined") {
                            phoneContactsJson[mobile] = contactData;
                        }
                    } //endif
                } //end for
            } //endif
        } //end for
        //Now insert in db
        this.contactInsertCounter = 0;
        this.contactTotalCounter = 0;
        this.contactErrorCounter = 0;
        this.contactInsertingDone = false;
        Object.keys(phoneContactsJson).forEach(function (element) {
            phoneContactsJson[element].mobile = phoneContactsJson[element].mobile.replace(/'/g, "`"); //replace the '
            db.executeSql("INSERT INTO cache_tapally_contacts (id,displayName,mobile,mobile_formatted,isUser) VALUES('" + phoneContactsJson[element].mobile + "','" + phoneContactsJson[element].displayName + "','" + phoneContactsJson[element].mobile + "','" + phoneContactsJson[element].mobile_formatted + "',0)", [])
                .then(function (res1) {
                _this.contactInsertCounter++;
                _this.calculateAndMarkDone();
            }).catch(function (err) {
                _this.contactErrorCounter++;
                _this.calculateAndMarkDone();
            });
            _this.contactTotalCounter++;
        }); //end loop
        this.calculateAndMarkDone();
        var collection = {
            contacts: phoneContactsJson,
            fromcache: false
        };
        return collection;
    };
    ContactService.prototype.calculateAndMarkDone = function () {
        if (this.contactInsertCounter + this.contactErrorCounter + this.ContactCounterMargin >= this.contactTotalCounter) {
            this.contactInsertingDone = true;
            ////console.log ("Going to publish contactInsertingDone : "+this.contactTotalCounter);
            ////console.log (this.contactInsertCounter+this.contactErrorCounter+this.ContactCounterMargin);
            ////console.log ("----------------");
            this.events.publish('contactInsertingDone');
        }
    };
    ContactService.prototype.removeDuplicates = function (originalArray, prop) {
        var newArray = [];
        var lookupObject = {};
        originalArray.forEach(function (item, index) {
            lookupObject[originalArray[index][prop]] = originalArray[index];
        });
        Object.keys(lookupObject).forEach(function (element) {
            newArray.push(lookupObject[element]);
        });
        return newArray;
    };
    // Call onSuccess contact res : might be unused
    ContactService.prototype.onSuccess = function (contacts) {
        var phoneContactsJson = [];
        for (var i = 0; i < contacts.length; i++) {
            var no = void 0;
            if (contacts[i].name != null) {
                no = contacts[i].name.formatted;
            }
            var phonenumber = contacts[i].phoneNumbers;
            if (phonenumber != null) {
                var type = phonenumber[0].type;
                if (type == 'mobile' || type == 'work' || type == 'home') {
                    var phone = phonenumber[0].value;
                    var tempMobile = void 0;
                    tempMobile = phone.replace(/[^0-9]/g, "");
                    var mobile = void 0;
                    if (tempMobile.length > 10) {
                        mobile = tempMobile.substr(tempMobile.length - 10);
                    }
                    else {
                        mobile = tempMobile;
                    }
                    var contactData = {
                        "_id": mobile,
                        "displayName": no,
                        "mobile": mobile,
                        "isUser": '0'
                    };
                    phoneContactsJson.push(contactData);
                }
            }
        }
        var collection = {
            contacts: phoneContactsJson
        };
        return collection;
    };
    ContactService.prototype.addContact = function (newContact) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var contact = _this.contacts.create();
            contact.displayName = newContact.displayName;
            var field = new _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_3__["ContactField"]();
            field.type = 'mobile';
            field.value = newContact.phoneNumber;
            field.pref = true;
            var numberSection = [];
            numberSection.push(field);
            contact.phoneNumbers = numberSection;
            contact.save().then(function (value) {
                resolve(true);
            }, function (error) {
                reject(error);
            });
        }).catch(function (error) { });
    };
    ContactService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Events"], _ionic_native_contacts_ngx__WEBPACK_IMPORTED_MODULE_3__["Contacts"], _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_4__["SQLite"]])
    ], ContactService);
    return ContactService;
}());



/***/ })

}]);
//# sourceMappingURL=default~contact-contact-module~contactimportprogress-contactimportprogress-module~invite-invite-modu~c8e59de8.js.map