(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~forms-filters-forms-filters-module~notifications-notifications-module"],{

/***/ "./src/app/components/counter-input/counter-input.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/counter-input/counter-input.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"button-outer\">\n  <div class=\"button-wrapper\">\n    <ion-button class=\"counter-icon\" (click)=\"decrease()\">\n      <ion-icon slot=\"icon-only\" name=\"remove\"></ion-icon>\n    </ion-button>\n  </div>\n</div>\n<span class=\"counter-value\">{{ _counterValue }}</span>\n<div class=\"button-outer\">\n  <div class=\"button-wrapper\">\n    <ion-button class=\"counter-icon\" (click)=\"increase()\">\n      <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\n    </ion-button>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/counter-input/counter-input.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/counter-input/counter-input.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-counter-input {\n  --counter-background: #000;\n  --counter-color: #FFF;\n  --counter-border-color: #000;\n  --counter-border-radius-outer: 50%;\n  --counter-border-radius-inner: 50%;\n  --counter-size: 30px;\n  display: flex;\n  align-items: center;\n  justify-content: flex-end; }\n  app-counter-input ion-button.counter-icon {\n    --background: var(--counter-background);\n    --color: var(--counter-color);\n    --color-activated: var(--counter-color);\n    --border-width: 1px;\n    --border-style: solid;\n    --border-color: var(--counter-border-color);\n    --box-shadow: none;\n    --border-radius: var(--counter-border-radius-outer) var(--counter-border-radius-inner) var(--counter-border-radius-inner) var(--counter-border-radius-outer);\n    --padding-bottom: 3px;\n    --padding-end: 3px;\n    --padding-start: 3px;\n    --padding-top: 3px;\n    margin: 0px; }\n  app-counter-input .counter-value {\n    color: var(--counter-color);\n    margin: 0px 10px; }\n  app-counter-input:not([basic]) .button-outer {\n    width: var(--counter-size); }\n  app-counter-input:not([basic]) .button-outer .button-wrapper {\n      display: block;\n      overflow: hidden;\n      position: relative;\n      width: 100%;\n      padding-bottom: 100%; }\n  app-counter-input:not([basic]) .button-outer .button-wrapper .counter-icon {\n        position: absolute;\n        top: 0px;\n        bottom: 0px;\n        left: 0px;\n        right: 0px;\n        height: auto;\n        width: 100%; }\n  app-counter-input[basic] {\n    --counter-border-radius-outer: 12px;\n    --counter-border-radius-inner: 0px; }\n  app-counter-input[basic] .counter-value {\n      display: none; }\n  app-counter-input[basic] .button-outer:first-child ion-button.counter-icon {\n      --border-radius: var(--counter-border-radius-outer) var(--counter-border-radius-inner) var(--counter-border-radius-inner) var(--counter-border-radius-outer); }\n  app-counter-input[basic] .button-outer:last-child {\n      margin-left: -1px; }\n  app-counter-input[basic] .button-outer:last-child ion-button.counter-icon {\n        --border-radius: var(--counter-border-radius-inner) var(--counter-border-radius-outer) var(--counter-border-radius-outer) var(--counter-border-radius-inner); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9jb21wb25lbnRzL2NvdW50ZXItaW5wdXQvY291bnRlci1pbnB1dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBCQUFxQjtFQUNyQixxQkFBZ0I7RUFDaEIsNEJBQXVCO0VBQ3ZCLGtDQUE4QjtFQUM5QixrQ0FBOEI7RUFDOUIsb0JBQWU7RUFFZixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHlCQUF5QixFQUFBO0VBVjNCO0lBYUksdUNBQWE7SUFDYiw2QkFBUTtJQUNSLHVDQUFrQjtJQUNsQixtQkFBZTtJQUNmLHFCQUFlO0lBQ2YsMkNBQWU7SUFDZixrQkFBYTtJQUNiLDRKQUFnQjtJQUNoQixxQkFBaUI7SUFDakIsa0JBQWM7SUFDZCxvQkFBZ0I7SUFDaEIsa0JBQWM7SUFFZCxXQUFXLEVBQUE7RUExQmY7SUE4QkksMkJBQTJCO0lBQzNCLGdCQUFnQixFQUFBO0VBL0JwQjtJQXFDTSwwQkFBMEIsRUFBQTtFQXJDaEM7TUF3Q1EsY0FBYztNQUNkLGdCQUFnQjtNQUNoQixrQkFBa0I7TUFDbEIsV0FBVztNQUNYLG9CQUFvQixFQUFBO0VBNUM1QjtRQStDVSxrQkFBa0I7UUFDbEIsUUFBUTtRQUNSLFdBQVc7UUFDWCxTQUFTO1FBQ1QsVUFBVTtRQUNWLFlBQVk7UUFDWixXQUFXLEVBQUE7RUFyRHJCO0lBNERJLG1DQUE4QjtJQUM5QixrQ0FBOEIsRUFBQTtFQTdEbEM7TUFnRU0sYUFBYSxFQUFBO0VBaEVuQjtNQXNFVSw0SkFBZ0IsRUFBQTtFQXRFMUI7TUE0RVEsaUJBQWlCLEVBQUE7RUE1RXpCO1FBK0VVLDRKQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb3VudGVyLWlucHV0L2NvdW50ZXItaW5wdXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtY291bnRlci1pbnB1dCB7XG4gIC0tY291bnRlci1iYWNrZ3JvdW5kOiAjMDAwO1xuICAtLWNvdW50ZXItY29sb3I6ICNGRkY7XG4gIC0tY291bnRlci1ib3JkZXItY29sb3I6ICMwMDA7XG4gIC0tY291bnRlci1ib3JkZXItcmFkaXVzLW91dGVyOiA1MCU7XG4gIC0tY291bnRlci1ib3JkZXItcmFkaXVzLWlubmVyOiA1MCU7XG4gIC0tY291bnRlci1zaXplOiAzMHB4O1xuXG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG5cbiAgaW9uLWJ1dHRvbi5jb3VudGVyLWljb24ge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0tY291bnRlci1iYWNrZ3JvdW5kKTtcbiAgICAtLWNvbG9yOiB2YXIoLS1jb3VudGVyLWNvbG9yKTtcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogdmFyKC0tY291bnRlci1jb2xvcik7XG4gICAgLS1ib3JkZXItd2lkdGg6IDFweDtcbiAgICAtLWJvcmRlci1zdHlsZTogc29saWQ7XG4gICAgLS1ib3JkZXItY29sb3I6IHZhcigtLWNvdW50ZXItYm9yZGVyLWNvbG9yKTtcbiAgICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgLS1ib3JkZXItcmFkaXVzOiB2YXIoLS1jb3VudGVyLWJvcmRlci1yYWRpdXMtb3V0ZXIpIHZhcigtLWNvdW50ZXItYm9yZGVyLXJhZGl1cy1pbm5lcikgdmFyKC0tY291bnRlci1ib3JkZXItcmFkaXVzLWlubmVyKSB2YXIoLS1jb3VudGVyLWJvcmRlci1yYWRpdXMtb3V0ZXIpO1xuICAgIC0tcGFkZGluZy1ib3R0b206IDNweDtcbiAgICAtLXBhZGRpbmctZW5kOiAzcHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAzcHg7XG4gICAgLS1wYWRkaW5nLXRvcDogM3B4O1xuXG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cblxuICAuY291bnRlci12YWx1ZSB7XG4gICAgY29sb3I6IHZhcigtLWNvdW50ZXItY29sb3IpO1xuICAgIG1hcmdpbjogMHB4IDEwcHg7XG4gIH1cblxuICAmOm5vdChbYmFzaWNdKSB7XG4gICAgLy8gRm9yY2UgZWFjaCBjb3VudGVyIGJ1dHRvbiB0byBoYXZlIGEgMToxIGFzcGVjdCByYXRpb1xuICAgIC5idXR0b24tb3V0ZXIge1xuICAgICAgd2lkdGg6IHZhcigtLWNvdW50ZXItc2l6ZSk7XG5cbiAgICAgIC5idXR0b24td3JhcHBlciB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTAwJTtcblxuICAgICAgICAuY291bnRlci1pY29uIHtcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgdG9wOiAwcHg7XG4gICAgICAgICAgYm90dG9tOiAwcHg7XG4gICAgICAgICAgbGVmdDogMHB4O1xuICAgICAgICAgIHJpZ2h0OiAwcHg7XG4gICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgJltiYXNpY10ge1xuICAgIC0tY291bnRlci1ib3JkZXItcmFkaXVzLW91dGVyOiAxMnB4O1xuICAgIC0tY291bnRlci1ib3JkZXItcmFkaXVzLWlubmVyOiAwcHg7XG5cbiAgICAuY291bnRlci12YWx1ZSB7XG4gICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cblxuICAgIC5idXR0b24tb3V0ZXIge1xuICAgICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIGlvbi1idXR0b24uY291bnRlci1pY29uIHtcbiAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IHZhcigtLWNvdW50ZXItYm9yZGVyLXJhZGl1cy1vdXRlcikgdmFyKC0tY291bnRlci1ib3JkZXItcmFkaXVzLWlubmVyKSB2YXIoLS1jb3VudGVyLWJvcmRlci1yYWRpdXMtaW5uZXIpIHZhcigtLWNvdW50ZXItYm9yZGVyLXJhZGl1cy1vdXRlcik7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgICAgLy8gVG8gYXZvaWQgZG91YmxlIGJvcmRlclxuICAgICAgICBtYXJnaW4tbGVmdDogLTFweDtcblxuICAgICAgICBpb24tYnV0dG9uLmNvdW50ZXItaWNvbiB7XG4gICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiB2YXIoLS1jb3VudGVyLWJvcmRlci1yYWRpdXMtaW5uZXIpIHZhcigtLWNvdW50ZXItYm9yZGVyLXJhZGl1cy1vdXRlcikgdmFyKC0tY291bnRlci1ib3JkZXItcmFkaXVzLW91dGVyKSB2YXIoLS1jb3VudGVyLWJvcmRlci1yYWRpdXMtaW5uZXIpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/counter-input/counter-input.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/counter-input/counter-input.component.ts ***!
  \*********************************************************************/
/*! exports provided: counterRangeValidator, CounterInputComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "counterRangeValidator", function() { return counterRangeValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CounterInputComponent", function() { return CounterInputComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");



function counterRangeValidator(minValue, maxValue) {
    return function (c) {
        var err = {
            rangeError: {
                given: c.value,
                min: minValue || 0,
                max: maxValue || 10
            }
        };
        return (c.value > +maxValue || c.value < +minValue) ? err : null;
    };
}
var CounterInputComponent = /** @class */ (function () {
    function CounterInputComponent() {
        // tslint:disable-next-line:no-input-rename
        this._counterValue = 0;
        this.propagateChange = function () { }; // Noop function
        this.validateFn = function () { }; // Noop function
    }
    CounterInputComponent_1 = CounterInputComponent;
    Object.defineProperty(CounterInputComponent.prototype, "counterValue", {
        get: function () {
            return this._counterValue;
        },
        set: function (val) {
            this._counterValue = val;
            this.propagateChange(val);
        },
        enumerable: true,
        configurable: true
    });
    CounterInputComponent.prototype.ngOnChanges = function (inputs) {
        if (inputs.counterRangeMax || inputs.counterRangeMin) {
            this.validateFn = counterRangeValidator(this.counterRangeMin, this.counterRangeMax);
        }
    };
    CounterInputComponent.prototype.writeValue = function (value) {
        if (value) {
            this.counterValue = value;
        }
    };
    CounterInputComponent.prototype.registerOnChange = function (fn) {
        this.propagateChange = fn;
    };
    CounterInputComponent.prototype.registerOnTouched = function () { };
    CounterInputComponent.prototype.increase = function () {
        this.counterValue++;
    };
    CounterInputComponent.prototype.decrease = function () {
        this.counterValue--;
    };
    CounterInputComponent.prototype.validate = function (c) {
        return this.validateFn(c);
    };
    var CounterInputComponent_1;
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('counterValue'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CounterInputComponent.prototype, "_counterValue", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('max'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CounterInputComponent.prototype, "counterRangeMax", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('min'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], CounterInputComponent.prototype, "counterRangeMin", void 0);
    CounterInputComponent = CounterInputComponent_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-counter-input',
            template: __webpack_require__(/*! ./counter-input.component.html */ "./src/app/components/counter-input/counter-input.component.html"),
            providers: [
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return CounterInputComponent_1; }), multi: true },
                { provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"], useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () { return CounterInputComponent_1; }), multi: true }
            ],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./counter-input.component.scss */ "./src/app/components/counter-input/counter-input.component.scss")]
        })
    ], CounterInputComponent);
    return CounterInputComponent;
}());



/***/ })

}]);
//# sourceMappingURL=default~forms-filters-forms-filters-module~notifications-notifications-module.js.map