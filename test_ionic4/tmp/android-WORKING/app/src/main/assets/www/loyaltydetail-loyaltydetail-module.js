(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["loyaltydetail-loyaltydetail-module"],{

/***/ "./src/app/loyaltydetail/loyaltydetail.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/loyaltydetail/loyaltydetail.module.ts ***!
  \*******************************************************/
/*! exports provided: LoyaltydetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltydetailPageModule", function() { return LoyaltydetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _loyaltydetail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./loyaltydetail.page */ "./src/app/loyaltydetail/loyaltydetail.page.ts");







var routes = [
    {
        path: '',
        component: _loyaltydetail_page__WEBPACK_IMPORTED_MODULE_6__["LoyaltydetailPage"]
    }
];
var LoyaltydetailPageModule = /** @class */ (function () {
    function LoyaltydetailPageModule() {
    }
    LoyaltydetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_loyaltydetail_page__WEBPACK_IMPORTED_MODULE_6__["LoyaltydetailPage"]]
        })
    ], LoyaltydetailPageModule);
    return LoyaltydetailPageModule;
}());



/***/ }),

/***/ "./src/app/loyaltydetail/loyaltydetail.page.html":
/*!*******************************************************!*\
  !*** ./src/app/loyaltydetail/loyaltydetail.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Loyality Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-row class=\"categories-list\">\n    <ion-col class=\"category-item fashion-category firstitem \" size=\"12\">\n      <div class=\" centertext\">\n            <ion-card (click)=\"fnmaincardwork()\" class=\"businessitem\">\n              <ion-card-header>\n                <ion-card-subtitle>Megna Insurance</ion-card-subtitle>\n                <ion-card-title>Mauli Peecock</ion-card-title>\n              </ion-card-header>\n              <ion-card-content class=\"centertext\">\n                <h1>3 out of 10 Scans</h1>\n\n                <ion-progress-bar color=\"primary\" value=\"0.4\"></ion-progress-bar>\n\n                <!-- Notes in a List -->\n                <div class=\"textinfo\">\n                  <ion-text color=\"dark\" class=\"\">\n                    7 Scans more needed to redeem <h3><ion-badge color=\"primary\">Free Ice Cream</ion-badge></h3>\n                  </ion-text>\n                </div>\n\n              </ion-card-content>\n            </ion-card>\n      </div>\n\n    </ion-col>\n</ion-row>\n\n\n<ion-row class=\"categories-list\">\n  <ion-col class=\"\" size=\"12\">\n\n    <ion-card style=\"margin-top:0px;padding-top:0px;color:#fff;\" class=\"centertext\" >\n      <ion-card-header color=\"success\">\n        <ion-card-subtitle style=\"color:#fff;\"><ion-icon name=\"gift\" slot=\"start\"></ion-icon> You Earned A Free Gift</ion-card-subtitle>\n      </ion-card-header>\n      <ion-item color=\"success\" href=\"#\" detail=\"false\" class=\" centertext\" >\n        <ion-label style=\"color:#fff;\">Free Ice Cream</ion-label>\n      </ion-item>\n      <ion-item color=\"dark\" href=\"#\" class=\"activated centertext\" >\n        <ion-label style=\"color:#fff;font-weight:bold;\" >Mark It Redeemed</ion-label>\n      </ion-item>\n\n    </ion-card>\n\n    <ion-list>\n      <ion-item lines=\"none\">\n        <ion-note>Loyality gifts are auto redeemed by Mauli Peackock on your 10th Scan </ion-note>\n      </ion-item>\n\n       <ion-list-header>\n         <ion-label>Loyality History</ion-label>\n       </ion-list-header>\n       <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label color=\"primary\" >REDEEMED</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n         <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n         <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n         <ion-item detail = false><ion-label>1 Scan</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n     </ion-list>\n\n  </ion-col>\n</ion-row>\n\n\n<!-- fab placed to the bottom end -->\n<ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n  <ion-fab-button color=\"tertiary\">\n    <ion-icon   name=\"barcode\"></ion-icon>\n  </ion-fab-button>\n</ion-fab>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/loyaltydetail/loyaltydetail.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/loyaltydetail/loyaltydetail.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".textinfo {\n  margin-top: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9sb3lhbHR5ZGV0YWlsL2xveWFsdHlkZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9sb3lhbHR5ZGV0YWlsL2xveWFsdHlkZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHRpbmZvIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/loyaltydetail/loyaltydetail.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/loyaltydetail/loyaltydetail.page.ts ***!
  \*****************************************************/
/*! exports provided: LoyaltydetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltydetailPage", function() { return LoyaltydetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoyaltydetailPage = /** @class */ (function () {
    function LoyaltydetailPage() {
    }
    LoyaltydetailPage.prototype.ngOnInit = function () {
    };
    LoyaltydetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loyaltydetail',
            template: __webpack_require__(/*! ./loyaltydetail.page.html */ "./src/app/loyaltydetail/loyaltydetail.page.html"),
            styles: [__webpack_require__(/*! ./loyaltydetail.page.scss */ "./src/app/loyaltydetail/loyaltydetail.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LoyaltydetailPage);
    return LoyaltydetailPage;
}());



/***/ })

}]);
//# sourceMappingURL=loyaltydetail-loyaltydetail-module.js.map