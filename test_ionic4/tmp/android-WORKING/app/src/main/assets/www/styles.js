(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["styles"],{

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/app/app.scss":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/app/app.scss ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "ion-title {\n  font-weight: 400;\n  letter-spacing: 0.4px; }\n\n.hide {\n  display: none; }\n\n.centertext {\n  text-align: center; }\n\n.aligncenter {\n  margin: 0 auto; }\n\n.iconleftmargin {\n  margin-left: 5px; }\n\n.iconright {\n  margin: 0px 10px 0px 0px;\n  font-size: 28px; }\n\n.iconleft {\n  font-size: 28px; }\n\n.iconbtninsidebtn {\n  font-size: 28px; }\n\n.leftmargin_topbtn, .iconleft {\n  margin: 0px 0px 0px 10px; }\n\n/*card start*/\n\n.leftbarinfo {\n  margin-left: 10px;\n  font-size: 12px; }\n\n.referral_for_friend {\n  color: #000;\n  margin: 0px;\n  border-width: 0px 1px 1px 1px;\n  border-color: #e2dbf9;\n  border-style: solid;\n  background-color: #f1eefc !important;\n  padding: 7px 10px 7px 15px; }\n\n.hide {\n  display: none; }\n\n.show {\n  display: block; }\n\n.fadetext {\n  overflow: hidden;\n  font-size: 1.4rem;\n  text-overflow: clip;\n  color: #666; }\n\n.notpaidbar {\n  padding: 10px;\n  background-color: #c2565a !important;\n  color: #fff;\n  border: 1px solid #c2565a;\n  font-size: 15px; }\n\n/*Ripple Effect Start*/\n\n.mainitemlist:active {\n  background-color: #90EE90;\n  -webkit-animation: all 0.4s;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: all 0.4s; }\n\n/*Ripple Effect End*/\n\nion-content {\n  -webkit-animation: fadein 100ms;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: fadein 100ms; }\n\n.fadeInAnimation_Slow {\n  -webkit-animation: fadein 1s;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: fadein 1s; }\n\n.maincard_bubble {\n  background-image: linear-gradient(45deg, #f1eefc 0%, #f1eefc 99%, #fff 100%);\n  border-radius: 15px 50px 30px;\n  max-width: 300px;\n  margin: 100px auto;\n  -webkit-animation: fadein 1s;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: fadein 1s; }\n\n@keyframes fadein {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n/* Firefox < 16 */\n\n/* Safari, Chrome and Opera > 12.1 */\n\n@-webkit-keyframes fadein {\n  from {\n    opacity: 0; }\n  to {\n    opacity: 1; } }\n\n/* Internet Explorer */\n\n/* Opera < 12.1 */\n\n/*card end*/\n\n.hide {\n  display: none; }\n\n.tabs .tabbar {\n  background-color: #f1eefc;\n  border: 1px solid #e2dbf9; }\n\n.tabs-ios .tab-button[aria-selected=true] .tab-button-icon,\n.tabs-md .tab-button[aria-selected=true] .tab-button-icon,\n.tabs-ios .tab-button[aria-selected=true],\n.tabs-md .tab-button[aria-selected=true] {\n  color: #333 !important; }\n\n.atthebottom button {\n  font-size: 12px;\n  text-align: right;\n  float: right;\n  padding: 0px 10px 0px 0px;\n  margin: 0px; }\n\n.header .toolbar {\n  padding: 0px;\n  /*border-bottom: 2px solid #7041c1;*/ }\n\n/*\n,\n.toolbar > .toolbar-background\n*/\n\n.content-md,\n.content-ios {\n  background-color: #fff !important; }\n\n.herocard .content-title {\n  font-size: 18px; }\n\n.herocard {\n  padding-bottom: 20px; }\n\n.iconpick {\n  font-size: 25px; }\n\n.header .toolbar-md-dark .backbtncss,\n.header .toolbar .backbtncss {\n  font-size: 30px;\n  margin-right: 15px;\n  margin-left: 10px; }\n\n.btnInHeaderVeryLast {\n  margin-right: 15px; }\n\n.header .toolbar-md-dark .rightsidebtncss,\n.header .toolbar .rightsidebtncss {\n  font-size: 30px;\n  margin-right: 5px;\n  margin-top: -6px; }\n\n.righbtnontop {\n  margin-right: 10px; }\n\n.strongwords {\n  font-weight: normal; }\n\n.givepadding {\n  padding: 50px 10px; }\n\n.hideme {\n  display: none; }\n\n.pull-center {\n  text-align: center; }\n\n.myCustomSelect {\n  width: 100%;\n  max-width: 100% !important; }\n\n.topmargin-20 {\n  margin-top: 20px; }\n\n.header .toolbar-md-dark,\n.header .toolbar {\n  padding: 0px; }\n\n.title-uname > h5 {\n  padding-bottom: 0px;\n  margin-bottom: 0px;\n  padding-top: 3px;\n  margin-top: 3px; }\n\n.header-ios .bar-buttons-ios .title-uname > h5 {\n  padding-bottom: 0px;\n  margin-bottom: 0px;\n  padding-top: 0px;\n  margin-top: 0px; }\n\n/*\n.title.title-md {\n    position: absolute;\n    left: 50%;\n    top: 0;\n    transform: translateX(-50%);\n    height: 100%;\n    width: 70%;\n    text-align: center;\n}\n\n\n.brandion img {\n    height: 35px;\n    margin-top: 5px;\n}\n.brand-wording {\n    top: -11px;\n    position:relative;\n    color:#fff;\n}\n\n\n.helpicon {\n   margin-left:5px;\n}\n\n.top-margin-large {\n    margin-top: 170px;\n}\n\n.flex-end-notif {\n  display: flex;\n  flex-direction: row;\n  justify-content: flex-end;\n  align-items: center;\n  order:3;\n  ion-icon {\n    &.notification {\n      font-size: 28px !important;\n    }\n  }\n  ion-badge {\n    &.notification {\n      margin-left: -13px;\n      margin-top: -13px;\n      font-size: 11px;\n      padding: 4px;\n    }\n  }\n}\n*/\n\n/*\nion-footer {\n  background-color: map-get($colors, primary);\n  color: #fff;\n}\n.text_color_primary {\n  color: map-get($colors, primary);\n}\n.text_color_sec {\n  color: map-get($colors, secondary);\n}\n.toolbar-content > .title {\n  background-color: map-get($colors, primary);\n}\n.toolbar-title {\n  color:#fff;\n}\nion-header > ion-navbar .bar-button-default-ios,\nion-header > ion-navbar .bar-button-default.bar-button-ios-default,\nion-header > ion-navbar .bar-button-clear-ios-default {\n    color:#fff;\n}\nbutton.btn {\n    background-color:map-get($colors, primary);\n}\n\nion-header,\nion-header > ion-navbar,\nion-header > ion-navbar .toolbar-background\n{\n      background-color: map-get($colors, primary);\n}\n.toolbar-background\n{\n      background-color: map-get($colors, primary);\n}\n*/\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9hcHAuc2NzcyIsInNyYy9hcHAvYXBwLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCLEVBQUE7O0FBRXZCO0VBQ0UsYUFBYSxFQUFBOztBQUVmO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsY0FBYyxFQUFBOztBQUVoQjtFQUNFLGdCQUFnQixFQUFBOztBQUlsQjtFQUNFLHdCQUF3QjtFQUN4QixlQUFlLEVBQUE7O0FBRWpCO0VBQ0UsZUFBZSxFQUFBOztBQUVqQjtFQUNFLGVBQWUsRUFBQTs7QUFFakI7RUFDRSx3QkFBd0IsRUFBQTs7QUFLMUIsYUFBQTs7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFjLEVBQUE7O0FBRWhCO0VBQ0UsV0FBVTtFQUNWLFdBQVU7RUFDViw2QkFBNkI7RUFDN0IscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixvQ0FBb0M7RUFDcEMsMEJBQTBCLEVBQUE7O0FBRTVCO0VBQ0UsYUFBYSxFQUFBOztBQUVmO0VBQ0UsY0FBYyxFQUFBOztBQUVoQjtFQUNFLGdCQUFpQjtFQUNqQixpQkFBaUI7RUFDakIsbUJBQW9CO0VBQ3BCLFdBQVcsRUFBQTs7QUFLYjtFQUNFLGFBQWE7RUFDYixvQ0FBb0M7RUFDcEMsV0FBVTtFQUNWLHlCQUF5QjtFQUN6QixlQUFlLEVBQUE7O0FBSWpCLHNCQUFBOztBQUNBO0VBQ0kseUJBQXlCO0VBQ3pCLDJCQUEyQjtFQUFFLG9DQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ3JCLG1CQUFtQixFQUFBOztBQUcvQixvQkFBQTs7QUFHQTtFQUNFLCtCQUErQjtFQUFFLG9DQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ3pCLHVCQUF1QixFQUFBOztBQUdqQztFQUNFLDRCQUE0QjtFQUFFLG9DQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ3RCLG9CQUFvQixFQUFBOztBQUc5QjtFQUVFLDRFQUE0RTtFQUM1RSw2QkFBNkI7RUFDN0IsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQiw0QkFBNEI7RUFBRSxvQ0FBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUN0QixvQkFBb0IsRUFBQTs7QUFFOUI7RUFDSTtJQUFPLFVBQVUsRUFBQTtFQUNqQjtJQUFPLFVBQVUsRUFBQSxFQUFBOztBQUdyQixpQkFBQTs7QUFNQSxvQ0FBQTs7QUFDQTtFQUNJO0lBQU8sVUFBVSxFQUFBO0VBQ2pCO0lBQU8sVUFBVSxFQUFBLEVBQUE7O0FBR3JCLHNCQUFBOztBQU1BLGlCQUFBOztBQU1BLFdBQUE7O0FBRUE7RUFDRSxhQUFZLEVBQUE7O0FBRWQ7RUFDSSx5QkFBeUI7RUFDekIseUJBQXdCLEVBQUE7O0FBRTVCOzs7O0VBS0ksc0JBQXNCLEVBQUE7O0FBRzFCO0VBQ0ssZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixZQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFdBQVUsRUFBQTs7QUFFZjtFQUNJLFlBQVk7RUFDWixvQ0FBQSxFQUFxQzs7QUFHekM7OztDQ0lDOztBREFEOztFQUdJLGlDQUFpQyxFQUFBOztBQUVyQztFQUNJLGVBQWUsRUFBQTs7QUFFbkI7RUFDSSxvQkFBbUIsRUFBQTs7QUFFdkI7RUFDRyxlQUFlLEVBQUE7O0FBS2xCOztFQUVFLGVBQWU7RUFDZixrQkFBaUI7RUFDakIsaUJBQWdCLEVBQUE7O0FBRWxCO0VBQ0Usa0JBQWlCLEVBQUE7O0FBRW5COztFQUVFLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0Usa0JBQWlCLEVBQUE7O0FBRW5CO0VBQ0UsbUJBQW1CLEVBQUE7O0FBRXJCO0VBQ0Usa0JBQWtCLEVBQUE7O0FBRXBCO0VBQ0UsYUFBWSxFQUFBOztBQUVkO0VBQ0csa0JBQWtCLEVBQUE7O0FBRXJCO0VBQ0UsV0FBVztFQUNiLDBCQUEwQixFQUFBOztBQUUxQjtFQUNFLGdCQUFlLEVBQUE7O0FBRWpCOztFQUVJLFlBQVksRUFBQTs7QUFFaEI7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGdCQUFlO0VBQ2YsZUFBYyxFQUFBOztBQUVoQjtFQUNFLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZ0JBQWU7RUFDZixlQUFjLEVBQUE7O0FBRWhCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0NnREM7O0FESUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztDQ2lDQyIsImZpbGUiOiJzcmMvYXBwL2FwcC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNHB4O1xufVxuLmhpZGUge1xuICBkaXNwbGF5OiBub25lO1xufVxuLmNlbnRlcnRleHQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYWxpZ25jZW50ZXIge1xuICBtYXJnaW46IDAgYXV0bztcbn1cbi5pY29ubGVmdG1hcmdpbiB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG4uY2lyY3VsYXJidG4ge1xufVxuLmljb25yaWdodCB7XG4gIG1hcmdpbjogMHB4IDEwcHggMHB4IDBweDtcbiAgZm9udC1zaXplOiAyOHB4O1xufVxuLmljb25sZWZ0IHtcbiAgZm9udC1zaXplOiAyOHB4O1xufVxuLmljb25idG5pbnNpZGVidG4ge1xuICBmb250LXNpemU6IDI4cHg7XG59XG4ubGVmdG1hcmdpbl90b3BidG4sLmljb25sZWZ0IHtcbiAgbWFyZ2luOiAwcHggMHB4IDBweCAxMHB4O1xufVxuXG5cblxuLypjYXJkIHN0YXJ0Ki9cbi5sZWZ0YmFyaW5mbyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6MTJweDtcbn1cbi5yZWZlcnJhbF9mb3JfZnJpZW5kIHtcbiAgY29sb3I6IzAwMDtcbiAgbWFyZ2luOjBweDtcbiAgYm9yZGVyLXdpZHRoOiAwcHggMXB4IDFweCAxcHg7XG4gIGJvcmRlci1jb2xvcjogI2UyZGJmOTtcbiAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YxZWVmYyAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA3cHggMTBweCA3cHggMTVweDtcbn1cbi5oaWRlIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5zaG93IHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4uZmFkZXRleHQge1xuICBvdmVyZmxvdzogIGhpZGRlbjtcbiAgZm9udC1zaXplOiAxLjRyZW07XG4gIHRleHQtb3ZlcmZsb3c6ICBjbGlwO1xuICBjb2xvcjogIzY2Njtcbn1cbi5pdGVtc2lkZSB7XG5cbn1cbi5ub3RwYWlkYmFyIHtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyNTY1YSAhaW1wb3J0YW50O1xuICBjb2xvcjojZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjYzI1NjVhO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cblxuLypSaXBwbGUgRWZmZWN0IFN0YXJ0Ki9cbi5tYWluaXRlbWxpc3Q6YWN0aXZlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTBFRTkwO1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBhbGwgMC40czsgLyogU2FmYXJpLCBDaHJvbWUgYW5kIE9wZXJhID4gMTIuMSAqL1xuICAgICAgIC1tb3otYW5pbWF0aW9uOiBhbGwgMC40czsgLyogRmlyZWZveCA8IDE2ICovXG4gICAgICAgIC1tcy1hbmltYXRpb246IGFsbCAwLjRzOyAvKiBJbnRlcm5ldCBFeHBsb3JlciAqL1xuICAgICAgICAgLW8tYW5pbWF0aW9uOiBhbGwgMC40czsgLyogT3BlcmEgPCAxMi4xICovXG4gICAgICAgICAgICBhbmltYXRpb246IGFsbCAwLjRzO1xuXG59XG4vKlJpcHBsZSBFZmZlY3QgRW5kKi9cblxuXG5pb24tY29udGVudCB7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlaW4gMTAwbXM7IC8qIFNhZmFyaSwgQ2hyb21lIGFuZCBPcGVyYSA+IDEyLjEgKi9cbiAgICAgLW1vei1hbmltYXRpb246IGZhZGVpbiAxMDBtczsgLyogRmlyZWZveCA8IDE2ICovXG4gICAgICAtbXMtYW5pbWF0aW9uOiBmYWRlaW4gMTAwbXM7IC8qIEludGVybmV0IEV4cGxvcmVyICovXG4gICAgICAgLW8tYW5pbWF0aW9uOiBmYWRlaW4gMTAwbXM7IC8qIE9wZXJhIDwgMTIuMSAqL1xuICAgICAgICAgIGFuaW1hdGlvbjogZmFkZWluIDEwMG1zO1xufVxuXG4uZmFkZUluQW5pbWF0aW9uX1Nsb3cge1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZWluIDFzOyAvKiBTYWZhcmksIENocm9tZSBhbmQgT3BlcmEgPiAxMi4xICovXG4gICAgIC1tb3otYW5pbWF0aW9uOiBmYWRlaW4gMXM7IC8qIEZpcmVmb3ggPCAxNiAqL1xuICAgICAgLW1zLWFuaW1hdGlvbjogZmFkZWluIDFzOyAvKiBJbnRlcm5ldCBFeHBsb3JlciAqL1xuICAgICAgIC1vLWFuaW1hdGlvbjogZmFkZWluIDFzOyAvKiBPcGVyYSA8IDEyLjEgKi9cbiAgICAgICAgICBhbmltYXRpb246IGZhZGVpbiAxcztcbn1cblxuLm1haW5jYXJkX2J1YmJsZSB7XG5cbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjZjFlZWZjIDAlLCAjZjFlZWZjIDk5JSwgI2ZmZiAxMDAlKTtcbiAgYm9yZGVyLXJhZGl1czogMTVweCA1MHB4IDMwcHg7XG4gIG1heC13aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogMTAwcHggYXV0bztcbiAgLXdlYmtpdC1hbmltYXRpb246IGZhZGVpbiAxczsgLyogU2FmYXJpLCBDaHJvbWUgYW5kIE9wZXJhID4gMTIuMSAqL1xuICAgICAtbW96LWFuaW1hdGlvbjogZmFkZWluIDFzOyAvKiBGaXJlZm94IDwgMTYgKi9cbiAgICAgIC1tcy1hbmltYXRpb246IGZhZGVpbiAxczsgLyogSW50ZXJuZXQgRXhwbG9yZXIgKi9cbiAgICAgICAtby1hbmltYXRpb246IGZhZGVpbiAxczsgLyogT3BlcmEgPCAxMi4xICovXG4gICAgICAgICAgYW5pbWF0aW9uOiBmYWRlaW4gMXM7XG59XG5Aa2V5ZnJhbWVzIGZhZGVpbiB7XG4gICAgZnJvbSB7IG9wYWNpdHk6IDA7IH1cbiAgICB0byAgIHsgb3BhY2l0eTogMTsgfVxufVxuXG4vKiBGaXJlZm94IDwgMTYgKi9cbkAtbW96LWtleWZyYW1lcyBmYWRlaW4ge1xuICAgIGZyb20geyBvcGFjaXR5OiAwOyB9XG4gICAgdG8gICB7IG9wYWNpdHk6IDE7IH1cbn1cblxuLyogU2FmYXJpLCBDaHJvbWUgYW5kIE9wZXJhID4gMTIuMSAqL1xuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVpbiB7XG4gICAgZnJvbSB7IG9wYWNpdHk6IDA7IH1cbiAgICB0byAgIHsgb3BhY2l0eTogMTsgfVxufVxuXG4vKiBJbnRlcm5ldCBFeHBsb3JlciAqL1xuQC1tcy1rZXlmcmFtZXMgZmFkZWluIHtcbiAgICBmcm9tIHsgb3BhY2l0eTogMDsgfVxuICAgIHRvICAgeyBvcGFjaXR5OiAxOyB9XG59XG5cbi8qIE9wZXJhIDwgMTIuMSAqL1xuQC1vLWtleWZyYW1lcyBmYWRlaW4ge1xuICAgIGZyb20geyBvcGFjaXR5OiAwOyB9XG4gICAgdG8gICB7IG9wYWNpdHk6IDE7IH1cbn1cblxuLypjYXJkIGVuZCovXG5cbi5oaWRlIHtcbiAgZGlzcGxheTpub25lO1xufVxuLnRhYnMgLnRhYmJhciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YxZWVmYztcbiAgICBib3JkZXI6MXB4IHNvbGlkICNlMmRiZjk7XG59XG4udGFicy1pb3MgLnRhYi1idXR0b25bYXJpYS1zZWxlY3RlZD10cnVlXSAudGFiLWJ1dHRvbi1pY29uLFxuLnRhYnMtbWQgLnRhYi1idXR0b25bYXJpYS1zZWxlY3RlZD10cnVlXSAudGFiLWJ1dHRvbi1pY29uLFxuLnRhYnMtaW9zIC50YWItYnV0dG9uW2FyaWEtc2VsZWN0ZWQ9dHJ1ZV0sXG4udGFicy1tZCAudGFiLWJ1dHRvblthcmlhLXNlbGVjdGVkPXRydWVdXG57XG4gICAgY29sb3I6ICMzMzMgIWltcG9ydGFudDtcbn1cblxuLmF0dGhlYm90dG9tIGJ1dHRvbiB7XG4gICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgIGZsb2F0OnJpZ2h0O1xuICAgICBwYWRkaW5nOiAwcHggMTBweCAwcHggMHB4O1xuICAgICBtYXJnaW46MHB4O1xufVxuLmhlYWRlciAudG9vbGJhciB7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIC8qYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICM3MDQxYzE7Ki9cbn1cblxuLypcbixcbi50b29sYmFyID4gLnRvb2xiYXItYmFja2dyb3VuZFxuKi9cbi5jb250ZW50LW1kLFxuLmNvbnRlbnQtaW9zXG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xufVxuLmhlcm9jYXJkIC5jb250ZW50LXRpdGxlIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG59XG4uaGVyb2NhcmQge1xuICAgIHBhZGRpbmctYm90dG9tOjIwcHg7XG59XG4uaWNvbnBpY2sge1xuICAgZm9udC1zaXplOiAyNXB4O1xufVxuXG5cblxuLmhlYWRlciAudG9vbGJhci1tZC1kYXJrIC5iYWNrYnRuY3NzLFxuLmhlYWRlciAudG9vbGJhciAuYmFja2J0bmNzcyB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgbWFyZ2luLXJpZ2h0OjE1cHg7XG4gIG1hcmdpbi1sZWZ0OjEwcHg7XG59XG4uYnRuSW5IZWFkZXJWZXJ5TGFzdCB7XG4gIG1hcmdpbi1yaWdodDoxNXB4O1xufVxuLmhlYWRlciAudG9vbGJhci1tZC1kYXJrIC5yaWdodHNpZGVidG5jc3MsXG4uaGVhZGVyIC50b29sYmFyIC5yaWdodHNpZGVidG5jc3Mge1xuICBmb250LXNpemU6IDMwcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAtNnB4O1xufVxuLnJpZ2hidG5vbnRvcCB7XG4gIG1hcmdpbi1yaWdodDoxMHB4O1xufVxuLnN0cm9uZ3dvcmRzIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbi5naXZlcGFkZGluZ3tcbiAgcGFkZGluZzogNTBweCAxMHB4O1xufVxuLmhpZGVtZSB7XG4gIGRpc3BsYXk6bm9uZTtcbn1cbi5wdWxsLWNlbnRlciB7XG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubXlDdXN0b21TZWxlY3R7XG4gIHdpZHRoOiAxMDAlO1xubWF4LXdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG59XG4udG9wbWFyZ2luLTIwIHtcbiAgbWFyZ2luLXRvcDoyMHB4O1xufVxuLmhlYWRlciAudG9vbGJhci1tZC1kYXJrLFxuLmhlYWRlciAudG9vbGJhciB7XG4gICAgcGFkZGluZzogMHB4O1xufVxuLnRpdGxlLXVuYW1lID4gaDUge1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHBhZGRpbmctdG9wOjNweDtcbiAgbWFyZ2luLXRvcDozcHg7XG59XG4uaGVhZGVyLWlvcyAuYmFyLWJ1dHRvbnMtaW9zIC50aXRsZS11bmFtZSA+IGg1IHtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLXRvcDowcHg7XG4gIG1hcmdpbi10b3A6MHB4O1xufVxuLypcbi50aXRsZS50aXRsZS1tZCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0b3A6IDA7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogNzAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG4uYnJhbmRpb24gaW1nIHtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufVxuLmJyYW5kLXdvcmRpbmcge1xuICAgIHRvcDogLTExcHg7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgY29sb3I6I2ZmZjtcbn1cblxuXG4uaGVscGljb24ge1xuICAgbWFyZ2luLWxlZnQ6NXB4O1xufVxuXG4udG9wLW1hcmdpbi1sYXJnZSB7XG4gICAgbWFyZ2luLXRvcDogMTcwcHg7XG59XG5cbi5mbGV4LWVuZC1ub3RpZiB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG9yZGVyOjM7XG4gIGlvbi1pY29uIHtcbiAgICAmLm5vdGlmaWNhdGlvbiB7XG4gICAgICBmb250LXNpemU6IDI4cHggIWltcG9ydGFudDtcbiAgICB9XG4gIH1cbiAgaW9uLWJhZGdlIHtcbiAgICAmLm5vdGlmaWNhdGlvbiB7XG4gICAgICBtYXJnaW4tbGVmdDogLTEzcHg7XG4gICAgICBtYXJnaW4tdG9wOiAtMTNweDtcbiAgICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICAgIHBhZGRpbmc6IDRweDtcbiAgICB9XG4gIH1cbn1cbiovXG4vKlxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQtY29sb3I6IG1hcC1nZXQoJGNvbG9ycywgcHJpbWFyeSk7XG4gIGNvbG9yOiAjZmZmO1xufVxuLnRleHRfY29sb3JfcHJpbWFyeSB7XG4gIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIHByaW1hcnkpO1xufVxuLnRleHRfY29sb3Jfc2VjIHtcbiAgY29sb3I6IG1hcC1nZXQoJGNvbG9ycywgc2Vjb25kYXJ5KTtcbn1cbi50b29sYmFyLWNvbnRlbnQgPiAudGl0bGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIHByaW1hcnkpO1xufVxuLnRvb2xiYXItdGl0bGUge1xuICBjb2xvcjojZmZmO1xufVxuaW9uLWhlYWRlciA+IGlvbi1uYXZiYXIgLmJhci1idXR0b24tZGVmYXVsdC1pb3MsXG5pb24taGVhZGVyID4gaW9uLW5hdmJhciAuYmFyLWJ1dHRvbi1kZWZhdWx0LmJhci1idXR0b24taW9zLWRlZmF1bHQsXG5pb24taGVhZGVyID4gaW9uLW5hdmJhciAuYmFyLWJ1dHRvbi1jbGVhci1pb3MtZGVmYXVsdCB7XG4gICAgY29sb3I6I2ZmZjtcbn1cbmJ1dHRvbi5idG4ge1xuICAgIGJhY2tncm91bmQtY29sb3I6bWFwLWdldCgkY29sb3JzLCBwcmltYXJ5KTtcbn1cblxuaW9uLWhlYWRlcixcbmlvbi1oZWFkZXIgPiBpb24tbmF2YmFyLFxuaW9uLWhlYWRlciA+IGlvbi1uYXZiYXIgLnRvb2xiYXItYmFja2dyb3VuZFxue1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogbWFwLWdldCgkY29sb3JzLCBwcmltYXJ5KTtcbn1cbi50b29sYmFyLWJhY2tncm91bmRcbntcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IG1hcC1nZXQoJGNvbG9ycywgcHJpbWFyeSk7XG59XG4qL1xuIiwiaW9uLXRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNHB4OyB9XG5cbi5oaWRlIHtcbiAgZGlzcGxheTogbm9uZTsgfVxuXG4uY2VudGVydGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjsgfVxuXG4uYWxpZ25jZW50ZXIge1xuICBtYXJnaW46IDAgYXV0bzsgfVxuXG4uaWNvbmxlZnRtYXJnaW4ge1xuICBtYXJnaW4tbGVmdDogNXB4OyB9XG5cbi5pY29ucmlnaHQge1xuICBtYXJnaW46IDBweCAxMHB4IDBweCAwcHg7XG4gIGZvbnQtc2l6ZTogMjhweDsgfVxuXG4uaWNvbmxlZnQge1xuICBmb250LXNpemU6IDI4cHg7IH1cblxuLmljb25idG5pbnNpZGVidG4ge1xuICBmb250LXNpemU6IDI4cHg7IH1cblxuLmxlZnRtYXJnaW5fdG9wYnRuLCAuaWNvbmxlZnQge1xuICBtYXJnaW46IDBweCAwcHggMHB4IDEwcHg7IH1cblxuLypjYXJkIHN0YXJ0Ki9cbi5sZWZ0YmFyaW5mbyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBmb250LXNpemU6IDEycHg7IH1cblxuLnJlZmVycmFsX2Zvcl9mcmllbmQge1xuICBjb2xvcjogIzAwMDtcbiAgbWFyZ2luOiAwcHg7XG4gIGJvcmRlci13aWR0aDogMHB4IDFweCAxcHggMXB4O1xuICBib3JkZXItY29sb3I6ICNlMmRiZjk7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWVlZmMgIWltcG9ydGFudDtcbiAgcGFkZGluZzogN3B4IDEwcHggN3B4IDE1cHg7IH1cblxuLmhpZGUge1xuICBkaXNwbGF5OiBub25lOyB9XG5cbi5zaG93IHtcbiAgZGlzcGxheTogYmxvY2s7IH1cblxuLmZhZGV0ZXh0IHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgZm9udC1zaXplOiAxLjRyZW07XG4gIHRleHQtb3ZlcmZsb3c6IGNsaXA7XG4gIGNvbG9yOiAjNjY2OyB9XG5cbi5ub3RwYWlkYmFyIHtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2MyNTY1YSAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZmZjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2MyNTY1YTtcbiAgZm9udC1zaXplOiAxNXB4OyB9XG5cbi8qUmlwcGxlIEVmZmVjdCBTdGFydCovXG4ubWFpbml0ZW1saXN0OmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5MEVFOTA7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBhbGwgMC40cztcbiAgLyogU2FmYXJpLCBDaHJvbWUgYW5kIE9wZXJhID4gMTIuMSAqL1xuICAtbW96LWFuaW1hdGlvbjogYWxsIDAuNHM7XG4gIC8qIEZpcmVmb3ggPCAxNiAqL1xuICAtbXMtYW5pbWF0aW9uOiBhbGwgMC40cztcbiAgLyogSW50ZXJuZXQgRXhwbG9yZXIgKi9cbiAgLW8tYW5pbWF0aW9uOiBhbGwgMC40cztcbiAgLyogT3BlcmEgPCAxMi4xICovXG4gIGFuaW1hdGlvbjogYWxsIDAuNHM7IH1cblxuLypSaXBwbGUgRWZmZWN0IEVuZCovXG5pb24tY29udGVudCB7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlaW4gMTAwbXM7XG4gIC8qIFNhZmFyaSwgQ2hyb21lIGFuZCBPcGVyYSA+IDEyLjEgKi9cbiAgLW1vei1hbmltYXRpb246IGZhZGVpbiAxMDBtcztcbiAgLyogRmlyZWZveCA8IDE2ICovXG4gIC1tcy1hbmltYXRpb246IGZhZGVpbiAxMDBtcztcbiAgLyogSW50ZXJuZXQgRXhwbG9yZXIgKi9cbiAgLW8tYW5pbWF0aW9uOiBmYWRlaW4gMTAwbXM7XG4gIC8qIE9wZXJhIDwgMTIuMSAqL1xuICBhbmltYXRpb246IGZhZGVpbiAxMDBtczsgfVxuXG4uZmFkZUluQW5pbWF0aW9uX1Nsb3cge1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZWluIDFzO1xuICAvKiBTYWZhcmksIENocm9tZSBhbmQgT3BlcmEgPiAxMi4xICovXG4gIC1tb3otYW5pbWF0aW9uOiBmYWRlaW4gMXM7XG4gIC8qIEZpcmVmb3ggPCAxNiAqL1xuICAtbXMtYW5pbWF0aW9uOiBmYWRlaW4gMXM7XG4gIC8qIEludGVybmV0IEV4cGxvcmVyICovXG4gIC1vLWFuaW1hdGlvbjogZmFkZWluIDFzO1xuICAvKiBPcGVyYSA8IDEyLjEgKi9cbiAgYW5pbWF0aW9uOiBmYWRlaW4gMXM7IH1cblxuLm1haW5jYXJkX2J1YmJsZSB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2YxZWVmYyAwJSwgI2YxZWVmYyA5OSUsICNmZmYgMTAwJSk7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHggNTBweCAzMHB4O1xuICBtYXgtd2lkdGg6IDMwMHB4O1xuICBtYXJnaW46IDEwMHB4IGF1dG87XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBmYWRlaW4gMXM7XG4gIC8qIFNhZmFyaSwgQ2hyb21lIGFuZCBPcGVyYSA+IDEyLjEgKi9cbiAgLW1vei1hbmltYXRpb246IGZhZGVpbiAxcztcbiAgLyogRmlyZWZveCA8IDE2ICovXG4gIC1tcy1hbmltYXRpb246IGZhZGVpbiAxcztcbiAgLyogSW50ZXJuZXQgRXhwbG9yZXIgKi9cbiAgLW8tYW5pbWF0aW9uOiBmYWRlaW4gMXM7XG4gIC8qIE9wZXJhIDwgMTIuMSAqL1xuICBhbmltYXRpb246IGZhZGVpbiAxczsgfVxuXG5Aa2V5ZnJhbWVzIGZhZGVpbiB7XG4gIGZyb20ge1xuICAgIG9wYWNpdHk6IDA7IH1cbiAgdG8ge1xuICAgIG9wYWNpdHk6IDE7IH0gfVxuXG4vKiBGaXJlZm94IDwgMTYgKi9cbkAtbW96LWtleWZyYW1lcyBmYWRlaW4ge1xuICBmcm9tIHtcbiAgICBvcGFjaXR5OiAwOyB9XG4gIHRvIHtcbiAgICBvcGFjaXR5OiAxOyB9IH1cblxuLyogU2FmYXJpLCBDaHJvbWUgYW5kIE9wZXJhID4gMTIuMSAqL1xuQC13ZWJraXQta2V5ZnJhbWVzIGZhZGVpbiB7XG4gIGZyb20ge1xuICAgIG9wYWNpdHk6IDA7IH1cbiAgdG8ge1xuICAgIG9wYWNpdHk6IDE7IH0gfVxuXG4vKiBJbnRlcm5ldCBFeHBsb3JlciAqL1xuQC1tcy1rZXlmcmFtZXMgZmFkZWluIHtcbiAgZnJvbSB7XG4gICAgb3BhY2l0eTogMDsgfVxuICB0byB7XG4gICAgb3BhY2l0eTogMTsgfSB9XG5cbi8qIE9wZXJhIDwgMTIuMSAqL1xuQC1vLWtleWZyYW1lcyBmYWRlaW4ge1xuICBmcm9tIHtcbiAgICBvcGFjaXR5OiAwOyB9XG4gIHRvIHtcbiAgICBvcGFjaXR5OiAxOyB9IH1cblxuLypjYXJkIGVuZCovXG4uaGlkZSB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cblxuLnRhYnMgLnRhYmJhciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmMWVlZmM7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmRiZjk7IH1cblxuLnRhYnMtaW9zIC50YWItYnV0dG9uW2FyaWEtc2VsZWN0ZWQ9dHJ1ZV0gLnRhYi1idXR0b24taWNvbixcbi50YWJzLW1kIC50YWItYnV0dG9uW2FyaWEtc2VsZWN0ZWQ9dHJ1ZV0gLnRhYi1idXR0b24taWNvbixcbi50YWJzLWlvcyAudGFiLWJ1dHRvblthcmlhLXNlbGVjdGVkPXRydWVdLFxuLnRhYnMtbWQgLnRhYi1idXR0b25bYXJpYS1zZWxlY3RlZD10cnVlXSB7XG4gIGNvbG9yOiAjMzMzICFpbXBvcnRhbnQ7IH1cblxuLmF0dGhlYm90dG9tIGJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogMHB4IDEwcHggMHB4IDBweDtcbiAgbWFyZ2luOiAwcHg7IH1cblxuLmhlYWRlciAudG9vbGJhciB7XG4gIHBhZGRpbmc6IDBweDtcbiAgLypib3JkZXItYm90dG9tOiAycHggc29saWQgIzcwNDFjMTsqLyB9XG5cbi8qXG4sXG4udG9vbGJhciA+IC50b29sYmFyLWJhY2tncm91bmRcbiovXG4uY29udGVudC1tZCxcbi5jb250ZW50LWlvcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmYgIWltcG9ydGFudDsgfVxuXG4uaGVyb2NhcmQgLmNvbnRlbnQtdGl0bGUge1xuICBmb250LXNpemU6IDE4cHg7IH1cblxuLmhlcm9jYXJkIHtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7IH1cblxuLmljb25waWNrIHtcbiAgZm9udC1zaXplOiAyNXB4OyB9XG5cbi5oZWFkZXIgLnRvb2xiYXItbWQtZGFyayAuYmFja2J0bmNzcyxcbi5oZWFkZXIgLnRvb2xiYXIgLmJhY2tidG5jc3Mge1xuICBmb250LXNpemU6IDMwcHg7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7IH1cblxuLmJ0bkluSGVhZGVyVmVyeUxhc3Qge1xuICBtYXJnaW4tcmlnaHQ6IDE1cHg7IH1cblxuLmhlYWRlciAudG9vbGJhci1tZC1kYXJrIC5yaWdodHNpZGVidG5jc3MsXG4uaGVhZGVyIC50b29sYmFyIC5yaWdodHNpZGVidG5jc3Mge1xuICBmb250LXNpemU6IDMwcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAtNnB4OyB9XG5cbi5yaWdoYnRub250b3Age1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7IH1cblxuLnN0cm9uZ3dvcmRzIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDsgfVxuXG4uZ2l2ZXBhZGRpbmcge1xuICBwYWRkaW5nOiA1MHB4IDEwcHg7IH1cblxuLmhpZGVtZSB7XG4gIGRpc3BsYXk6IG5vbmU7IH1cblxuLnB1bGwtY2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5teUN1c3RvbVNlbGVjdCB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCUgIWltcG9ydGFudDsgfVxuXG4udG9wbWFyZ2luLTIwIHtcbiAgbWFyZ2luLXRvcDogMjBweDsgfVxuXG4uaGVhZGVyIC50b29sYmFyLW1kLWRhcmssXG4uaGVhZGVyIC50b29sYmFyIHtcbiAgcGFkZGluZzogMHB4OyB9XG5cbi50aXRsZS11bmFtZSA+IGg1IHtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nLXRvcDogM3B4O1xuICBtYXJnaW4tdG9wOiAzcHg7IH1cblxuLmhlYWRlci1pb3MgLmJhci1idXR0b25zLWlvcyAudGl0bGUtdW5hbWUgPiBoNSB7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgbWFyZ2luLXRvcDogMHB4OyB9XG5cbi8qXG4udGl0bGUudGl0bGUtbWQge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdG9wOiAwO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDcwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cblxuLmJyYW5kaW9uIGltZyB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIG1hcmdpbi10b3A6IDVweDtcbn1cbi5icmFuZC13b3JkaW5nIHtcbiAgICB0b3A6IC0xMXB4O1xuICAgIHBvc2l0aW9uOnJlbGF0aXZlO1xuICAgIGNvbG9yOiNmZmY7XG59XG5cblxuLmhlbHBpY29uIHtcbiAgIG1hcmdpbi1sZWZ0OjVweDtcbn1cblxuLnRvcC1tYXJnaW4tbGFyZ2Uge1xuICAgIG1hcmdpbi10b3A6IDE3MHB4O1xufVxuXG4uZmxleC1lbmQtbm90aWYge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBvcmRlcjozO1xuICBpb24taWNvbiB7XG4gICAgJi5ub3RpZmljYXRpb24ge1xuICAgICAgZm9udC1zaXplOiAyOHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuICB9XG4gIGlvbi1iYWRnZSB7XG4gICAgJi5ub3RpZmljYXRpb24ge1xuICAgICAgbWFyZ2luLWxlZnQ6IC0xM3B4O1xuICAgICAgbWFyZ2luLXRvcDogLTEzcHg7XG4gICAgICBmb250LXNpemU6IDExcHg7XG4gICAgICBwYWRkaW5nOiA0cHg7XG4gICAgfVxuICB9XG59XG4qL1xuLypcbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIHByaW1hcnkpO1xuICBjb2xvcjogI2ZmZjtcbn1cbi50ZXh0X2NvbG9yX3ByaW1hcnkge1xuICBjb2xvcjogbWFwLWdldCgkY29sb3JzLCBwcmltYXJ5KTtcbn1cbi50ZXh0X2NvbG9yX3NlYyB7XG4gIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIHNlY29uZGFyeSk7XG59XG4udG9vbGJhci1jb250ZW50ID4gLnRpdGxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbWFwLWdldCgkY29sb3JzLCBwcmltYXJ5KTtcbn1cbi50b29sYmFyLXRpdGxlIHtcbiAgY29sb3I6I2ZmZjtcbn1cbmlvbi1oZWFkZXIgPiBpb24tbmF2YmFyIC5iYXItYnV0dG9uLWRlZmF1bHQtaW9zLFxuaW9uLWhlYWRlciA+IGlvbi1uYXZiYXIgLmJhci1idXR0b24tZGVmYXVsdC5iYXItYnV0dG9uLWlvcy1kZWZhdWx0LFxuaW9uLWhlYWRlciA+IGlvbi1uYXZiYXIgLmJhci1idXR0b24tY2xlYXItaW9zLWRlZmF1bHQge1xuICAgIGNvbG9yOiNmZmY7XG59XG5idXR0b24uYnRuIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOm1hcC1nZXQoJGNvbG9ycywgcHJpbWFyeSk7XG59XG5cbmlvbi1oZWFkZXIsXG5pb24taGVhZGVyID4gaW9uLW5hdmJhcixcbmlvbi1oZWFkZXIgPiBpb24tbmF2YmFyIC50b29sYmFyLWJhY2tncm91bmRcbntcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IG1hcC1nZXQoJGNvbG9ycywgcHJpbWFyeSk7XG59XG4udG9vbGJhci1iYWNrZ3JvdW5kXG57XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIHByaW1hcnkpO1xufVxuKi9cbiJdfQ== */", '', '']]

/***/ }),

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/global.scss":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/global.scss ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "html.ios {\n  --ion-default-font: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n}\n\nhtml.md {\n  --ion-default-font: \"Roboto\", \"Helvetica Neue\", sans-serif;\n}\n\nhtml {\n  --ion-font-family: var(--ion-default-font);\n}\n\nbody {\n  background: var(--ion-background-color);\n}\n\nbody.backdrop-no-scroll {\n  overflow: hidden;\n}\n\n.ion-color-primary {\n  --ion-color-base: var(--ion-color-primary, #3880ff) !important;\n  --ion-color-base-rgb: var(--ion-color-primary-rgb, 56, 128, 255) !important;\n  --ion-color-contrast: var(--ion-color-primary-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-primary-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-primary-shade, #3171e0) !important;\n  --ion-color-tint: var(--ion-color-primary-tint, #4c8dff) !important;\n}\n\n.ion-color-secondary {\n  --ion-color-base: var(--ion-color-secondary, #0cd1e8) !important;\n  --ion-color-base-rgb: var(--ion-color-secondary-rgb, 12, 209, 232) !important;\n  --ion-color-contrast: var(--ion-color-secondary-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-secondary-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-secondary-shade, #0bb8cc) !important;\n  --ion-color-tint: var(--ion-color-secondary-tint, #24d6ea) !important;\n}\n\n.ion-color-tertiary {\n  --ion-color-base: var(--ion-color-tertiary, #7044ff) !important;\n  --ion-color-base-rgb: var(--ion-color-tertiary-rgb, 112, 68, 255) !important;\n  --ion-color-contrast: var(--ion-color-tertiary-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-tertiary-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-tertiary-shade, #633ce0) !important;\n  --ion-color-tint: var(--ion-color-tertiary-tint, #7e57ff) !important;\n}\n\n.ion-color-success {\n  --ion-color-base: var(--ion-color-success, #10dc60) !important;\n  --ion-color-base-rgb: var(--ion-color-success-rgb, 16, 220, 96) !important;\n  --ion-color-contrast: var(--ion-color-success-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-success-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-success-shade, #0ec254) !important;\n  --ion-color-tint: var(--ion-color-success-tint, #28e070) !important;\n}\n\n.ion-color-warning {\n  --ion-color-base: var(--ion-color-warning, #ffce00) !important;\n  --ion-color-base-rgb: var(--ion-color-warning-rgb, 255, 206, 0) !important;\n  --ion-color-contrast: var(--ion-color-warning-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-warning-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-warning-shade, #e0b500) !important;\n  --ion-color-tint: var(--ion-color-warning-tint, #ffd31a) !important;\n}\n\n.ion-color-danger {\n  --ion-color-base: var(--ion-color-danger, #f04141) !important;\n  --ion-color-base-rgb: var(--ion-color-danger-rgb, 240, 65, 65) !important;\n  --ion-color-contrast: var(--ion-color-danger-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-danger-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-danger-shade, #d33939) !important;\n  --ion-color-tint: var(--ion-color-danger-tint, #f25454) !important;\n}\n\n.ion-color-light {\n  --ion-color-base: var(--ion-color-light, #f4f5f8) !important;\n  --ion-color-base-rgb: var(--ion-color-light-rgb, 244, 245, 248) !important;\n  --ion-color-contrast: var(--ion-color-light-contrast, #000) !important;\n  --ion-color-contrast-rgb: var(--ion-color-light-contrast-rgb, 0, 0, 0) !important;\n  --ion-color-shade: var(--ion-color-light-shade, #d7d8da) !important;\n  --ion-color-tint: var(--ion-color-light-tint, #f5f6f9) !important;\n}\n\n.ion-color-medium {\n  --ion-color-base: var(--ion-color-medium, #989aa2) !important;\n  --ion-color-base-rgb: var(--ion-color-medium-rgb, 152, 154, 162) !important;\n  --ion-color-contrast: var(--ion-color-medium-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-medium-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-medium-shade, #86888f) !important;\n  --ion-color-tint: var(--ion-color-medium-tint, #a2a4ab) !important;\n}\n\n.ion-color-dark {\n  --ion-color-base: var(--ion-color-dark, #222428) !important;\n  --ion-color-base-rgb: var(--ion-color-dark-rgb, 34, 36, 40) !important;\n  --ion-color-contrast: var(--ion-color-dark-contrast, #fff) !important;\n  --ion-color-contrast-rgb: var(--ion-color-dark-contrast-rgb, 255, 255, 255) !important;\n  --ion-color-shade: var(--ion-color-dark-shade, #1e2023) !important;\n  --ion-color-tint: var(--ion-color-dark-tint, #383a3e) !important;\n}\n\n.ion-page {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  display: flex;\n  position: absolute;\n  flex-direction: column;\n  justify-content: space-between;\n  contain: layout size style;\n  overflow: hidden;\n  z-index: 0;\n}\n\nion-route,\nion-route-redirect,\nion-router,\nion-select-option,\nion-nav-controller,\nion-menu-controller,\nion-action-sheet-controller,\nion-alert-controller,\nion-loading-controller,\nion-modal-controller,\nion-picker-controller,\nion-popover-controller,\nion-toast-controller,\n.ion-page-hidden,\n[hidden] {\n  /* stylelint-disable-next-line declaration-no-important */\n  display: none !important;\n}\n\n.ion-page-invisible {\n  opacity: 0;\n}\n\nhtml.plt-ios.plt-hybrid, html.plt-ios.plt-pwa {\n  --ion-statusbar-padding: 20px;\n}\n\n@supports (padding-top: 20px) {\n  html {\n    --ion-safe-area-top: var(--ion-statusbar-padding);\n  }\n}\n\n@supports (padding-top: constant(safe-area-inset-top)) {\n  html {\n    --ion-safe-area-top: constant(safe-area-inset-top);\n    --ion-safe-area-bottom: constant(safe-area-inset-bottom);\n    --ion-safe-area-left: constant(safe-area-inset-left);\n    --ion-safe-area-right: constant(safe-area-inset-right);\n  }\n}\n\n@supports (padding-top: env(safe-area-inset-top)) {\n  html {\n    --ion-safe-area-top: env(safe-area-inset-top);\n    --ion-safe-area-bottom: env(safe-area-inset-bottom);\n    --ion-safe-area-left: env(safe-area-inset-left);\n    --ion-safe-area-right: env(safe-area-inset-right);\n  }\n}\n\naudio,\ncanvas,\nprogress,\nvideo {\n  vertical-align: baseline;\n}\n\naudio:not([controls]) {\n  display: none;\n  height: 0;\n}\n\nb,\nstrong {\n  font-weight: bold;\n}\n\nimg {\n  max-width: 100%;\n  border: 0;\n}\n\nsvg:not(:root) {\n  overflow: hidden;\n}\n\nfigure {\n  margin: 1em 40px;\n}\n\nhr {\n  height: 1px;\n  border-width: 0;\n  box-sizing: content-box;\n}\n\npre {\n  overflow: auto;\n}\n\ncode,\nkbd,\npre,\nsamp {\n  font-family: monospace, monospace;\n  font-size: 1em;\n}\n\nlabel,\ninput,\nselect,\ntextarea {\n  font-family: inherit;\n  line-height: normal;\n}\n\ntextarea {\n  overflow: auto;\n  height: auto;\n  font: inherit;\n  color: inherit;\n}\n\ntextarea::-webkit-input-placeholder {\n  padding-left: 2px;\n}\n\ntextarea::-moz-placeholder {\n  padding-left: 2px;\n}\n\ntextarea:-ms-input-placeholder {\n  padding-left: 2px;\n}\n\ntextarea::-ms-input-placeholder {\n  padding-left: 2px;\n}\n\ntextarea::placeholder {\n  padding-left: 2px;\n}\n\nform,\ninput,\noptgroup,\nselect {\n  margin: 0;\n  font: inherit;\n  color: inherit;\n}\n\nhtml input[type=button],\ninput[type=reset],\ninput[type=submit] {\n  cursor: pointer;\n  -webkit-appearance: button;\n}\n\na,\na div,\na span,\na ion-icon,\na ion-label,\nbutton,\nbutton div,\nbutton span,\nbutton ion-icon,\nbutton ion-label,\n.ion-tappable,\n[tappable],\n[tappable] div,\n[tappable] span,\n[tappable] ion-icon,\n[tappable] ion-label,\ninput,\ntextarea {\n  touch-action: manipulation;\n}\n\na ion-label,\nbutton ion-label {\n  pointer-events: none;\n}\n\nbutton {\n  border: 0;\n  border-radius: 0;\n  font-family: inherit;\n  font-style: inherit;\n  font-variant: inherit;\n  line-height: 1;\n  text-transform: none;\n  cursor: pointer;\n  -webkit-appearance: button;\n}\n\n[tappable] {\n  cursor: pointer;\n}\n\na[disabled],\nbutton[disabled],\nhtml input[disabled] {\n  cursor: default;\n}\n\nbutton::-moz-focus-inner,\ninput::-moz-focus-inner {\n  padding: 0;\n  border: 0;\n}\n\ninput[type=checkbox],\ninput[type=radio] {\n  padding: 0;\n  box-sizing: border-box;\n}\n\ninput[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  height: auto;\n}\n\ninput[type=search]::-webkit-search-cancel-button,\ninput[type=search]::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\n\ntd,\nth {\n  padding: 0;\n}\n\n* {\n  box-sizing: border-box;\n  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\n  -webkit-tap-highlight-color: transparent;\n  -webkit-touch-callout: none;\n}\n\nhtml {\n  width: 100%;\n  height: 100%;\n  -webkit-text-size-adjust: 100%;\n     -moz-text-size-adjust: 100%;\n      -ms-text-size-adjust: 100%;\n          text-size-adjust: 100%;\n}\n\nhtml:not(.hydrated) body {\n  display: none;\n}\n\nhtml.plt-pwa {\n  height: 100vh;\n}\n\nbody {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  margin-left: 0;\n  margin-right: 0;\n  margin-top: 0;\n  margin-bottom: 0;\n  padding-left: 0;\n  padding-right: 0;\n  padding-top: 0;\n  padding-bottom: 0;\n  position: fixed;\n  width: 100%;\n  max-width: 100%;\n  height: 100%;\n  max-height: 100%;\n  text-rendering: optimizeLegibility;\n  overflow: hidden;\n  touch-action: manipulation;\n  -webkit-user-drag: none;\n  -ms-content-zooming: none;\n  word-wrap: break-word;\n  overscroll-behavior-y: none;\n  -webkit-text-size-adjust: none;\n     -moz-text-size-adjust: none;\n      -ms-text-size-adjust: none;\n          text-size-adjust: none;\n}\n\nhtml {\n  font-family: var(--ion-font-family);\n}\n\na {\n  background-color: transparent;\n  color: var(--ion-color-primary, #3880ff);\n}\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  margin-top: 16px;\n  margin-bottom: 10px;\n  font-weight: 500;\n  line-height: 1.2;\n}\n\nh1 {\n  margin-top: 20px;\n  font-size: 26px;\n}\n\nh2 {\n  margin-top: 18px;\n  font-size: 24px;\n}\n\nh3 {\n  font-size: 22px;\n}\n\nh4 {\n  font-size: 20px;\n}\n\nh5 {\n  font-size: 18px;\n}\n\nh6 {\n  font-size: 16px;\n}\n\nsmall {\n  font-size: 75%;\n}\n\nsub,\nsup {\n  position: relative;\n  font-size: 75%;\n  line-height: 0;\n  vertical-align: baseline;\n}\n\nsup {\n  top: -0.5em;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\n.ion-hide {\n  display: none !important;\n}\n\n.ion-hide-up {\n  display: none !important;\n}\n\n@media (max-width: 575px) {\n  .ion-hide-down {\n    display: none !important;\n  }\n}\n\n@media (min-width: 576px) {\n  .ion-hide-sm-up {\n    display: none !important;\n  }\n}\n\n@media (max-width: 767px) {\n  .ion-hide-sm-down {\n    display: none !important;\n  }\n}\n\n@media (min-width: 768px) {\n  .ion-hide-md-up {\n    display: none !important;\n  }\n}\n\n@media (max-width: 991px) {\n  .ion-hide-md-down {\n    display: none !important;\n  }\n}\n\n@media (min-width: 992px) {\n  .ion-hide-lg-up {\n    display: none !important;\n  }\n}\n\n@media (max-width: 1199px) {\n  .ion-hide-lg-down {\n    display: none !important;\n  }\n}\n\n@media (min-width: 1200px) {\n  .ion-hide-xl-up {\n    display: none !important;\n  }\n}\n\n.ion-hide-xl-down {\n  display: none !important;\n}\n\n.ion-no-padding,\n[no-padding] {\n  --padding-start: 0;\n  --padding-end: 0;\n  --padding-top: 0;\n  --padding-bottom: 0;\n  padding-left: 0;\n  padding-right: 0;\n  padding-top: 0;\n  padding-bottom: 0;\n}\n\n.ion-padding,\n[padding] {\n  --padding-start: var(--ion-padding, 16px);\n  --padding-end: var(--ion-padding, 16px);\n  --padding-top: var(--ion-padding, 16px);\n  --padding-bottom: var(--ion-padding, 16px);\n  padding-left: var(--ion-padding, 16px);\n  padding-right: var(--ion-padding, 16px);\n  padding-top: var(--ion-padding, 16px);\n  padding-bottom: var(--ion-padding, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-padding,\n[padding] {\n    padding-left: unset;\n    padding-right: unset;\n    -webkit-padding-start: var(--ion-padding, 16px);\n    padding-inline-start: var(--ion-padding, 16px);\n    -webkit-padding-end: var(--ion-padding, 16px);\n    padding-inline-end: var(--ion-padding, 16px);\n  }\n}\n\n.ion-padding-top,\n[padding-top] {\n  --padding-top: var(--ion-padding, 16px);\n  padding-top: var(--ion-padding, 16px);\n}\n\n.ion-padding-start,\n[padding-start] {\n  --padding-start: var(--ion-padding, 16px);\n  padding-left: var(--ion-padding, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-padding-start,\n[padding-start] {\n    padding-left: unset;\n    -webkit-padding-start: var(--ion-padding, 16px);\n    padding-inline-start: var(--ion-padding, 16px);\n  }\n}\n\n.ion-padding-end,\n[padding-end] {\n  --padding-end: var(--ion-padding, 16px);\n  padding-right: var(--ion-padding, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-padding-end,\n[padding-end] {\n    padding-right: unset;\n    -webkit-padding-end: var(--ion-padding, 16px);\n    padding-inline-end: var(--ion-padding, 16px);\n  }\n}\n\n.ion-padding-bottom,\n[padding-bottom] {\n  --padding-bottom: var(--ion-padding, 16px);\n  padding-bottom: var(--ion-padding, 16px);\n}\n\n.ion-padding-vertical,\n[padding-vertical] {\n  --padding-top: var(--ion-padding, 16px);\n  --padding-bottom: var(--ion-padding, 16px);\n  padding-top: var(--ion-padding, 16px);\n  padding-bottom: var(--ion-padding, 16px);\n}\n\n.ion-padding-horizontal,\n[padding-horizontal] {\n  --padding-start: var(--ion-padding, 16px);\n  --padding-end: var(--ion-padding, 16px);\n  padding-left: var(--ion-padding, 16px);\n  padding-right: var(--ion-padding, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-padding-horizontal,\n[padding-horizontal] {\n    padding-left: unset;\n    padding-right: unset;\n    -webkit-padding-start: var(--ion-padding, 16px);\n    padding-inline-start: var(--ion-padding, 16px);\n    -webkit-padding-end: var(--ion-padding, 16px);\n    padding-inline-end: var(--ion-padding, 16px);\n  }\n}\n\n.ion-no-margin,\n[no-margin] {\n  --margin-start: 0;\n  --margin-end: 0;\n  --margin-top: 0;\n  --margin-bottom: 0;\n  margin-left: 0;\n  margin-right: 0;\n  margin-top: 0;\n  margin-bottom: 0;\n}\n\n.ion-margin,\n[margin] {\n  --margin-start: var(--ion-margin, 16px);\n  --margin-end: var(--ion-margin, 16px);\n  --margin-top: var(--ion-margin, 16px);\n  --margin-bottom: var(--ion-margin, 16px);\n  margin-left: var(--ion-margin, 16px);\n  margin-right: var(--ion-margin, 16px);\n  margin-top: var(--ion-margin, 16px);\n  margin-bottom: var(--ion-margin, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-margin,\n[margin] {\n    margin-left: unset;\n    margin-right: unset;\n    -webkit-margin-start: var(--ion-margin, 16px);\n    margin-inline-start: var(--ion-margin, 16px);\n    -webkit-margin-end: var(--ion-margin, 16px);\n    margin-inline-end: var(--ion-margin, 16px);\n  }\n}\n\n.ion-margin-top,\n[margin-top] {\n  --margin-top: var(--ion-margin, 16px);\n  margin-top: var(--ion-margin, 16px);\n}\n\n.ion-margin-start,\n[margin-start] {\n  --margin-start: var(--ion-margin, 16px);\n  margin-left: var(--ion-margin, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-margin-start,\n[margin-start] {\n    margin-left: unset;\n    -webkit-margin-start: var(--ion-margin, 16px);\n    margin-inline-start: var(--ion-margin, 16px);\n  }\n}\n\n.ion-margin-end,\n[margin-end] {\n  --margin-end: var(--ion-margin, 16px);\n  margin-right: var(--ion-margin, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-margin-end,\n[margin-end] {\n    margin-right: unset;\n    -webkit-margin-end: var(--ion-margin, 16px);\n    margin-inline-end: var(--ion-margin, 16px);\n  }\n}\n\n.ion-margin-bottom,\n[margin-bottom] {\n  --margin-bottom: var(--ion-margin, 16px);\n  margin-bottom: var(--ion-margin, 16px);\n}\n\n.ion-margin-vertical,\n[margin-vertical] {\n  --margin-top: var(--ion-margin, 16px);\n  --margin-bottom: var(--ion-margin, 16px);\n  margin-top: var(--ion-margin, 16px);\n  margin-bottom: var(--ion-margin, 16px);\n}\n\n.ion-margin-horizontal,\n[margin-horizontal] {\n  --margin-start: var(--ion-margin, 16px);\n  --margin-end: var(--ion-margin, 16px);\n  margin-left: var(--ion-margin, 16px);\n  margin-right: var(--ion-margin, 16px);\n}\n\n@supports ((-webkit-margin-start: 0) or (margin-inline-start: 0)) or (-webkit-margin-start: 0) {\n  .ion-margin-horizontal,\n[margin-horizontal] {\n    margin-left: unset;\n    margin-right: unset;\n    -webkit-margin-start: var(--ion-margin, 16px);\n    margin-inline-start: var(--ion-margin, 16px);\n    -webkit-margin-end: var(--ion-margin, 16px);\n    margin-inline-end: var(--ion-margin, 16px);\n  }\n}\n\n.ion-float-left,\n[float-left] {\n  float: left !important;\n}\n\n.ion-float-right,\n[float-right] {\n  float: right !important;\n}\n\n.ion-float-start,\n[float-start] {\n  float: left !important;\n}\n\n[dir=rtl] .ion-float-start, :host-context([dir=rtl]) .ion-float-start, [dir=rtl] [float-start], :host-context([dir=rtl]) [float-start] {\n  float: right !important;\n}\n\n.ion-float-end,\n[float-end] {\n  float: right !important;\n}\n\n[dir=rtl] .ion-float-end, :host-context([dir=rtl]) .ion-float-end, [dir=rtl] [float-end], :host-context([dir=rtl]) [float-end] {\n  float: left !important;\n}\n\n@media (min-width: 576px) {\n  .ion-float-sm-left,\n[float-sm-left] {\n    float: left !important;\n  }\n\n  .ion-float-sm-right,\n[float-sm-right] {\n    float: right !important;\n  }\n\n  .ion-float-sm-start,\n[float-sm-start] {\n    float: left !important;\n  }\n  [dir=rtl] .ion-float-sm-start, :host-context([dir=rtl]) .ion-float-sm-start, [dir=rtl] [float-sm-start], :host-context([dir=rtl]) [float-sm-start] {\n    float: right !important;\n  }\n\n  .ion-float-sm-end,\n[float-sm-end] {\n    float: right !important;\n  }\n  [dir=rtl] .ion-float-sm-end, :host-context([dir=rtl]) .ion-float-sm-end, [dir=rtl] [float-sm-end], :host-context([dir=rtl]) [float-sm-end] {\n    float: left !important;\n  }\n}\n\n@media (min-width: 768px) {\n  .ion-float-md-left,\n[float-md-left] {\n    float: left !important;\n  }\n\n  .ion-float-md-right,\n[float-md-right] {\n    float: right !important;\n  }\n\n  .ion-float-md-start,\n[float-md-start] {\n    float: left !important;\n  }\n  [dir=rtl] .ion-float-md-start, :host-context([dir=rtl]) .ion-float-md-start, [dir=rtl] [float-md-start], :host-context([dir=rtl]) [float-md-start] {\n    float: right !important;\n  }\n\n  .ion-float-md-end,\n[float-md-end] {\n    float: right !important;\n  }\n  [dir=rtl] .ion-float-md-end, :host-context([dir=rtl]) .ion-float-md-end, [dir=rtl] [float-md-end], :host-context([dir=rtl]) [float-md-end] {\n    float: left !important;\n  }\n}\n\n@media (min-width: 992px) {\n  .ion-float-lg-left,\n[float-lg-left] {\n    float: left !important;\n  }\n\n  .ion-float-lg-right,\n[float-lg-right] {\n    float: right !important;\n  }\n\n  .ion-float-lg-start,\n[float-lg-start] {\n    float: left !important;\n  }\n  [dir=rtl] .ion-float-lg-start, :host-context([dir=rtl]) .ion-float-lg-start, [dir=rtl] [float-lg-start], :host-context([dir=rtl]) [float-lg-start] {\n    float: right !important;\n  }\n\n  .ion-float-lg-end,\n[float-lg-end] {\n    float: right !important;\n  }\n  [dir=rtl] .ion-float-lg-end, :host-context([dir=rtl]) .ion-float-lg-end, [dir=rtl] [float-lg-end], :host-context([dir=rtl]) [float-lg-end] {\n    float: left !important;\n  }\n}\n\n@media (min-width: 1200px) {\n  .ion-float-xl-left,\n[float-xl-left] {\n    float: left !important;\n  }\n\n  .ion-float-xl-right,\n[float-xl-right] {\n    float: right !important;\n  }\n\n  .ion-float-xl-start,\n[float-xl-start] {\n    float: left !important;\n  }\n  [dir=rtl] .ion-float-xl-start, :host-context([dir=rtl]) .ion-float-xl-start, [dir=rtl] [float-xl-start], :host-context([dir=rtl]) [float-xl-start] {\n    float: right !important;\n  }\n\n  .ion-float-xl-end,\n[float-xl-end] {\n    float: right !important;\n  }\n  [dir=rtl] .ion-float-xl-end, :host-context([dir=rtl]) .ion-float-xl-end, [dir=rtl] [float-xl-end], :host-context([dir=rtl]) [float-xl-end] {\n    float: left !important;\n  }\n}\n\n.ion-text-center,\n[text-center] {\n  text-align: center !important;\n}\n\n.ion-text-justify,\n[text-justify] {\n  text-align: justify !important;\n}\n\n.ion-text-start,\n[text-start] {\n  text-align: start !important;\n}\n\n.ion-text-end,\n[text-end] {\n  text-align: end !important;\n}\n\n.ion-text-left,\n[text-left] {\n  text-align: left !important;\n}\n\n.ion-text-right,\n[text-right] {\n  text-align: right !important;\n}\n\n.ion-text-nowrap,\n[text-nowrap] {\n  white-space: nowrap !important;\n}\n\n.ion-text-wrap,\n[text-wrap] {\n  white-space: normal !important;\n}\n\n@media (min-width: 576px) {\n  .ion-text-sm-center,\n[text-sm-center] {\n    text-align: center !important;\n  }\n\n  .ion-text-sm-justify,\n[text-sm-justify] {\n    text-align: justify !important;\n  }\n\n  .ion-text-sm-start,\n[text-sm-start] {\n    text-align: start !important;\n  }\n\n  .ion-text-sm-end,\n[text-sm-end] {\n    text-align: end !important;\n  }\n\n  .ion-text-sm-left,\n[text-sm-left] {\n    text-align: left !important;\n  }\n\n  .ion-text-sm-right,\n[text-sm-right] {\n    text-align: right !important;\n  }\n\n  .ion-text-sm-nowrap,\n[text-sm-nowrap] {\n    white-space: nowrap !important;\n  }\n\n  .ion-text-sm-wrap,\n[text-sm-wrap] {\n    white-space: normal !important;\n  }\n}\n\n@media (min-width: 768px) {\n  .ion-text-md-center,\n[text-md-center] {\n    text-align: center !important;\n  }\n\n  .ion-text-md-justify,\n[text-md-justify] {\n    text-align: justify !important;\n  }\n\n  .ion-text-md-start,\n[text-md-start] {\n    text-align: start !important;\n  }\n\n  .ion-text-md-end,\n[text-md-end] {\n    text-align: end !important;\n  }\n\n  .ion-text-md-left,\n[text-md-left] {\n    text-align: left !important;\n  }\n\n  .ion-text-md-right,\n[text-md-right] {\n    text-align: right !important;\n  }\n\n  .ion-text-md-nowrap,\n[text-md-nowrap] {\n    white-space: nowrap !important;\n  }\n\n  .ion-text-md-wrap,\n[text-md-wrap] {\n    white-space: normal !important;\n  }\n}\n\n@media (min-width: 992px) {\n  .ion-text-lg-center,\n[text-lg-center] {\n    text-align: center !important;\n  }\n\n  .ion-text-lg-justify,\n[text-lg-justify] {\n    text-align: justify !important;\n  }\n\n  .ion-text-lg-start,\n[text-lg-start] {\n    text-align: start !important;\n  }\n\n  .ion-text-lg-end,\n[text-lg-end] {\n    text-align: end !important;\n  }\n\n  .ion-text-lg-left,\n[text-lg-left] {\n    text-align: left !important;\n  }\n\n  .ion-text-lg-right,\n[text-lg-right] {\n    text-align: right !important;\n  }\n\n  .ion-text-lg-nowrap,\n[text-lg-nowrap] {\n    white-space: nowrap !important;\n  }\n\n  .ion-text-lg-wrap,\n[text-lg-wrap] {\n    white-space: normal !important;\n  }\n}\n\n@media (min-width: 1200px) {\n  .ion-text-xl-center,\n[text-xl-center] {\n    text-align: center !important;\n  }\n\n  .ion-text-xl-justify,\n[text-xl-justify] {\n    text-align: justify !important;\n  }\n\n  .ion-text-xl-start,\n[text-xl-start] {\n    text-align: start !important;\n  }\n\n  .ion-text-xl-end,\n[text-xl-end] {\n    text-align: end !important;\n  }\n\n  .ion-text-xl-left,\n[text-xl-left] {\n    text-align: left !important;\n  }\n\n  .ion-text-xl-right,\n[text-xl-right] {\n    text-align: right !important;\n  }\n\n  .ion-text-xl-nowrap,\n[text-xl-nowrap] {\n    white-space: nowrap !important;\n  }\n\n  .ion-text-xl-wrap,\n[text-xl-wrap] {\n    white-space: normal !important;\n  }\n}\n\n.ion-text-uppercase,\n[text-uppercase] {\n  /* stylelint-disable-next-line declaration-no-important */\n  text-transform: uppercase !important;\n}\n\n.ion-text-lowercase,\n[text-lowercase] {\n  /* stylelint-disable-next-line declaration-no-important */\n  text-transform: lowercase !important;\n}\n\n.ion-text-capitalize,\n[text-capitalize] {\n  /* stylelint-disable-next-line declaration-no-important */\n  text-transform: capitalize !important;\n}\n\n@media (min-width: 576px) {\n  .ion-text-sm-uppercase,\n[text-sm-uppercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: uppercase !important;\n  }\n\n  .ion-text-sm-lowercase,\n[text-sm-lowercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: lowercase !important;\n  }\n\n  .ion-text-sm-capitalize,\n[text-sm-capitalize] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: capitalize !important;\n  }\n}\n\n@media (min-width: 768px) {\n  .ion-text-md-uppercase,\n[text-md-uppercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: uppercase !important;\n  }\n\n  .ion-text-md-lowercase,\n[text-md-lowercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: lowercase !important;\n  }\n\n  .ion-text-md-capitalize,\n[text-md-capitalize] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: capitalize !important;\n  }\n}\n\n@media (min-width: 992px) {\n  .ion-text-lg-uppercase,\n[text-lg-uppercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: uppercase !important;\n  }\n\n  .ion-text-lg-lowercase,\n[text-lg-lowercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: lowercase !important;\n  }\n\n  .ion-text-lg-capitalize,\n[text-lg-capitalize] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: capitalize !important;\n  }\n}\n\n@media (min-width: 1200px) {\n  .ion-text-xl-uppercase,\n[text-xl-uppercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: uppercase !important;\n  }\n\n  .ion-text-xl-lowercase,\n[text-xl-lowercase] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: lowercase !important;\n  }\n\n  .ion-text-xl-capitalize,\n[text-xl-capitalize] {\n    /* stylelint-disable-next-line declaration-no-important */\n    text-transform: capitalize !important;\n  }\n}\n\n.ion-align-self-start,\n[align-self-start] {\n  align-self: flex-start !important;\n}\n\n.ion-align-self-end,\n[align-self-end] {\n  align-self: flex-end !important;\n}\n\n.ion-align-self-center,\n[align-self-center] {\n  align-self: center !important;\n}\n\n.ion-align-self-stretch,\n[align-self-stretch] {\n  align-self: stretch !important;\n}\n\n.ion-align-self-baseline,\n[align-self-baseline] {\n  align-self: baseline !important;\n}\n\n.ion-align-self-auto,\n[align-self-auto] {\n  align-self: auto !important;\n}\n\n.ion-wrap,\n[wrap] {\n  flex-wrap: wrap !important;\n}\n\n.ion-nowrap,\n[nowrap] {\n  flex-wrap: nowrap !important;\n}\n\n.ion-wrap-reverse,\n[wrap-reverse] {\n  flex-wrap: wrap-reverse !important;\n}\n\n.ion-justify-content-start,\n[justify-content-start] {\n  justify-content: flex-start !important;\n}\n\n.ion-justify-content-center,\n[justify-content-center] {\n  justify-content: center !important;\n}\n\n.ion-justify-content-end,\n[justify-content-end] {\n  justify-content: flex-end !important;\n}\n\n.ion-justify-content-around,\n[justify-content-around] {\n  justify-content: space-around !important;\n}\n\n.ion-justify-content-between,\n[justify-content-between] {\n  justify-content: space-between !important;\n}\n\n.ion-justify-content-evenly,\n[justify-content-evenly] {\n  justify-content: space-evenly !important;\n}\n\n.ion-align-items-start,\n[align-items-start] {\n  align-items: flex-start !important;\n}\n\n.ion-align-items-center,\n[align-items-center] {\n  align-items: center !important;\n}\n\n.ion-align-items-end,\n[align-items-end] {\n  align-items: flex-end !important;\n}\n\n.ion-align-items-stretch,\n[align-items-stretch] {\n  align-items: stretch !important;\n}\n\n.ion-align-items-baseline,\n[align-items-baseline] {\n  align-items: baseline !important;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL2NvcmUuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvY29yZS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL3RoZW1lcy9pb25pYy5taXhpbnMuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvdGhlbWVzL2lvbmljLmdsb2JhbHMuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL25vcm1hbGl6ZS5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9ub3JtYWxpemUuY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL3NyYy9jc3Mvc3RydWN0dXJlLnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3N0cnVjdHVyZS5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy90eXBvZ3JhcGh5LnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3R5cG9ncmFwaHkuY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL3NyYy9jc3MvZGlzcGxheS5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9kaXNwbGF5LmNzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9zcmMvY3NzL3BhZGRpbmcuc2NzcyIsIm5vZGVfbW9kdWxlcy9AaW9uaWMvYW5ndWxhci9jc3MvcGFkZGluZy5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy9mbG9hdC1lbGVtZW50cy5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9mbG9hdC1lbGVtZW50cy5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy90ZXh0LWFsaWdubWVudC5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy90ZXh0LWFsaWdubWVudC5jc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvc3JjL2Nzcy90ZXh0LXRyYW5zZm9ybWF0aW9uLnNjc3MiLCJub2RlX21vZHVsZXMvQGlvbmljL2FuZ3VsYXIvY3NzL3RleHQtdHJhbnNmb3JtYXRpb24uY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL3NyYy9jc3MvZmxleC11dGlscy5zY3NzIiwibm9kZV9tb2R1bGVzL0Bpb25pYy9hbmd1bGFyL2Nzcy9mbGV4LXV0aWxzLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQTtFQUNFLDZGQUFBO0FDSkY7O0FETUE7RUFDRSwwREFBQTtBQ0hGOztBRE1BO0VBQ0UsMENBQUE7QUNIRjs7QURNQTtFQUNFLHVDQUFBO0FDSEY7O0FETUE7RUFDRSxnQkFBQTtBQ0hGOztBRGtDRTtFQVRBLDhEQUFBO0VBQ0EsMkVBQUE7RUFDQSx3RUFBQTtFQUNBLHlGQUFBO0VBQ0EscUVBQUE7RUFDQSxtRUFBQTtBQ3JCRjs7QUR5QkU7RUFUQSxnRUFBQTtFQUNBLDZFQUFBO0VBQ0EsMEVBQUE7RUFDQSwyRkFBQTtFQUNBLHVFQUFBO0VBQ0EscUVBQUE7QUNaRjs7QURnQkU7RUFUQSwrREFBQTtFQUNBLDRFQUFBO0VBQ0EseUVBQUE7RUFDQSwwRkFBQTtFQUNBLHNFQUFBO0VBQ0Esb0VBQUE7QUNIRjs7QURPRTtFQVRBLDhEQUFBO0VBQ0EsMEVBQUE7RUFDQSx3RUFBQTtFQUNBLHlGQUFBO0VBQ0EscUVBQUE7RUFDQSxtRUFBQTtBQ01GOztBREZFO0VBVEEsOERBQUE7RUFDQSwwRUFBQTtFQUNBLHdFQUFBO0VBQ0EseUZBQUE7RUFDQSxxRUFBQTtFQUNBLG1FQUFBO0FDZUY7O0FEWEU7RUFUQSw2REFBQTtFQUNBLHlFQUFBO0VBQ0EsdUVBQUE7RUFDQSx3RkFBQTtFQUNBLG9FQUFBO0VBQ0Esa0VBQUE7QUN3QkY7O0FEcEJFO0VBVEEsNERBQUE7RUFDQSwwRUFBQTtFQUNBLHNFQUFBO0VBQ0EsaUZBQUE7RUFDQSxtRUFBQTtFQUNBLGlFQUFBO0FDaUNGOztBRDdCRTtFQVRBLDZEQUFBO0VBQ0EsMkVBQUE7RUFDQSx1RUFBQTtFQUNBLHdGQUFBO0VBQ0Esb0VBQUE7RUFDQSxrRUFBQTtBQzBDRjs7QUR0Q0U7RUFUQSwyREFBQTtFQUNBLHNFQUFBO0VBQ0EscUVBQUE7RUFDQSxzRkFBQTtFQUNBLGtFQUFBO0VBQ0EsZ0VBQUE7QUNtREY7O0FEdENBO0VFK05NLE9GOU51QjtFRStOdkIsUUYvTmlCO0VFd1ByQixNRnhQa0I7RUV5UGxCLFNGelB3QjtFQUV4QixhQUFBO0VBQ0Esa0JBQUE7RUFFQSxzQkFBQTtFQUNBLDhCQUFBO0VBRUEsMEJBQUE7RUFDQSxnQkFBQTtFQUNBLFVHdEIrQjtBRitEakM7O0FEdENBOzs7Ozs7Ozs7Ozs7Ozs7RUFlRSx5REFBQTtFQUNBLHdCQUFBO0FDeUNGOztBRHRDQTtFQUNFLFVBQUE7QUN5Q0Y7O0FEbkNBO0VBQ0UsNkJBQUE7QUNzQ0Y7O0FEbkNBO0VBQ0U7SUFDRSxpREFBQTtFQ3NDRjtBQUNGOztBRGxDQTtFQUNFO0lBQ0Usa0RBQUE7SUFDQSx3REFBQTtJQUNBLG9EQUFBO0lBQ0Esc0RBQUE7RUNvQ0Y7QUFDRjs7QURqQ0E7RUFDRTtJQUNFLDZDQUFBO0lBQ0EsbURBQUE7SUFDQSwrQ0FBQTtJQUNBLGlEQUFBO0VDbUNGO0FBQ0Y7O0FHM0pBOzs7O0VBSUUsd0JBQUE7QUNORjs7QURXQTtFQUNFLGFBQUE7RUFFQSxTQUFBO0FDVEY7O0FEaUJBOztFQUVFLGlCQUFBO0FDZEY7O0FEc0JBO0VBQ0UsZUFBQTtFQUVBLFNBQUE7QUNwQkY7O0FEd0JBO0VBQ0UsZ0JBQUE7QUNyQkY7O0FENkJBO0VBQ0UsZ0JBQUE7QUMxQkY7O0FENkJBO0VBQ0UsV0FBQTtFQUVBLGVBQUE7RUFFQSx1QkFBQTtBQzVCRjs7QURnQ0E7RUFDRSxjQUFBO0FDN0JGOztBRGlDQTs7OztFQUlFLGlDQUFBO0VBQ0EsY0FBQTtBQzlCRjs7QUQ4Q0E7Ozs7RUFJRSxvQkFBQTtFQUNBLG1CQUFBO0FDM0NGOztBRDhDQTtFQUNFLGNBQUE7RUFFQSxZQUFBO0VBRUEsYUFBQTtFQUNBLGNBQUE7QUM3Q0Y7O0FEZ0RBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FENENBO0VBQ0UsaUJBQUE7QUM3Q0Y7O0FEZ0RBOzs7O0VBSUUsU0FBQTtFQUVBLGFBQUE7RUFDQSxjQUFBO0FDOUNGOztBRHNEQTs7O0VBR0UsZUFBQTtFQUVBLDBCQUFBO0FDcERGOztBRHdEQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBa0JFLDBCQUFBO0FDckRGOztBRHdEQTs7RUFFRSxvQkFBQTtBQ3JERjs7QUR3REE7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBRUEsMEJBQUE7QUN0REY7O0FEeURBO0VBQ0UsZUFBQTtBQ3RERjs7QUQwREE7OztFQUdFLGVBQUE7QUN2REY7O0FEMkRBOztFQUVFLFVBQUE7RUFFQSxTQUFBO0FDekRGOztBRCtEQTs7RUFFRSxVQUFBO0VBRUEsc0JBQUE7QUM3REY7O0FEbUVBOztFQUVFLFlBQUE7QUNoRUY7O0FEc0VBOztFQUVFLHdCQUFBO0FDbkVGOztBRDJFQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7QUN4RUY7O0FEMkVBOztFQUVFLFVBQUE7QUN4RUY7O0FDekpBO0VBQ0Usc0JBQUE7RUFFQSw2Q0FBQTtFQUNBLHdDQUFBO0VBQ0EsMkJBQUE7QUNSRjs7QURXQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBRUEsOEJBQUE7S0FBQSwyQkFBQTtNQUFBLDBCQUFBO1VBQUEsc0JBQUE7QUNURjs7QURZQTtFQUNFLGFBQUE7QUNURjs7QURZQTtFQUNFLGFBQUE7QUNURjs7QURZQTtFSlNFLGtDQUFBO0VBQ0EsbUNBQUE7RUErSkUsY0l2S2M7RUp3S2QsZUl4S2M7RUo0TWhCLGFJNU1nQjtFSjZNaEIsZ0JJN01nQjtFSnVLZCxlSXRLZTtFSnVLZixnQkl2S2U7RUoyTWpCLGNJM01pQjtFSjRNakIsaUJJNU1pQjtFQUVqQixlQUFBO0VBRUEsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFFQSxrQ0FBQTtFQUVBLGdCQUFBO0VBRUEsMEJBQUE7RUFFQSx1QkFBQTtFQUVBLHlCQUFBO0VBRUEscUJBQUE7RUFFQSwyQkFBQTtFQUVBLDhCQUFBO0tBQUEsMkJBQUE7TUFBQSwwQkFBQTtVQUFBLHNCQUFBO0FDWkY7O0FDZkE7RUFDRSxtQ0FBQTtBQzdCRjs7QURnQ0E7RUFDRSw2QkFBQTtFQUNBLHdDQUFBO0FDN0JGOztBRGdDQTs7Ozs7O0VOc01FLGdCTWhNZ0I7RU5pTWhCLG1CTWpNNEI7RUFFNUIsZ0JBeEM2QjtFQTBDN0IsZ0JBdkM2QjtBQ1MvQjs7QURpQ0E7RU55TEUsZ0JNeExnQjtFQUVoQixlQTFDNkI7QUNVL0I7O0FEbUNBO0VObUxFLGdCTWxMZ0I7RUFFaEIsZUE3QzZCO0FDVy9COztBRHFDQTtFQUNFLGVBOUM2QjtBQ1cvQjs7QURzQ0E7RUFDRSxlQS9DNkI7QUNZL0I7O0FEc0NBO0VBQ0UsZUFoRDZCO0FDYS9COztBRHNDQTtFQUNFLGVBakQ2QjtBQ2MvQjs7QURzQ0E7RUFDRSxjQUFBO0FDbkNGOztBRHNDQTs7RUFFRSxrQkFBQTtFQUVBLGNBQUE7RUFFQSxjQUFBO0VBRUEsd0JBQUE7QUN0Q0Y7O0FEeUNBO0VBQ0UsV0FBQTtBQ3RDRjs7QUR5Q0E7RUFDRSxlQUFBO0FDdENGOztBQ3ZEQTtFQUNFLHdCQUFBO0FDTkY7O0FEZ0JJO0VBQ0Usd0JBQUE7QUNiTjs7QVR5SEk7RVFyR0E7SUFDRSx3QkFBQTtFQ2hCSjtBQUNGOztBVCtFSTtFUXpFQTtJQUNFLHdCQUFBO0VDSEo7QUFDRjs7QVQ4R0k7RVFyR0E7SUFDRSx3QkFBQTtFQ05KO0FBQ0Y7O0FUcUVJO0VRekVBO0lBQ0Usd0JBQUE7RUNPSjtBQUNGOztBVG9HSTtFUXJHQTtJQUNFLHdCQUFBO0VDSUo7QUFDRjs7QVQyREk7RVF6RUE7SUFDRSx3QkFBQTtFQ2lCSjtBQUNGOztBVDBGSTtFUXJHQTtJQUNFLHdCQUFBO0VDY0o7QUFDRjs7QVRpREk7RVF6RUE7SUFDRSx3QkFBQTtFQzJCSjtBQUNGOztBRHJCSTtFQUNFLHdCQUFBO0FDdUJOOztBQ25DQTs7RUFFRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFVm9MRSxlVWxMZTtFVm1MZixnQlVuTGU7RVZ1TmpCLGNVdk5pQjtFVndOakIsaUJVeE5pQjtBQ1puQjs7QURlQTs7RUFFRSx5Q0FBQTtFQUNBLHVDQUFBO0VBQ0EsdUNBQUE7RUFDQSwwQ0FBQTtFVjhLRSxzQ1VuTU07RVZvTU4sdUNVcE1NO0VWb09SLHFDVXBPUTtFVnFPUix3Q1VyT1E7QUNhVjs7QVgwTE07RUFDRTs7SUFFSSxtQkFBQTtJQUdBLG9CQUFBO0lBR0YsK0NVaE5BO0lWaU5BLDhDVWpOQTtJVmtOQSw2Q1VsTkE7SVZtTkEsNENVbk5BO0VDdUJSO0FBQ0Y7O0FERUE7O0VBRUUsdUNBQUE7RVZ3TUEscUNVcE9RO0FDOEJWOztBREdBOztFQUVFLHlDQUFBO0VWZ0tFLHNDVW5NTTtBQ21DVjs7QVhvS007RUFDRTs7SUFFSSxtQkFBQTtJQU1GLCtDVWhOQTtJVmlOQSw4Q1VqTkE7RUMwQ1I7QUFDRjs7QURIQTs7RUFFRSx1Q0FBQTtFVjBKRSx1Q1VwTU07QUNpRFY7O0FYc0pNO0VBQ0U7O0lBS0ksb0JBQUE7SUFLRiw2Q1VsTkE7SVZtTkEsNENVbk5BO0VDd0RSO0FBQ0Y7O0FEVkE7O0VBRUUsMENBQUE7RVZvTEEsd0NVck9RO0FDK0RWOztBRFRBOztFQUVFLHVDQUFBO0VBQ0EsMENBQUE7RVYyS0EscUNVcE9RO0VWcU9SLHdDVXJPUTtBQ3NFVjs7QURSQTs7RUFFRSx5Q0FBQTtFQUNBLHVDQUFBO0VWa0lFLHNDVW5NTTtFVm9NTix1Q1VwTU07QUM2RVY7O0FYMEhNO0VBQ0U7O0lBRUksbUJBQUE7SUFHQSxvQkFBQTtJQUdGLCtDVWhOQTtJVmlOQSw4Q1VqTkE7SVZrTkEsNkNVbE5BO0lWbU5BLDRDVW5OQTtFQ3VGUjtBQUNGOztBRGRBOztFQUVFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFVmdIRSxjVTlHYztFVitHZCxlVS9HYztFVm1KaEIsYVVuSmdCO0VWb0poQixnQlVwSmdCO0FDbUJsQjs7QURoQkE7O0VBRUUsdUNBQUE7RUFDQSxxQ0FBQTtFQUNBLHFDQUFBO0VBQ0Esd0NBQUE7RVYwR0Usb0NVbE1LO0VWbU1MLHFDVW5NSztFVm1PUCxtQ1VuT087RVZvT1Asc0NVcE9PO0FDK0dUOztBWHVGTTtFQUNFOztJQUVJLGtCQUFBO0lBR0EsbUJBQUE7SUFHRiw2Q1UvTUQ7SVZnTkMsNENVaE5EO0lWaU5DLDJDVWpORDtJVmtOQywwQ1VsTkQ7RUN5SFA7QUFDRjs7QUQ3QkE7O0VBRUUscUNBQUE7RVZvSUEsbUNVbk9PO0FDZ0lUOztBRDVCQTs7RUFFRSx1Q0FBQTtFVjRGRSxvQ1VsTUs7QUNxSVQ7O0FYaUVNO0VBQ0U7O0lBRUksa0JBQUE7SUFNRiw2Q1UvTUQ7SVZnTkMsNENVaE5EO0VDNElQO0FBQ0Y7O0FEbENBOztFQUVFLHFDQUFBO0VWc0ZFLHFDVW5NSztBQ21KVDs7QVhtRE07RUFDRTs7SUFLSSxtQkFBQTtJQUtGLDJDVWpORDtJVmtOQywwQ1VsTkQ7RUMwSlA7QUFDRjs7QUR6Q0E7O0VBRUUsd0NBQUE7RVZnSEEsc0NVcE9PO0FDaUtUOztBRHhDQTs7RUFFRSxxQ0FBQTtFQUNBLHdDQUFBO0VWdUdBLG1DVW5PTztFVm9PUCxzQ1VwT087QUN3S1Q7O0FEdkNBOztFQUVFLHVDQUFBO0VBQ0EscUNBQUE7RVY4REUsb0NVbE1LO0VWbU1MLHFDVW5NSztBQytLVDs7QVh1Qk07RUFDRTs7SUFFSSxrQkFBQTtJQUdBLG1CQUFBO0lBR0YsNkNVL01EO0lWZ05DLDRDVWhORDtJVmlOQywyQ1VqTkQ7SVZrTkMsMENVbE5EO0VDeUxQO0FBQ0Y7O0FDdkxJOztFWmdYRSxzQkFBQTtBYTFYTjs7QURlSTs7RVoyV0UsdUJBQUE7QWFyWE47O0FEZUk7O0Vad1ZFLHNCQUFBO0FhbFdOOztBYitJVztFQXNOTCx1QkFBQTtBYWxXTjs7QURZSTs7RVowVkUsdUJBQUE7QWFqV047O0FidUlXO0VBNk5MLHNCQUFBO0FhaldOOztBYm1FSTtFWTlFQTs7SVpnWEUsc0JBQUE7RWEvVko7O0VEWkU7O0laMldFLHVCQUFBO0VhMVZKOztFRFpFOztJWndWRSxzQkFBQTtFYXZVSjtFYm9IUztJQXNOTCx1QkFBQTtFYXZVSjs7RURmRTs7SVowVkUsdUJBQUE7RWF0VUo7RWI0R1M7SUE2Tkwsc0JBQUE7RWF0VUo7QUFDRjs7QWJ1Q0k7RVk5RUE7O0laZ1hFLHNCQUFBO0VhcFVKOztFRHZDRTs7SVoyV0UsdUJBQUE7RWEvVEo7O0VEdkNFOztJWndWRSxzQkFBQTtFYTVTSjtFYnlGUztJQXNOTCx1QkFBQTtFYTVTSjs7RUQxQ0U7O0laMFZFLHVCQUFBO0VhM1NKO0ViaUZTO0lBNk5MLHNCQUFBO0VhM1NKO0FBQ0Y7O0FiWUk7RVk5RUE7O0laZ1hFLHNCQUFBO0VhelNKOztFRGxFRTs7SVoyV0UsdUJBQUE7RWFwU0o7O0VEbEVFOztJWndWRSxzQkFBQTtFYWpSSjtFYjhEUztJQXNOTCx1QkFBQTtFYWpSSjs7RURyRUU7O0laMFZFLHVCQUFBO0VhaFJKO0Vic0RTO0lBNk5MLHNCQUFBO0VhaFJKO0FBQ0Y7O0FiZkk7RVk5RUE7O0laZ1hFLHNCQUFBO0VhOVFKOztFRDdGRTs7SVoyV0UsdUJBQUE7RWF6UUo7O0VEN0ZFOztJWndWRSxzQkFBQTtFYXRQSjtFYm1DUztJQXNOTCx1QkFBQTtFYXRQSjs7RURoR0U7O0laMFZFLHVCQUFBO0VhclBKO0ViMkJTO0lBNk5MLHNCQUFBO0VhclBKO0FBQ0Y7O0FDeEhJOztFQUVFLDZCQUFBO0FDWk47O0FEZUk7O0VBRUUsOEJBQUE7QUNaTjs7QURlSTs7RUFFRSw0QkFBQTtBQ1pOOztBRGVJOztFQUVFLDBCQUFBO0FDWk47O0FEZUk7O0VBRUUsMkJBQUE7QUNaTjs7QURlSTs7RUFFRSw0QkFBQTtBQ1pOOztBRGVJOztFQUVFLDhCQUFBO0FDWk47O0FEZUk7O0VBRUUsOEJBQUE7QUNaTjs7QWZxREk7RWM5RUE7O0lBRUUsNkJBQUE7RUM2Qko7O0VEMUJFOztJQUVFLDhCQUFBO0VDNkJKOztFRDFCRTs7SUFFRSw0QkFBQTtFQzZCSjs7RUQxQkU7O0lBRUUsMEJBQUE7RUM2Qko7O0VEMUJFOztJQUVFLDJCQUFBO0VDNkJKOztFRDFCRTs7SUFFRSw0QkFBQTtFQzZCSjs7RUQxQkU7O0lBRUUsOEJBQUE7RUM2Qko7O0VEMUJFOztJQUVFLDhCQUFBO0VDNkJKO0FBQ0Y7O0FmV0k7RWM5RUE7O0lBRUUsNkJBQUE7RUNzRUo7O0VEbkVFOztJQUVFLDhCQUFBO0VDc0VKOztFRG5FRTs7SUFFRSw0QkFBQTtFQ3NFSjs7RURuRUU7O0lBRUUsMEJBQUE7RUNzRUo7O0VEbkVFOztJQUVFLDJCQUFBO0VDc0VKOztFRG5FRTs7SUFFRSw0QkFBQTtFQ3NFSjs7RURuRUU7O0lBRUUsOEJBQUE7RUNzRUo7O0VEbkVFOztJQUVFLDhCQUFBO0VDc0VKO0FBQ0Y7O0FmOUJJO0VjOUVBOztJQUVFLDZCQUFBO0VDK0dKOztFRDVHRTs7SUFFRSw4QkFBQTtFQytHSjs7RUQ1R0U7O0lBRUUsNEJBQUE7RUMrR0o7O0VENUdFOztJQUVFLDBCQUFBO0VDK0dKOztFRDVHRTs7SUFFRSwyQkFBQTtFQytHSjs7RUQ1R0U7O0lBRUUsNEJBQUE7RUMrR0o7O0VENUdFOztJQUVFLDhCQUFBO0VDK0dKOztFRDVHRTs7SUFFRSw4QkFBQTtFQytHSjtBQUNGOztBZnZFSTtFYzlFQTs7SUFFRSw2QkFBQTtFQ3dKSjs7RURySkU7O0lBRUUsOEJBQUE7RUN3Sko7O0VEckpFOztJQUVFLDRCQUFBO0VDd0pKOztFRHJKRTs7SUFFRSwwQkFBQTtFQ3dKSjs7RURySkU7O0lBRUUsMkJBQUE7RUN3Sko7O0VEckpFOztJQUVFLDRCQUFBO0VDd0pKOztFRHJKRTs7SUFFRSw4QkFBQTtFQ3dKSjs7RURySkU7O0lBRUUsOEJBQUE7RUN3Sko7QUFDRjs7QUM5TEk7O0VBRUUseURBQUE7RUFDQSxvQ0FBQTtBQ1pOOztBRGVJOztFQUVFLHlEQUFBO0VBQ0Esb0NBQUE7QUNaTjs7QURlSTs7RUFFRSx5REFBQTtFQUNBLHFDQUFBO0FDWk47O0FqQjJFSTtFZ0I5RUE7O0lBRUUseURBQUE7SUFDQSxvQ0FBQTtFQ09KOztFREpFOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUNPSjs7RURKRTs7SUFFRSx5REFBQTtJQUNBLHFDQUFBO0VDT0o7QUFDRjs7QWpCdURJO0VnQjlFQTs7SUFFRSx5REFBQTtJQUNBLG9DQUFBO0VDMEJKOztFRHZCRTs7SUFFRSx5REFBQTtJQUNBLG9DQUFBO0VDMEJKOztFRHZCRTs7SUFFRSx5REFBQTtJQUNBLHFDQUFBO0VDMEJKO0FBQ0Y7O0FqQm9DSTtFZ0I5RUE7O0lBRUUseURBQUE7SUFDQSxvQ0FBQTtFQzZDSjs7RUQxQ0U7O0lBRUUseURBQUE7SUFDQSxvQ0FBQTtFQzZDSjs7RUQxQ0U7O0lBRUUseURBQUE7SUFDQSxxQ0FBQTtFQzZDSjtBQUNGOztBakJpQkk7RWdCOUVBOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUNnRUo7O0VEN0RFOztJQUVFLHlEQUFBO0lBQ0Esb0NBQUE7RUNnRUo7O0VEN0RFOztJQUVFLHlEQUFBO0lBQ0EscUNBQUE7RUNnRUo7QUFDRjs7QUNyRkE7O0VBRUUsaUNBQUE7QUNQRjs7QURVQTs7RUFFRSwrQkFBQTtBQ1BGOztBRFVBOztFQUVFLDZCQUFBO0FDUEY7O0FEVUE7O0VBRUUsOEJBQUE7QUNQRjs7QURVQTs7RUFFRSwrQkFBQTtBQ1BGOztBRFVBOztFQUVFLDJCQUFBO0FDUEY7O0FEY0E7O0VBRUUsMEJBQUE7QUNYRjs7QURjQTs7RUFFRSw0QkFBQTtBQ1hGOztBRGNBOztFQUVFLGtDQUFBO0FDWEY7O0FEa0JBOztFQUVFLHNDQUFBO0FDZkY7O0FEa0JBOztFQUVFLGtDQUFBO0FDZkY7O0FEa0JBOztFQUVFLG9DQUFBO0FDZkY7O0FEa0JBOztFQUVFLHdDQUFBO0FDZkY7O0FEa0JBOztFQUVFLHlDQUFBO0FDZkY7O0FEa0JBOztFQUVFLHdDQUFBO0FDZkY7O0FEc0JBOztFQUVFLGtDQUFBO0FDbkJGOztBRHNCQTs7RUFFRSw4QkFBQTtBQ25CRjs7QURzQkE7O0VBRUUsZ0NBQUE7QUNuQkY7O0FEc0JBOztFQUVFLCtCQUFBO0FDbkJGOztBRHNCQTs7RUFFRSxnQ0FBQTtBQ25CRiIsImZpbGUiOiJzcmMvZ2xvYmFsLnNjc3MifQ== */", '', '']]

/***/ }),

/***/ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/theme/variables.scss":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src??embedded!./node_modules/sass-loader/lib/loader.js??ref--14-3!./src/theme/variables.scss ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = [[module.i, "/** Ionic CSS Variables **/\n:root {\n  /** primary **/\n  --ion-color-primary: #7e57c2;\n  --ion-color-primary-rgb: 28,28,28;\n  --ion-color-primary-contrast: #ffffff;\n  --ion-color-primary-contrast-rgb: 255,255,255;\n  --ion-color-primary-shade: #191919;\n  --ion-color-primary-tint: #333333;\n  /** secondary **/\n  --ion-color-secondary: #FF0062;\n  --ion-color-secondary-rgb: 255,0,98;\n  --ion-color-secondary-contrast: #ffffff;\n  --ion-color-secondary-contrast-rgb: 255,255,255;\n  --ion-color-secondary-shade: #e00056;\n  --ion-color-secondary-tint: #ff1a72;\n  /** tertiary **/\n  --ion-color-tertiary: #00AFFF;\n  --ion-color-tertiary-rgb: 0,175,255;\n  --ion-color-tertiary-contrast: #000000;\n  --ion-color-tertiary-contrast-rgb: 0,0,0;\n  --ion-color-tertiary-shade: #009ae0;\n  --ion-color-tertiary-tint: #1ab7ff;\n  /** success **/\n  --ion-color-success: #20dc6a;\n  --ion-color-success-rgb: 32,220,106;\n  --ion-color-success-contrast: #000000;\n  --ion-color-success-contrast-rgb: 0,0,0;\n  --ion-color-success-shade: #1cc25d;\n  --ion-color-success-tint: #36e079;\n  /** warning **/\n  --ion-color-warning: #ffed11;\n  --ion-color-warning-rgb: 255,237,17;\n  --ion-color-warning-contrast: #000000;\n  --ion-color-warning-contrast-rgb: 0,0,0;\n  --ion-color-warning-shade: #e0d10f;\n  --ion-color-warning-tint: #ffef29;\n  /** danger **/\n  --ion-color-danger: #f4344f;\n  --ion-color-danger-rgb: 244,52,79;\n  --ion-color-danger-contrast: #ffffff;\n  --ion-color-danger-contrast-rgb: 255,255,255;\n  --ion-color-danger-shade: #d72e46;\n  --ion-color-danger-tint: #f54861;\n  /** lightest **/\n  --ion-color-lightest: #FFFFFF;\n  --ion-color-lightest-rgb: 255,255,255;\n  /** light **/\n  --ion-color-light: #f4f5f8;\n  --ion-color-light-rgb: 244,244,244;\n  --ion-color-light-contrast: #000000;\n  --ion-color-light-contrast-rgb: 0,0,0;\n  --ion-color-light-shade: #d7d8da;\n  --ion-color-light-shade-rgb: 215,216,218;\n  --ion-color-light-tint: #f5f6f9;\n  --ion-color-light-tint-rgb: 245,246,249;\n  /** medium **/\n  --ion-color-medium: #989aa2;\n  --ion-color-medium-rgb: 152,154,162;\n  --ion-color-medium-contrast: #ffffff;\n  --ion-color-medium-contrast-rgb: 255,255,255;\n  --ion-color-medium-shade: #86888f;\n  --ion-color-medium-shade-rgb: 134,136,143;\n  --ion-color-medium-tint: #a2a4ab;\n  --ion-color-medium-tint-rgb: 162,164,171;\n  /** dark **/\n  --ion-color-dark: #222428;\n  --ion-color-dark-rgb: 34,34,34;\n  --ion-color-dark-contrast: #ffffff;\n  --ion-color-dark-contrast-rgb: 255,255,255;\n  --ion-color-dark-shade: #1e2023;\n  --ion-color-dark-shade-rgb: 30,32,35;\n  --ion-color-dark-tint: #383a3e;\n  --ion-color-dark-tint-rgb: 56,58,62;\n  /** darkest **/\n  --ion-color-darkest: #000000;\n  --ion-color-darkest-rgb: 0,0,0; }\n:root {\n  /* Set the font family of the entire app */\n  --app-background: #FFFFFF;\n  --app-background-shade: var(--ion-background-color-step-50, #F2F2F2);\n  --app-background-alt: var(--ion-color-primary);\n  --app-background-alt-shade: var(--ion-color-primary-shade);\n  --app-narrow-margin: 12px;\n  --app-fair-margin: 16px;\n  --app-broad-margin: 20px;\n  --app-narrow-radius: 4px;\n  --app-fair-radius: 8px;\n  --app-broad-radius: 12px; }\nhtml.ios {\n  --app-header-height: 44px; }\nhtml.md {\n  --app-header-height: 56px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL3RoZW1lL3ZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLDBCQUFBO0FBQ0E7RUFDRSxjQUFBO0VBQ0EsNEJBQW9CO0VBQ3BCLGlDQUF3QjtFQUN4QixxQ0FBNkI7RUFDN0IsNkNBQWlDO0VBQ2pDLGtDQUEwQjtFQUMxQixpQ0FBeUI7RUFFekIsZ0JBQUE7RUFDQSw4QkFBc0I7RUFDdEIsbUNBQTBCO0VBQzFCLHVDQUErQjtFQUMvQiwrQ0FBbUM7RUFDbkMsb0NBQTRCO0VBQzVCLG1DQUEyQjtFQUUzQixlQUFBO0VBQ0EsNkJBQXFCO0VBQ3JCLG1DQUF5QjtFQUN6QixzQ0FBOEI7RUFDOUIsd0NBQWtDO0VBQ2xDLG1DQUEyQjtFQUMzQixrQ0FBMEI7RUFFMUIsY0FBQTtFQUNBLDRCQUFvQjtFQUNwQixtQ0FBd0I7RUFDeEIscUNBQTZCO0VBQzdCLHVDQUFpQztFQUNqQyxrQ0FBMEI7RUFDMUIsaUNBQXlCO0VBRXpCLGNBQUE7RUFDQSw0QkFBb0I7RUFDcEIsbUNBQXdCO0VBQ3hCLHFDQUE2QjtFQUM3Qix1Q0FBaUM7RUFDakMsa0NBQTBCO0VBQzFCLGlDQUF5QjtFQUV6QixhQUFBO0VBQ0EsMkJBQW1CO0VBQ25CLGlDQUF1QjtFQUN2QixvQ0FBNEI7RUFDNUIsNENBQWdDO0VBQ2hDLGlDQUF5QjtFQUN6QixnQ0FBd0I7RUFFeEIsZUFBQTtFQUNBLDZCQUFxQjtFQUNyQixxQ0FBeUI7RUFFekIsWUFBQTtFQUNBLDBCQUFrQjtFQUNsQixrQ0FBc0I7RUFDdEIsbUNBQTJCO0VBQzNCLHFDQUErQjtFQUMvQixnQ0FBd0I7RUFDeEIsd0NBQTRCO0VBQzVCLCtCQUF1QjtFQUN2Qix1Q0FBMkI7RUFFM0IsYUFBQTtFQUNBLDJCQUFtQjtFQUNuQixtQ0FBdUI7RUFDdkIsb0NBQTRCO0VBQzVCLDRDQUFnQztFQUNoQyxpQ0FBeUI7RUFDekIseUNBQTZCO0VBQzdCLGdDQUF3QjtFQUN4Qix3Q0FBNEI7RUFFNUIsV0FBQTtFQUNBLHlCQUFpQjtFQUNqQiw4QkFBcUI7RUFDckIsa0NBQTBCO0VBQzFCLDBDQUE4QjtFQUM5QiwrQkFBdUI7RUFDdkIsb0NBQTJCO0VBQzNCLDhCQUFzQjtFQUN0QixtQ0FBMEI7RUFFMUIsY0FBQTtFQUNBLDRCQUFvQjtFQUNwQiw4QkFBd0IsRUFBQTtBQUcxQjtFQUNFLDBDQUFBO0VBRUEseUJBQWlCO0VBQ2pCLG9FQUF1QjtFQUN2Qiw4Q0FBcUI7RUFDckIsMERBQTJCO0VBRTNCLHlCQUFvQjtFQUNwQix1QkFBa0I7RUFDbEIsd0JBQW1CO0VBRW5CLHdCQUFvQjtFQUNwQixzQkFBa0I7RUFDbEIsd0JBQW1CLEVBQUE7QUFHckI7RUFDRSx5QkFBb0IsRUFBQTtBQUd0QjtFQUNFLHlCQUFvQixFQUFBIiwiZmlsZSI6InNyYy90aGVtZS92YXJpYWJsZXMuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIElvbmljIFZhcmlhYmxlcyBhbmQgVGhlbWluZy4gRm9yIG1vcmUgaW5mbywgcGxlYXNlIHNlZTpcbi8vIGh0dHA6Ly9pb25pY2ZyYW1ld29yay5jb20vZG9jcy90aGVtaW5nL1xuXG4vKiogSW9uaWMgQ1NTIFZhcmlhYmxlcyAqKi9cbjpyb290IHtcbiAgLyoqIHByaW1hcnkgKiovXG4gIC0taW9uLWNvbG9yLXByaW1hcnk6ICM3ZTU3YzI7IC8vICMxYzFjMWNcbiAgLS1pb24tY29sb3ItcHJpbWFyeS1yZ2I6IDI4LDI4LDI4O1xuICAtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1wcmltYXJ5LWNvbnRyYXN0LXJnYjogMjU1LDI1NSwyNTU7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGU6ICMxOTE5MTk7XG4gIC0taW9uLWNvbG9yLXByaW1hcnktdGludDogIzMzMzMzMztcblxuICAvKiogc2Vjb25kYXJ5ICoqL1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnk6ICNGRjAwNjI7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1yZ2I6IDI1NSwwLDk4O1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3Q6ICNmZmZmZmY7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS1jb250cmFzdC1yZ2I6IDI1NSwyNTUsMjU1O1xuICAtLWlvbi1jb2xvci1zZWNvbmRhcnktc2hhZGU6ICNlMDAwNTY7XG4gIC0taW9uLWNvbG9yLXNlY29uZGFyeS10aW50OiAjZmYxYTcyO1xuXG4gIC8qKiB0ZXJ0aWFyeSAqKi9cbiAgLS1pb24tY29sb3ItdGVydGlhcnk6ICMwMEFGRkY7XG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LXJnYjogMCwxNzUsMjU1O1xuICAtLWlvbi1jb2xvci10ZXJ0aWFyeS1jb250cmFzdDogIzAwMDAwMDtcbiAgLS1pb24tY29sb3ItdGVydGlhcnktY29udHJhc3QtcmdiOiAwLDAsMDtcbiAgLS1pb24tY29sb3ItdGVydGlhcnktc2hhZGU6ICMwMDlhZTA7XG4gIC0taW9uLWNvbG9yLXRlcnRpYXJ5LXRpbnQ6ICMxYWI3ZmY7XG5cbiAgLyoqIHN1Y2Nlc3MgKiovXG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3M6ICMyMGRjNmE7XG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3MtcmdiOiAzMiwyMjAsMTA2O1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0OiAjMDAwMDAwO1xuICAtLWlvbi1jb2xvci1zdWNjZXNzLWNvbnRyYXN0LXJnYjogMCwwLDA7XG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3Mtc2hhZGU6ICMxY2MyNWQ7XG4gIC0taW9uLWNvbG9yLXN1Y2Nlc3MtdGludDogIzM2ZTA3OTtcblxuICAvKiogd2FybmluZyAqKi9cbiAgLS1pb24tY29sb3Itd2FybmluZzogI2ZmZWQxMTtcbiAgLS1pb24tY29sb3Itd2FybmluZy1yZ2I6IDI1NSwyMzcsMTc7XG4gIC0taW9uLWNvbG9yLXdhcm5pbmctY29udHJhc3Q6ICMwMDAwMDA7XG4gIC0taW9uLWNvbG9yLXdhcm5pbmctY29udHJhc3QtcmdiOiAwLDAsMDtcbiAgLS1pb24tY29sb3Itd2FybmluZy1zaGFkZTogI2UwZDEwZjtcbiAgLS1pb24tY29sb3Itd2FybmluZy10aW50OiAjZmZlZjI5O1xuXG4gIC8qKiBkYW5nZXIgKiovXG4gIC0taW9uLWNvbG9yLWRhbmdlcjogI2Y0MzQ0ZjtcbiAgLS1pb24tY29sb3ItZGFuZ2VyLXJnYjogMjQ0LDUyLDc5O1xuICAtLWlvbi1jb2xvci1kYW5nZXItY29udHJhc3Q6ICNmZmZmZmY7XG4gIC0taW9uLWNvbG9yLWRhbmdlci1jb250cmFzdC1yZ2I6IDI1NSwyNTUsMjU1O1xuICAtLWlvbi1jb2xvci1kYW5nZXItc2hhZGU6ICNkNzJlNDY7XG4gIC0taW9uLWNvbG9yLWRhbmdlci10aW50OiAjZjU0ODYxO1xuXG4gIC8qKiBsaWdodGVzdCAqKi9cbiAgLS1pb24tY29sb3ItbGlnaHRlc3Q6ICNGRkZGRkY7XG4gIC0taW9uLWNvbG9yLWxpZ2h0ZXN0LXJnYjogMjU1LDI1NSwyNTU7XG5cbiAgLyoqIGxpZ2h0ICoqL1xuICAtLWlvbi1jb2xvci1saWdodDogI2Y0ZjVmODtcbiAgLS1pb24tY29sb3ItbGlnaHQtcmdiOiAyNDQsMjQ0LDI0NDtcbiAgLS1pb24tY29sb3ItbGlnaHQtY29udHJhc3Q6ICMwMDAwMDA7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LWNvbnRyYXN0LXJnYjogMCwwLDA7XG4gIC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlOiAjZDdkOGRhO1xuICAtLWlvbi1jb2xvci1saWdodC1zaGFkZS1yZ2I6IDIxNSwyMTYsMjE4O1xuICAtLWlvbi1jb2xvci1saWdodC10aW50OiAjZjVmNmY5O1xuICAtLWlvbi1jb2xvci1saWdodC10aW50LXJnYjogMjQ1LDI0NiwyNDk7XG5cbiAgLyoqIG1lZGl1bSAqKi9cbiAgLS1pb24tY29sb3ItbWVkaXVtOiAjOTg5YWEyO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tcmdiOiAxNTIsMTU0LDE2MjtcbiAgLS1pb24tY29sb3ItbWVkaXVtLWNvbnRyYXN0OiAjZmZmZmZmO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tY29udHJhc3QtcmdiOiAyNTUsMjU1LDI1NTtcbiAgLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlOiAjODY4ODhmO1xuICAtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUtcmdiOiAxMzQsMTM2LDE0MztcbiAgLS1pb24tY29sb3ItbWVkaXVtLXRpbnQ6ICNhMmE0YWI7XG4gIC0taW9uLWNvbG9yLW1lZGl1bS10aW50LXJnYjogMTYyLDE2NCwxNzE7XG5cbiAgLyoqIGRhcmsgKiovXG4gIC0taW9uLWNvbG9yLWRhcms6ICMyMjI0Mjg7XG4gIC0taW9uLWNvbG9yLWRhcmstcmdiOiAzNCwzNCwzNDtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdDogI2ZmZmZmZjtcbiAgLS1pb24tY29sb3ItZGFyay1jb250cmFzdC1yZ2I6IDI1NSwyNTUsMjU1O1xuICAtLWlvbi1jb2xvci1kYXJrLXNoYWRlOiAjMWUyMDIzO1xuICAtLWlvbi1jb2xvci1kYXJrLXNoYWRlLXJnYjogMzAsMzIsMzU7XG4gIC0taW9uLWNvbG9yLWRhcmstdGludDogIzM4M2EzZTtcbiAgLS1pb24tY29sb3ItZGFyay10aW50LXJnYjogNTYsNTgsNjI7XG5cbiAgLyoqIGRhcmtlc3QgKiovXG4gIC0taW9uLWNvbG9yLWRhcmtlc3Q6ICMwMDAwMDA7XG4gIC0taW9uLWNvbG9yLWRhcmtlc3QtcmdiOiAwLDAsMDtcbn1cblxuOnJvb3Qge1xuICAvKiBTZXQgdGhlIGZvbnQgZmFtaWx5IG9mIHRoZSBlbnRpcmUgYXBwICovIFxuXG4gIC0tYXBwLWJhY2tncm91bmQ6ICNGRkZGRkY7XG4gIC0tYXBwLWJhY2tncm91bmQtc2hhZGU6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLXN0ZXAtNTAsICNGMkYyRjIpO1xuICAtLWFwcC1iYWNrZ3JvdW5kLWFsdDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAtLWFwcC1iYWNrZ3JvdW5kLWFsdC1zaGFkZTogdmFyKC0taW9uLWNvbG9yLXByaW1hcnktc2hhZGUpO1xuXG4gIC0tYXBwLW5hcnJvdy1tYXJnaW46IDEycHg7XG4gIC0tYXBwLWZhaXItbWFyZ2luOiAxNnB4O1xuICAtLWFwcC1icm9hZC1tYXJnaW46IDIwcHg7XG5cbiAgLS1hcHAtbmFycm93LXJhZGl1czogNHB4O1xuICAtLWFwcC1mYWlyLXJhZGl1czogOHB4O1xuICAtLWFwcC1icm9hZC1yYWRpdXM6IDEycHg7XG59XG5cbmh0bWwuaW9zIHtcbiAgLS1hcHAtaGVhZGVyLWhlaWdodDogNDRweDtcbn1cblxuaHRtbC5tZCB7XG4gIC0tYXBwLWhlYWRlci1oZWlnaHQ6IDU2cHg7XG59XG4iXX0= */", '', '']]

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/app/app.scss":
/*!**************************!*\
  !*** ./src/app/app.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../../node_modules/postcss-loader/src??embedded!../../node_modules/sass-loader/lib/loader.js??ref--14-3!./app.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/app/app.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/global.scss":
/*!*************************!*\
  !*** ./src/global.scss ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../node_modules/postcss-loader/src??embedded!../node_modules/sass-loader/lib/loader.js??ref--14-3!./global.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/global.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./src/theme/variables.scss":
/*!**********************************!*\
  !*** ./src/theme/variables.scss ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!../../node_modules/postcss-loader/src??embedded!../../node_modules/sass-loader/lib/loader.js??ref--14-3!./variables.scss */ "./node_modules/@angular-devkit/build-angular/src/angular-cli-files/plugins/raw-css-loader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/lib/loader.js?!./src/theme/variables.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ 3:
/*!*****************************************************************************!*\
  !*** multi ./src/theme/variables.scss ./src/global.scss ./src/app/app.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/macbookpro/Documents/code/app/ta4pwa/test_ionic4/src/theme/variables.scss */"./src/theme/variables.scss");
__webpack_require__(/*! /Users/macbookpro/Documents/code/app/ta4pwa/test_ionic4/src/global.scss */"./src/global.scss");
module.exports = __webpack_require__(/*! /Users/macbookpro/Documents/code/app/ta4pwa/test_ionic4/src/app/app.scss */"./src/app/app.scss");


/***/ })

},[[3,"runtime"]]]);
//# sourceMappingURL=styles.js.map