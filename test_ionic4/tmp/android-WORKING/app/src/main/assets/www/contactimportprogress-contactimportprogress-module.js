(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contactimportprogress-contactimportprogress-module"],{

/***/ "./src/app/contactimportprogress/contactimportprogress.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/contactimportprogress/contactimportprogress.module.ts ***!
  \***********************************************************************/
/*! exports provided: ContactimportprogressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactimportprogressPageModule", function() { return ContactimportprogressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contactimportprogress_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contactimportprogress.page */ "./src/app/contactimportprogress/contactimportprogress.page.ts");







var routes = [
    {
        path: '',
        component: _contactimportprogress_page__WEBPACK_IMPORTED_MODULE_6__["ContactimportprogressPage"]
    }
];
var ContactimportprogressPageModule = /** @class */ (function () {
    function ContactimportprogressPageModule() {
    }
    ContactimportprogressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_contactimportprogress_page__WEBPACK_IMPORTED_MODULE_6__["ContactimportprogressPage"]]
        })
    ], ContactimportprogressPageModule);
    return ContactimportprogressPageModule;
}());



/***/ }),

/***/ "./src/app/contactimportprogress/contactimportprogress.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/contactimportprogress/contactimportprogress.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Import Customers</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n\n  <ion-card    text-center padding  >\n    <ion-card-header>\n      <ion-card-subtitle>Checking Contacts</ion-card-subtitle>\n      <ion-card-title> {{contactProvider.contactInsertCounter}} of {{contactProvider.contactTotalCounter}}</ion-card-title>\n    </ion-card-header>\n\n    <ion-card-content >\n      <p>\n         Your contacts data is not sent to server without your permission. It is used to make it easy for you to exchange referrals. The database stays in your phone\n      </p>\n      <p></p>\n    </ion-card-content>\n </ion-card>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/contactimportprogress/contactimportprogress.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/contactimportprogress/contactimportprogress.page.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhY3RpbXBvcnRwcm9ncmVzcy9jb250YWN0aW1wb3J0cHJvZ3Jlc3MucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/contactimportprogress/contactimportprogress.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/contactimportprogress/contactimportprogress.page.ts ***!
  \*********************************************************************/
/*! exports provided: ContactimportprogressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactimportprogressPage", function() { return ContactimportprogressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tpstorage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tpstorage.service */ "./src/app/tpstorage.service.ts");
/* harmony import */ var _services_contact_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/contact.service */ "./src/services/contact.service.ts");






var ContactimportprogressPage = /** @class */ (function () {
    function ContactimportprogressPage(route, contactProvider, tpStorageService, events, router) {
        this.route = route;
        this.contactProvider = contactProvider;
        this.tpStorageService = tpStorageService;
        this.events = events;
        this.router = router;
    }
    ContactimportprogressPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            if (params && params.source) {
                _this.sourcePage = params.source;
                console.log("paramData");
                console.log(_this.sourcePage);
            }
        });
        this.tpStorageService.getItem('userUID').then(function (res) {
            if (res) {
                _this.userId = res;
            }
        }).catch(function (e) { });
        //first delete the records
        this.contactProvider.deleteFromTable()
            .then(function (res) {
            //then insert them back
            _this.contactProvider.getSimJsonContacts()
                .then(function (res) {
                _this.tpStorageService.setItem('contactsImportedFromDevice', '1');
                _this.events.subscribe('contactInsertingDone', function () {
                    _this.events.unsubscribe('contactInsertingDone');
                    console.log("> " + _this.contactProvider.contactInsertingDone);
                    if (!_this.contactProvider.contactInsertingDone) {
                    }
                    else {
                        _this.sendToContactsPage();
                    }
                });
            }).catch(function (e) {
                //mytodo error handling 
            });
        }).catch(function (e) {
            //mytodo : error handling : send to back or retry
        });
    };
    ContactimportprogressPage.prototype.sendToContactsPage = function () {
        var navigationExtras = {
            queryParams: {
                source: "contactimportprogresspage"
            }
        };
        this.router.navigate(['/contact'], navigationExtras);
    };
    ContactimportprogressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-contactimportprogress',
            template: __webpack_require__(/*! ./contactimportprogress.page.html */ "./src/app/contactimportprogress/contactimportprogress.page.html"),
            styles: [__webpack_require__(/*! ./contactimportprogress.page.scss */ "./src/app/contactimportprogress/contactimportprogress.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_contact_service__WEBPACK_IMPORTED_MODULE_5__["ContactService"],
            _tpstorage_service__WEBPACK_IMPORTED_MODULE_4__["TpstorageProvider"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Events"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ContactimportprogressPage);
    return ContactimportprogressPage;
}());



/***/ })

}]);
//# sourceMappingURL=contactimportprogress-contactimportprogress-module.js.map