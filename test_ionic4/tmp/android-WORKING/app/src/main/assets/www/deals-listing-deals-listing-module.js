(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["deals-listing-deals-listing-module"],{

/***/ "./src/app/deals/listing/deals-listing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/deals/listing/deals-listing.module.ts ***!
  \*******************************************************/
/*! exports provided: DealsListingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealsListingPageModule", function() { return DealsListingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _deals_listing_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./deals-listing.page */ "./src/app/deals/listing/deals-listing.page.ts");
/* harmony import */ var _deals_listing_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./deals-listing.resolver */ "./src/app/deals/listing/deals-listing.resolver.ts");
/* harmony import */ var _deals_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../deals.service */ "./src/app/deals/deals.service.ts");










var routes = [
    {
        path: '',
        component: _deals_listing_page__WEBPACK_IMPORTED_MODULE_7__["DealsListingPage"],
        resolve: {
            data: _deals_listing_resolver__WEBPACK_IMPORTED_MODULE_8__["DealsListingResolver"]
        }
    }
];
var DealsListingPageModule = /** @class */ (function () {
    function DealsListingPageModule() {
    }
    DealsListingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]
            ],
            declarations: [
                _deals_listing_page__WEBPACK_IMPORTED_MODULE_7__["DealsListingPage"]
            ],
            providers: [
                _deals_listing_resolver__WEBPACK_IMPORTED_MODULE_8__["DealsListingResolver"],
                _deals_service__WEBPACK_IMPORTED_MODULE_9__["DealsService"]
            ]
        })
    ], DealsListingPageModule);
    return DealsListingPageModule;
}());



/***/ }),

/***/ "./src/app/deals/listing/deals-listing.page.html":
/*!*******************************************************!*\
  !*** ./src/app/deals/listing/deals-listing.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Deals</ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\" (click)=\"askForReferral()\"  name=\"add-circle\"></ion-icon>    \n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"deals-listing-content\">\n  <ng-container *ngIf=\"listing?.items\">\n    <div class=\"listing-item\" *ngFor=\"let item of listing.items\" [ngClass]=\"{'ended': ((item.expirationDate | appTimeDifference) < 0), 'imminent-end': ((item.expirationDate | appTimeDifference) > 0 && (item.expirationDate | appTimeDifference) <= 2), 'ends-soon': ((item.expirationDate | appTimeDifference) > 0 && (item.expirationDate | appTimeDifference) > 2 && (item.expirationDate | appTimeDifference) < 10), 'distant-end': ((item.expirationDate | appTimeDifference) >= 10)}\">\n      <ion-row class=\"top-row\">\n        <ion-col class=\"logo-col\" size=\"6\">\n          <app-aspect-ratio [ratio]=\"{w: 128, h: 32}\">\n            <app-image-shell [src]=\"item.logo\" [alt]=\"'deals logo'\" class=\"deal-logo\" animation=\"spinner\"></app-image-shell>\n          </app-aspect-ratio>\n        </ion-col>\n        <ion-col *ngIf=\"((item.expirationDate | appTimeDifference) > 0)\" class=\"call-to-action-col\">\n          <ion-button class=\"claim-button\" expand=\"block\" color=\"claim\" [routerLink]=\"['/app/deals/detail/', item.slug]\">\n            <span class=\"button-cta\">CLAIM</span>\n            <ion-icon slot=\"end\" name=\"ios-arrow-forward\"></ion-icon>\n          </ion-button>\n        </ion-col>\n        <ion-col *ngIf=\"((item.expirationDate | appTimeDifference) < 0)\" class=\"call-to-action-col\">\n          <span class=\"expired-cta\">EXPIRED</span>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"middle-row\">\n        <ion-col class=\"info-col\">\n          <h4 class=\"item-name\">\n            <a class=\"name-anchor\" [routerLink]=\"['/app/deals/detail/', item.slug]\">\n              <app-text-shell animation=\"bouncing\" [data]=\"item.name\"></app-text-shell>\n            </a>\n          </h4>\n          <p class=\"item-description\">\n            <app-text-shell animation=\"bouncing\" lines=\"3\" [data]=\"item.description\"></app-text-shell>\n          </p>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-button class=\"bookmark-button\" expand=\"block\" fill=\"clear\" color=\"claim\">\n            <ion-icon slot=\"icon-only\" name=\"pricetag\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"bottom-row\">\n        <ion-col class=\"code-wrapper\">\n          <span class=\"code-cta\">Use this code:</span>\n          <span class=\"item-code\" [ngClass]=\"{'expired': ((item.expirationDate | appTimeDifference) < 0)}\">\n            <app-text-shell [data]=\"item.code\"></app-text-shell>\n          </span>\n        </ion-col>\n        <ion-col class=\"time-left-wrapper\" size=\"5\" [ngClass]=\"{'countdown-active': ((item.expirationDate | appTimeDifference) > 0 && (item.expirationDate | appTimeDifference) <= 2)}\">\n          <span class=\"expiration-cta\">DEAL {{ ((item.expirationDate | appTimeDifference) < 0) ? 'EXPIRED' : 'EXPIRES IN' }}:</span>\n          <ng-container *ngIf=\"((item.expirationDate | appTimeDifference) < 0 || (item.expirationDate | appTimeDifference) > 2)\">\n            <span class=\"item-time-left\">\n              <app-text-shell [data]=\"(item.expirationDate | appTimeAgo)\"></app-text-shell>\n            </span>\n          </ng-container>\n          <ng-container *ngIf=\"((item.expirationDate | appTimeDifference) > 0 && (item.expirationDate | appTimeDifference) <= 2)\">\n            <ion-row class=\"countdown-wrapper\">\n              <app-countdown-timer class=\"item-countdown\" fill=\"inner-time\" [end]=\"item.expirationDate\" [units]=\"{from: 'hour', to: 'second'}\"></app-countdown-timer>\n            </ion-row>\n          </ng-container>\n        </ion-col>\n      </ion-row>\n    </div>\n\n\n    <!-- fab placed to the bottom start -->\n    <ion-fab   vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n      <ion-fab-button color=\"success\">\n        <ion-icon name=\"add\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab>\n\n  </ng-container>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/deals/listing/deals-listing.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/deals/listing/deals-listing.page.ts ***!
  \*****************************************************/
/*! exports provided: DealsListingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealsListingPage", function() { return DealsListingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var DealsListingPage = /** @class */ (function () {
    function DealsListingPage(router, route) {
        this.router = router;
        this.route = route;
    }
    Object.defineProperty(DealsListingPage.prototype, "isShell", {
        get: function () {
            return (this.listing && this.listing.isShell) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    DealsListingPage.prototype.askForReferral = function () {
        this.router.navigateByUrl('/referral');
    };
    DealsListingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (resolvedRouteData) {
            var listingDataStore = resolvedRouteData['data'];
            listingDataStore.state.subscribe(function (state) {
                _this.listing = state;
            }, function (error) { });
        }, function (error) { });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.is-shell'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DealsListingPage.prototype, "isShell", null);
    DealsListingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-deals-listing',
            template: __webpack_require__(/*! ./deals-listing.page.html */ "./src/app/deals/listing/deals-listing.page.html"),
            styles: [__webpack_require__(/*! ./styles/deals-listing.page.scss */ "./src/app/deals/listing/styles/deals-listing.page.scss"), __webpack_require__(/*! ./styles/deals-listing.shell.scss */ "./src/app/deals/listing/styles/deals-listing.shell.scss"), __webpack_require__(/*! ./styles/deals-listing.ios.scss */ "./src/app/deals/listing/styles/deals-listing.ios.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], DealsListingPage);
    return DealsListingPage;
}());



/***/ }),

/***/ "./src/app/deals/listing/deals-listing.resolver.ts":
/*!*********************************************************!*\
  !*** ./src/app/deals/listing/deals-listing.resolver.ts ***!
  \*********************************************************/
/*! exports provided: DealsListingResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealsListingResolver", function() { return DealsListingResolver; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _deals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../deals.service */ "./src/app/deals/deals.service.ts");



var DealsListingResolver = /** @class */ (function () {
    function DealsListingResolver(dealsService) {
        this.dealsService = dealsService;
    }
    DealsListingResolver.prototype.resolve = function () {
        var dataSource = this.dealsService.getListingDataSource();
        var dataStore = this.dealsService.getListingStore(dataSource);
        return dataStore;
    };
    DealsListingResolver = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_deals_service__WEBPACK_IMPORTED_MODULE_2__["DealsService"]])
    ], DealsListingResolver);
    return DealsListingResolver;
}());



/***/ }),

/***/ "./src/app/deals/listing/styles/deals-listing.ios.scss":
/*!*************************************************************!*\
  !*** ./src/app/deals/listing/styles/deals-listing.ios.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host-context(.plt-mobile.ios) .claim-button {\n  height: 2.2em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9kZWFscy9saXN0aW5nL3N0eWxlcy9kZWFscy1saXN0aW5nLmlvcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBR0ksYUFBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZGVhbHMvbGlzdGluZy9zdHlsZXMvZGVhbHMtbGlzdGluZy5pb3Muc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0LWNvbnRleHQoLnBsdC1tb2JpbGUuaW9zKSB7XG4gIC8vIEN1c3RvbSBwbGF0Zm9ybSBzdHlsZXMgaGVyZVxuICAuY2xhaW0tYnV0dG9uIHtcbiAgICBoZWlnaHQ6IDIuMmVtO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/deals/listing/styles/deals-listing.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/deals/listing/styles/deals-listing.page.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-broad-margin);\n  --page-background: var(--app-background-shade);\n  --page-countdown-fill-shadow-color: rgba(var(--ion-color-darkest-rgb), 0.2);\n  --page-item-shadow-color: rgba(var(--ion-color-dark-rgb), .1);\n  --page-deal-code-height: 30px;\n  --page-countdown-gutter: 3px;\n  --page-expired-color: var(--ion-color-medium-tint);\n  --page-ends-soon-color: #FFAB6B;\n  --page-distant-end-color: #70DF70;\n  --page-deal-color: var(--ion-color-medium);\n  --page-deal-border-color: var(--ion-color-light-shade);\n  --page-color: #70DF70;\n  --ion-color-claim: #70DF70;\n  --ion-color-claim-rgb: 112,223,112;\n  --ion-color-claim-contrast: #FFFFFF;\n  --ion-color-claim-contrast-rgb: 255,255,255;\n  --ion-color-claim-shade: #63C463;\n  --ion-color-claim-tint: #7EE27E; }\n  :host .ion-color-claim {\n    --ion-color-base: var(--ion-color-claim) !important;\n    --ion-color-base-rgb: var(--ion-color-claim-rgb) !important;\n    --ion-color-contrast: var(--ion-color-claim-contrast) !important;\n    --ion-color-contrast-rgb: var(--ion-color-claim-contrast-rgb) !important;\n    --ion-color-shade: var(--ion-color-claim-shade) !important;\n    --ion-color-tint: var(--ion-color-claim-tint) !important; }\n  .deals-listing-content {\n  --background: var(--page-background); }\n  .listing-item {\n  background-color: var(--ion-color-lightest);\n  margin: var(--page-margin);\n  border-radius: var(--app-fair-radius);\n  box-shadow: 1px 1px 4px 1px var(--page-item-shadow-color);\n  border-bottom-style: solid;\n  border-bottom-width: calc((var(--page-margin) / 2) * 3);\n  border-bottom-color: var(--page-deal-border-color); }\n  .listing-item.ended {\n    --page-deal-color: var(--page-expired-color);\n    --page-deal-border-color: var(--page-expired-color);\n    border-bottom-style: none; }\n  .listing-item.imminent-end {\n    --page-deal-color: var(--page-ends-soon-color);\n    --page-deal-border-color: var(--page-distant-end-color); }\n  .listing-item.ends-soon {\n    --page-deal-color: var(--page-ends-soon-color);\n    --page-deal-border-color: var(--page-ends-soon-color);\n    border-bottom-style: none; }\n  .listing-item.distant-end {\n    --page-deal-color: var(--page-distant-end-color);\n    --page-deal-border-color: var(--page-distant-end-color);\n    border-bottom-style: none; }\n  .listing-item .top-row {\n    --ion-grid-column-padding: 0px;\n    border-bottom: 2px dashed rgba(var(--ion-color-dark-rgb), 0.1);\n    padding: calc(var(--page-margin) / 2);\n    justify-content: space-between;\n    align-items: center; }\n  .listing-item .top-row .logo-col {\n      align-self: center; }\n  .listing-item .top-row .call-to-action-col {\n      flex-grow: 0; }\n  .listing-item .top-row .call-to-action-col .expired-cta {\n        color: var(--page-expired-color);\n        font-size: 16px;\n        font-weight: 600;\n        letter-spacing: 0.8px; }\n  .listing-item .top-row .call-to-action-col .claim-button {\n        --box-shadow: none;\n        --border-radius: var(--app-fair-radius);\n        margin: 0px; }\n  .listing-item .top-row .call-to-action-col .claim-button:focus {\n          outline: none; }\n  .listing-item .top-row .call-to-action-col .claim-button .button-cta {\n          display: block;\n          line-height: 18px;\n          font-size: 16px;\n          font-weight: 600; }\n  .listing-item .top-row .call-to-action-col .claim-button ion-icon {\n          font-size: 18px; }\n  .listing-item .middle-row {\n    --ion-grid-column-padding: 0px;\n    padding: calc(var(--page-margin) / 2);\n    padding-bottom: var(--page-margin); }\n  .listing-item .middle-row .info-col {\n      padding-right: calc(var(--page-margin) / 2); }\n  .listing-item .middle-row .item-name {\n      margin: 0px;\n      margin-bottom: calc(var(--page-margin) / 2);\n      color: var(--ion-color-dark-tint);\n      font-size: 16px; }\n  .listing-item .middle-row .item-name .name-anchor {\n        display: block;\n        text-decoration: none; }\n  .listing-item .middle-row .item-description {\n      margin: 0px;\n      color: var(--ion-color-medium-tint);\n      font-size: 14px; }\n  .listing-item .middle-row .bookmark-button {\n      --padding-start: 5px;\n      --padding-end: 5px;\n      margin: 0px;\n      font-size: 16px; }\n  .listing-item .bottom-row {\n    --ion-grid-column-padding: 0px;\n    padding: 0px calc(var(--page-margin) / 2) calc(var(--page-margin) / 2);\n    justify-content: space-between; }\n  .listing-item .bottom-row .code-wrapper {\n      max-width: -webkit-fit-content;\n      max-width: -moz-fit-content;\n      max-width: fit-content; }\n  .listing-item .bottom-row .code-wrapper .code-cta {\n        font-size: 12px;\n        font-weight: 500;\n        margin-bottom: 5px;\n        display: block; }\n  .listing-item .bottom-row .code-wrapper .item-code {\n        height: var(--page-deal-code-height);\n        text-transform: uppercase;\n        padding: calc(var(--page-margin) / 4) calc(var(--page-margin) / 2);\n        border: 2px solid var(--page-color);\n        font-size: 12px;\n        font-weight: 500;\n        line-height: 1;\n        border-radius: calc(var(--page-deal-code-height) / 2);\n        display: flex;\n        align-items: center;\n        justify-content: center; }\n  .listing-item .bottom-row .code-wrapper .item-code.expired {\n          border-color: var(--page-expired-color);\n          color: var(--page-expired-color);\n          opacity: 0.6; }\n  .listing-item .bottom-row .time-left-wrapper {\n      flex-shrink: 0;\n      max-width: -webkit-fit-content;\n      max-width: -moz-fit-content;\n      max-width: fit-content;\n      display: flex;\n      flex-direction: column;\n      justify-content: space-between;\n      align-items: flex-end; }\n  .listing-item .bottom-row .time-left-wrapper .expiration-cta {\n        font-size: 12px;\n        font-weight: 500;\n        margin-bottom: 5px;\n        display: block;\n        text-transform: uppercase;\n        text-align: end; }\n  .listing-item .bottom-row .time-left-wrapper:not(.countdown-active) .item-time-left {\n        color: var(--page-deal-color);\n        font-size: 14px;\n        font-weight: 500;\n        display: block;\n        text-align: end; }\n  .listing-item .bottom-row .time-left-wrapper.countdown-active {\n        display: block;\n        position: relative; }\n  .listing-item .bottom-row .time-left-wrapper.countdown-active .countdown-wrapper {\n          width: 100%;\n          display: flex;\n          justify-content: flex-end;\n          position: absolute;\n          right: 0px; }\n  .listing-item .bottom-row .time-left-wrapper.countdown-active .countdown-wrapper app-countdown-timer.item-countdown {\n            --countdown-time-margin: 0px;\n            --countdown-time-padding: 0px;\n            --countdown-inner-time-margin: var(--page-countdown-gutter) var(--page-countdown-gutter) 0px;\n            --countdown-inner-time-padding: calc(var(--page-margin) / 2) calc(var(--page-margin) / 4);\n            --countdown-fill-border: none;\n            --countdown-fill-border-radius: var(--app-narrow-radius);\n            --countdown-fill-background: var(--ion-color-lightest);\n            --countdown-fill-shadow: 0px 0px 5px 0px var(--page-countdown-fill-shadow-color);\n            --countdown-value-color: var(--ion-color-darkest);\n            --countdown-unit-color: var(--ion-color-medium-shade);\n            --countdown-time-flex-direction: column;\n            font-weight: 500;\n            font-size: 14px;\n            margin-right: calc(var(--page-countdown-gutter) * -1); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9kZWFscy9saXN0aW5nL3N0eWxlcy9kZWFscy1saXN0aW5nLnBhZ2Uuc2NzcyIsIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL3RoZW1lL21peGlucy9pb24tY29sb3Iuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNFLHNDQUFjO0VBQ2QsOENBQWtCO0VBRWxCLDJFQUFtQztFQUNuQyw2REFBeUI7RUFDekIsNkJBQXdCO0VBQ3hCLDRCQUF3QjtFQUV4QixrREFBcUI7RUFDckIsK0JBQXVCO0VBQ3ZCLGlDQUF5QjtFQUV6QiwwQ0FBa0I7RUFDbEIsc0RBQXlCO0VBRXpCLHFCQUFhO0VBSVgsMEJBQWtCO0VBQ2xCLGtDQUFzQjtFQUN0QixtQ0FBMkI7RUFDM0IsMkNBQStCO0VBQy9CLGdDQUF3QjtFQUN4QiwrQkFBdUIsRUFBQTtFQXpCM0I7SUNBSSxtREFBaUI7SUFDakIsMkRBQXFCO0lBQ3JCLGdFQUFxQjtJQUNyQix3RUFBeUI7SUFDekIsMERBQWtCO0lBQ2xCLHdEQUFpQixFQUFBO0VEeUJyQjtFQUNFLG9DQUFhLEVBQUE7RUFHZjtFQXFCRSwyQ0FBMkM7RUFDM0MsMEJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyx5REFBeUQ7RUFDekQsMEJBQTBCO0VBQzFCLHVEQUF1RDtFQUN2RCxrREFBa0QsRUFBQTtFQTNCcEQ7SUFFSSw0Q0FBa0I7SUFDbEIsbURBQXlCO0lBQ3pCLHlCQUF5QixFQUFBO0VBSjdCO0lBT0ksOENBQWtCO0lBQ2xCLHVEQUF5QixFQUFBO0VBUjdCO0lBV0ksOENBQWtCO0lBQ2xCLHFEQUF5QjtJQUN6Qix5QkFBeUIsRUFBQTtFQWI3QjtJQWdCSSxnREFBa0I7SUFDbEIsdURBQXlCO0lBQ3pCLHlCQUF5QixFQUFBO0VBbEI3QjtJQThCSSw4QkFBMEI7SUFFMUIsOERBQTZEO0lBQzdELHFDQUFxQztJQUNyQyw4QkFBOEI7SUFDOUIsbUJBQW1CLEVBQUE7RUFuQ3ZCO01Bc0NNLGtCQUFrQixFQUFBO0VBdEN4QjtNQTBDTSxZQUFZLEVBQUE7RUExQ2xCO1FBNkNRLGdDQUFnQztRQUNoQyxlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLHFCQUFxQixFQUFBO0VBaEQ3QjtRQW9EUSxrQkFBYTtRQUNiLHVDQUFnQjtRQUVoQixXQUFXLEVBQUE7RUF2RG5CO1VBMERVLGFBQWEsRUFBQTtFQTFEdkI7VUE4RFUsY0FBYztVQUNkLGlCQUFpQjtVQUNqQixlQUFlO1VBQ2YsZ0JBQWdCLEVBQUE7RUFqRTFCO1VBcUVVLGVBQWUsRUFBQTtFQXJFekI7SUE0RUksOEJBQTBCO0lBRTFCLHFDQUFxQztJQUNyQyxrQ0FBa0MsRUFBQTtFQS9FdEM7TUFrRk0sMkNBQTJDLEVBQUE7RUFsRmpEO01Bc0ZNLFdBQVc7TUFDZCwyQ0FBMkM7TUFDeEMsaUNBQWlDO01BQ2pDLGVBQWUsRUFBQTtFQXpGckI7UUE0RlEsY0FBYztRQUNkLHFCQUFxQixFQUFBO0VBN0Y3QjtNQWtHTSxXQUFXO01BQ1gsbUNBQW1DO01BQ25DLGVBQWUsRUFBQTtFQXBHckI7TUF3R00sb0JBQWdCO01BQ2hCLGtCQUFjO01BRWQsV0FBVztNQUNYLGVBQWUsRUFBQTtFQTVHckI7SUFpSEksOEJBQTBCO0lBRTFCLHNFQUFzRTtJQUN0RSw4QkFBOEIsRUFBQTtFQXBIbEM7TUF1SE0sOEJBQXNCO01BQXRCLDJCQUFzQjtNQUF0QixzQkFBc0IsRUFBQTtFQXZINUI7UUEwSFEsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIsY0FBYyxFQUFBO0VBN0h0QjtRQWlJUSxvQ0FBb0M7UUFDcEMseUJBQXlCO1FBQ3pCLGtFQUFrRTtRQUNsRSxtQ0FBbUM7UUFDbkMsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2QscURBQXFEO1FBQ3JELGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsdUJBQXVCLEVBQUE7RUEzSS9CO1VBOElVLHVDQUF1QztVQUN2QyxnQ0FBZ0M7VUFDaEMsWUFBWSxFQUFBO0VBaEp0QjtNQXNKTSxjQUFjO01BQ2QsOEJBQXNCO01BQXRCLDJCQUFzQjtNQUF0QixzQkFBc0I7TUFDdEIsYUFBYTtNQUNiLHNCQUFzQjtNQUN0Qiw4QkFBOEI7TUFDOUIscUJBQXFCLEVBQUE7RUEzSjNCO1FBOEpRLGVBQWU7UUFDZixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCx5QkFBeUI7UUFDekIsZUFBZSxFQUFBO0VBbkt2QjtRQXdLVSw2QkFBNkI7UUFDN0IsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixjQUFjO1FBQ2QsZUFBZSxFQUFBO0VBNUt6QjtRQWlMUSxjQUFjO1FBQ2Qsa0JBQWtCLEVBQUE7RUFsTDFCO1VBcUxVLFdBQVc7VUFDWCxhQUFhO1VBQ2IseUJBQXlCO1VBRXpCLGtCQUFrQjtVQUNsQixVQUFVLEVBQUE7RUExTHBCO1lBNkxZLDRCQUF3QjtZQUN4Qiw2QkFBeUI7WUFDekIsNEZBQThCO1lBQzlCLHlGQUErQjtZQUMvQiw2QkFBd0I7WUFDeEIsd0RBQStCO1lBQy9CLHNEQUE0QjtZQUM1QixnRkFBd0I7WUFDeEIsaURBQXdCO1lBQ3hCLHFEQUF1QjtZQUN2Qix1Q0FBZ0M7WUFFaEMsZ0JBQWdCO1lBQ2hCLGVBQWU7WUFDZixxREFBcUQsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2RlYWxzL2xpc3Rpbmcvc3R5bGVzL2RlYWxzLWxpc3RpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3RoZW1lL21peGlucy9pb24tY29sb3JcIjtcblxuLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1icm9hZC1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQtc2hhZGUpO1xuXG4gIC0tcGFnZS1jb3VudGRvd24tZmlsbC1zaGFkb3ctY29sb3I6IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmtlc3QtcmdiKSwgMC4yKTtcbiAgLS1wYWdlLWl0ZW0tc2hhZG93LWNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIC4xKTtcbiAgLS1wYWdlLWRlYWwtY29kZS1oZWlnaHQ6IDMwcHg7XG4gIC0tcGFnZS1jb3VudGRvd24tZ3V0dGVyOiAzcHg7XG5cbiAgLS1wYWdlLWV4cGlyZWQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tdGludCk7XG4gIC0tcGFnZS1lbmRzLXNvb24tY29sb3I6ICNGRkFCNkI7XG4gIC0tcGFnZS1kaXN0YW50LWVuZC1jb2xvcjogIzcwREY3MDtcblxuICAtLXBhZ2UtZGVhbC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIC0tcGFnZS1kZWFsLWJvcmRlci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcblxuICAtLXBhZ2UtY29sb3I6ICM3MERGNzA7XG5cbiAgLy8gQWRkIGN1c3RvbSBjb2xvcnMgdG8gdXNlIHdpdGggW2NvbG9yXSBwcm9wZXJ0eVxuICBAaW5jbHVkZSBpb24tY29sb3IoJ2NsYWltJykge1xuICAgIC0taW9uLWNvbG9yLWNsYWltOiAjNzBERjcwO1xuICAgIC0taW9uLWNvbG9yLWNsYWltLXJnYjogMTEyLDIyMywxMTI7XG4gICAgLS1pb24tY29sb3ItY2xhaW0tY29udHJhc3Q6ICNGRkZGRkY7XG4gICAgLS1pb24tY29sb3ItY2xhaW0tY29udHJhc3QtcmdiOiAyNTUsMjU1LDI1NTtcbiAgICAtLWlvbi1jb2xvci1jbGFpbS1zaGFkZTogIzYzQzQ2MztcbiAgICAtLWlvbi1jb2xvci1jbGFpbS10aW50OiAjN0VFMjdFO1xuICB9XG59XG5cbi8vIE5vdGU6ICBBbGwgdGhlIENTUyB2YXJpYWJsZXMgZGVmaW5lZCBiZWxvdyBhcmUgb3ZlcnJpZGVzIG9mIElvbmljIGVsZW1lbnRzIENTUyBDdXN0b20gUHJvcGVydGllc1xuLmRlYWxzLWxpc3RpbmctY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcbn1cblxuLmxpc3RpbmctaXRlbSB7XG4gICYuZW5kZWQge1xuICAgIC0tcGFnZS1kZWFsLWNvbG9yOiB2YXIoLS1wYWdlLWV4cGlyZWQtY29sb3IpO1xuICAgIC0tcGFnZS1kZWFsLWJvcmRlci1jb2xvcjogdmFyKC0tcGFnZS1leHBpcmVkLWNvbG9yKTtcbiAgICBib3JkZXItYm90dG9tLXN0eWxlOiBub25lO1xuICB9XG4gICYuaW1taW5lbnQtZW5kIHtcbiAgICAtLXBhZ2UtZGVhbC1jb2xvcjogdmFyKC0tcGFnZS1lbmRzLXNvb24tY29sb3IpO1xuICAgIC0tcGFnZS1kZWFsLWJvcmRlci1jb2xvcjogdmFyKC0tcGFnZS1kaXN0YW50LWVuZC1jb2xvcik7XG4gIH1cbiAgJi5lbmRzLXNvb24ge1xuICAgIC0tcGFnZS1kZWFsLWNvbG9yOiB2YXIoLS1wYWdlLWVuZHMtc29vbi1jb2xvcik7XG4gICAgLS1wYWdlLWRlYWwtYm9yZGVyLWNvbG9yOiB2YXIoLS1wYWdlLWVuZHMtc29vbi1jb2xvcik7XG4gICAgYm9yZGVyLWJvdHRvbS1zdHlsZTogbm9uZTtcbiAgfVxuICAmLmRpc3RhbnQtZW5kIHtcbiAgICAtLXBhZ2UtZGVhbC1jb2xvcjogdmFyKC0tcGFnZS1kaXN0YW50LWVuZC1jb2xvcik7XG4gICAgLS1wYWdlLWRlYWwtYm9yZGVyLWNvbG9yOiB2YXIoLS1wYWdlLWRpc3RhbnQtZW5kLWNvbG9yKTtcbiAgICBib3JkZXItYm90dG9tLXN0eWxlOiBub25lO1xuICB9XG5cbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgbWFyZ2luOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIGJvcmRlci1yYWRpdXM6IHZhcigtLWFwcC1mYWlyLXJhZGl1cyk7XG4gIGJveC1zaGFkb3c6IDFweCAxcHggNHB4IDFweCB2YXIoLS1wYWdlLWl0ZW0tc2hhZG93LWNvbG9yKTtcbiAgYm9yZGVyLWJvdHRvbS1zdHlsZTogc29saWQ7XG4gIGJvcmRlci1ib3R0b20td2lkdGg6IGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpICogMyk7XG4gIGJvcmRlci1ib3R0b20tY29sb3I6IHZhcigtLXBhZ2UtZGVhbC1ib3JkZXItY29sb3IpO1xuXG4gIC50b3Atcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBib3JkZXItYm90dG9tOiAycHggZGFzaGVkIHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgLjEpO1xuICAgIHBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAubG9nby1jb2wge1xuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgIH1cblxuICAgIC5jYWxsLXRvLWFjdGlvbi1jb2wge1xuICAgICAgZmxleC1ncm93OiAwO1xuXG4gICAgICAuZXhwaXJlZC1jdGEge1xuICAgICAgICBjb2xvcjogdmFyKC0tcGFnZS1leHBpcmVkLWNvbG9yKTtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMC44cHg7XG4gICAgICB9XG5cbiAgICAgIC5jbGFpbS1idXR0b24ge1xuICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogdmFyKC0tYXBwLWZhaXItcmFkaXVzKTtcblxuICAgICAgICBtYXJnaW46IDBweDtcblxuICAgICAgICAmOmZvY3VzIHtcbiAgICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgICAgICB9XG5cbiAgICAgICAgLmJ1dHRvbi1jdGEge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWljb24ge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC5taWRkbGUtcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBwYWRkaW5nOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgIHBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG5cbiAgICAuaW5mby1jb2wge1xuICAgICAgcGFkZGluZy1yaWdodDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICB9XG5cbiAgICAuaXRlbS1uYW1lIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuXHRcdFx0bWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyay10aW50KTtcbiAgICAgIGZvbnQtc2l6ZTogMTZweDtcblxuICAgICAgLm5hbWUtYW5jaG9yIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuaXRlbS1kZXNjcmlwdGlvbiB7XG4gICAgICBtYXJnaW46IDBweDtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXRpbnQpO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cblxuICAgIC5ib29rbWFyay1idXR0b24ge1xuICAgICAgLS1wYWRkaW5nLXN0YXJ0OiA1cHg7XG4gICAgICAtLXBhZGRpbmctZW5kOiA1cHg7XG5cbiAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgIH1cbiAgfVxuXG4gIC5ib3R0b20tcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBwYWRkaW5nOiAwcHggY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKSBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcblxuICAgIC5jb2RlLXdyYXBwZXIge1xuICAgICAgbWF4LXdpZHRoOiBmaXQtY29udGVudDtcblxuICAgICAgLmNvZGUtY3RhIHtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgfVxuXG4gICAgICAuaXRlbS1jb2RlIHtcbiAgICAgICAgaGVpZ2h0OiB2YXIoLS1wYWdlLWRlYWwtY29kZS1oZWlnaHQpO1xuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICBwYWRkaW5nOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDQpIGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLXBhZ2UtY29sb3IpO1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxO1xuICAgICAgICBib3JkZXItcmFkaXVzOiBjYWxjKHZhcigtLXBhZ2UtZGVhbC1jb2RlLWhlaWdodCkgLyAyKTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG5cbiAgICAgICAgJi5leHBpcmVkIHtcbiAgICAgICAgICBib3JkZXItY29sb3I6IHZhcigtLXBhZ2UtZXhwaXJlZC1jb2xvcik7XG4gICAgICAgICAgY29sb3I6IHZhcigtLXBhZ2UtZXhwaXJlZC1jb2xvcik7XG4gICAgICAgICAgb3BhY2l0eTogMC42O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLnRpbWUtbGVmdC13cmFwcGVyIHtcbiAgICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgICAgbWF4LXdpZHRoOiBmaXQtY29udGVudDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuXG4gICAgICAuZXhwaXJhdGlvbi1jdGEge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIHRleHQtYWxpZ246IGVuZDtcbiAgICAgIH1cblxuICAgICAgJjpub3QoLmNvdW50ZG93bi1hY3RpdmUpIHtcbiAgICAgICAgLml0ZW0tdGltZS1sZWZ0IHtcbiAgICAgICAgICBjb2xvcjogdmFyKC0tcGFnZS1kZWFsLWNvbG9yKTtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICB0ZXh0LWFsaWduOiBlbmQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgJi5jb3VudGRvd24tYWN0aXZlIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcblxuICAgICAgICAuY291bnRkb3duLXdyYXBwZXIge1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICAgICAgICAvLyBTZXQgcG9zaXRpb24gYWJzb2x1dGUgc28gdGhhdCB0aGUgcGFyZW50IGRvZW4ndCBjb3VudCB0aGUgaGVpZ2h0IG9mIHRoaXMgZWxlbWVudC5cbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgcmlnaHQ6IDBweDtcblxuICAgICAgICAgIGFwcC1jb3VudGRvd24tdGltZXIuaXRlbS1jb3VudGRvd24ge1xuICAgICAgICAgICAgLS1jb3VudGRvd24tdGltZS1tYXJnaW46IDBweDtcbiAgICAgICAgICAgIC0tY291bnRkb3duLXRpbWUtcGFkZGluZzogMHB4O1xuICAgICAgICAgICAgLS1jb3VudGRvd24taW5uZXItdGltZS1tYXJnaW46IHZhcigtLXBhZ2UtY291bnRkb3duLWd1dHRlcikgdmFyKC0tcGFnZS1jb3VudGRvd24tZ3V0dGVyKSAwcHg7XG4gICAgICAgICAgICAtLWNvdW50ZG93bi1pbm5lci10aW1lLXBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMikgY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyA0KTtcbiAgICAgICAgICAgIC0tY291bnRkb3duLWZpbGwtYm9yZGVyOiBub25lO1xuICAgICAgICAgICAgLS1jb3VudGRvd24tZmlsbC1ib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtbmFycm93LXJhZGl1cyk7XG4gICAgICAgICAgICAtLWNvdW50ZG93bi1maWxsLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgICAgICAgICAtLWNvdW50ZG93bi1maWxsLXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHZhcigtLXBhZ2UtY291bnRkb3duLWZpbGwtc2hhZG93LWNvbG9yKTtcbiAgICAgICAgICAgIC0tY291bnRkb3duLXZhbHVlLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFya2VzdCk7XG4gICAgICAgICAgICAtLWNvdW50ZG93bi11bml0LWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcbiAgICAgICAgICAgIC0tY291bnRkb3duLXRpbWUtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcblxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogY2FsYyh2YXIoLS1wYWdlLWNvdW50ZG93bi1ndXR0ZXIpICogLTEpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIiwiQG1peGluIGlvbi1jb2xvcigkY29sb3IpIHtcbiAgQGNvbnRlbnQ7XG5cbiAgLmlvbi1jb2xvci0jeyRjb2xvcn0ge1xuICAgIC0taW9uLWNvbG9yLWJhc2U6IHZhcigtLWlvbi1jb2xvci0jeyRjb2xvcn0pICFpbXBvcnRhbnQ7XG4gICAgLS1pb24tY29sb3ItYmFzZS1yZ2I6IHZhcigtLWlvbi1jb2xvci0jeyRjb2xvcn0tcmdiKSAhaW1wb3J0YW50O1xuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiB2YXIoLS1pb24tY29sb3ItI3skY29sb3J9LWNvbnRyYXN0KSAhaW1wb3J0YW50O1xuICAgIC0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogdmFyKC0taW9uLWNvbG9yLSN7JGNvbG9yfS1jb250cmFzdC1yZ2IpICFpbXBvcnRhbnQ7XG4gICAgLS1pb24tY29sb3Itc2hhZGU6IHZhcigtLWlvbi1jb2xvci0jeyRjb2xvcn0tc2hhZGUpICFpbXBvcnRhbnQ7XG4gICAgLS1pb24tY29sb3ItdGludDogdmFyKC0taW9uLWNvbG9yLSN7JGNvbG9yfS10aW50KSAhaW1wb3J0YW50O1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/deals/listing/styles/deals-listing.shell.scss":
/*!***************************************************************!*\
  !*** ./src/app/deals/listing/styles/deals-listing.shell.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --shell-color: #70DF70;\n  --shell-color-rgb: 112,223,112; }\n\napp-image-shell.deal-logo {\n  --image-shell-loading-background: rgba(var(--shell-color-rgb), .10);\n  --image-shell-spinner-color: rgba(var(--shell-color-rgb), .30);\n  --image-shell-spinner-size: 18px; }\n\n.item-name app-text-shell {\n  --text-shell-line-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px; }\n\n.item-description > app-text-shell {\n  --text-shell-line-color: rgba(var(--shell-color-rgb), .20);\n  --text-shell-line-height: 14px; }\n\n.item-code > app-text-shell {\n  --text-shell-line-color: rgba(var(--shell-color-rgb), .15);\n  --text-shell-line-height: 14px;\n  min-width: 60px; }\n\n.item-code > app-text-shell.text-loaded {\n    min-width: 0px; }\n\n.item-time-left > app-text-shell {\n  --text-shell-line-color: rgba(var(--shell-color-rgb), .10);\n  --text-shell-line-height: 14px;\n  min-width: 60px; }\n\n.item-time-left > app-text-shell.text-loaded {\n    min-width: 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9kZWFscy9saXN0aW5nL3N0eWxlcy9kZWFscy1saXN0aW5nLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxzQkFBYztFQUNkLDhCQUFrQixFQUFBOztBQVVwQjtFQUNFLG1FQUFpQztFQUNqQyw4REFBNEI7RUFDNUIsZ0NBQTJCLEVBQUE7O0FBRzdCO0VBQ0UsMERBQXdCO0VBQ3hCLDhCQUF5QixFQUFBOztBQUczQjtFQUNFLDBEQUF3QjtFQUN4Qiw4QkFBeUIsRUFBQTs7QUFHM0I7RUFDRSwwREFBd0I7RUFDeEIsOEJBQXlCO0VBQ3pCLGVBQWUsRUFBQTs7QUFIakI7SUFNSSxjQUFjLEVBQUE7O0FBSWxCO0VBQ0UsMERBQXdCO0VBQ3hCLDhCQUF5QjtFQUN6QixlQUFlLEVBQUE7O0FBSGpCO0lBTUksY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZGVhbHMvbGlzdGluZy9zdHlsZXMvZGVhbHMtbGlzdGluZy5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tc2hlbGwtY29sb3I6ICM3MERGNzA7XG4gIC0tc2hlbGwtY29sb3ItcmdiOiAxMTIsMjIzLDExMjtcbn1cblxuLy8gWW91IGNhbiBhbHNvIGFwcGx5IHNoZWVsIHN0eWxlcyB0byB0aGUgZW50aXJlIHBhZ2Vcbjpob3N0KC5pcy1zaGVsbCkge1xuICAvLyBpb24tY29udGVudCB7XG4gIC8vICAgb3BhY2l0eTogMC44O1xuICAvLyB9XG59XG5cbmFwcC1pbWFnZS1zaGVsbC5kZWFsLWxvZ28ge1xuICAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjMwKTtcbiAgLS1pbWFnZS1zaGVsbC1zcGlubmVyLXNpemU6IDE4cHg7XG59XG5cbi5pdGVtLW5hbWUgIGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjMwKTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xufVxuXG4uaXRlbS1kZXNjcmlwdGlvbiA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uaXRlbS1jb2RlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtbGluZS1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTUpO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG4gIG1pbi13aWR0aDogNjBweDtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtaW4td2lkdGg6IDBweDtcbiAgfVxufVxuXG4uaXRlbS10aW1lLWxlZnQgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4xMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbiAgbWluLXdpZHRoOiA2MHB4O1xuXG4gICYudGV4dC1sb2FkZWQge1xuICAgIG1pbi13aWR0aDogMHB4O1xuICB9XG59XG4iXX0= */"

/***/ })

}]);
//# sourceMappingURL=deals-listing-deals-listing-module.js.map