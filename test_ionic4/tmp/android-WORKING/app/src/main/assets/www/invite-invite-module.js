(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["invite-invite-module"],{

/***/ "./src/app/invite/invite.module.ts":
/*!*****************************************!*\
  !*** ./src/app/invite/invite.module.ts ***!
  \*****************************************/
/*! exports provided: InvitePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvitePageModule", function() { return InvitePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _invite_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./invite.page */ "./src/app/invite/invite.page.ts");







var routes = [
    {
        path: '',
        component: _invite_page__WEBPACK_IMPORTED_MODULE_6__["InvitePage"]
    }
];
var InvitePageModule = /** @class */ (function () {
    function InvitePageModule() {
    }
    InvitePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_invite_page__WEBPACK_IMPORTED_MODULE_6__["InvitePage"]]
        })
    ], InvitePageModule);
    return InvitePageModule;
}());



/***/ }),

/***/ "./src/app/invite/invite.page.html":
/*!*****************************************!*\
  !*** ./src/app/invite/invite.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-title>Invite Contacts</ion-title>\n    <ion-buttons tappable slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list detail=\"false\">\n    <ion-list-header>\n      Invite Socially Or Through Email\n    </ion-list-header>\n\n    <!-- Item as a Button -->\n    <ion-item (click)=\"openContacts()\" detail>\n      <ion-label>\n        <ion-icon name=\"contacts\"></ion-icon> Contacts\n      </ion-label>\n    </ion-item>\n    <ion-item (click)=\"openContacts()\" detail>\n      <ion-label>\n        <ion-icon name=\"logo-facebook\"></ion-icon> Facebook\n      </ion-label>\n    </ion-item>\n    <ion-item (click)=\"openContacts()\" detail>\n      <ion-label>\n        <ion-icon name=\"logo-twitter\"></ion-icon> Twitter\n      </ion-label>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-row>\n        <ion-col size=\"3\">\n\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-icon name=\"logo-twitter\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-icon name=\"logo-google\"></ion-icon>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-icon name=\"mail\"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/invite/invite.page.scss":
/*!*****************************************!*\
  !*** ./src/app/invite/invite.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludml0ZS9pbnZpdGUucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/invite/invite.page.ts":
/*!***************************************!*\
  !*** ./src/app/invite/invite.page.ts ***!
  \***************************************/
/*! exports provided: InvitePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvitePage", function() { return InvitePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_contact_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/contact.service */ "./src/services/contact.service.ts");




var InvitePage = /** @class */ (function () {
    function InvitePage(router, contactProvider) {
        this.router = router;
        this.contactProvider = contactProvider;
        this.redirectTo = '/contactimportprogress';
    }
    InvitePage.prototype.ngOnInit = function () {
        var _this = this;
        this.contactProvider.ContactsCount()
            .then(function (res_count) {
            console.log("1> res_count : ");
            console.log(res_count);
            if (res_count > 0) {
                _this.redirectTo = '/contact';
            }
        }).catch(function (e) {
        });
    }; //end function
    InvitePage.prototype.openContacts = function () {
        var navigationExtras = {
            queryParams: {
                source: "invitepage"
            }
        };
        //console.log ("2> res_count : "+res_count);
        this.router.navigate([this.redirectTo], navigationExtras);
    }; //end function
    InvitePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-invite',
            template: __webpack_require__(/*! ./invite.page.html */ "./src/app/invite/invite.page.html"),
            styles: [__webpack_require__(/*! ./invite.page.scss */ "./src/app/invite/invite.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _services_contact_service__WEBPACK_IMPORTED_MODULE_3__["ContactService"]])
    ], InvitePage);
    return InvitePage;
}());



/***/ })

}]);
//# sourceMappingURL=invite-invite-module.js.map