(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["loyalty-loyalty-module"],{

/***/ "./src/app/loyalty/loyalty.module.ts":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.module.ts ***!
  \*******************************************/
/*! exports provided: LoyaltyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltyPageModule", function() { return LoyaltyPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _loyalty_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./loyalty.page */ "./src/app/loyalty/loyalty.page.ts");







var routes = [
    {
        path: '',
        component: _loyalty_page__WEBPACK_IMPORTED_MODULE_6__["LoyaltyPage"]
    }
];
var LoyaltyPageModule = /** @class */ (function () {
    function LoyaltyPageModule() {
    }
    LoyaltyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_loyalty_page__WEBPACK_IMPORTED_MODULE_6__["LoyaltyPage"]]
        })
    ], LoyaltyPageModule);
    return LoyaltyPageModule;
}());



/***/ }),

/***/ "./src/app/loyalty/loyalty.page.html":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Earn Loyality\n    </ion-title>\n    <ion-icon tappable slot=\"end\" class=\"iconright\"   (click)=\"askForReferral()\"  name=\"add-circle\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <!-- fab placed to the bottom start -->\n  <ion-fab   vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"tertiary\">\n      <ion-icon name=\"business\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n\n  <ion-row class=\"categories-list\">\n    <ion-col class=\"category-item travel-category firstitem \" size=\"12\">\n      <div class=\"category-anchor centertext\">\n            <h2 class=\"category-title\">\n              <ion-icon name=\"barcode\" class=\"iconmain\"></ion-icon>\n              <br> Tap & Scan Loyality\n              <br><small>Scan To Earn Loyality <br>From Businesses </small>\n            </h2>\n      </div>\n    </ion-col>\n\n    <ion-col class=\"sometopmargin\" size=\"12\">\n\n      <ion-list detail=\"true\">\n        <ion-list-header>\n          Loyality History\n        </ion-list-header>\n\n        <!-- Searchbar with a placeholder -->\n        <ion-searchbar placeholder=\"Search Loyality History\"></ion-searchbar>\n        <ion-item (click)=\"onDetail()\" detail=\"true\"><ion-label>Quantum Insurance</ion-label> <ion-note slot=\"end\"> 11 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>ABC Realtor</ion-label> <ion-note slot=\"end\"> 1 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Kitchen Cabinet</ion-label> <ion-note slot=\"end\"> 5 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Accounting Firm</ion-label> <ion-note slot=\"end\"> 18 Scans </ion-note></ion-item>\n        <ion-item (click)=\"onDetail()\" detail=\"true\"><ion-label>Quantum Insurance</ion-label> <ion-note slot=\"end\"> 11 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>ABC Realtor</ion-label> <ion-note slot=\"end\"> 1 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Kitchen Cabinet</ion-label> <ion-note slot=\"end\"> 5 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Accounting Firm</ion-label> <ion-note slot=\"end\"> 18 Scans </ion-note></ion-item>\n      </ion-list>\n\n\n    </ion-col>\n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/loyalty/loyalty.page.scss":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-narrow-margin);\n  --page-categories-gutter: calc(var(--page-margin) / 4);\n  --page-category-background: var(--ion-color-medium);\n  --page-category-background-rgb: var(--ion-color-medium-rgb); }\n\n.sometopmargin {\n  margin-top: 15px; }\n\n.businessitem {\n  margin-top: 30px !important; }\n\n.firstitem {\n  margin-top: 18px !important;\n  margin-bottom: 0px !important; }\n\nion-card-content {\n  margin-top: 0px !important; }\n\n.category-cover1,\n.category-cover2,\n.category-cover3 {\n  text-align: center; }\n\n.iconmain {\n  font-size: 50px;\n  margin: 0 auto; }\n\n.categories-list {\n  --ion-grid-column-padding: var(--page-categories-gutter);\n  padding: calc(var(--page-categories-gutter) * 3);\n  height: 100%;\n  align-content: flex-start;\n  overflow: scroll; }\n\n.categories-list .category-item .category-anchor {\n    height: 100%;\n    text-decoration: none;\n    display: flex;\n    justify-content: flex-start;\n    align-items: flex-start; }\n\n.categories-list .category-item .category-anchor .category-title {\n      margin: auto;\n      text-transform: uppercase;\n      font-weight: 400;\n      font-size: 18px;\n      letter-spacing: 1px;\n      padding: calc((var(--page-margin) / 4) * 3) var(--page-margin);\n      color: var(--ion-color-lightest);\n      background-color: var(--page-category-background);\n      border-radius: var(--app-fair-radius); }\n\n.categories-list .travel-category {\n    --page-category-background: #00AFFF;\n    --page-category-background-rgb: 0,175,255; }\n\n.categories-list .fashion-category {\n    --page-category-background: #cb328f;\n    --page-category-background-rgb: 203,50,143; }\n\n.categories-list .food-category {\n    --page-category-background: #ebbb00;\n    --page-category-background-rgb: 235,187,0; }\n\n.categories-list .deals-category {\n    --page-category-background: #70df70;\n    --page-category-background-rgb: 112,223,112; }\n\n.categories-list .real-state-category {\n    --page-category-background: #d9453a;\n    --page-category-background-rgb: 217,69,58; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9sb3lhbHR5L2xveWFsdHkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsdUNBQWM7RUFFZCxzREFBeUI7RUFFekIsbURBQTJCO0VBQzNCLDJEQUErQixFQUFBOztBQUVqQztFQUNFLGdCQUFnQixFQUFBOztBQUVsQjtFQUNFLDJCQUEyQixFQUFBOztBQUc3QjtFQUNFLDJCQUEyQjtFQUMzQiw2QkFBNkIsRUFBQTs7QUFFL0I7RUFDRSwwQkFBMEIsRUFBQTs7QUFFNUI7OztFQUdFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBS2hCO0VBQ0Usd0RBQTBCO0VBRTFCLGdEQUFnRDtFQUNoRCxZQUFZO0VBQ1oseUJBQXlCO0VBQ3pCLGdCQUFnQixFQUFBOztBQU5sQjtJQVVNLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQix1QkFBdUIsRUFBQTs7QUFkN0I7TUFpQlEsWUFBWTtNQUNaLHlCQUF5QjtNQUN6QixnQkFBZ0I7TUFDaEIsZUFBZTtNQUNmLG1CQUFtQjtNQUNuQiw4REFBOEQ7TUFDOUQsZ0NBQWdDO01BQ2hDLGlEQUFpRDtNQUNqRCxxQ0FBcUMsRUFBQTs7QUF6QjdDO0lBK0JJLG1DQUEyQjtJQUMzQix5Q0FBK0IsRUFBQTs7QUFoQ25DO0lBb0NJLG1DQUEyQjtJQUMzQiwwQ0FBK0IsRUFBQTs7QUFyQ25DO0lBeUNJLG1DQUEyQjtJQUMzQix5Q0FBK0IsRUFBQTs7QUExQ25DO0lBOENJLG1DQUEyQjtJQUMzQiwyQ0FBK0IsRUFBQTs7QUEvQ25DO0lBbURJLG1DQUEyQjtJQUMzQix5Q0FBK0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xveWFsdHkvbG95YWx0eS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIgLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1uYXJyb3ctbWFyZ2luKTtcblxuICAtLXBhZ2UtY2F0ZWdvcmllcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XG5cbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tcmdiKTtcbn1cbi5zb21ldG9wbWFyZ2luIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbi5idXNpbmVzc2l0ZW0ge1xuICBtYXJnaW4tdG9wOiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5maXJzdGl0ZW0ge1xuICBtYXJnaW4tdG9wOiAxOHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNhcmQtY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDBweCAhaW1wb3J0YW50O1xufVxuLmNhdGVnb3J5LWNvdmVyMSxcbi5jYXRlZ29yeS1jb3ZlcjIsXG4uY2F0ZWdvcnktY292ZXIzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uaWNvbm1haW4ge1xuICBmb250LXNpemU6IDUwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG5cbi8vIE5vdGU6ICBBbGwgdGhlIENTUyB2YXJpYWJsZXMgZGVmaW5lZCBiZWxvdyBhcmUgb3ZlcnJpZGVzIG9mIElvbmljIGVsZW1lbnRzIENTUyBDdXN0b20gUHJvcGVydGllc1xuLmNhdGVnb3JpZXMtbGlzdCB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IHZhcigtLXBhZ2UtY2F0ZWdvcmllcy1ndXR0ZXIpO1xuXG4gIHBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcikgKiAzKTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBhbGlnbi1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuXG4gIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcblxuICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gICAgICAgIHBhZGRpbmc6IGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDQpICogMykgdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kKTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogdmFyKC0tYXBwLWZhaXItcmFkaXVzKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAudHJhdmVsLWNhdGVnb3J5IHtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogIzAwQUZGRjtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDAsMTc1LDI1NTtcbiAgfVxuXG4gIC5mYXNoaW9uLWNhdGVnb3J5IHtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogI2NiMzI4ZjtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIwMyw1MCwxNDM7XG4gIH1cblxuICAuZm9vZC1jYXRlZ29yeSB7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNlYmJiMDA7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAyMzUsMTg3LDA7XG4gIH1cblxuICAuZGVhbHMtY2F0ZWdvcnkge1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjNzBkZjcwO1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMTEyLDIyMywxMTI7XG4gIH1cblxuICAucmVhbC1zdGF0ZS1jYXRlZ29yeSB7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNkOTQ1M2E7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAyMTcsNjksNTg7XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/loyalty/loyalty.page.ts":
/*!*****************************************!*\
  !*** ./src/app/loyalty/loyalty.page.ts ***!
  \*****************************************/
/*! exports provided: LoyaltyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltyPage", function() { return LoyaltyPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var LoyaltyPage = /** @class */ (function () {
    function LoyaltyPage(router) {
        this.router = router;
    }
    LoyaltyPage.prototype.ngOnInit = function () {
    };
    LoyaltyPage.prototype.askForReferral = function () {
        console.log("Clicked " + new Date());
        this.router.navigateByUrl('/referral');
    };
    LoyaltyPage.prototype.onDetail = function () {
        this.router.navigateByUrl('/loyaltydetail');
    };
    LoyaltyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loyalty',
            template: __webpack_require__(/*! ./loyalty.page.html */ "./src/app/loyalty/loyalty.page.html"),
            styles: [__webpack_require__(/*! ./loyalty.page.scss */ "./src/app/loyalty/loyalty.page.scss"), __webpack_require__(/*! ./loyalty.responsive.scss */ "./src/app/loyalty/loyalty.responsive.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoyaltyPage);
    return LoyaltyPage;
}());



/***/ }),

/***/ "./src/app/loyalty/loyalty.responsive.scss":
/*!*************************************************!*\
  !*** ./src/app/loyalty/loyalty.responsive.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* ----------- iPhone 4 and 4S ----------- */\n@media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 2 / 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 16px; } }\n/* ----------- iPhone 5, 5S, 5C and 5SE ----------- */\n/* ----------- iPhone 6, 6S, 7 and 8 ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n/* ----------- iPhone X ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n/* ----------- iPhone 6+, 7+ and 8+ ----------- */\n@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9sb3lhbHR5L2xveWFsdHkucmVzcG9uc2l2ZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBLDRDQUFBO0FBQ0E7RUFVRTtJQUlRLGVBQWUsRUFBQSxFQUNoQjtBQU1ULHFEQUFBO0FBY0Esa0RBQUE7QUFDQTtFQVNFO0lBSVEsZUFBZTtJQUNmLDhEQUE4RCxFQUFBLEVBQy9EO0FBTVQscUNBQUE7QUFDQTtFQVNFO0lBSVEsZUFBZTtJQUNmLDhEQUE4RCxFQUFBLEVBQy9EO0FBTVQsaURBQUE7QUFDQTtFQVNFO0lBSVEsZUFBZTtJQUNmLDhEQUE4RCxFQUFBLEVBQy9EIiwiZmlsZSI6InNyYy9hcHAvbG95YWx0eS9sb3lhbHR5LnJlc3BvbnNpdmUuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIChOb3RlOiBEb24ndCBjaGFuZ2UgdGhlIG9yZGVyIG9mIHRoZSBkZXZpY2VzIGFzIGl0IG1heSBicmVhayB0aGUgY29ycmVjdCBjc3MgcHJlY2VkZW5jZSlcblxuLy8gKHNlZTogaHR0cHM6Ly9jc3MtdHJpY2tzLmNvbS9zbmlwcGV0cy9jc3MvbWVkaWEtcXVlcmllcy1mb3Itc3RhbmRhcmQtZGV2aWNlcy8jaXBob25lLXF1ZXJpZXMpXG4vLyAoc2VlOiBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvNDc3NTAyNjEvMTExNjk1OSlcblxuLyogLS0tLS0tLS0tLS0gaVBob25lIDQgYW5kIDRTIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogMzIwcHgpXG4gIGFuZCAobWF4LWRldmljZS13aWR0aCA6IDQ4MHB4KVxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMilcbiAgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiAyLzMpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBwb3J0cmFpdDpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSlcbntcbiAgLmNhdGVnb3JpZXMtbGlzdCB7XG4gICAgLmNhdGVnb3J5LWl0ZW0ge1xuICAgICAgLmNhdGVnb3J5LWFuY2hvciB7XG4gICAgICAgIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA1LCA1UywgNUMgYW5kIDVTRSAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA1NjhweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXG4gIGFuZCAoZGV2aWNlLWFzcGVjdC1yYXRpbzogNDAgLyA3MSlcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuXG59XG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2LCA2UywgNyBhbmQgOCAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDM3NXB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA2NjdweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBwb3J0cmFpdDpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSlcbntcbiAgLmNhdGVnb3JpZXMtbGlzdCB7XG4gICAgLmNhdGVnb3J5LWl0ZW0ge1xuICAgICAgLmNhdGVnb3J5LWFuY2hvciB7XG4gICAgICAgIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKSBjYWxjKCh2YXIoLS1wYWdlLW1hcmdpbikgKiAzKSAvIDIpIDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgWCAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDM3NXB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA4MTJweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW8gOiAzKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKSA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLyogLS0tLS0tLS0tLS0gaVBob25lIDYrLCA3KyBhbmQgOCsgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiA0MTRweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNzM2cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAzKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKSA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ== */"

/***/ })

}]);
//# sourceMappingURL=loyalty-loyalty-module.js.map