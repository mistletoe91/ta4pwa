(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sendareferral-info-sendareferral-info-module"],{

/***/ "./src/app/sendareferral-info/sendareferral-info.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/sendareferral-info/sendareferral-info.module.ts ***!
  \*****************************************************************/
/*! exports provided: SendareferralInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendareferralInfoPageModule", function() { return SendareferralInfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sendareferral_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sendareferral-info.page */ "./src/app/sendareferral-info/sendareferral-info.page.ts");







var routes = [
    {
        path: '',
        component: _sendareferral_info_page__WEBPACK_IMPORTED_MODULE_6__["SendareferralInfoPage"]
    }
];
var SendareferralInfoPageModule = /** @class */ (function () {
    function SendareferralInfoPageModule() {
    }
    SendareferralInfoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_sendareferral_info_page__WEBPACK_IMPORTED_MODULE_6__["SendareferralInfoPage"]]
        })
    ], SendareferralInfoPageModule);
    return SendareferralInfoPageModule;
}());



/***/ }),

/***/ "./src/app/sendareferral-info/sendareferral-info.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/sendareferral-info/sendareferral-info.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Loyality Detail</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-row class=\"categories-list\">\n    <ion-col class=\"category-item fashion-category firstitem \" size=\"12\">\n      <div class=\" centertext\">\n            <ion-card (click)=\"fnmaincardwork()\" class=\"businessitem\">\n              <ion-card-header>\n                <ion-card-subtitle>Megna Insurance</ion-card-subtitle>\n                <ion-card-title>Mauli Peecock</ion-card-title>\n              </ion-card-header>\n              <ion-card-content class=\"centertext\">\n                <h1>Your 3 out of 5 Referrals Are Marked Redeemed</h1>\n                <ion-progress-bar color=\"danger\" value=\"0.4\"></ion-progress-bar>\n                <!-- Notes in a List -->\n                <div class=\"textinfo\">\n                  <ion-text color=\"dark\" class=\"\">\n                    Mauli Peecock will redeem you following incentive for each sale closed through your referral <h3><ion-badge color=\"primary\">$50 Gift Card</ion-badge></h3>\n                  </ion-text>\n                </div>\n\n              </ion-card-content>\n            </ion-card>\n      </div>\n\n    </ion-col>\n</ion-row>\n\n\n<ion-row class=\"categories-list\">\n  <ion-col class=\"centertext\" size=\"12\">\n      <ion-button color=\"secondary\">Send Redeem Request</ion-button>\n  </ion-col>\n </ion-row>\n\n<ion-row class=\"categories-list\">\n  <ion-col class=\"\" size=\"12\">\n\n    <ion-list>\n       <ion-list-header>\n         <ion-label>Referrals You Sent (5)</ion-label>\n       </ion-list-header>\n       <ion-item detail = false><ion-label>Raveender Paul</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>John Mary</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n       <ion-item detail = false><ion-label>Vinessa Bally</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n\n       <ion-item lines=\"none\" detail=\"false\"><ion-label color=\"primary\" text-wrap>Mauli Peecock Sent You Incentives ($25 Gift Card)</ion-label><ion-note slot=\"end\">29 June 2019</ion-note>\n      </ion-item>\n        <ion-item detail=\"false\" >\n          <ion-label color=\"primary\" text-wrap>Note From Mauli Peecock : A Gift card of $25 is send at your address. Please call us if you do not recieve withing 5-7 business days</ion-label>\n       </ion-item>\n\n       <ion-item detail = false><ion-label>Parang Hio</ion-label><ion-note slot=\"end\">29 June 2019</ion-note> </ion-item>\n     </ion-list>\n\n  </ion-col>\n</ion-row>\n\n\n<!-- fab placed to the bottom end -->\n<ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n  <ion-fab-button color=\"primary\">\n    <ion-icon   name=\"paper-plane\"></ion-icon>\n  </ion-fab-button>\n</ion-fab>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/sendareferral-info/sendareferral-info.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/sendareferral-info/sendareferral-info.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".incentivetext {\n  margin: 5px 0px 40px 0px;\n  padding: 15px;\n  background-color: #efefef;\n  text-align: center; }\n\n.businessitem {\n  margin-top: 50px; }\n\n.textinfo {\n  margin-top: 15px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9zZW5kYXJlZmVycmFsLWluZm8vc2VuZGFyZWZlcnJhbC1pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLHdCQUF3QjtFQUN4QixhQUFhO0VBQ2IseUJBQXlCO0VBQ3pCLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2VuZGFyZWZlcnJhbC1pbmZvL3NlbmRhcmVmZXJyYWwtaW5mby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5pbmNlbnRpdmV0ZXh0IHtcbiAgbWFyZ2luOiA1cHggMHB4IDQwcHggMHB4O1xuICBwYWRkaW5nOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWZlZmVmO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYnVzaW5lc3NpdGVtIHtcbiAgbWFyZ2luLXRvcDogNTBweDtcblxufVxuLnRleHRpbmZvIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/sendareferral-info/sendareferral-info.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/sendareferral-info/sendareferral-info.page.ts ***!
  \***************************************************************/
/*! exports provided: SendareferralInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendareferralInfoPage", function() { return SendareferralInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SendareferralInfoPage = /** @class */ (function () {
    function SendareferralInfoPage() {
    }
    SendareferralInfoPage.prototype.ngOnInit = function () {
    };
    SendareferralInfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sendareferral-info',
            template: __webpack_require__(/*! ./sendareferral-info.page.html */ "./src/app/sendareferral-info/sendareferral-info.page.html"),
            styles: [__webpack_require__(/*! ./sendareferral-info.page.scss */ "./src/app/sendareferral-info/sendareferral-info.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SendareferralInfoPage);
    return SendareferralInfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=sendareferral-info-sendareferral-info-module.js.map