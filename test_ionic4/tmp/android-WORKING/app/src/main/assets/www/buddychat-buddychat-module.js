(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["buddychat-buddychat-module"],{

/***/ "./src/app/buddychat/buddychat.module.ts":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.module.ts ***!
  \***********************************************/
/*! exports provided: BuddychatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPageModule", function() { return BuddychatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _buddychat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./buddychat.page */ "./src/app/buddychat/buddychat.page.ts");







var routes = [
    {
        path: '',
        component: _buddychat_page__WEBPACK_IMPORTED_MODULE_6__["BuddychatPage"]
    }
];
var BuddychatPageModule = /** @class */ (function () {
    function BuddychatPageModule() {
    }
    BuddychatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_buddychat_page__WEBPACK_IMPORTED_MODULE_6__["BuddychatPage"]]
        })
    ], BuddychatPageModule);
    return BuddychatPageModule;
}());



/***/ }),

/***/ "./src/app/buddychat/buddychat.page.html":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Anguls Dev</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content #content padding class=\"buddychat-content\">\n\n    <div class=\"buddychat\">\n      <p>his talk-bubble uses .left-in class to show a triangle on the left slightly indented. Still a blocky square.</p>\n    </div>\n\n    <div class=\"mychat\">\n       <p> Hello this is sample chat and lore ipusm and good thoughts of nice air </p>\n    </div>\n\n    <div class=\"mychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Referral Sent</span></h4>\n      <p>Hi, I am sending a referral to you Hi, I am sending a referral to you Hi, I am sending a referral to you</p>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Referral Sent</span></h4>\n      <p>Hi, I am sending a referral to you (Gurbakhsak Kalhi) (647) 123-1234</p>\n\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"primary\" >Save Contact</ion-button>\n      </div>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"medium\" >Open Contact</ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Loyality Redeemed</span></h4>\n      <p>Hi, I have redeemed the loyality points and got Free Ice Cream</p>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Request To Release Incentive</span></h4>\n      <p>Please release my incentives for the referrals I sent you so far</p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"primary\" >Check Request</ion-button>\n      </div>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Asking For Deal</span></h4>\n      <p>Hi, I am intersting in your deal. Please let me know how to redeem it </p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"success\" >Check Deal</ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Asking For Referral</span></h4>\n      <p>Hi, I am looking for a (Accountant). Please send me a trusted referral </p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"dark\" >Send A Referral</ion-button>\n      </div>\n    </div>\n\n</ion-content>\n\n<ion-footer ion-fixed>\n        <ion-toolbar class=\"no-border\" color=\"white\">\n          <!-- Textarea in an item with a placeholder -->\n          <ion-item lines=\"none\">\n            <ion-textarea placeholder=\"Type to send message\"></ion-textarea>\n            <ion-buttons end>\n                 <ion-button color=\"primary\"><ion-icon name=\"send\"></ion-icon></ion-button>\n            </ion-buttons>\n          </ion-item>\n\n        </ion-toolbar>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.scss":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background-shade);\n  --page-tags-gutter: 5px;\n  --page-color-radio-items-per-row: 5;\n  --page-color-radio-gutter: 2%;\n  --page-color-radio-width: calc((100% - (2 * var(--page-color-radio-gutter) * var(--page-color-radio-items-per-row))) / var(--page-color-radio-items-per-row)); }\n\n.buddychat-content {\n  --background: var(--page-background); }\n\n.buddychat-content ion-item-divider {\n    --background: var(--page-background);\n    --padding-bottom: calc(var(--page-margin) / 2);\n    --padding-top: calc(var(--page-margin) / 2);\n    --padding-start: var(--page-margin);\n    --padding-end: var(--page-margin);\n    border: none; }\n\n.headinginchat {\n  border-bottom: 1px solid #ccc; }\n\n.buddychat, .mychat {\n  padding: 7px 15px;\n  margin-bottom: 15px;\n  background-color: #fff;\n  width: 80%;\n  clear: both; }\n\n.buddychat {\n  border-radius: 0px 20px 20px 20px;\n  border: 1px solid #ccc;\n  color: #000;\n  background-color: #eee;\n  float: left; }\n\n.mychat {\n  border-radius: 20px 20px 0px 20px;\n  border: 1px solid #fff;\n  float: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvdGVzdF9pb25pYzQvc3JjL2FwcC9idWRkeWNoYXQvYnVkZHljaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHFDQUFjO0VBQ2QsOENBQWtCO0VBRWxCLHVCQUFtQjtFQUVuQixtQ0FBaUM7RUFDakMsNkJBQTBCO0VBQzFCLDZKQUF5QixFQUFBOztBQUczQjtFQUNFLG9DQUFhLEVBQUE7O0FBRGY7SUFJRSxvQ0FBYTtJQUNiLDhDQUFpQjtJQUNqQiwyQ0FBYztJQUNkLG1DQUFnQjtJQUNoQixpQ0FBYztJQUVkLFlBQVksRUFBQTs7QUFHZDtFQUNFLDZCQUE2QixFQUFBOztBQUUvQjtFQUNFLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsc0JBQXNCO0VBQ3RCLFVBQVU7RUFDVixXQUFVLEVBQUE7O0FBRVo7RUFDRSxpQ0FBaUM7RUFDakMsc0JBQXNCO0VBQ3RCLFdBQVU7RUFDVixzQkFBc0I7RUFDdEIsV0FBVSxFQUFBOztBQUVaO0VBQ0UsaUNBQWlDO0VBQ2pDLHNCQUFzQjtFQUN0QixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9idWRkeWNoYXQvYnVkZHljaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEN1c3RvbSB2YXJpYWJsZXNcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xuOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQtc2hhZGUpO1xuXG4gIC0tcGFnZS10YWdzLWd1dHRlcjogNXB4O1xuXG4gIC0tcGFnZS1jb2xvci1yYWRpby1pdGVtcy1wZXItcm93OiA1O1xuICAtLXBhZ2UtY29sb3ItcmFkaW8tZ3V0dGVyOiAyJTtcbiAgLS1wYWdlLWNvbG9yLXJhZGlvLXdpZHRoOiBjYWxjKCgxMDAlIC0gKDIgKiB2YXIoLS1wYWdlLWNvbG9yLXJhZGlvLWd1dHRlcikgKiB2YXIoLS1wYWdlLWNvbG9yLXJhZGlvLWl0ZW1zLXBlci1yb3cpKSkgLyB2YXIoLS1wYWdlLWNvbG9yLXJhZGlvLWl0ZW1zLXBlci1yb3cpKTtcbn1cblxuLmJ1ZGR5Y2hhdC1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xuXG5cdGlvbi1pdGVtLWRpdmlkZXIge1xuXHRcdC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcblx0XHQtLXBhZGRpbmctYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuXHRcdC0tcGFkZGluZy10b3A6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG5cdFx0LS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG5cdFx0LS1wYWRkaW5nLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuXG5cdFx0Ym9yZGVyOiBub25lO1xuXHR9XG59XG4uaGVhZGluZ2luY2hhdCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xufVxuLmJ1ZGR5Y2hhdCwubXljaGF0IHtcbiAgcGFkZGluZzogN3B4IDE1cHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIHdpZHRoOiA4MCU7XG4gIGNsZWFyOmJvdGg7XG59XG4uYnVkZHljaGF0IHtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDIwcHggMjBweCAyMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBjb2xvcjojMDAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xuICBmbG9hdDpsZWZ0O1xufVxuLm15Y2hhdCB7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHggMjBweCAwcHggMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.ts":
/*!*********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.ts ***!
  \*********************************************/
/*! exports provided: BuddychatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPage", function() { return BuddychatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BuddychatPage = /** @class */ (function () {
    function BuddychatPage() {
    }
    BuddychatPage.prototype.scrollToBottomOnInit = function () {
        this.content.scrollToBottom(300);
    };
    BuddychatPage.prototype.ngOnInit = function () {
        this.scrollToBottomOnInit();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('content'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], BuddychatPage.prototype, "content", void 0);
    BuddychatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-buddychat',
            template: __webpack_require__(/*! ./buddychat.page.html */ "./src/app/buddychat/buddychat.page.html"),
            styles: [__webpack_require__(/*! ./buddychat.page.scss */ "./src/app/buddychat/buddychat.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BuddychatPage);
    return BuddychatPage;
}());



/***/ })

}]);
//# sourceMappingURL=buddychat-buddychat-module.js.map