(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["referral-referral-module"],{

/***/ "./src/app/referral/referral.module.ts":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.module.ts ***!
  \*********************************************/
/*! exports provided: ReferralPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPageModule", function() { return ReferralPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _referral_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./referral.page */ "./src/app/referral/referral.page.ts");







var routes = [
    {
        path: '',
        component: _referral_page__WEBPACK_IMPORTED_MODULE_6__["ReferralPage"]
    }
];
var ReferralPageModule = /** @class */ (function () {
    function ReferralPageModule() {
    }
    ReferralPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_referral_page__WEBPACK_IMPORTED_MODULE_6__["ReferralPage"]]
        })
    ], ReferralPageModule);
    return ReferralPageModule;
}());



/***/ }),

/***/ "./src/app/referral/referral.page.html":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Get Referral</ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\" (click)=\"askForReferral()\"  name=\"arrow-dropleft-circle\"></ion-icon>\n  </ion-toolbar>\n  <ion-searchbar (ionInput)=\"getItems($event)\" placeholder=\"Filter Categories\"></ion-searchbar>\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Get Referral</ion-card-subtitle>\n      <ion-card-title>Ask For A Referral</ion-card-title>\n    </ion-card-header>\n\n    <ion-card-content>\n      Post a request to get referral in your network. Get trusted referral with ease\n    </ion-card-content>\n  </ion-card>\n\n\n  <!-- List of Text Items -->\n  <ion-list detail=\"true\">\n    <ion-list-header>\n      <ion-label text-wrap>Select Category of work you are looking for </ion-label>\n    </ion-list-header>\n    <!-- Searchbar with a placeholder -->\n\n    <ion-list no-lines>\n      <ion-item detail=\"true\" tappable  class=\"itemlistref mainitemlist\"  *ngFor=\"let item of items\" (click)=\"selCategory(item.id)\">\n           <ion-label  >{{item.name}}</ion-label>\n      </ion-item>\n    </ion-list>\n \n  </ion-list>\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/referral/referral.page.scss":
/*!*********************************************!*\
  !*** ./src/app/referral/referral.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JlZmVycmFsL3JlZmVycmFsLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/referral/referral.page.ts":
/*!*******************************************!*\
  !*** ./src/app/referral/referral.page.ts ***!
  \*******************************************/
/*! exports provided: ReferralPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferralPage", function() { return ReferralPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _referral_modal_referral_modal_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../referral-modal/referral-modal.page */ "./src/app/referral-modal/referral-modal.page.ts");
/* harmony import */ var _services_cats_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/cats.service */ "./src/services/cats.service.ts");







var ReferralPage = /** @class */ (function () {
    function ReferralPage(platform, catservice, modalController, router) {
        this.catservice = catservice;
        this.modalController = modalController;
        this.router = router;
        this.myrequests = [];
        this.requestcounter = null;
        this.showheader = true;
        this.isVirgin = false;
        console.log("ReferralPage : Referral Page Construction " + new Date());
        platform.ready().then(function () {
            console.log("ReferralPage : Platform Ready " + new Date());
            //this.initializeItems();
        });
    }
    ReferralPage.prototype.initializeItems = function () {
        this.items = this.catservice.getCats();
    };
    ReferralPage.prototype.ngOnInit = function () {
        console.log("ReferralPage : ngOnInit" + new Date());
    };
    ReferralPage.prototype.askForReferral = function () {
        this.router.navigateByUrl('/referral');
    };
    ReferralPage.prototype.selCategory = function (item_id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _referral_modal_referral_modal_page__WEBPACK_IMPORTED_MODULE_4__["ReferralModalPage"],
                            componentProps: {
                                'item_id': item_id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ReferralPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ReferralPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-referral',
            template: __webpack_require__(/*! ./referral.page.html */ "./src/app/referral/referral.page.html"),
            styles: [__webpack_require__(/*! ./referral.page.scss */ "./src/app/referral/referral.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _services_cats_service__WEBPACK_IMPORTED_MODULE_5__["CatsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ReferralPage);
    return ReferralPage;
}());



/***/ }),

/***/ "./src/services/cats.service.ts":
/*!**************************************!*\
  !*** ./src/services/cats.service.ts ***!
  \**************************************/
/*! exports provided: CatsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatsService", function() { return CatsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CatsService = /** @class */ (function () {
    function CatsService() {
    }
    //Master sheet at
    //https://docs.google.com/spreadsheets/d/1M4vrnEUQYYV-AVcqTySOnE2zrQYEzJBP-wOUw7BAybg/edit?usp=sharing
    CatsService.prototype.getCats = function () {
        return [
            { "id": 0, "name": "	Uncategorized	", "sort": 0 },
            { "id": 1, "name": "	Accountants	", "sort": 1 },
            { "id": 2, "name": "	Insurance Agents	", "sort": 2 },
            { "id": 3, "name": "	Real Estate Agents	", "sort": 3 },
            { "id": 4, "name": "	Mortgage/ Loan Agents	", "sort": 4 },
            { "id": 5, "name": "	Photographers/ Videographers/ DJs	", "sort": 5 },
            { "id": 6, "name": "	Lawyers	", "sort": 6 },
            { "id": 7, "name": "	Dentists	", "sort": 7 },
            { "id": 8, "name": "	Plumbers	", "sort": 8 },
            { "id": 9, "name": "	Construction/ Renovation	", "sort": 9 },
            { "id": 10, "name": "	Painters Paralegals/ Traffic Tickets	", "sort": 10 },
            { "id": 11, "name": "	Electricians	", "sort": 11 },
            { "id": 12, "name": "	Travel Tickets Agents	", "sort": 12 },
            { "id": 13, "name": "	Appliances Sales & Service	", "sort": 13 },
            { "id": 14, "name": "	Architect/ Engineer	", "sort": 14 },
            { "id": 15, "name": "	Auto Repair & Sales	", "sort": 15 },
            { "id": 16, "name": "	Ayurvedic, Herbal & Nutritional products	", "sort": 16 },
            { "id": 17, "name": "	Bankruptcy/ Debt Consolidation	", "sort": 17 },
            { "id": 18, "name": "	Banquet Halls & Convention Centre	", "sort": 18 },
            { "id": 19, "name": "	Beauty Parlor/ Salon/ Spa	", "sort": 19 },
            { "id": 20, "name": "	Car/ Truck Rental	", "sort": 20 },
            { "id": 21, "name": "	Carpet/ Flooring Stores	", "sort": 21 },
            { "id": 22, "name": "	Cheque Cashing/ Cash for Gold	", "sort": 22 },
            { "id": 23, "name": "	Chiropractor/ Physiotherapist	", "sort": 23 },
            { "id": 24, "name": "	Clothing Stores/ Boutiques	", "sort": 24 },
            { "id": 25, "name": "	Concrete/ Paving	", "sort": 25 },
            { "id": 26, "name": "	Drapery/ Blinds/ Shutters	", "sort": 26 },
            { "id": 27, "name": "	Duct/ Carpet Cleaning	", "sort": 27 },
            { "id": 28, "name": "	Foreign Exchange	", "sort": 28 },
            { "id": 29, "name": "	Furniture/ Mattress Stores	", "sort": 29 },
            { "id": 30, "name": "	Garage Doors	", "sort": 30 },
            { "id": 31, "name": "	Gift Stores	", "sort": 31 },
            { "id": 32, "name": "	Heating/ Air Conditioning	", "sort": 32 },
            { "id": 33, "name": "	Homeopathy	", "sort": 33 },
            { "id": 34, "name": "	Immigration Consultants	", "sort": 34 },
            { "id": 35, "name": "	Interpreters/ Translators	", "sort": 35 },
            { "id": 36, "name": "	Jewellery Shops	", "sort": 36 },
            { "id": 37, "name": "	Kitchen Cabinets/ Woodworking	", "sort": 37 },
            { "id": 38, "name": "	Landscaping/ Snow Removal	", "sort": 38 },
            { "id": 39, "name": "	Locksmith	", "sort": 39 },
            { "id": 40, "name": "	Limousine	", "sort": 40 },
            { "id": 41, "name": "	Media	", "sort": 41 },
            { "id": 42, "name": "	Moving/ Storage	", "sort": 42 },
            { "id": 43, "name": "	Optical Stores/ Opticians	", "sort": 43 },
            { "id": 44, "name": "	Pest Control	", "sort": 44 },
            { "id": 45, "name": "	Printers/ Signs	", "sort": 45 },
            { "id": 46, "name": "	Real Estate Brokers/ Agents	", "sort": 46 },
            { "id": 47, "name": "	Refrigeration Solutions	", "sort": 47 },
            { "id": 48, "name": "	Restaurants/ Sweet Shops	", "sort": 48 },
            { "id": 49, "name": "	Roofing	", "sort": 49 },
            { "id": 50, "name": "	Staffing/ Job/ Employment Agency	", "sort": 50 },
            { "id": 51, "name": "	Security Cameras/ Computers/ Networking	", "sort": 51 },
            { "id": 52, "name": "	Solar Panels	", "sort": 52 },
            { "id": 53, "name": "	Telephone /Cell/ Mobile Phones	", "sort": 53 },
            { "id": 54, "name": "	Tent Services	", "sort": 54 },
            { "id": 55, "name": "	Tile/ Granite/ Marble	", "sort": 55 },
            { "id": 56, "name": "	Towing	", "sort": 56 },
            { "id": 57, "name": "	Training Institutes/ Colleges	", "sort": 57 },
            { "id": 58, "name": "	Trucking Industry	", "sort": 58 },
            { "id": 59, "name": "	Tutors	", "sort": 59 },
            { "id": 60, "name": "	TV Repair/ Electronic Parts	", "sort": 60 },
            { "id": 61, "name": "	Wedding Planners	", "sort": 61 },
            { "id": 62, "name": "	Windows/ Doors	", "sort": 62 }
        ];
    };
    CatsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CatsService);
    return CatsService;
}());



/***/ })

}]);
//# sourceMappingURL=referral-referral-module.js.map