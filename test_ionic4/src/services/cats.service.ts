import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatsService {

  constructor() { }

  
    //Master sheet at
    //https://docs.google.com/spreadsheets/d/1M4vrnEUQYYV-AVcqTySOnE2zrQYEzJBP-wOUw7BAybg/edit?usp=sharing
    getCats (){
    return [
      {"id":	0	,"name":"	Uncategorized	","sort":	0	},
      {"id":	1	,"name":"	Accountants	","sort":	1	},
      {"id":	2	,"name":"	Insurance Agents	","sort":	2	},
      {"id":	3	,"name":"	Real Estate Agents	","sort":	3	},
      {"id":	4	,"name":"	Mortgage/ Loan Agents	","sort":	4	},
      {"id":	5	,"name":"	Photographers/ Videographers/ DJs	","sort":	5	},
      {"id":	6	,"name":"	Lawyers	","sort":	6	},
      {"id":	7	,"name":"	Dentists	","sort":	7	},
      {"id":	8	,"name":"	Plumbers	","sort":	8	},
      {"id":	9	,"name":"	Construction/ Renovation	","sort":	9	},
      {"id":	10	,"name":"	Painters Paralegals/ Traffic Tickets	","sort":	10	},
      {"id":	11	,"name":"	Electricians	","sort":	11	},
      {"id":	12	,"name":"	Travel Tickets Agents	","sort":	12	},
      {"id":	13	,"name":"	Appliances Sales & Service	","sort":	13	},
      {"id":	14	,"name":"	Architect/ Engineer	","sort":	14	},
      {"id":	15	,"name":"	Auto Repair & Sales	","sort":	15	},
      {"id":	16	,"name":"	Ayurvedic, Herbal & Nutritional products	","sort":	16	},
      {"id":	17	,"name":"	Bankruptcy/ Debt Consolidation	","sort":	17	},
      {"id":	18	,"name":"	Banquet Halls & Convention Centre	","sort":	18	},
      {"id":	19	,"name":"	Beauty Parlor/ Salon/ Spa	","sort":	19	},
      {"id":	20	,"name":"	Car/ Truck Rental	","sort":	20	},
      {"id":	21	,"name":"	Carpet/ Flooring Stores	","sort":	21	},
      {"id":	22	,"name":"	Cheque Cashing/ Cash for Gold	","sort":	22	},
      {"id":	23	,"name":"	Chiropractor/ Physiotherapist	","sort":	23	},
      {"id":	24	,"name":"	Clothing Stores/ Boutiques	","sort":	24	},
      {"id":	25	,"name":"	Concrete/ Paving	","sort":	25	},
      {"id":	26	,"name":"	Drapery/ Blinds/ Shutters	","sort":	26	},
      {"id":	27	,"name":"	Duct/ Carpet Cleaning	","sort":	27	},
      {"id":	28	,"name":"	Foreign Exchange	","sort":	28	},
      {"id":	29	,"name":"	Furniture/ Mattress Stores	","sort":	29	},
      {"id":	30	,"name":"	Garage Doors	","sort":	30	},
      {"id":	31	,"name":"	Gift Stores	","sort":	31	},
      {"id":	32	,"name":"	Heating/ Air Conditioning	","sort":	32	},
      {"id":	33	,"name":"	Homeopathy	","sort":	33	},
      {"id":	34	,"name":"	Immigration Consultants	","sort":	34	},
      {"id":	35	,"name":"	Interpreters/ Translators	","sort":	35	},
      {"id":	36	,"name":"	Jewellery Shops	","sort":	36	},
      {"id":	37	,"name":"	Kitchen Cabinets/ Woodworking	","sort":	37	},
      {"id":	38	,"name":"	Landscaping/ Snow Removal	","sort":	38	},
      {"id":	39	,"name":"	Locksmith	","sort":	39	},
      {"id":	40	,"name":"	Limousine	","sort":	40	},
      {"id":	41	,"name":"	Media	","sort":	41	},
      {"id":	42	,"name":"	Moving/ Storage	","sort":	42	},
      {"id":	43	,"name":"	Optical Stores/ Opticians	","sort":	43	},
      {"id":	44	,"name":"	Pest Control	","sort":	44	},
      {"id":	45	,"name":"	Printers/ Signs	","sort":	45	},
      {"id":	46	,"name":"	Real Estate Brokers/ Agents	","sort":	46	},
      {"id":	47	,"name":"	Refrigeration Solutions	","sort":	47	},
      {"id":	48	,"name":"	Restaurants/ Sweet Shops	","sort":	48	},
      {"id":	49	,"name":"	Roofing	","sort":	49	},
      {"id":	50	,"name":"	Staffing/ Job/ Employment Agency	","sort":	50	},
      {"id":	51	,"name":"	Security Cameras/ Computers/ Networking	","sort":	51	},
      {"id":	52	,"name":"	Solar Panels	","sort":	52	},
      {"id":	53	,"name":"	Telephone /Cell/ Mobile Phones	","sort":	53	},
      {"id":	54	,"name":"	Tent Services	","sort":	54	},
      {"id":	55	,"name":"	Tile/ Granite/ Marble	","sort":	55	},
      {"id":	56	,"name":"	Towing	","sort":	56	},
      {"id":	57	,"name":"	Training Institutes/ Colleges	","sort":	57	},
      {"id":	58	,"name":"	Trucking Industry	","sort":	58	},
      {"id":	59	,"name":"	Tutors	","sort":	59	},
      {"id":	60	,"name":"	TV Repair/ Electronic Parts	","sort":	60	},
      {"id":	61	,"name":"	Wedding Planners	","sort":	61	},
      {"id":	62	,"name":"	Windows/ Doors	","sort":	62	}
        ];
    }

}
