import { Injectable } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Events,Platform} from '@ionic/angular';
import { userPicArr } from '../app/app.angularfireconfig';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../app/const';
import { TpstorageProvider } from '../app/tpstorage.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  firedata = firebase.database().ref('/chatusers');
  firebuddychats = firebase.database().ref('/buddychats');
  firebuddymessagecounter = firebase.database().ref('/buddychats');
  fireuserStatus = firebase.database().ref('/userstatus');
  fireStar = firebase.database().ref('/starredmessage');
  firefriends = firebase.database().ref('/friends');
  fireReferral_sent = firebase.database().ref('/referral_sent');
  fireReferral_received = firebase.database().ref('/referral_received');
  fireBusiness = firebase.database().ref('/business');
  tmpKeyMine:any;
  tmpKeyBuddy:any;
  buddy: any;
  buddymessages = [];
  msgcount = 0;
  buddyStatus: any;
  userId;
  constructor(public events: Events, public tpStorageService: TpstorageProvider , public userservice: UserService) {

    this.tpStorageService.getItem('userUID').then((userID: any) => {
      if (userID != undefined) {
        this.userId = userID;
      } else {
        this.userId = firebase.auth().currentUser.uid;
      }
		}).catch(e => { });

  }



    //jaswinder probbaly not used
    buddymessageRead(limit) {
      this.firebuddychats.child(this.userId).child(this.buddy.uid)
        .limitToLast(limit).once('value', (snapshot) => {
          let allmessahes = snapshot.val();
          for (var key in allmessahes) {
            if (allmessahes[key].isRead == false) {
              this.firebuddychats.child(this.buddy.uid).child(this.userId).child(key).update({ isRead: true })
            }
          }
        })
    }

    initializebuddy (buddy){
      if(this.userId) {
         this.initializebuddy_ (buddy);
      } else {
          this.tpStorageService.getItem('userUID').then((userId__: any) => {
                this.userId = userId__;
                this.initializebuddy_ (buddy);
          }).catch(e => { });
      }//endif
    }


    initializebuddy_(buddy) {
      this.buddy = buddy;
      ////console.log ("initializebuddy_ Done");
      ////console.log (this.buddy);
      //this.buddymessageRead(10);

    }
    formatAMPM(date) {
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }
    formatDate(date) {
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var yyyy = date.getFullYear();
      if (dd < 10)
        dd = '0' + dd;
      if (mm < 10)
        mm = '0' + mm;
      return dd + '/' + mm + '/' + yyyy;
    }

    addnewmessageDirectReferral(msg, type, buddy, referral, referralType ) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (buddy) {
        var promise = new Promise((resolve, reject) => {
          this.tpStorageService.getItem('userUID').then((userId__: any) => {
          this.userId = userId__;
          this.firebuddychats.child(this.userId).child(buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            messageId: '',
            selectCatId: 0,
            multiMessage: false,
            request_to_release_incentive : '',
            isStarred: false,
            referral_type: referralType,
            referral : referral,
            referralSavedOnPhone : false,
            userprio:''
          })
            .then((item) => {

              //Log info
              this.fireReferral_sent.child(this.userId).child(buddy.uid).push({
                  sent_by : this.userId,
                  received_by : buddy.uid,
                  referral_type: referralType,
                  referral : referral,
                  timestamp: firebase.database.ServerValue.TIMESTAMP,
                  timeofmsg: time,
                  dateofmsg: date,
                  redeemStatus : 0,
                  incentiveType : "",
                  incentiveDetail : "",
                  business_referral_detail : "",
                  business_referral_type_id : ""
              }).then((referral_sent) => {
                this.fireReferral_received.child(buddy.uid).child(this.userId).child(referral_sent.key).set({
                    sent_by : this.userId,
                    received_by : buddy.uid,
                    referral_type: referralType,
                    referral : referral,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    redeemStatus : 0,
                    incentiveType : "",
                    incentiveDetail : "",
                    business_referral_detail : "",
                    business_referral_type_id : ""
                }).then((referral_received) => {
                    //Now check and see if the guy who recieved referral is actually busienss owner at that time
                     this.fireBusiness.child(buddy.uid).once('value', (snapshot_business_of_referral_rec) => {
                       let businessGuy = snapshot_business_of_referral_rec.val();
                       if(businessGuy){
                         this.fireReferral_sent.child(this.userId).child(buddy.uid).child(referral_sent.key).update(
                           {
                             business_referral_detail: businessGuy.referral_detail,
                             business_referral_type_id: businessGuy.referral_type_id
                           }
                        );
                        this.fireReferral_received.child(buddy.uid).child(this.userId).child(referral_sent.key).update(
                          {
                            business_referral_detail: businessGuy.referral_detail,
                            business_referral_type_id: businessGuy.referral_type_id
                          }
                        );
                       }//endif
                     }).catch((err) => {

                     });
                });
              });



              this.firefriends.child(this.userId).child(buddy.uid).update({ isActive: 1, userprio: new Date() });
              this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})

              this.userservice.getstatusblock(buddy).then((res) => {
                //////console.log ("going_to_call gotstatusblock ");
                if (res == false) {

                  //mytodo : check if buddystatus is online
                  //if (buddystatus == "online") {
                  if (false) {
                    //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                  } else {
                    //////console.log ("going_to_call firebuddychats 1");
                    this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                  }

                  //Also add to buddy
                  this.firebuddychats.child(buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    isRead: true,
                    isStarred: false,
                    selectCatId: 0,
                    request_to_release_incentive : '',
                    referral_type: referralType,
                    referral : referral,
                    referralSavedOnPhone : false,
                    userprio:''
                  }).then((items) => {
                    this.firebuddychats.child(buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {
                       this.firefriends.child(buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                       resolve(true);
                    })
                  })


                }   else {
                  //Buddy has blocked us
                  //////console.log ("going_to_call firebuddychats 2");
                  this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: false });
                  resolve(true);
                }
              })
            })
          }).catch(e => { });
        })
        return promise;
      }
    }

    //buddy is receipient
    addnewmessageBroadcast(msg, type, buddy, selectCatId, bothways ) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (buddy) {
        var promise = new Promise((resolve, reject) => {
          this.tpStorageService.getItem('userUID').then((userId__: any) => {
          this.userId = userId__;
          this.userservice.getstatusblock(buddy).then((res) => {
            if (res == false) {
              //user has not blocked buddy
              this.firebuddychats.child(buddy.uid).child(this.userId).push({
                sentby: this.userId,
                message: msg,
                type: type,
                timestamp: firebase.database.ServerValue.TIMESTAMP,
                timeofmsg: time,
                dateofmsg: date,
                request_to_release_incentive : '',
                messageId: '',
                selectCatId: selectCatId,
                multiMessage: false,
                isStarred: false,
                userprio:''
              })
                .then((item) => {

                  if(bothways){
                    //This is both way broadcast so include me as whitelabeled
                    this.firebuddychats.child(this.userId).child(buddy.uid).push({
                      sentby: this.userId,
                      message: msg,
                      type: type,
                      timestamp: firebase.database.ServerValue.TIMESTAMP,
                      timeofmsg: time,
                      dateofmsg: date,
                      request_to_release_incentive : '',
                      messageId: '',
                      selectCatId: selectCatId,
                      multiMessage: false,
                      isStarred: false,
                      userprio:''
                    }).then((item) => {});
                    //mytodo : broadcast in groups too
                  } else {

                  //A referral request has been sent
                  //somebody who posted referral request
                  }

                  this.firefriends.child(buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                  this.firebuddychats.child(buddy.uid).child(this.userId).child(item.key).update({ messageId: item.key , userprio:new Date()})

                  resolve(true);
                      //mytodo : check if buddystatus is online
                      //if (buddystatus == "online") {
                      if (false) {
                        //Send push notification
                        //this.firebuddychats.child(this.userId).child(buddy.uid).child(item.key).update({ isRead: true });
                      } else {
                        //Need this because we did not send push notification
                        this.firebuddychats.child(buddy.uid).child(this.userId).child(item.key).update({ isRead: false });
                      }
                })

            }//endif
          });//getstatusblock

          }).catch(e => { });
        })
        return promise;
      }
    }

    //under dev : not used
    addnewmessageAskForIncentives(msg, type, buddystatus, referral_id,referral_send_by,referral_received_by) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (this.buddy) {
        var promise = new Promise((resolve, reject) => {
          this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            request_to_release_incentive : referral_id,
            referral_send_by : referral_send_by,
            referral_received_by : referral_received_by,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            messageId: '',
            multiMessage: false,
            isStarred: false,
            userprio:''
          })
            .then((item) => {
              this.firefriends.child(this.userId).child(this.buddy.uid).update({ isActive: 1, userprio: new Date() });
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
              this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    request_to_release_incentive : referral_id,
                    isRead: true,
                    isStarred: false,
                    userprio:''
                  }).then((items) => {
                    this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()});
                    this.firefriends.child(this.buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                    resolve(true);
                 })
              });

        });
        return promise;
      }
    }

    addnewmessageRedeemDone(msg, type, buddystatus, referral_id,referral_send_by,referral_received_by) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (this.buddy) {
        var promise = new Promise((resolve, reject) => {
          this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            referral_redeemed : 1,
            referral_send_by : referral_send_by,
            referral_received_by : referral_received_by,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            messageId: '',
            multiMessage: false,
            isStarred: false,
            userprio:''
          })
            .then((item) => {
              this.firefriends.child(this.userId).child(this.buddy.uid).update({ isActive: 1, userprio: new Date() });
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
              this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    referral_redeemed : 1,
                    referral_send_by : referral_send_by,
                    referral_received_by : referral_received_by,
                    isRead: true,
                    isStarred: false,
                    userprio:''
                  }).then((items) => {
                    this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()});
                    this.firefriends.child(this.buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                    resolve(true);
                 })
              });

        });
        return promise;
      }
    }

    addnewmessageRedeemRequest(msg, type, buddystatus, referral_id,referral_send_by,referral_received_by) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (this.buddy) {
        var promise = new Promise((resolve, reject) => {
          this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            request_to_release_incentive : referral_id,
            referral_send_by : referral_send_by,
            referral_received_by : referral_received_by,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            messageId: '',
            multiMessage: false,
            isStarred: false,
            userprio:''
          })
            .then((item) => {
              this.firefriends.child(this.userId).child(this.buddy.uid).update({ isActive: 1, userprio: new Date() });
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
              this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    messageId: '',
                    request_to_release_incentive : referral_id,
                    referral_send_by : referral_send_by,
                    referral_received_by : referral_received_by,
                    isRead: true,
                    isStarred: false,
                    userprio:''
                  }).then((items) => {
                    this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()});
                    this.firefriends.child(this.buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                    resolve(true);
                 })
              });

        });
        return promise;
      }
    }

    addnewmessage(msg, type, buddystatus) {
      //console.log ("addnewmessageaddnewmessage");
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (this.buddy) {
        var promise = new Promise((resolve, reject) => {
          //console.log ("GGGGGGGGGGGGG1");
          this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            request_to_release_incentive : '',
            messageId: '',
            multiMessage: false,
            isStarred: false,
            userprio:''
          })
            .then((item) => {
              ////console.log ("GGGGGGGGGGGGG2");
              this.firefriends.child(this.userId).child(this.buddy.uid).update({ isActive: 1, userprio: new Date() });
              ////console.log ("GGGGGGGGGGGGG3");
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
              ////console.log ("GGGGGGGGGGGGG4");
              this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive : '',
                    messageId: '',
                    isRead: true,
                    isStarred: false,
                    userprio:''
                  }).then((items) => {
                    ////console.log ("GGGGGGGGGGGGG5");
                    this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()});
                    ////console.log ("GGGGGGGGGGGGG6");
                    this.firefriends.child(this.buddy.uid).child(this.userId).update({ isActive: 1, userprio: new Date() });
                    resolve(true);
                 })
              });

        });
        return promise;
      }
    }
    addnewmessagemultiple(msg, type, buddystatus) {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      if (this.buddy) {
        var promise = new Promise((resolve, reject) => {
          this.firebuddychats.child(this.userId).child(this.buddy.uid).push({
            sentby: this.userId,
            message: msg,
            type: type,
            timestamp: firebase.database.ServerValue.TIMESTAMP,
            timeofmsg: time,
            dateofmsg: date,
            request_to_release_incentive : '',
            messageId: '',
            isRead: true,
            multiMessage: true,
            isStarred: false,
            userprio:''
          })
            .then((item) => {
              this.getfirendlist(this.userId).then((res) => {
                let friends = res;
                for (var tmpkey in friends) {
                  if (friends[tmpkey].uid == this.buddy.uid) {
                    this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                    resolve(true);
                  }
                }
              })
              this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ messageId: item.key , userprio:new Date()})
              if (buddystatus == "online") {
                this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: true });
              } else {
                this.userservice.getstatusblock(this.buddy).then((res) => {
                  if (res == false) {
                    this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: false });
                  } else {
                    this.firebuddychats.child(this.userId).child(this.buddy.uid).child(item.key).update({ isRead: true });
                  }
                })
              }
              this.userservice.getstatusblock(this.buddy).then((res) => {
                if (res == false) {
                  this.firebuddychats.child(this.buddy.uid).child(this.userId).push({
                    sentby: this.userId,
                    message: msg,
                    type: type,
                    timestamp: firebase.database.ServerValue.TIMESTAMP,
                    timeofmsg: time,
                    dateofmsg: date,
                    request_to_release_incentive : '',
                    messageId: '',
                    multiMessage: true,
                    isStarred: false,
                    userprio:''
                  }).then((items) => {
                    this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ messageId: items.key , userprio:new Date()}).then(() => {
                      if (buddystatus == "online") {
                        this.firebuddychats.child(this.buddy.uid).child(this.userId).child(items.key).update({ isRead: true });
                      } else {
                      }
                      this.getfirendlist(this.buddy.uid).then((res) => {
                        let friends = res;
                        for (var tmpkey in friends) {
                          if (friends[tmpkey].uid == this.userId) {
                            this.firefriends.child(this.buddy.uid).child(tmpkey).update({ isActive: 1, userprio: new Date() });
                            resolve(true);
                          }
                        }
                      })
                    })
                  })
                } else {
                  resolve(true);
                }

              })
            })
        })
        return promise;
      }
    }
    deleteMessages(items) {
      return new Promise((resolve, reject) => {
        for (let i = 0; i < items.length; i++) {
          this.firebuddychats.child(this.userId).child(this.buddy.uid).child(items[i].messageId).remove()
            .then((res) => {
                  this.getfirendlist(this.userId).then((res) => {
                    let friends = res;
                    for (var tmpkey in friends) {
                      if (friends[tmpkey].uid == this.buddy.uid) {
                        this.firebuddychats.child(this.userId).child(this.buddy.uid).once('value',(snapshot)=>{
                          var tempdata =this.converanobj(snapshot.val());
                          if(tempdata.length>0){
                            var lastMsg = tempdata[tempdata.length-1];
                            this.firefriends.child(this.userId).child(tmpkey).update({  userprio: lastMsg.userprio });
                            resolve(true);
                          }else{
                            this.firefriends.child(this.userId).child(tmpkey).update({  userprio: '' });
                            resolve(true);
                          }
                        })
                      }
                    }
                  })

            }).catch((err) => {
              reject(false);
            })
        }
      })
    }
    deleteUserMessages(allbuddyuser) {
      return new Promise((resolve, reject) => {
        if (allbuddyuser.length > 0) {
          for (let i = 0; i < allbuddyuser.length; i++) {
            this.firebuddychats.child(this.userId).child(allbuddyuser[i].uid).remove()
              .then((res) => {
                this.getfirendlist(this.userId).then((res) => {
                  let friends = res;
                  for (var tmpkey in friends) {
                    if (friends[tmpkey].uid == allbuddyuser[i].uid) {
                      this.firefriends.child(this.userId).child(tmpkey).update({ isActive: 0 }).then((res) => {
                        resolve(true);
                        this.events.publish('friends');
                      })
                    }
                  }
                })
              })
          }
        }
      })
    }



    getfirendlist(uid) {
      return new Promise((resolve, reject) => {
        //////console.log ("<<<<<<<<Finding Frenids in DB>>>>>>>>");
        this.firefriends.child(uid).on('value', (snapshot) => {
          let friendsAll = snapshot.val();
          resolve(friendsAll);
        })
      })
    }

    updateContactSaved(msgId) {
      var promise = new Promise((resolve, reject) => {
        this.firebuddychats.child(this.userId).child(this.buddy.uid).child(msgId).update({
          referralSavedOnPhone: true,
        }).then(() => {
          resolve(true);
        }).catch((err) => {
          reject(err);
        })
      })
      return promise;
    }


    getbuddymessagesForSecondaPage ( limit ){
      if(this.userId) {
         this.getbuddymessagesForSecondaPage_ ( limit );
      } else {
          this.tpStorageService.getItem('userUID').then((userId__: any) => {
                this.userId = userId__;
                this.getbuddymessagesForSecondaPage_ ( limit );
          }).catch(e => { });
      }//endif
    }

    getbuddymessagesForSecondaPage_(limit) {
      this.buddymessages = [];
      this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', (snapshot) => {
        //Check for unique value
        let tmpObj = snapshot.val();
        let isMatch  = false;
        for (var myKey in this.buddymessages) {
            if( this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
            this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
            this.buddymessages[myKey]['message'] == tmpObj.message &&
            this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
            this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
            this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
            this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
            this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
            this.buddymessages[myKey]['type'] == tmpObj.type
          ){
              //This is redundant message
              isMatch = true;
              break;
          }
         }//end for
         if(!isMatch){
           this.buddymessages.push(tmpObj);
           //console.log ("publishing event :"+tmpObj.messageId);
           //this.events.publish('newmessage_secondpage', tmpObj);
         }
      })
    }



    getbuddymessages ( limit ){
      if(this.userId) {
         this.getbuddymessages_ ( limit );
      } else {
          this.tpStorageService.getItem('userUID').then((userId__: any) => {
                this.userId = userId__;
                this.getbuddymessages_ ( limit );
          }).catch(e => { });
      }//endif
    }

    getbuddymessages_(limit) {
      this.buddymessages = [];
      this.firebuddychats.child(this.userId).child(this.buddy.uid).limitToLast(limit).on('child_added', (snapshot) => {
        //Check for unique value
        let tmpObj = snapshot.val();
        let isMatch  = false;
        for (var myKey in this.buddymessages) {
            if( this.buddymessages[myKey]['dateofmsg'] == tmpObj.dateofmsg &&
            this.buddymessages[myKey]['isStarred'] == tmpObj.isStarred &&
            this.buddymessages[myKey]['message'] == tmpObj.message &&
            this.buddymessages[myKey]['messageId'] == tmpObj.messageId &&
            this.buddymessages[myKey]['multiMessage'] == tmpObj.multiMessage &&
            this.buddymessages[myKey]['sentby'] == tmpObj.sentby &&
            this.buddymessages[myKey]['timeofmsg'] == tmpObj.timeofmsg &&
            this.buddymessages[myKey]['timestamp'] == tmpObj.timestamp &&
            this.buddymessages[myKey]['type'] == tmpObj.type
          ){
              //This is redundant message
              isMatch = true;
              break;
          }
         }//end for

         if(!isMatch){
           this.buddymessages.push(tmpObj);
           this.events.publish('newmessage');
         }


      })
    }

    getbuddyStatus() {
      let tmpStatus;

      this.fireuserStatus.child(this.buddy.uid).on('value', (statuss) => {
        tmpStatus = statuss.val();
        if (tmpStatus.status == 1) {
          this.buddyStatus = tmpStatus.data
        } else {
          let date = tmpStatus.timestamp;
          this.buddyStatus = date
        }
        this.events.publish('onlieStatus');
      })
    }
    setstatusUser() {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());
      var promise = new Promise((resolve, reject) => {
        this.fireuserStatus.child(this.userId).set({
          status: 1,
          data: 'online',
          timestamp: date + ' at ' + time
        }).then(() => {
          resolve(true);
        }).catch((err) => {
          reject(err);
        })
      })
      return promise;
    }
    setStatusOffline() {
      let time = this.formatAMPM(new Date());
      let date = this.formatDate(new Date());

      var promise = new Promise((resolve, reject) => {
        this.fireuserStatus.child(this.userId).update({
          status: 0,
          data: 'offline',
          timestamp: date + ' at ' + time
        }).then(() => {
          resolve(true);
        }).catch((err) => {
          reject(err);
        })

      })
      return promise;
    }

    converanobj(obj) {
      let tmp = []
      for (var key in obj) {
        tmp.push(obj[key]);
      }
      return tmp;
    }
}
