import { Injectable } from '@angular/core';
import { LoadingController,ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  	loading:any;

  	constructor(public toastCtrl: ToastController,
  				public loadingCtrl: LoadingController) {


  	}

  	// For present loading
  	async presentLoadingNew() {

  			//https://medium.muz.li/top-30-most-captivating-preloaders-for-your-website-95ed1beff99d
  			this.loading = await  this.loadingCtrl.create({
          message: 'Please wait, we are working hard on it'
  			});

        //await this.loading.present();
  			/*
        this.loading.present().then(()=>{
  			   //setTimeout(()=>{
  			     // this.loading.dismiss();
  			   //},300);
  			});
        */

  	}

    async presentLoading() {

  	}
  	//dismiss
  	async dismissMyLoading (){
  	  //console.log ("Trying dismiising loading");
  		if(this.loading){
  			await this.loading.dismiss();
  			this.loading = null;
  	  }
  	}

  	// Toast message
  	async presentToast(text) {
  		let toast = await this.toastCtrl.create({
  			message: text,
  			duration: 3000,
  			position: 'top'
  		});
  		await toast.present();
  	}
}
