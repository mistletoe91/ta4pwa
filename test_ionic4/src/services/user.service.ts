import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';//firebase/app
import { Events,Platform} from '@ionic/angular';
import { userPicArr } from '../app/app.angularfireconfig';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../app/const';
import { TpstorageProvider } from '../app/tpstorage.service';
import { HTTP } from '@ionic-native/http/ngx';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  userId;
  userName;
  userPic;
  firedata = firebase.database().ref('/chatusers');
  firefriend = firebase.database().ref('/friends');
  firebuddychat = firebase.database().ref('/buddychats');
  users = firebase.database().ref('/users');
  userstatus = firebase.database().ref('/userstatus');
  firereq = firebase.database().ref('/requests');
  firenotify = firebase.database().ref('/notification');
  fireBusiness = firebase.database().ref('/business');
  //fireInvitationSent = firebase.database().ref('/invitationsent');
  firePhones = firebase.database().ref('/phones');
  fireWebsiteUsers = firebase.database().ref('/website_users');
  fireReferral_sent = firebase.database().ref('/referral_sent');
  fireReferral_received = firebase.database().ref('/referral_received');

  isUserExits: boolean = true;
  blockUsers: any = [];
  unblockUsers: any = [];
  isuserBlock: boolean = false;
  blockUsersCounter = 0;
  isNotify: boolean;
  constructor(
    private sqlite: SQLite,
    private afireAuth: AngularFireAuth, 
    public platform: Platform,
    public events: Events,
    public http:HTTP,
    public tpStorageService: TpstorageProvider
  ) {
    this.platform.ready().then((readySource) => {

        this.tpStorageService.getItem('userUID').then((res: any) => {
          if(res){
            this.userId = res;
          }
          if (!this.userId) {
            this.userId = firebase.auth().currentUser.uid;
          }
          //////console.log ("this.userIdthis.userIdthis.userIdthis.userId:" + this.userId);
        }).catch(e => {
          if(firebase && firebase.auth().currentUser){
            this.userId = firebase.auth().currentUser.uid;
          }
        });

        this.tpStorageService.getItem('userName').then((res: any) => {
          if(res){
            this.userName = res;
          } else {
            this.userName = this.afireAuth.auth.currentUser.displayName;
          }
        }).catch(e => {
          if(this.afireAuth && this.afireAuth.auth && this.afireAuth.auth.currentUser){
            this.userName = this.afireAuth.auth.currentUser.displayName;
          }
        });

        this.tpStorageService.getItem('userPic').then((res: any) => {
          if(res){
            this.userPic = res;
          } else {
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
          }
        }).catch(e => {
          if(this.afireAuth && this.afireAuth.auth && this.afireAuth.auth.currentUser){
            this.userPic = this.afireAuth.auth.currentUser.photoURL;
          }
        });

    });
  }

  alphaNumericEmail (email_){
    email_ = email_.toLowerCase ();
    return  email_.replace(/[^0-9a-z]+/gi,"");
  }

  getWebsiteUserDetails(email_) {
    var cleanEmail = this.alphaNumericEmail (email_);
    console.log ("cleanEmail:");
    console.log (cleanEmail);
    var promise = new Promise((resolve, reject) => {
      this.fireWebsiteUsers.child(cleanEmail).once('value', (snapshot) => {
        resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  updateWebsiteUserDetails (email_ , userid_, paid_ ) {
        let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];

        var cleanEmail = this.alphaNumericEmail (email_);
        console.log ("email_" + email_);
        console.log ("DO THIS NOW");
        console.log (cleanEmail);
        this.fireWebsiteUsers.child(cleanEmail).update({
          paid: paid_,
          userId : userid_
        }).then(() => {
        }).catch((err) => {
        })

        this.tpStorageService.setItem('myuserPic', myuserPic);

        this.firedata.child(userid_).update({
          photoURL: myuserPic,
          timestamp : firebase.database.ServerValue.TIMESTAMP,
          uid : userid_,
          useremail : email_
        }).then(() => {
            console.log ("::Done ");
        }).catch((err) => {
        })

  }//end function

  getuserdetails() {
    var promise = new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((res: any) => {
        this.firedata.child(this.userId).once('value', (snapshot) => {
          let res= snapshot.val();
          this.tpStorageService.setItem('mymobile',res['mobile']);
          this.tpStorageService.setItem('displayName',res['displayName']);
          this.tpStorageService.setItem('photoURL',res['photoURL']);
          resolve(res);
        }).catch((err) => {
          reject(err);
        })
      }).catch(e => {
      });
    })
    return promise;
  }

   //this is good
  updatedisplayname(newname) {
    //////console.log ("trying to updaet display name");
    this.tpStorageService.setItem('myDisplayName', newname);
    var promise = new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((res: any) => {
        this.userId = res;
        this.firedata.child(this.userId).update({
          displayName: newname
        }).then(() => {
           //this.updatedisplayname_DB(newname);//do it in parellel
           resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })

      }).catch(e => {
         if(this.userId){
         } else {
           if(firebase && firebase.auth().currentUser){
             this.userId = firebase.auth().currentUser.uid;
           }
         }//endif
         this.tpStorageService.setItem('userUID', this.userId);

         this.firedata.child(this.userId).update({
           displayName: newname
         }).then(() => {
            //this.updatedisplayname_DB(newname);//do it in parellel
            resolve({ success: true });
         }).catch((err) => {
           reject(err);
         })

      });


    })
    return promise;
  }

  initializeItem(userID) {
    return new Promise((resolve, reject) => {
      this.firenotify.child(userID).child('isNotify').once('value', (snapshot) => {
        this.isNotify = snapshot.val();
        resolve(this.isNotify);
      }).catch((err) => {
        reject(err);
      })
    })
  }

  adduser (newuser){
     if(this.userId){
       return this.adduser_(newuser);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.adduser_(newuser);
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.adduser_(newuser);
       });
     }
  }

  //mytodo : is this being used anywhere ?
  adduser_(newuser) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    var promise = new Promise((resolve, reject) => {
      this.afireAuth.auth.createUserWithEmailAndPassword(newuser.email, newuser.password).then(() => {
        this.afireAuth.auth.currentUser.updateProfile({
          displayName: newuser.username,
          photoURL: myuserPic
        }).then(() => {
          this.firedata.child(this.userId).set({
            uid: this.userId,
            displayName: newuser.username,
            photoURL: myuserPic,
            invited_by:"none"
          }).then(() => {
            resolve(true);
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise
  }
  //This doesnt look like being used
  addmobileUser(verificationCredential, code, mobile) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    return new Promise((resolve, reject) => {
      this.tpStorageService.setItem('verificationCredential', verificationCredential);
      this.tpStorageService.setItem('code', code);

      let signInData = firebase.auth.PhoneAuthProvider.credential(verificationCredential, code);
      //////console.log ("signInData");
      //////console.log (signInData);

      this.afireAuth.auth.signInWithCredential(signInData).then((info) => {
        let profileImage;
        if (this.afireAuth.auth.currentUser.photoURL != null) {
          profileImage = this.afireAuth.auth.currentUser.photoURL;
        } else {
          profileImage = myuserPic
        }

        this.afireAuth.auth.currentUser.updateProfile({ displayName: this.afireAuth.auth.currentUser.displayName, photoURL: profileImage }).then(() => {
          this.firedata.child(this.afireAuth.auth.currentUser.uid).set({
            uid: this.afireAuth.auth.currentUser.uid,
            mobile: mobile,
            countryCode: verificationCredential,
            displayName: this.afireAuth.auth.currentUser.displayName,
            disc: '',
            photoURL: profileImage,
            deviceToken: ''
          }).then(() => {
            resolve({ success: true });
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
        })
      }).catch((err) => {
        resolve({ success: false, msg: JSON.stringify(err.message) });
      })
    })
  }

  addmobileUserIOS(countryCode, mobile) {
    let myuserPic = userPicArr [Math.floor(Math.random() * Object.keys(userPicArr).length-1)];
    return new Promise((resolve, reject) => {
      this.tpStorageService.setItem('verificationCredential', countryCode);
      this.tpStorageService.setItem('code', mobile);
      this.tpStorageService.setItem('fullPhoneNumber', countryCode+mobile);

      //this.firedata.once('value', (snep) => {
        /*let allData = snep.val();
        for (let tmpkey in allData) {
          if (allData[tmpkey].mobile == mobile) {
            this.isUserExits = false;
            this.tpStorageService.setItem('userUID', allData[tmpkey].uid);
          }
        }*/

        if (this.isUserExits) {
          this.afireAuth.auth.signInAnonymously().then((info) => {

            let profileImage;
            if (this.afireAuth.auth.currentUser.photoURL != null) {
              profileImage = this.afireAuth.auth.currentUser.photoURL;
            } else {
              profileImage = myuserPic
            }

            this.tpStorageService.setItem('userUID', this.afireAuth.auth.currentUser.uid);

            this.afireAuth.auth.currentUser.updateProfile(
            {
              displayName: this.afireAuth.auth.currentUser.displayName,
              photoURL: profileImage }
                      ).then(() => {

                        //mytodo_critical  : test this
                        this.http.get('https://tapally.com/wp-json/log/v1/createuser?phone='+mobile+'&countrycode='+countryCode+'&uid='+this.afireAuth.auth.currentUser.uid, {}, {})
                          .then(data => {
                          })
                          .catch(error => {
                          });



              this.firedata.child(this.afireAuth.auth.currentUser.uid).set({
                uid: this.afireAuth.auth.currentUser.uid,
                mobile: mobile,
                countryCode: countryCode,
                displayName: this.afireAuth.auth.currentUser.displayName,
                disc: '',
                photoURL: profileImage,
                deviceToken: '',
                timestamp: firebase.database.ServerValue.TIMESTAMP
              }).then(() => {

                //Check if user is invited by someone else
                this.firePhones.child(mobile).once('value', (snapshot) => {
                  let phoneNumberOb = snapshot.val();
                  if(phoneNumberOb){
                    if(phoneNumberOb.uid){
                       //it already have uid so this means that this user is trying to register again perhaps he/she uninstalled the app
                       //Lets remove some pieces of his old profile
                       this.removeOldProfile (phoneNumberOb.uid,this.afireAuth.auth.currentUser.uid );

                       //update business profile with new value
                       this.fireBusiness.child(phoneNumberOb.uid).once('value', (snapshot_business) => {
                           this.fireBusiness.child(this.afireAuth.auth.currentUser.uid).set(snapshot_business.val());
                           this.fireBusiness.child(phoneNumberOb.uid).remove();
                       });

                    } else {

                      if(phoneNumberOb.invited_by){
                        //I was actually invited by someone so update both cache and database
                        this.tpStorageService.setItem('invited_by', phoneNumberOb.invited_by);

                        this.firedata.child(this.afireAuth.auth.currentUser.uid).update({
                          invited_by: phoneNumberOb.invited_by
                        });

                        //if i am invieted. lets make friends
                        this.acceptrequest (
                          this.afireAuth.auth.currentUser.uid,
                          phoneNumberOb.invited_by,
                          this.afireAuth.auth.currentUser.displayName,
                          mobile,
                          countryCode,
                          profileImage,
                          firebase.database.ServerValue.TIMESTAMP
                        );

                      }//endif
                    }

                    //Object is not properly set so set it
                    this.firePhones.child(mobile).update({
                      uid: this.afireAuth.auth.currentUser.uid,
                    });

                  } else {
                    //Object does not exist
                    this.firePhones.child(mobile).update({
                      uid: this.afireAuth.auth.currentUser.uid,
                    });
                  }
                }).catch((err) => {
                  //Object does not exist
                  this.firePhones.child(mobile).update({
                    uid: this.afireAuth.auth.currentUser.uid,
                  });
                });

                resolve({ success: true });
              }).catch((err) => {
                reject(err);
              })
            }).catch((err) => {
            })
          }).catch((err) => {
            resolve({ success: false, msg: JSON.stringify(err.message) });
          });
        } else {
          resolve({ success: true });
        }
      //});
    });
  }

  //duplicate function in requests.ts  but not entirly same
  acceptrequest(me_uid, buddy_uid, displayName,mobile,countryCode,profileImage, timestamp_ ) {
      this.userId = me_uid;
      this.getbuddydetails(buddy_uid).then((buddy: any) => {

            //mytodo : update  deviceToken
            //this.getbuddydetails(me_uid).then((aboutMe: any) => {
              this.firefriend.child(this.userId).child(buddy.uid).set({
                uid: buddy.uid,
                displayName: buddy.displayName,
                photoURL: buddy.photoURL,
                isActive: 1,
                isBlock: false,
                deviceToken: buddy.deviceToken,
                countryCode: buddy.countryCode,
                disc: buddy.disc,
                mobile: buddy.mobile,
                timestamp: buddy.timestamp
              }).then(() => {
                this.firefriend.child(buddy.uid).child(this.userId).set({
                  uid: this.userId,
                  displayName: displayName,
                  photoURL: profileImage,
                  isActive: 1,
                  isBlock: false,
                  deviceToken: "",
                  countryCode: countryCode,
                  disc: '',
                  mobile: mobile,
                  timestamp: timestamp_
                }).then(() => {

               });
            });
          //});

      });
  }//end function

  //it remove and re-attach
  removeOldProfile(oldUid, newuid ) {
/*
 //This function works great but is it still relevent ? We need similar function because we are now using email instead of phone as primary key

    this.firefriend.child(oldUid).once('value', (snapshot) => {
        let allFriendsWithOldUid = snapshot.val();
        for (var key in allFriendsWithOldUid) {
           this.firefriend.child(key).once('value', (snapshot_friends) => {
                let allFriendsWithOldUid_friends = snapshot_friends.val();
                for (var key_friends in allFriendsWithOldUid_friends) {
                    if(key_friends == oldUid) {
                       var pathToRemove = snapshot_friends.ref.path.toString();
                       pathToRemove = pathToRemove.replace ('/friends/', '')
                       ////console.log("Adding : "+"(pathToRemove"+"/"+this.afireAuth.auth.currentUser.uid);
                       ////console.log (allFriendsWithOldUid_friends [oldUid]);
                       allFriendsWithOldUid_friends [oldUid].uid = this.afireAuth.auth.currentUser.uid;
                       this.firefriend.child(pathToRemove+"/"+this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid_friends [oldUid]);
                       this.firefriend.child(pathToRemove+"/"+oldUid).remove();
                       break;
                    }
                }//end for
           });
        }//end for

        //now change the old friend record to new record
        this.firefriend.child(this.afireAuth.auth.currentUser.uid).set(allFriendsWithOldUid);
        this.firefriend.child(oldUid).remove();
      })
      */
  }

  updateimage (args){
     if(this.userId){
       return this.updateimage_(args);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updateimage_(args);
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updateimage_(args);
       });
     }
  }

  updateimage_(imageurl) {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({ photoURL: imageurl }).then(() => {
        firebase.database().ref('/users/' + this.userId).update({
          photoURL: imageurl
        }).then(() => {
          resolve({ success: true });
        }).catch((err) => {
          reject(err);
        })
      })
    })
    return promise;
  }

  /*getuserdetails (){
     if(this.userId){
       return this.getuserdetails_();
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.getuserdetails_();
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.getuserdetails_();
       });
     }
  }
  */



  getbuddydetails(buddyID) {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(buddyID).once('value', (snapshot) => {
        resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }


/*
  updatedisplayname (args){
     if(this.userId){
       return this.updatedisplayname_(args);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updatedisplayname_(args);
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updatedisplayname_(args);
       });
     }
  }
  */


 updatedisplayname_DB (args){
    if(this.userId){
      return this.updatedisplayname_DB_(args);
    } else {
      this.tpStorageService.getItem('userUID').then((res: any) => {
        if(res){
          this.userId = res;
        }
        if (!this.userId) {
          this.userId = firebase.auth().currentUser.uid;
        }
        return this.updatedisplayname_DB_(args);
      }).catch(e => {
        if(firebase && firebase.auth().currentUser){
          this.userId = firebase.auth().currentUser.uid;
        }
        return this.updatedisplayname_DB_(args);
      });
    }
 }


 saveUserIdInSqlLite(userId) {
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("SELECT * FROM cache_users_local", [])
             .then(res => {
                 if(res.rows.length>0){
                 } else {
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid)   VALUES('"+this.userId+"')", [])
                        .then(res1 => {
                            //////console.log ("executeSql INSERT");//testa
                        }).catch(e => {//////console.log (e);
                      });
                 }//endif
             })
             .catch(e => {
                 //Error in operation
             });
         })
         .catch(e => {
             //Error in operation
         });//create table
       });//create database
 }//end function

 updatedisplayname_DB_(newname) {
       //////console.log ("Querying DB");
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("SELECT * FROM cache_users_local", [])
             .then(res => {
                 if(res.rows.length>0){
                        //////console.log ("user.ts record exists");
                        //UPDATE THE RECORD
                        db.executeSql("UPDATE cache_users_local SET uid='"+this.userId+"',user_name='"+newname+"',user_email='"+res.rows.item(0).user_email+"'", [])
                        .then(res1 => {
                           //////console.log ("executeSql UPDATE");
                        }).catch(e => {//////console.log (e);
                      });

                 } else {
                        //////console.log ("user.ts record does not exists");
                        //INSERT IN THE RECORD
                        db.executeSql("INSERT INTO cache_users_local (uid,user_name)   VALUES('"+this.userId+"','"+newname+"')", [])
                        .then(res1 => {
                            //////console.log ("executeSql INSERT");//testa
                        }).catch(e => {//////console.log (e);
                      });
                 }//endif
             })
             .catch(e => {
                 //Error in operation
             });
         })
         .catch(e => {
             //Error in operation
         });//create table
       });//create database
 }//end function


  updateDeviceToken(token, userID) {

    var promise = new Promise((resolve, reject) => {

      //Update the one who invited me
      //The google cloud function should do this job but sometime its lazy and take lot of time to run. So doing here quickly now
      //Do not run additional call to google cloud or something. That should happen through google cloud triggers function on cloud
      this.tpStorageService.getItem('invited_by').then((invited_by_res: any) => {
          this.tpStorageService.getItem('myDisplayName').then((myDisplayName_res: any) => {
                this.firefriend.child(invited_by_res).child(userID).update({
                  deviceToken : token,
                  displayName : myDisplayName_res
                });
          }).catch(e => {
          });
      }).catch(e => {
      });

      this.firedata.child(userID).update({
        deviceToken: token
      }).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  updatedisc (args){
     if(this.userId){
       return this.updatedisc_(args);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updatedisc_(args);
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.updatedisc_(args);
       });
     }
  }
  updatedisc_(disc) {
    var promise = new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({
        disc: disc
      }).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  //We need to remove getallusers_modified and getallusers . (mytodo) This is just for placeholder
  getallusers_modified(obj) {
    var promise = new Promise((resolve, reject) => {
        resolve(obj);
    });
    return promise;
  }

  getallusers() {
    //////console.log ("[[[[[[[[[[[[[[[[[GET ALL USERS]]]]]]]]]]]]]]]]]");
    var promise = new Promise((resolve, reject) => {
      this.firedata.orderByChild('uid').once('value', (snapshot) => {
        //////console.log (snapshot.val());
        //mytodo immediate : Why its getting all users. it should only get me
        let userdata = snapshot.val();
        let temparr = [];
        for (var key in userdata) {
          temparr.push(userdata[key]);
        }
        resolve(temparr);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }
  directLogin() {
    return new Promise((resolve, reject) => {
      resolve({ success: true })
    })
  }

  deleteUser ( ){
     if(this.userId){
       return this.deleteUser_( );
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.deleteUser_( );
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.deleteUser_( );
       });
     }
  }
  deleteUser_() {
    return new Promise((resolve, reject) => {
      this.firefriend
      this.firedata.child(this.userId).remove().then(() => {
        this.deletefriend(this.userId);
        this.deletefirebuddychat(this.userId);
        this.deleteusers(this.userId);
        this.deleteuserstatus(this.userId);
        this.tpStorageService.clear();
        resolve({ success: true })
      }).catch((err) => {
        reject({ success: true })
      })
    });
  }
  deletefriend(id) {
    this.firedata.child(id).remove().then((res) => {
    })
  }
  deletefirebuddychat(id) {
    this.firebuddychat.child(id).remove().then((res) => {
    })
  }
  deleteusers(id) {
    this.users.child(id).remove().then((res) => {
    })
  }
  deleteuserstatus(id) {
    this.userstatus.child(id).remove().then((res) => {
    })
  }

  blockUser (args ){
     if(this.userId){
       return this.blockUser_( args);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.blockUser_(args );
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.blockUser_(args );
       });
     }
  }
  blockUser_(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == buddy.uid) {
            this.firefriend.child(this.userId).child(key).update({ isBlock: true });
            resolve(true);
          }
        }
      })
    })
  }

  /*This is proxy function for unblockUser_ */
  unblockUser (args ){
     if(this.userId){
       return this.unblockUser_( args);
     } else {
       this.tpStorageService.getItem('userUID').then((res: any) => {
         if(res){
           this.userId = res;
         }
         if (!this.userId) {
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.unblockUser_(args );
       }).catch(e => {
         if(firebase && firebase.auth().currentUser){
           this.userId = firebase.auth().currentUser.uid;
         }
         return this.unblockUser_(args );
       });
     }
  }

  unblockUser_(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == buddy.uid) {
            this.firefriend.child(this.userId).child(key).update({ isBlock: false });
            resolve(true);
          }
        }
      })
    })
  }

  //mytodo : create proxy functions for all the following (The one has this.userId arggument)
  //The reason for theses proxy functions is to make sure this.userId exists before they are called
  //Chances are rare but it still happens. if happen it throw error on user screen and its ugly error
  getstatus(buddy) {
    return new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;
        this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
          let allfriends = snapshot.val();
          for (var key in allfriends) {
            if (allfriends[key].uid == buddy.uid) {
              resolve(allfriends[key].isBlock)
            }
          }
        })
      }).catch(e => { });
    })
  }

  getstatusblock(buddy) {
    return new Promise((resolve, reject) => {

      this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;
        //console.log ("firefriend/"+buddy.uid+"/"+ this.userId);
        this.firefriend.child(buddy.uid).child(this.userId).once('value', (snapshot) => {
          let buddyS = snapshot.val();
          resolve(buddyS.isBlock);
        })
      }).catch(e => { });

    })
  }

  getuserblock(buddy) {
    return new Promise((resolve, reject) => {


      this.firefriend.child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == this.userId) {
            resolve(allfriends[key].isBlock);
          }
        }
      })


    })
  }

  getmsgblock(buddy) {
    return new Promise((resolve, reject) => {
      this.firefriend.child(buddy).orderByChild('uid').once('value', (snapshot) => {
        let allfriends = snapshot.val();
        for (var key in allfriends) {
          if (allfriends[key].uid == this.userId) {
            resolve(allfriends[key].isBlock);
          }
        }
      })
    })
  }

  getstatusblockuser(buddy) {
    this.firefriend.child(buddy.uid).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      for (var key in allfriends) {
        if (allfriends[key].uid == this.userId) {
          this.isuserBlock = allfriends[key].isBlock;
        }
      }
      this.events.publish('isblock-user');
    })
  }
  getAllBlockUsers() {
    this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;

    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let blockuser = []
      for (var key in allfriends) {
        if (allfriends[key].isBlock) {
          this.firedata.child(allfriends[key].uid).once('value', (snapsho) => {
            blockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.blockUsers = [];
        this.blockUsers = blockuser;
      }
      this.events.publish('block-users');
    })

    }).catch(e => { });
  }
  getAllunBlockUsers() {
    this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;

    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let unblockuser = [];
      for (var key in allfriends) {
        if (allfriends[key].isBlock == false) {
          this.firedata.child(allfriends[key].uid).once('value', (snapsho) => {
            unblockuser.push(snapsho.val());
          }).catch((err) => {
          })
        }
        this.unblockUsers = [];
        this.unblockUsers = unblockuser;
      }
      this.events.publish('unblock-users');
    })

    }).catch(e => { });
  }
  getAllBlockUsersCounter() {
    this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;

    // block-users-counter
    this.firefriend.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
      let allfriends = snapshot.val();
      let blockalluser = [];
      for (let tmpkey in allfriends) {
        if (allfriends[tmpkey].isBlock) {
          blockalluser.push(allfriends[tmpkey]);
        }
      }
      this.firereq.child(this.userId).orderByChild('uid').once('value', (snapshot) => {
        let allrequest = snapshot.val();
        for (let tmp in allrequest) {
          if (allrequest[tmp].isBlock) {
            blockalluser.push(allrequest[tmp]);
          }
        }
        this.blockUsersCounter = 0;
        this.blockUsersCounter = blockalluser.length;
        this.events.publish('block-users-counter');
      })
    })

    }).catch(e => { });
  }
  notifyUser(isnotify) {
    this.tpStorageService.getItem('userUID').then((userId__: any) => {this.userId=userId__;
      this.firenotify.child(this.userId).set({
        isNotify: isnotify
      })
    }).catch(e => { });
  }
  getNotifyStatus(buddy) {
    return new Promise((resolve, reject) => {
      this.firenotify.child(buddy.uid).child('isNotify').once('value', (snapshot) => {
        let isNotify = snapshot.val();
        if (isNotify == true)
          resolve(true);
        else
          resolve(false);
      })
    })
  }




  registerBusiness(newname,useremail,bcats,rtlist,referral_incentive_detail_txt) {
    //////console.log (">registerBusiness");
    //update email
    this.tpStorageService.setItem('useremail', useremail);
    new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({
        useremail: useremail
      }).then(() => {
        //////console.log ("Done1");
      }).catch((err) => {
        //////console.log ("Tapally Error 9122");
        //////console.log(err);
      })
    });


    let timeStamp:string = String(Date.now());// get timestamp
    //update/create business name
    var promise = new Promise((resolve, reject) => {
      this.fireBusiness.child(this.userId).update({
        displayName: newname,
        timestamp_user_local_time_updated : timeStamp,
        bcats: bcats,
        referral_type_id:rtlist,
        referral_detail:referral_incentive_detail_txt
      }).then(() => {
          this.tpStorageService.setItem('businessName', newname);

          this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
          }).catch(e => {
              this.tpStorageService.setItem('business_created', timeStamp);
          });

          this.tpStorageService.setItem('bcats', bcats);

          this.tpStorageService.setItem('business_my_referral_type_id', rtlist);
          this.tpStorageService.setItem('business_my_referral_detail', referral_incentive_detail_txt);

          //Check for paid flag and update it with userid
          let clean_email = useremail.replace(/[^0-9A-Za-z]/gi, '');
          //////console.log ("clean_email:"+clean_email);
          new Promise((resolve, reject) => {
            clean_email = clean_email.toLowerCase();//mytodo : please check on website if its lower case as well
            this.fireWebsiteUsers.child(clean_email).once('value', (snapshot_websiteUser) => {
                //////console.log (snapshot_websiteUser.val());
                //User register on website yet
                let websiteData = snapshot_websiteUser.val();
                let paidvalue = 0;
                if(websiteData && websiteData.paid>0){
                    // User has paid business account
                    this.tpStorageService.setItem('paid', "1");
                    paidvalue = 1;
                } else {
                    this.tpStorageService.setItem('paid', "0");
                     paidvalue = 0;
                }//endif
                //////console.log ("websiteData.paid");
                //////console.log (websiteData.paid);


                this.fireBusiness.child(this.userId).update({
                    paid:paidvalue,
                });

                //now update websiteuserdata  it with our id
                this.fireWebsiteUsers.child(clean_email).update({
                  userId: this.userId
                }).then(() => {
                }).catch((err) => {
                })
            });
          });
          // end website_user


          //////console.log ("Promise Resolved 1");
          resolve(true);
      }).catch((err) => {
          //////console.log ("Tapally Error 29832");
          //////console.log(err);
          resolve(false);
      });
    });
    return promise;

  }//end function

  registerBusiness_old (newname,useremail,bcats) {
    //update email
    this.tpStorageService.setItem('useremail', useremail);
    new Promise((resolve, reject) => {
      this.firedata.child(this.userId).update({
        useremail: useremail
      }).then(() => {
        resolve(true);
      }).catch((err) => {
        resolve(false);
      })
    })

    let timeStamp:string = String(Date.now());//

    //update/create business name
    var promise = new Promise((resolve, reject) => {
      this.fireBusiness.child(this.userId).update({
        displayName: newname,
        timestamp_user_local_time_updated : timeStamp
      }).then(() => {

        if(this.tpStorageService.getItem('businessName') || this.tpStorageService.getItem('businessName')!= newname){
           this.tpStorageService.setItem('businessName', "");
        }

        if(!this.tpStorageService.getItem('businessName')){
            //Business Just got registered
            //var timestamp = firebase.database.ServerValue.TIMESTAMP;
            new Promise((resolve, reject) => {
              this.fireBusiness.child(this.userId).update({
                timestamp_user_local_time_created: timeStamp
              }).then(() => {
                 resolve({ success: true });
              }).catch((err) => {
              })
            });

            this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
            }).catch(e => {
                this.tpStorageService.setItem('business_created', timeStamp);
            });
        }//endif

        this.tpStorageService.setItem('businessName', newname);
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })


        //update cat
        new Promise((resolve, reject) => {
          this.fireBusiness.child(this.userId).update({
            bcats: bcats
          }).then(() => {
            this.tpStorageService.setItem('bcats', bcats);
          }).catch((err) => {
          })
        })

    return promise;
  }//end function

  checkIfBusinessIsPaid (businessId){
    var promise_cibp = new Promise((resolve, reject) => {
      this.fireBusiness.child(businessId).once('value', (snapshot) => {
         resolve(snapshot.val());
      }).catch((err) => {
        reject(err);
      })
    })
    return promise_cibp;
  }

  getBusinessDetailsOther(uid) {
    return new Promise((resolve, reject) => {
            this.fireBusiness.child(uid).once('value', (snapshot) => {
               let res = snapshot.val();
               resolve(res);
            }).catch((err) => {
              reject(err);
            })
    })
  }//end function

  getAnotherUser(uid) {
    return new Promise((resolve, reject) => {
            this.firedata.child(uid).once('value', (snapshot) => {
               let res = snapshot.val();
               resolve(res);
            }).catch((err) => {
              reject(err);
            })
    })
  }//end function

  getBusinessDetails() {
    var promise = new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((userId__: any) => {
            this.fireBusiness.child(this.userId).once('value', (snapshot) => {
               let res = snapshot.val();
               if(res){
                     if(Object.keys(res).length>0){

                       this.tpStorageService.getItem('business_created').then((business_created_raw: any) => {
                       }).catch(e => {
                           this.tpStorageService.setItem('business_created', res['timestamp_user_local_time_created']);
                       });

                       this.tpStorageService.setItem('businessName', res['displayName']);
                       this.tpStorageService.setItem('bcats', res['bcats']);
                       if(res['paid']){
                          this.tpStorageService.setItem('paid', "1");
                       } else {
                          this.tpStorageService.setItem('paid', "-1");
                       }
                     } else {
                          this.tpStorageService.setItem('paid', "-1");
                     }
               }//endif   .
               resolve(res);
            }).catch((err) => {
              reject(err);
            })
      }).catch(e => { });

    })
    return promise;
  }//end function

  fnMarkIncentiveRedeemed_(referral_send_by, referral_received_by ,requestId) {
    var promise = new Promise((resolve, reject) => {
        this.fireReferral_sent.child(referral_send_by).child(referral_received_by).child(requestId).update({
            redeemStatus : 2,
        });

        this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(requestId).update({
            redeemStatus : 2,
        });
        resolve (true);
    })
    return promise;
  }//end function


  getReferralRecieved() {
    var promise = new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((userId__: any) => {
            this.fireReferral_sent.child(this.userId).once('value', (snapshot) => {
               resolve(snapshot.val());
            }).catch((err) => {
              reject(err);
            })
      }).catch(e => { });
    })
    return promise;
  }//end function

  getReferralRecievedByID(referral_send_by, referral_received_by, Id) {
    var promise = new Promise((resolve, reject) => {
      this.tpStorageService.getItem('userUID').then((userId__: any) => {
            if(referral_received_by == this.userId){
              //i am reciever of this referral
              this.fireReferral_received.child(referral_received_by).child(referral_send_by).child(Id).once('value', (snapshot) => {
                 resolve(snapshot.val());
              }).catch((err) => {
                reject(err);
              })

            } else {
              //following is not used right now
              //somebody else is reciever ; I probably (might not necessarily) sent
              this.fireReferral_received.child(referral_send_by).child(referral_received_by).child(Id).once('value', (snapshot) => {
                 resolve(snapshot.val());
              }).catch((err) => {
                reject(err);
              })
            }
      }).catch(e => { });
    })
    return promise;
  }//end function


}
