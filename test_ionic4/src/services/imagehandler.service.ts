import { Injectable } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Events,Platform,LoadingController, ActionSheetController, ToastController} from '@ionic/angular';
import { LoadingService } from './loading.service';
import { userPicArr } from '../app/app.angularfireconfig';
import { SQLite,SQLiteObject   } from '@ionic-native/sqlite/ngx';
import { CONST} from '../app/const';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TpstorageProvider } from '../app/tpstorage.service'; 
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture/ngx';
import { Chooser } from '@ionic-native/chooser/ngx';

@Injectable({
  providedIn: 'root'
})
export class ImagehandlerService {
  nativepath: any;
  firestore = firebase.storage();
  userId;
  constructor(

    public loadingCtrl: LoadingController,
    public loadingProvider: LoadingService,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl: ToastController,
    private imagePicker: ImagePicker,
    public pf: Platform,
    private iab: InAppBrowser,
    private mediaCapture: MediaCapture,
    private chooser: Chooser,
    public tpStorageService: TpstorageProvider
  ) {

    this.tpStorageService.getItem('userUID').then((res: any) => {
			 if(res){
				 this.userId = res;
			 } else {
         this.userId = firebase.auth().currentUser.uid;
       }
		}).catch(e => { });

  }


  uploadimage (){
    if (this.pf.is('ios')) {
        return this.uploadimage_android();
    } else {
        return this.uploadimage_android();
    }//endif
  }

  uploadimage_ios() {
    return new Promise( async (resolve, reject) => {
      let actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image From',
        buttons: [{
          text: 'Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    })
  }

/*
  async uploadimage_ios_async (){
    let actionSheet = await this.actionSheetCtrl.create({
      header: 'Select Image From',
      buttons: [{
        text: 'Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI).then((data) => {
            //resolve(data)
            return true;
          }).catch((err) => {
            //reject(err)
            return false;
          })
        }
      }, {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  async uploadimage_ios__() {
    return new Promise((resolve, reject) => {
       let ret = this.uploadimage_ios_async ();
       if(ret){
          resolve(true);
       } else {
          reject (false);
       }
    })
  }
  */

  uploadimage_android() {
    return new Promise(async (resolve, reject) => {
      let actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image From',
        buttons: [{
          text: 'Load from Gallery',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.DestinationType.FILE_URI).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    })
  }
  takePicture(sourceType, destinationType) {
    let options = {
      quality: 100,
      sourceType: sourceType,
      destinationType: destinationType,
      saveToPhotoAlbum: true,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true
    };
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();


        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          console.log ("local url");
          console.log (url);
          res.file((resFile) => {
            let reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var imageStore = this.firestore.ref('/profileimages').child(this.userId);
              imageStore.put(imgBlob).then((res) => {
                this.firestore.ref('/profileimages').child(this.userId).getDownloadURL().then((url) => {
                  console.log ("Uploaded to firestore");
                  resolve(url);
                }).catch((err) => {
                  reject(err);
                })
              }).catch((err) => {
                reject(err);
              })
            }
          })
        })
      }, (err) => {
        this.presentToast('Error while selecting image.');
        reject(err);
      });
    })
  }
  getAllIomage() {
    return new Promise((resolve, reject) => {
      this.firestore.ref('/picmsgs').child(this.userId).getDownloadURL().then((url) => {
      })
    })
  }
  picmsgstore() {
    return new Promise((resolve, reject) => {
      var options = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.FILE_URI,
        saveToPhotoAlbum: true,
        allowEdit: true,
        targetWidth: 500,
        targetHeight: 500,
        correctOrientation: true
      };

      this.camera.getPicture(options).then((results_in) => {
        let results = [];
        results[0] = results_in;
        console.log (results);

      //this.imagePicker.getPictures(options).then((results) => {
        var imageName = '';
        let imageCounter = 0;
        let responseArr = [];
        if (results.length > 0) {
          this.loadingProvider.presentLoading();
          for (var i = 0; i < results.length; i++) {
            if (this.pf.is('ios')) {
              imageName = 'file://' + results[i];
            } else {
              imageName = results[i];
            }
              //jaswinder
            (<any>window).resolveLocalFileSystemURL(imageName, (res) => {
              res.file((resFile) => {
                var reader = new FileReader();
                reader.readAsArrayBuffer(resFile);
                reader.onloadend = (evt: any) => {
                  var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
                  var uuid = this.guid();
                  var imageStore = this.firestore.ref('/picmsgs').child(this.userId).child('picmsg' + uuid);
                  imageStore.put(imgBlob).then((res) => {
                    imageCounter++;
                    responseArr.push(res.downloadURL);
                    if (results.length == imageCounter) {
                      this.loadingProvider.dismissMyLoading();
                      resolve(responseArr);
                    }
                  }).catch((err) => {
                    console.log ("Error 92123");
                    this.loadingProvider.dismissMyLoading();
                    reject(err);
                  }).catch((err) => {
                    console.log ("Error 41232");
                    this.loadingProvider.dismissMyLoading();
                    reject(err);
                  })
                }
              })
            }, (err) => {
              console.log ("Error 93823");
              console.log (err);
              this.loadingProvider.dismissMyLoading();
            })
          }
        } else {
          resolve(true)
        }
      }).catch((err) => {
        console.log ("ERROR");
        console.log (err);
        this.loadingProvider.dismissMyLoading();
      })
    }).catch((err) => {
    })
  }
  cameraPicmsgStore() {
    return new Promise((resolve, reject) => {
      // Create options for the Camera Dialog
      var options = {
        quality: 100,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.FILE_URI,
        saveToPhotoAlbum: true,
        allowEdit: true,
        targetWidth: 500,
        targetHeight: 500,
        correctOrientation: true
      };
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var uuid = this.guid();
              var imageStore = this.firestore.ref('/picmsgs').child(this.userId).child('picmsg' + uuid);
              imageStore.put(imgBlob).then((res) => {
                this.loadingProvider.dismissMyLoading();
                resolve(res.downloadURL);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      }).catch((err) => {
      });
      this.loadingProvider.dismissMyLoading();
    })

  }
  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }
  async presentToast(text) {
    let toast = await this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    await toast.present();
  }
  gpuploadimage(groupname) {
    return new Promise(async (resolve, reject) => {
      let actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image From',
        buttons: [{
          text: 'Load from Gallery',
          handler: () => {
            this.gptakePicture(this.camera.PictureSourceType.PHOTOLIBRARY, this.camera.DestinationType.FILE_URI, groupname).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Camera',
          handler: () => {
            this.gptakePicture(this.camera.PictureSourceType.CAMERA, this.camera.DestinationType.FILE_URI, groupname).then((data) => {
              resolve(data)
            }).catch((err) => {
              reject(err)
            })
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
        ]
      });
      await actionSheet.present();
    })
  }
  gptakePicture(sourceType, destinationType, groupname) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      destinationType: destinationType,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true
    };

    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((url) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(url, (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'image/jpeg' });
              var imageStore = this.firestore.ref('/groupimages').child(this.userId).child(groupname);
              imageStore.put(imgBlob).then((res) => {
                this.firestore.ref('/groupimages').child(this.userId).child(groupname).getDownloadURL().then((url) => {
                  resolve(url);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                })
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      }, (err) => {
        reject(err);
      });
    })
  }
  get_url_extension(url) {
    return url.split(/\#|\?/)[0].split('.').pop().trim();
  }
  selectDocument() {
    return new Promise((resolve, reject) => {
      this.chooser.getFile('application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet').then((url: any) => {
        if ((url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'txt') || (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'xls') || (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'xlsx') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'doc') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'docx') ||
        (url.mediaType == 'application/octet-stream' && this.get_url_extension(url.uri) == 'pdf') ||    url.mediaType == 'application/pdf' || url.mediaType == 'application/msword'|| url.mediaType == 'application/vnd.ms-excel' || url.mediaType == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || url.mediaType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
          this.loadingProvider.presentLoading();
          (<any>window).resolveLocalFileSystemURL(url.uri, (res) => {
            res.file((resFile) => {
              var reader = new FileReader();
              reader.readAsArrayBuffer(resFile);
              reader.onloadend = (evt: any) => {
                var imgBlob = new Blob([evt.target.result], { type: 'document' });
                var uuid = this.guid();
                var imageStore = this.firestore.ref('/docs').child(this.userId).child('doc' + uuid);
                imageStore.put(imgBlob).then((res) => {
                  this.loadingProvider.dismissMyLoading();
                  resolve(res.downloadURL);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                }).catch((err) => {
                  this.loadingProvider.dismissMyLoading();
                  reject(err);
                })
              }
            })
          })
        } else {
          this.presentToast('Upload only document files.');
        }
      }).catch((err) => {
        this.loadingProvider.dismissMyLoading();

      })
    })
  }
  openDocument(path) {
    return new Promise((resolve, reject) => {
      const browser = this.iab.create(path, "_system", { location: "yes" });
      browser.on('loadstop').subscribe(event => {
        resolve("File opened");
      });

    })
  }

  recordAudio() {
    return new Promise((resolve, reject) => {
      this.mediaCapture.captureAudio().then((data: MediaFile[]) => {
        this.loadingProvider.presentLoading();
        (<any>window).resolveLocalFileSystemURL(data[0]["localURL"], (res) => {
          res.file((resFile) => {
            var reader = new FileReader();
            reader.readAsArrayBuffer(resFile);
            reader.onloadend = (evt: any) => {
              var imgBlob = new Blob([evt.target.result], { type: 'audio' });
              var uuid = this.guid();
              var imageStore = this.firestore.ref('/audios').child(this.userId).child('audio' + uuid);
              imageStore.put(imgBlob).then((res) => {
                this.loadingProvider.dismissMyLoading();
                resolve(res.downloadURL);

              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              }).catch((err) => {
                this.loadingProvider.dismissMyLoading();
                reject(err);
              })
            }
          })
        })
      })
      this.loadingProvider.dismissMyLoading();
    })
  }
}
