import { NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendareferralInfoPage } from './sendareferral-info.page';

describe('SendareferralInfoPage', () => {
  let component: SendareferralInfoPage;
  let fixture: ComponentFixture<SendareferralInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendareferralInfoPage ],
      schemas: [NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendareferralInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
