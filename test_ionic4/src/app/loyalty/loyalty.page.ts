import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-loyalty',
  templateUrl: './loyalty.page.html',
  styleUrls: [
    './loyalty.page.scss',
    './loyalty.responsive.scss'
  ]
})
export class LoyaltyPage implements OnInit {

  constructor(private router: Router) {
  }
  ngOnInit() {
  }
  askForReferral (){
    console.log ("Clicked " +  new Date());
    this.router.navigateByUrl('/referral');
  }
  onDetail() {
      this.router.navigateByUrl('/loyaltydetail');
  }

}
