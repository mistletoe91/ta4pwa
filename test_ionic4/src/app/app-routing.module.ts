import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TpstorageProvider } from './tpstorage.service';
import {  Platform } from '@ionic/angular';
import { SQLite,SQLiteObject } from '@ionic-native/sqlite/ngx';
import { CONST} from './const';
import { Router } from '@angular/router';
//Default Route : walkthrough
const routes: Routes = [
  { path: '', redirectTo: '/app/loyalty', pathMatch: 'full' },
  { path: 'walkthrough', loadChildren: './walkthrough/walkthrough.module#WalkthroughPageModule' },
  { path: 'auth/login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'auth/signup', loadChildren: './signup/signup.module#SignupPageModule' },
  { path: 'auth/forgot-password', loadChildren: './forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'app', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'contact-card', loadChildren: './contact-card/contact-card.module#ContactCardPageModule' },
  { path: 'forms-and-validations', loadChildren: './forms/validations/forms-validations.module#FormsValidationsPageModule' },
  { path: 'forms-filters', loadChildren: './forms/filters/forms-filters.module#FormsFiltersPageModule' },
  { path: 'page-not-found', loadChildren: './page-not-found/page-not-found.module#PageNotFoundModule' },
  { path: 'sendareferral-info', loadChildren: './sendareferral-info/sendareferral-info.module#SendareferralInfoPageModule' },
  { path: 'loyalty', loadChildren: './loyalty/loyalty.module#LoyaltyPageModule' },
  { path: 'loyaltydetail', loadChildren: './loyaltydetail/loyaltydetail.module#LoyaltydetailPageModule' },
  { path: 'notif', loadChildren: './notif/notif.module#NotifPageModule' },
  { path: 'buddychat', loadChildren: './buddychat/buddychat.module#BuddychatPageModule' },
  { path: 'referral', loadChildren: './referral/referral.module#ReferralPageModule' },
  { path: 'invite', loadChildren: './invite/invite.module#InvitePageModule' },
  { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  { path: 'contactimportprogress', loadChildren: './contactimportprogress/contactimportprogress.module#ContactimportprogressPageModule' },
  { path: '**', redirectTo: 'page-not-found' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(
    private platform: Platform,
    public tpStorageService: TpstorageProvider,
    private sqlite: SQLite,
    private router: Router
  ) {
      console.log ("AppRoutingModule constructor");
      this.platform.ready().then((readySource) => {
          //this.dropAllStorage ();
          console.log ("Platform Ready ");
          this.checkUserAndRedirectUser  ();
      });
  }


  dropAllStorage (){
    console.log("DROP Everything and start fresh ");
    this.dropTable ('cache_users_local');
    this.dropTable ('cache_tapally_contacts');
    this.dropTable ('cache_tapally_friends');
    this.dropTable ('key_val');
    this.tpStorageService.clear();
  }

     dropTable(tbl) {
         this.sqlite.create({
           name: 'gr.db',
           location: 'default'
         }).then((db: SQLiteObject) => {
           console.log ("DROPPED TABLE "+tbl);
           db.executeSql( "DROP TABLE "+tbl, [])
           .then(res => {
           })
           .catch(e => {
               //Error in operation
           });//create table

         });//create database
    }//end function

   deleteFromTable() {
       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("DELETE   FROM cache_users_local", [])
             .then(res => {
             })
             .catch(e => {
                 //Error in operation
             });
         })
         .catch(e => {
             //Error in operation
         });//create table

       });//create database
  }//end function

  fnGotToPage (pg){
    console.log ("Redirecting to "+ pg);
    this.router.navigateByUrl(pg);
  }

   checkUserAndRedirectUser() {

                              console.log ("checkUserAndRedirectUser")    ;
                              this.tpStorageService.getItem('userUID').then((userId__: any) => {
                                 if(userId__){
                                     //user exist - now check if name exists
                                     this.tpStorageService.getItem('myDisplayName').then((user_full_name: any) => {
                                        if(user_full_name){
                                            //user full name exists
                                            this.sendToFirstTab ();
                                        } else {
                                            this.sendToDisplayName ();
                                        }
                                     }).catch(e => {
                                        this.sendToDisplayName ();
                                     });
                                 } else {
                                     console.log ("RECORD DOES NOT EXISTS");
                                     this.sendToTutorialPage ();
                                 }
                              }).catch(e => {
                                 console.log ("RECORD DOES NOT EXISTS");
                                 this.sendToTutorialPage ();//for_prod
                              });

   }//end function

   sendToFirstTab (){
     this.fnGotToPage ('/app/loyalty');// /app/loyalty
   }

   sendToDisplayName (){
     this.fnGotToPage ('forms-and-validations');// forms-and-validations
   }

   sendToTutorialPage (){
     this.fnGotToPage ('walkthrough');
   }

/*
   checkUserAndRedirectUser__OLD() {
       console.log ("CHECK1");

       this.sqlite.create({
         name: 'gr.db',
         location: 'default'
       }).then((db: SQLiteObject) => {
         console.log ("CHECK2");
         db.executeSql(CONST.create_table_statement, [])
         .then(res => {
             db.executeSql("SELECT *  FROM cache_users_local", [])
             .then(res => {
                  console.log ("CHECK 4");
                 if(res.rows.length>0){
                         console.log (res.rows.item(0));

                         //User exists so go to next page
                         if(!res.rows.item(0).uid || res.rows.item(0).uid == undefined){
                             this.fnGotToPage ('walkthrough' );
                         }

                         console.log ("App component Trying to get userUID");
                         this.tpStorageService.getItem('userUID').then((res: any) => {
                            console.log ("Inside this.tpStorageService.getItem");
                            if (res && res != undefined) {
                              console.log ("userUID Found in tpStorageService");
                            } else {
                              console.log ("userUID NOT Found in tpStorageService so setting to "+ res.rows.item(0).uid);
                              this.tpStorageService.setItem('userUID', res.rows.item(0).uid);
                            }
                     		}).catch(e => { });
                        this.tpStorageService.getItem('newname').then((res: any) => {
                           if (res && res != undefined) {
                           } else {
                             this.tpStorageService.setItem('newname', res.rows.item(0).user_name);
                           }
                       }).catch(e => { });
                       this.tpStorageService.getItem('useremail').then((res: any) => {
                          if (res && res != undefined) {
                          } else {
                            this.tpStorageService.setItem('useremail', res.rows.item(0).useremail);
                          }
                      }).catch(e => { });


                         console.log ("++++--->");
                         console.log (res.rows.item(0).user_name);


                         if(!res.rows.item(0).user_name || res.rows.item(0).user_name == undefined){
                             this.fnGotToPage  ('DisplaynamePage');
                         }
                 } else {

                         this.tpStorageService.getItem('userUID').then((userId__: any) => {
                            if(userId__){
                                //A situation when we have userId in storage but not in sqlite
                                //this.userService.saveUserIdInSqlLite (userId__);
                                this.fnGotToPage ('/app/loyalty');
                            } else {
                                console.log ("RECORD DOES NOT EXISTS");
                                this.fnGotToPage ('walkthrough');
                            }
                         }).catch(e => {
                            console.log ("RECORD DOES NOT EXISTS");
                            this.fnGotToPage ('walkthrough');//for_prod
                         });

                 }//endif
             })
             .catch(e => {
                 //Error in operation
                 console.log ("ERROR11:");
                 console.log (e);
             });

         })
         .catch(e => {
             //Error in operation
             console.log ("ERROR12:");
             console.log (e);
         });//create table

       });//create database
  }//end function
*/

}


/*
Modals :::
{ path: 'referral-modal', loadChildren: './referral-modal/referral-modal.module#ReferralModalPageModule' },
*/
