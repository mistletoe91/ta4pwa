import { Injectable } from '@angular/core';
import {  Platform } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
@Injectable({
  providedIn: 'root'
})
export class TpstorageProvider {

    /*
    storage
    localStorage,
    nativeStorage
    */
    constructor(public nativeStorage: NativeStorage,public platform: Platform) {
    }

    clear(){
      this.nativeStorage.clear();
    }

    isArray(obj : any ) {
       return Array.isArray(obj)
    }

    removeItem (nam){
       this.setItem (nam,"");
    }

    set (nam,val){
      if(this.isArray(val)){
        val  = JSON.stringify (val);
      }
      return this.setItem (nam,val);
    }

    get (nam){
      return this.getItem (nam);
    }

    setItem (nam, val){
      if(this.isArray(val)){
        val  = JSON.stringify (val);
      }
      this.nativeStorage.setItem(nam, val).then(
        () => {},
        error => {}
        );
    }

    getItem (nam){ 
      return this.nativeStorage.getItem(nam);
    }



}
