import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoyaltydetailPage } from './loyaltydetail.page';

const routes: Routes = [
  {
    path: '',
    component: LoyaltydetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoyaltydetailPage]
})
export class LoyaltydetailPageModule {}
