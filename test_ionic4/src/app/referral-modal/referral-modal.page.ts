import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
@Component({
  selector: 'app-referral-modal',
  templateUrl: './referral-modal.page.html',
  styleUrls: ['./referral-modal.page.scss'],
})
export class ReferralModalPage implements OnInit {
  validationsForm: FormGroup;
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private router: Router
  ) { }

  ngOnInit() {
      console.table(this.navParams);

      this.validationsForm = new FormGroup({
        'name': new FormControl('', Validators.required)
      });

  }
  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }


    fnGoToHomeAfterINeedTodayPage(item_id) {

      let navigationExtras: NavigationExtras = {
            queryParams: {
              cat_id: item_id
            }
      };
      console.log (item_id);
      this.router.navigate([ '/contactimportprogress'], navigationExtras );//HomeAfterINeedTodayPage
    }
}
