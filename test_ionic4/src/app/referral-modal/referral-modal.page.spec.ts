import { NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralModalPage } from './referral-modal.page';

describe('ReferralModalPage', () => {
  let component: ReferralModalPage;
  let fixture: ComponentFixture<ReferralModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralModalPage ],
      schemas: [NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
