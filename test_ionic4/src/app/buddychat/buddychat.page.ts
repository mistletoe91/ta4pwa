import { Component, ViewChild,OnInit } from '@angular/core';

@Component({
  selector: 'app-buddychat',
  templateUrl: './buddychat.page.html',
  styleUrls: ['./buddychat.page.scss'],
})
export class BuddychatPage implements OnInit {
    @ViewChild('content') private content: any;

    scrollToBottomOnInit() {
      this.content.scrollToBottom(300);
    }


  constructor() {

  }

  ngOnInit() {
     this.scrollToBottomOnInit();
  }

}
