import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { Platform} from '@ionic/angular';
import { ReferralModalPage } from '../referral-modal/referral-modal.page';
import { CatsService } from '../../services/cats.service';
@Component({
  selector: 'app-referral',
  templateUrl: './referral.page.html',
  styleUrls: ['./referral.page.scss'],
})
export class ReferralPage implements OnInit {
  myrequests = [];
  requestcounter = null;
  items;
  showheader:boolean = true;
  isVirgin:boolean = false;
  constructor(platform: Platform,public catservice: CatsService, public modalController: ModalController,private router: Router) {
    console.log ("ReferralPage : Referral Page Construction "+  new Date());
        platform.ready().then(() => {
          console.log ("ReferralPage : Platform Ready "+  new Date());
         this.initializeItems();
    });
  }
  initializeItems() {
      //this.items = this.catservice.getCats();
  }
  ngOnInit() {
     console.log ("ReferralPage : ngOnInit" +  new Date() );
  }
  askForReferral (){
    this.router.navigateByUrl('/referral');
  }
  async selCategory(item_id) {
    const modal = await this.modalController.create({
      component: ReferralModalPage,
      componentProps: {
        'item_id': item_id
      }
    });
    return await modal.present();
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
