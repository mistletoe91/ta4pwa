import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: [
    './styles/categories.page.scss',
    './styles/categories.shell.scss',
    './styles/categories.responsive.scss'
  ]
})
export class CategoriesPage {

  constructor(private router: Router) {
  }

  askForReferral (){
    this.router.navigateByUrl('/referral');
  }
  fnSendReferral (){
    console.log ("Send Referral");
  }

  fnmaincardwork (){
    console.log ("Going");
    this.router.navigateByUrl('/sendareferral-info');
  }

}
