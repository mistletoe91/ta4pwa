import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ContactimportprogressPage } from './contactimportprogress.page';

const routes: Routes = [
  {
    path: '',
    component: ContactimportprogressPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactimportprogressPage]
})
export class ContactimportprogressPageModule {}
