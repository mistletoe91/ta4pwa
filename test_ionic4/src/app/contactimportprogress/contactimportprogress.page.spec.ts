import { NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactimportprogressPage } from './contactimportprogress.page';

describe('ContactimportprogressPage', () => {
  let component: ContactimportprogressPage;
  let fixture: ComponentFixture<ContactimportprogressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactimportprogressPage ],
      schemas: [NO_ERRORS_SCHEMA,CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactimportprogressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
