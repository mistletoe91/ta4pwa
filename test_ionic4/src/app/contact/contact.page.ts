import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,NavigationExtras  } from '@angular/router';
import { ModalController, MenuController, Events,AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import {  Platform } from '@ionic/angular';
import { connreq } from '../../models/interfaces/request';
import { TpstorageProvider } from '../tpstorage.service';
import { UserService } from '../../services/user.service';
import { LoadingService } from '../../services/loading.service';
import { ContactService } from '../../services/contact.service';
import { RequestsService } from '../../services/requests.service';
import { SmsService } from '../../services/sms.service';
import { ChatService } from '../../services/chat.service';
//import { FcmService } from '../../services/fcm.service';
import { userPicArr } from '../app.angularfireconfig';
import { SQLite   } from '@ionic-native/sqlite/ngx';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  contactList: any = [];
  sourcePage: any;
	allregisteUserData: any = [];
	metchContact: any = [];
	mainArray: any = [];
	tempArray: any = [];
	myfriends;
	newrequest = {} as connreq;
	OnIsSearchBarActive:boolean = false;
	sourceImportpPage_:boolean = false;
	userpic = userPicArr;
	showheader:boolean = true;
	readContactsFromCache:boolean = false;
	cssClassChecking = 'hide';
	cssClassContacts = 'show';
	isData: boolean = false;
	reqStatus = false;
	isInviteArray: any = [];
	temparr = [];
	filteredusers = [];
	updatedRef:String;
	contactCounter = 0;
	userId;
  constructor(
    private route: ActivatedRoute,
    public events: Events,
    public router: Router,
		private platform: Platform,
		public loadingProvider: LoadingService,
		public contactProvider: ContactService,
    public alertCtrl: AlertController,
		private sqlite: SQLite,
		public userservice: UserService,
		public requestservice: RequestsService,
		public chatservice: ChatService,
    public SmsProvider: SmsService,
		public modalCtrl: ModalController,
		private toastCtrl: ToastController,
		public tpStorageService: TpstorageProvider
  ) {

    this.route.queryParams.subscribe(params => {
          if (params && params.source) {
            this.sourcePage  = params.source;
            console.log ("paramData");
            console.log (this.sourcePage);
          }
    });

  }

  ngOnInit() {
  }
  backBtn (){
    console.log ("BTN back clickd");
    this.router.navigate([ '/app/notifications'] );
    /*if(this.sourcePage == "contactimportprogresspage"){

    } else {

    }
    */
  }

  //public fcm: FcmService,

  ionViewDidEnter() {
      console.log ("Page loadi...");
      this.ionViewDidLoad_ ();
  }


  	searchuser(ev: any) {
        return this.searchuser_android (ev);
  	}

  	searchuser_android(ev: any) {
  		//initize the array
  		if(this.tempArray.length<=0){
  			this.tempArray = this.mainArray
  		} else {
  			this.mainArray = this.tempArray;
  		}//endif
  		let val = ev.target.value;
  		if (val && val.trim() != '') {
  				this.mainArray = this.mainArray.filter((item) => {
  					if (item.displayName != undefined) {
  						return (  (item.displayName.toLowerCase().indexOf(val.toLowerCase())) > -1);
  					}
  				});
      } else {
          //nothing in search
  		}//endif

  	}//end function
  tpInitilizeFromStorage (){
    this.tpStorageService.getItem('userUID').then((res: any) => {
       if(res){
         this.userId = res;
       }
    }).catch(e => { });
    this.tpStorageService.getItem('isInvitedContact').then((res: string) => {
       if(res){
         this.isInviteArray = JSON.parse(res);

       }
    }).catch(e => { });
    this.tpStorageService.getItem('updated').then((res: any) => {
       if(res){
         this.updatedRef = res;
       }
    }).catch(e => { });
  }

  	sendSMS(item) {
  		this.SmsProvider.sendSms(item.mobile);
  	}

  ionViewDidLoad_() {
        this.loadingProvider.presentLoading();
        this.tpInitilizeFromStorage ();
        this.contactProvider.getSimJsonContacts()
        .then(res => {
            console.log ("Done");
            console.log (res);

            if(res){
              /*--Start*/
              Object.keys(res).forEach(ele => {


                 if(ele == "fromcache"){

                   if(!res["fromcache"]){
                      //came from device
                      this.readContactsFromCache = true;
                      this.cssClassChecking = 'show';
                      this.cssClassContacts = 'hide';

                      this.events.subscribe('contactInsertingDone', () => {
                        this.events.unsubscribe('contactInsertingDone');
                        if(!this.contactProvider.contactInsertingDone){
                          //still inserting
                          this.readContactsFromCache = true;
                          this.cssClassChecking = 'show';
                          this.cssClassContacts = 'hide';
                        } else {
                          //done inserting
                          //this.router.navigateByUrl('/app/notifications');
                        }
                      });

                   } else {
                     //came from database
                     this.readContactsFromCache = false;
                     this.cssClassChecking = 'hide';
                     this.cssClassContacts = 'show';

                   }
                 }
                 if(ele == "contacts"){

                   this.mainArray = [];
                   this.contactCounter = 0;


                    Object.keys(res["contacts"] as any).forEach(key  => {
                      this.mainArray.push ({
                        "displayName": res["contacts"][key].displayName,
                        "mobile":  res["contacts"][key].mobile,
                        "mobile_formatted":  res["contacts"][key].mobile_formatted,
                        "status": "",
                        "isBlock":false,
                        "isUser":0,
                        "isdisable":false,
                        "dim" : false ,
                        "photoURL":res["contacts"][key].photoURL,
                        "uid":res["contacts"][key].uid
                      });
                    });

                    this.loadingProvider.dismissMyLoading();

                    //check and fix the invited one
                    this.tpStorageService.getItem('isInvitedContact').then((res: string) => {
                       if(res){
                         this.isInviteArray = JSON.parse(res);
                         for(let g=0;g<this.isInviteArray.length;g++){
                           for(let x=0;x<this.mainArray.length;x++){
                              if( this.mainArray[x].mobile == this.isInviteArray[g] ||  this.mainArray[x].mobile_formatted == this.isInviteArray[g] ){
                                this.mainArray [x].isdisable = true;
                                break;
                              }
                           }
                         }
                       }
                    }).catch(e => { });


                 }//endif
              });

              /*--End */
            }//endif




        }).catch(e => { });





  }//end function


  removeDuplicates(originalArray: any[], prop) {
    let newArray = [];
    let lookupObject = {};

    originalArray.forEach((item, index) => {
      lookupObject[originalArray[index][prop]] = originalArray[index];
    });

    Object.keys(lookupObject).forEach(element => {
      newArray.push(lookupObject[element]);
    });
    return newArray;
  }

async  refreshPage() {
    //this.ionViewDidLoad();
    //this.tpStorageService.setItem('updated', 'show');
    const confirm = await this.alertCtrl.create({
      header: 'Sync All Contacts',
      message: 'It may take few minutes to re-sync all contacts from your device. Do you agree ? ',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            //console.log('Agree clicked');
            //this.navCtrl.setRoot('ContactimportprogressPage', {source : 'ContactPage'});
            this.openContacts();
          }
        }
      ]
    });
    await confirm.present();

  }


  openContacts (){
    let navigationExtras: NavigationExtras = {
          queryParams: {
            source: "contactspage"
          }
    };
    this.router.navigate([ '/contactimportprogress'], navigationExtras );
  }//end function

  async inviteReq(recipient) {
    let alert = await this.alertCtrl.create({
      header: 'Invitation',
      subHeader: 'Invitation already sent to ' + recipient.displayName + '.',
      buttons: ['Ok']
    });
    await alert.present();
  }

  async sendreq(recipient) {
    this.reqStatus = true;
    this.newrequest.sender = this.userId;
    this.newrequest.recipient = recipient.uid;
    if (this.newrequest.sender === this.newrequest.recipient)
      alert('You are your friend always');
    else {


      let userName: any;

      this.userservice.getuserdetails().then((res: any) => {
        userName = res.displayName;

        let newMessage = userName + " has sent you friend request.";

        //this.fcm.sendNotification(recipient, newMessage, 'sendreq');
      });

      this.requestservice.sendrequest(this.newrequest).then((res: any) => {
        if (res.success) {
          this.reqStatus = false;
          /*
          let successalert = await this.alertCtrl.create({
            title: 'Request sent',
            subTitle: 'Your request has been sent to ' + recipient.displayName + '.',
            buttons: ['ok']
          });
          await successalert.present();
          */
          let sentuser = this.mainArray.indexOf(recipient);
          this.mainArray[sentuser].status = "pending";
        }
      }).catch((err) => {
      })
    }
  }

  //href="sms:{{item.mobile}}?body=Hi, I'm offering referral incentives. Please download my app at tapally.com and start earning"
  sanitizeAndSms (mobile){
    console.log ('sms:'+mobile) ;
    return 'sms:' + mobile;
  }


}
