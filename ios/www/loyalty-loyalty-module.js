(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["loyalty-loyalty-module"],{

/***/ "./src/app/loyalty/loyalty.module.ts":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.module.ts ***!
  \*******************************************/
/*! exports provided: LoyaltyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltyPageModule", function() { return LoyaltyPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _loyalty_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./loyalty.page */ "./src/app/loyalty/loyalty.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _loyalty_page__WEBPACK_IMPORTED_MODULE_5__["LoyaltyPage"]
    }
];
var LoyaltyPageModule = /** @class */ (function () {
    function LoyaltyPageModule() {
    }
    LoyaltyPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_loyalty_page__WEBPACK_IMPORTED_MODULE_5__["LoyaltyPage"]]
        })
    ], LoyaltyPageModule);
    return LoyaltyPageModule;
}());



/***/ }),

/***/ "./src/app/loyalty/loyalty.page.html":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      Earn Loyality\n    </ion-title>\n    <ion-icon slot=\"end\" class=\"iconright\"   (click)=\"askForReferral()\"  name=\"add-circle\"></ion-icon>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <!-- fab placed to the bottom start -->\n  <ion-fab   vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button color=\"tertiary\">\n      <ion-icon name=\"business\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n\n\n  <ion-row class=\"categories-list\">\n    <ion-col class=\"category-item travel-category firstitem \" size=\"12\">\n      <div class=\"category-anchor centertext\">\n            <h2 class=\"category-title\">\n              <ion-icon name=\"barcode\" class=\"iconmain\"></ion-icon>\n              <br> Tap & Scan Loyality\n              <br><small>Scan To Earn Loyality <br>From Businesses </small>\n            </h2>\n      </div>\n    </ion-col>\n\n    <ion-col class=\"sometopmargin\" size=\"12\">\n\n      <ion-list detail=\"true\">\n        <ion-list-header>\n          Loyality History\n        </ion-list-header>\n\n        <!-- Searchbar with a placeholder -->\n        <ion-searchbar placeholder=\"Search Loyality History\"></ion-searchbar>\n        <ion-item (click)=\"onDetail()\" detail=\"true\"><ion-label>Quantum Insurance</ion-label> <ion-note slot=\"end\"> 11 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>ABC Realtor</ion-label> <ion-note slot=\"end\"> 1 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Kitchen Cabinet</ion-label> <ion-note slot=\"end\"> 5 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Accounting Firm</ion-label> <ion-note slot=\"end\"> 18 Scans </ion-note></ion-item>\n        <ion-item (click)=\"onDetail()\" detail=\"true\"><ion-label>Quantum Insurance</ion-label> <ion-note slot=\"end\"> 11 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>ABC Realtor</ion-label> <ion-note slot=\"end\"> 1 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Kitchen Cabinet</ion-label> <ion-note slot=\"end\"> 5 Scans </ion-note></ion-item>\n        <ion-item click=\"onDetail()\" detail=\"true\"><ion-label>Accounting Firm</ion-label> <ion-note slot=\"end\"> 18 Scans </ion-note></ion-item>\n      </ion-list>\n\n\n    </ion-col>\n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/loyalty/loyalty.page.scss":
/*!*******************************************!*\
  !*** ./src/app/loyalty/loyalty.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-narrow-margin);\n  --page-categories-gutter: calc(var(--page-margin) / 4);\n  --page-category-background: var(--ion-color-medium);\n  --page-category-background-rgb: var(--ion-color-medium-rgb); }\n\n.sometopmargin {\n  margin-top: 15px; }\n\n.businessitem {\n  margin-top: 30px !important; }\n\n.firstitem {\n  margin-top: 18px !important;\n  margin-bottom: 0px !important; }\n\nion-card-content {\n  margin-top: 0px !important; }\n\n.category-cover1,\n.category-cover2,\n.category-cover3 {\n  text-align: center; }\n\n.iconmain {\n  font-size: 50px;\n  margin: 0 auto; }\n\n.categories-list {\n  --ion-grid-column-padding: var(--page-categories-gutter);\n  padding: calc(var(--page-categories-gutter) * 3);\n  height: 100%;\n  align-content: flex-start;\n  overflow: scroll; }\n\n.categories-list .category-item .category-anchor {\n    height: 100%;\n    text-decoration: none;\n    display: -webkit-box;\n    display: flex;\n    -webkit-box-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align: start;\n            align-items: flex-start; }\n\n.categories-list .category-item .category-anchor .category-title {\n      margin: auto;\n      text-transform: uppercase;\n      font-weight: 400;\n      font-size: 18px;\n      letter-spacing: 1px;\n      padding: calc((var(--page-margin) / 4) * 3) var(--page-margin);\n      color: var(--ion-color-lightest);\n      background-color: var(--page-category-background);\n      border-radius: var(--app-fair-radius); }\n\n.categories-list .travel-category {\n    --page-category-background: #00AFFF;\n    --page-category-background-rgb: 0,175,255; }\n\n.categories-list .fashion-category {\n    --page-category-background: #cb328f;\n    --page-category-background-rgb: 203,50,143; }\n\n.categories-list .food-category {\n    --page-category-background: #ebbb00;\n    --page-category-background-rgb: 235,187,0; }\n\n.categories-list .deals-category {\n    --page-category-background: #70df70;\n    --page-category-background-rgb: 112,223,112; }\n\n.categories-list .real-state-category {\n    --page-category-background: #d9453a;\n    --page-category-background-rgb: 217,69,58; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvbG95YWx0eS9sb3lhbHR5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHVDQUFjO0VBRWQsc0RBQXlCO0VBRXpCLG1EQUEyQjtFQUMzQiwyREFBK0IsRUFBQTs7QUFFakM7RUFDRSxnQkFBZ0IsRUFBQTs7QUFFbEI7RUFDRSwyQkFBMkIsRUFBQTs7QUFHN0I7RUFDRSwyQkFBMkI7RUFDM0IsNkJBQTZCLEVBQUE7O0FBRS9CO0VBQ0UsMEJBQTBCLEVBQUE7O0FBRTVCOzs7RUFHRSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUtoQjtFQUNFLHdEQUEwQjtFQUUxQixnREFBZ0Q7RUFDaEQsWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixnQkFBZ0IsRUFBQTs7QUFObEI7SUFVTSxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLG9CQUFhO0lBQWIsYUFBYTtJQUNiLHVCQUEyQjtZQUEzQiwyQkFBMkI7SUFDM0Isd0JBQXVCO1lBQXZCLHVCQUF1QixFQUFBOztBQWQ3QjtNQWlCUSxZQUFZO01BQ1oseUJBQXlCO01BQ3pCLGdCQUFnQjtNQUNoQixlQUFlO01BQ2YsbUJBQW1CO01BQ25CLDhEQUE4RDtNQUM5RCxnQ0FBZ0M7TUFDaEMsaURBQWlEO01BQ2pELHFDQUFxQyxFQUFBOztBQXpCN0M7SUErQkksbUNBQTJCO0lBQzNCLHlDQUErQixFQUFBOztBQWhDbkM7SUFvQ0ksbUNBQTJCO0lBQzNCLDBDQUErQixFQUFBOztBQXJDbkM7SUF5Q0ksbUNBQTJCO0lBQzNCLHlDQUErQixFQUFBOztBQTFDbkM7SUE4Q0ksbUNBQTJCO0lBQzNCLDJDQUErQixFQUFBOztBQS9DbkM7SUFtREksbUNBQTJCO0lBQzNCLHlDQUErQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbG95YWx0eS9sb3lhbHR5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAvLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLW5hcnJvdy1tYXJnaW4pO1xuXG4gIC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyA0KTtcblxuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1yZ2IpO1xufVxuLnNvbWV0b3BtYXJnaW4ge1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuLmJ1c2luZXNzaXRlbSB7XG4gIG1hcmdpbi10b3A6IDMwcHggIWltcG9ydGFudDtcbn1cblxuLmZpcnN0aXRlbSB7XG4gIG1hcmdpbi10b3A6IDE4cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY2FyZC1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG59XG4uY2F0ZWdvcnktY292ZXIxLFxuLmNhdGVnb3J5LWNvdmVyMixcbi5jYXRlZ29yeS1jb3ZlcjMge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pY29ubWFpbiB7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cblxuLy8gTm90ZTogIEFsbCB0aGUgQ1NTIHZhcmlhYmxlcyBkZWZpbmVkIGJlbG93IGFyZSBvdmVycmlkZXMgb2YgSW9uaWMgZWxlbWVudHMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG4uY2F0ZWdvcmllcy1saXN0IHtcbiAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogdmFyKC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcik7XG5cbiAgcGFkZGluZzogY2FsYyh2YXIoLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyKSAqIDMpO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG5cbiAgLmNhdGVnb3J5LWl0ZW0ge1xuICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICAgICAgcGFkZGluZzogY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pIC8gNCkgKiAzKSB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtZmFpci1yYWRpdXMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC50cmF2ZWwtY2F0ZWdvcnkge1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjMDBBRkZGO1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMCwxNzUsMjU1O1xuICB9XG5cbiAgLmZhc2hpb24tY2F0ZWdvcnkge1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjY2IzMjhmO1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMjAzLDUwLDE0MztcbiAgfVxuXG4gIC5mb29kLWNhdGVnb3J5IHtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogI2ViYmIwMDtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIzNSwxODcsMDtcbiAgfVxuXG4gIC5kZWFscy1jYXRlZ29yeSB7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICM3MGRmNzA7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAxMTIsMjIzLDExMjtcbiAgfVxuXG4gIC5yZWFsLXN0YXRlLWNhdGVnb3J5IHtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogI2Q5NDUzYTtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIxNyw2OSw1ODtcbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/loyalty/loyalty.page.ts":
/*!*****************************************!*\
  !*** ./src/app/loyalty/loyalty.page.ts ***!
  \*****************************************/
/*! exports provided: LoyaltyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoyaltyPage", function() { return LoyaltyPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoyaltyPage = /** @class */ (function () {
    function LoyaltyPage(router) {
        this.router = router;
    }
    LoyaltyPage.prototype.ngOnInit = function () {
    };
    LoyaltyPage.prototype.askForReferral = function () {
        this.router.navigateByUrl('/referral');
    };
    LoyaltyPage.prototype.onDetail = function () {
        this.router.navigateByUrl('/loyaltydetail');
    };
    LoyaltyPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-loyalty',
            template: __webpack_require__(/*! ./loyalty.page.html */ "./src/app/loyalty/loyalty.page.html"),
            styles: [__webpack_require__(/*! ./loyalty.page.scss */ "./src/app/loyalty/loyalty.page.scss"), __webpack_require__(/*! ./loyalty.responsive.scss */ "./src/app/loyalty/loyalty.responsive.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LoyaltyPage);
    return LoyaltyPage;
}());



/***/ }),

/***/ "./src/app/loyalty/loyalty.responsive.scss":
/*!*************************************************!*\
  !*** ./src/app/loyalty/loyalty.responsive.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* ----------- iPhone 4 and 4S ----------- */\n@media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 2 / 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 16px; } }\n/* ----------- iPhone 5, 5S, 5C and 5SE ----------- */\n/* ----------- iPhone 6, 6S, 7 and 8 ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n/* ----------- iPhone X ----------- */\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n/* ----------- iPhone 6+, 7+ and 8+ ----------- */\n@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list .category-item .category-anchor .category-title {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2); } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvbG95YWx0eS9sb3lhbHR5LnJlc3BvbnNpdmUuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQSw0Q0FBQTtBQUNBO0VBVUU7SUFJUSxlQUFlLEVBQUEsRUFDaEI7QUFNVCxxREFBQTtBQWNBLGtEQUFBO0FBQ0E7RUFTRTtJQUlRLGVBQWU7SUFDZiw4REFBOEQsRUFBQSxFQUMvRDtBQU1ULHFDQUFBO0FBQ0E7RUFTRTtJQUlRLGVBQWU7SUFDZiw4REFBOEQsRUFBQSxFQUMvRDtBQU1ULGlEQUFBO0FBQ0E7RUFTRTtJQUlRLGVBQWU7SUFDZiw4REFBOEQsRUFBQSxFQUMvRCIsImZpbGUiOiJzcmMvYXBwL2xveWFsdHkvbG95YWx0eS5yZXNwb25zaXZlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAoTm90ZTogRG9uJ3QgY2hhbmdlIHRoZSBvcmRlciBvZiB0aGUgZGV2aWNlcyBhcyBpdCBtYXkgYnJlYWsgdGhlIGNvcnJlY3QgY3NzIHByZWNlZGVuY2UpXG5cbi8vIChzZWU6IGh0dHBzOi8vY3NzLXRyaWNrcy5jb20vc25pcHBldHMvY3NzL21lZGlhLXF1ZXJpZXMtZm9yLXN0YW5kYXJkLWRldmljZXMvI2lwaG9uZS1xdWVyaWVzKVxuLy8gKHNlZTogaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzQ3NzUwMjYxLzExMTY5NTkpXG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA0IGFuZCA0UyAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA0ODBweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXG4gIGFuZCAoZGV2aWNlLWFzcGVjdC1yYXRpbzogMi8zKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNSwgNVMsIDVDIGFuZCA1U0UgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzMjBweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNTY4cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICBhbmQgKGRldmljZS1hc3BlY3QtcmF0aW86IDQwIC8gNzEpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBwb3J0cmFpdDpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSlcbntcblxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNiwgNlMsIDcgYW5kIDggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNjY3cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKSA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLyogLS0tLS0tLS0tLS0gaVBob25lIFggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogODEycHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvIDogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2KywgNysgYW5kIDgrIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogNDE0cHgpXG4gIGFuZCAobWF4LWRldmljZS13aWR0aCA6IDczNnB4KVxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0= */"

/***/ })

}]);
//# sourceMappingURL=loyalty-loyalty-module.js.map