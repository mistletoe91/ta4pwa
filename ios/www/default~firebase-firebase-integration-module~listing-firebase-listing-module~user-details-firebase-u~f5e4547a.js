(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~firebase-firebase-integration-module~listing-firebase-listing-module~user-details-firebase-u~f5e4547a"],{

/***/ "./node_modules/@angular/fire/firestore/collection-group/collection-group.js":
/*!***********************************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/collection-group/collection-group.js ***!
  \***********************************************************************************/
/*! exports provided: AngularFirestoreCollectionGroup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollectionGroup", function() { return AngularFirestoreCollectionGroup; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _observable_fromRef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../observable/fromRef */ "./node_modules/@angular/fire/firestore/observable/fromRef.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _collection_collection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../collection/collection */ "./node_modules/@angular/fire/firestore/collection/collection.js");
/* harmony import */ var _collection_changes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../collection/changes */ "./node_modules/@angular/fire/firestore/collection/changes.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");






var AngularFirestoreCollectionGroup = (function () {
    function AngularFirestoreCollectionGroup(query, afs) {
        this.query = query;
        this.afs = afs;
    }
    AngularFirestoreCollectionGroup.prototype.stateChanges = function (events) {
        if (!events || events.length === 0) {
            return this.afs.scheduler.keepUnstableUntilFirst(this.afs.scheduler.runOutsideAngular(Object(_collection_changes__WEBPACK_IMPORTED_MODULE_4__["docChanges"])(this.query)));
        }
        return this.afs.scheduler.keepUnstableUntilFirst(this.afs.scheduler.runOutsideAngular(Object(_collection_changes__WEBPACK_IMPORTED_MODULE_4__["docChanges"])(this.query)))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) { return actions.filter(function (change) { return events.indexOf(change.type) > -1; }); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (changes) { return changes.length > 0; }));
    };
    AngularFirestoreCollectionGroup.prototype.auditTrail = function (events) {
        return this.stateChanges(events).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["scan"])(function (current, action) { return current.concat(action); }, []));
    };
    AngularFirestoreCollectionGroup.prototype.snapshotChanges = function (events) {
        var validatedEvents = Object(_collection_collection__WEBPACK_IMPORTED_MODULE_3__["validateEventsArray"])(events);
        var sortedChanges$ = Object(_collection_changes__WEBPACK_IMPORTED_MODULE_4__["sortedChanges"])(this.query, validatedEvents);
        var scheduledSortedChanges$ = this.afs.scheduler.runOutsideAngular(sortedChanges$);
        return this.afs.scheduler.keepUnstableUntilFirst(scheduledSortedChanges$);
    };
    AngularFirestoreCollectionGroup.prototype.valueChanges = function () {
        var fromCollectionRef$ = Object(_observable_fromRef__WEBPACK_IMPORTED_MODULE_1__["fromCollectionRef"])(this.query);
        var scheduled$ = this.afs.scheduler.runOutsideAngular(fromCollectionRef$);
        return this.afs.scheduler.keepUnstableUntilFirst(scheduled$)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) { return actions.payload.docs.map(function (a) { return a.data(); }); }));
    };
    AngularFirestoreCollectionGroup.prototype.get = function (options) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["from"])(this.query.get(options)).pipe(Object(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["runInZone"])(this.afs.scheduler.zone));
    };
    return AngularFirestoreCollectionGroup;
}());

//# sourceMappingURL=collection-group.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/collection/changes.js":
/*!********************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/collection/changes.js ***!
  \********************************************************************/
/*! exports provided: docChanges, sortedChanges, combineChanges, combineChange */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "docChanges", function() { return docChanges; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sortedChanges", function() { return sortedChanges; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "combineChanges", function() { return combineChanges; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "combineChange", function() { return combineChange; });
/* harmony import */ var _observable_fromRef__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../observable/fromRef */ "./node_modules/@angular/fire/firestore/observable/fromRef.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");


function docChanges(query) {
    return Object(_observable_fromRef__WEBPACK_IMPORTED_MODULE_0__["fromCollectionRef"])(query)
        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (action) {
        return action.payload.docChanges()
            .map(function (change) { return ({ type: change.type, payload: change }); });
    }));
}
function sortedChanges(query, events) {
    return Object(_observable_fromRef__WEBPACK_IMPORTED_MODULE_0__["fromCollectionRef"])(query)
        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (changes) { return changes.payload.docChanges(); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["scan"])(function (current, changes) { return combineChanges(current, changes, events); }, []), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (changes) { return changes.map(function (c) { return ({ type: c.type, payload: c }); }); }));
}
function combineChanges(current, changes, events) {
    changes.forEach(function (change) {
        if (events.indexOf(change.type) > -1) {
            current = combineChange(current, change);
        }
    });
    return current;
}
function combineChange(combined, change) {
    switch (change.type) {
        case 'added':
            if (combined[change.newIndex] && combined[change.newIndex].doc.ref.isEqual(change.doc.ref)) {
            }
            else {
                combined.splice(change.newIndex, 0, change);
            }
            break;
        case 'modified':
            if (combined[change.oldIndex] == null || combined[change.oldIndex].doc.ref.isEqual(change.doc.ref)) {
                if (change.oldIndex !== change.newIndex) {
                    combined.splice(change.oldIndex, 1);
                    combined.splice(change.newIndex, 0, change);
                }
                else {
                    combined.splice(change.newIndex, 1, change);
                }
            }
            break;
        case 'removed':
            if (combined[change.oldIndex] && combined[change.oldIndex].doc.ref.isEqual(change.doc.ref)) {
                combined.splice(change.oldIndex, 1);
            }
            break;
    }
    return combined;
}
//# sourceMappingURL=changes.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/collection/collection.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/collection/collection.js ***!
  \***********************************************************************/
/*! exports provided: validateEventsArray, AngularFirestoreCollection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateEventsArray", function() { return validateEventsArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollection", function() { return AngularFirestoreCollection; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _observable_fromRef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../observable/fromRef */ "./node_modules/@angular/fire/firestore/observable/fromRef.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _changes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./changes */ "./node_modules/@angular/fire/firestore/collection/changes.js");
/* harmony import */ var _document_document__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../document/document */ "./node_modules/@angular/fire/firestore/document/document.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};






function validateEventsArray(events) {
    if (!events || events.length === 0) {
        events = ['added', 'removed', 'modified'];
    }
    return events;
}
var AngularFirestoreCollection = (function () {
    function AngularFirestoreCollection(ref, query, afs) {
        this.ref = ref;
        this.query = query;
        this.afs = afs;
    }
    AngularFirestoreCollection.prototype.stateChanges = function (events) {
        if (!events || events.length === 0) {
            return this.afs.scheduler.keepUnstableUntilFirst(this.afs.scheduler.runOutsideAngular(Object(_changes__WEBPACK_IMPORTED_MODULE_3__["docChanges"])(this.query)));
        }
        return this.afs.scheduler.keepUnstableUntilFirst(this.afs.scheduler.runOutsideAngular(Object(_changes__WEBPACK_IMPORTED_MODULE_3__["docChanges"])(this.query)))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) { return actions.filter(function (change) { return events.indexOf(change.type) > -1; }); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (changes) { return changes.length > 0; }));
    };
    AngularFirestoreCollection.prototype.auditTrail = function (events) {
        return this.stateChanges(events).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["scan"])(function (current, action) { return current.concat(action); }, []));
    };
    AngularFirestoreCollection.prototype.snapshotChanges = function (events) {
        var validatedEvents = validateEventsArray(events);
        var sortedChanges$ = Object(_changes__WEBPACK_IMPORTED_MODULE_3__["sortedChanges"])(this.query, validatedEvents);
        var scheduledSortedChanges$ = this.afs.scheduler.runOutsideAngular(sortedChanges$);
        return this.afs.scheduler.keepUnstableUntilFirst(scheduledSortedChanges$);
    };
    AngularFirestoreCollection.prototype.valueChanges = function (options) {
        if (options === void 0) { options = {}; }
        var fromCollectionRef$ = Object(_observable_fromRef__WEBPACK_IMPORTED_MODULE_1__["fromCollectionRef"])(this.query);
        var scheduled$ = this.afs.scheduler.runOutsideAngular(fromCollectionRef$);
        return this.afs.scheduler.keepUnstableUntilFirst(scheduled$)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (actions) { return actions.payload.docs.map(function (a) {
            var _a;
            if (options.idField) {
                return __assign({}, a.data(), (_a = {}, _a[options.idField] = a.id, _a));
            }
            else {
                return a.data();
            }
        }); }));
    };
    AngularFirestoreCollection.prototype.get = function (options) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["from"])(this.query.get(options)).pipe(Object(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["runInZone"])(this.afs.scheduler.zone));
    };
    AngularFirestoreCollection.prototype.add = function (data) {
        return this.ref.add(data);
    };
    AngularFirestoreCollection.prototype.doc = function (path) {
        return new _document_document__WEBPACK_IMPORTED_MODULE_4__["AngularFirestoreDocument"](this.ref.doc(path), this.afs);
    };
    return AngularFirestoreCollection;
}());

//# sourceMappingURL=collection.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/document/document.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/document/document.js ***!
  \*******************************************************************/
/*! exports provided: AngularFirestoreDocument */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreDocument", function() { return AngularFirestoreDocument; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _observable_fromRef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../observable/fromRef */ "./node_modules/@angular/fire/firestore/observable/fromRef.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../firestore */ "./node_modules/@angular/fire/firestore/firestore.js");
/* harmony import */ var _collection_collection__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../collection/collection */ "./node_modules/@angular/fire/firestore/collection/collection.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");






var AngularFirestoreDocument = (function () {
    function AngularFirestoreDocument(ref, afs) {
        this.ref = ref;
        this.afs = afs;
    }
    AngularFirestoreDocument.prototype.set = function (data, options) {
        return this.ref.set(data, options);
    };
    AngularFirestoreDocument.prototype.update = function (data) {
        return this.ref.update(data);
    };
    AngularFirestoreDocument.prototype.delete = function () {
        return this.ref.delete();
    };
    AngularFirestoreDocument.prototype.collection = function (path, queryFn) {
        var collectionRef = this.ref.collection(path);
        var _a = Object(_firestore__WEBPACK_IMPORTED_MODULE_3__["associateQuery"])(collectionRef, queryFn), ref = _a.ref, query = _a.query;
        return new _collection_collection__WEBPACK_IMPORTED_MODULE_4__["AngularFirestoreCollection"](ref, query, this.afs);
    };
    AngularFirestoreDocument.prototype.snapshotChanges = function () {
        var fromDocRef$ = Object(_observable_fromRef__WEBPACK_IMPORTED_MODULE_1__["fromDocRef"])(this.ref);
        var scheduledFromDocRef$ = this.afs.scheduler.runOutsideAngular(fromDocRef$);
        return this.afs.scheduler.keepUnstableUntilFirst(scheduledFromDocRef$);
    };
    AngularFirestoreDocument.prototype.valueChanges = function () {
        return this.snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (action) {
            return action.payload.data();
        }));
    };
    AngularFirestoreDocument.prototype.get = function (options) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["from"])(this.ref.get(options)).pipe(Object(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["runInZone"])(this.afs.scheduler.zone));
    };
    return AngularFirestoreDocument;
}());

//# sourceMappingURL=document.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/firestore.js":
/*!***********************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/firestore.js ***!
  \***********************************************************/
/*! exports provided: EnablePersistenceToken, PersistenceSettingsToken, FirestoreSettingsToken, DefaultFirestoreSettings, associateQuery, AngularFirestore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnablePersistenceToken", function() { return EnablePersistenceToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PersistenceSettingsToken", function() { return PersistenceSettingsToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirestoreSettingsToken", function() { return FirestoreSettingsToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DefaultFirestoreSettings", function() { return DefaultFirestoreSettings; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "associateQuery", function() { return associateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFirestore", function() { return AngularFirestore; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _document_document__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./document/document */ "./node_modules/@angular/fire/firestore/document/document.js");
/* harmony import */ var _collection_collection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./collection/collection */ "./node_modules/@angular/fire/firestore/collection/collection.js");
/* harmony import */ var _collection_group_collection_group__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./collection-group/collection-group */ "./node_modules/@angular/fire/firestore/collection-group/collection-group.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};








var EnablePersistenceToken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('angularfire2.enableFirestorePersistence');
var PersistenceSettingsToken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('angularfire2.firestore.persistenceSettings');
var FirestoreSettingsToken = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('angularfire2.firestore.settings');
var major = parseInt(firebase_app__WEBPACK_IMPORTED_MODULE_7___default.a.SDK_VERSION.split('.')[0]);
var minor = parseInt(firebase_app__WEBPACK_IMPORTED_MODULE_7___default.a.SDK_VERSION.split('.')[1]);
var DefaultFirestoreSettings = ((major < 5 || (major == 5 && minor < 8)) ? { timestampsInSnapshots: true } : {});
function associateQuery(collectionRef, queryFn) {
    if (queryFn === void 0) { queryFn = function (ref) { return ref; }; }
    var query = queryFn(collectionRef);
    var ref = collectionRef;
    return { query: query, ref: ref };
}
var AngularFirestore = (function () {
    function AngularFirestore(options, nameOrConfig, shouldEnablePersistence, settings, platformId, zone, persistenceSettings) {
        var _this = this;
        this.scheduler = new _angular_fire__WEBPACK_IMPORTED_MODULE_5__["FirebaseZoneScheduler"](zone, platformId);
        this.firestore = zone.runOutsideAngular(function () {
            var app = Object(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["_firebaseAppFactory"])(options, nameOrConfig);
            var firestore = app.firestore();
            firestore.settings(settings || DefaultFirestoreSettings);
            return firestore;
        });
        if (shouldEnablePersistence && !Object(_angular_common__WEBPACK_IMPORTED_MODULE_6__["isPlatformServer"])(platformId)) {
            var enablePersistence = function () {
                try {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["from"])(_this.firestore.enablePersistence(persistenceSettings || undefined).then(function () { return true; }, function () { return false; }));
                }
                catch (e) {
                    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(false);
                }
            };
            this.persistenceEnabled$ = zone.runOutsideAngular(enablePersistence);
        }
        else {
            this.persistenceEnabled$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(false);
        }
    }
    AngularFirestore.prototype.collection = function (pathOrRef, queryFn) {
        var collectionRef;
        if (typeof pathOrRef === 'string') {
            collectionRef = this.firestore.collection(pathOrRef);
        }
        else {
            collectionRef = pathOrRef;
        }
        var _a = associateQuery(collectionRef, queryFn), ref = _a.ref, query = _a.query;
        return new _collection_collection__WEBPACK_IMPORTED_MODULE_3__["AngularFirestoreCollection"](ref, query, this);
    };
    AngularFirestore.prototype.collectionGroup = function (collectionId, queryGroupFn) {
        if (major < 6) {
            throw "collection group queries require Firebase JS SDK >= 6.0";
        }
        var queryFn = queryGroupFn || (function (ref) { return ref; });
        var firestore = this.firestore;
        var collectionGroup = firestore.collectionGroup(collectionId);
        return new _collection_group_collection_group__WEBPACK_IMPORTED_MODULE_4__["AngularFirestoreCollectionGroup"](queryFn(collectionGroup), this);
    };
    AngularFirestore.prototype.doc = function (pathOrRef) {
        var ref;
        if (typeof pathOrRef === 'string') {
            ref = this.firestore.doc(pathOrRef);
        }
        else {
            ref = pathOrRef;
        }
        return new _document_document__WEBPACK_IMPORTED_MODULE_2__["AngularFirestoreDocument"](ref, this);
    };
    AngularFirestore.prototype.createId = function () {
        return this.firestore.collection('_').doc().id;
    };
    AngularFirestore = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["FirebaseOptionsToken"])),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_fire__WEBPACK_IMPORTED_MODULE_5__["FirebaseNameOrConfigToken"])),
        __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), __param(2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(EnablePersistenceToken)),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(FirestoreSettingsToken)),
        __param(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"])),
        __param(6, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), __param(6, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(PersistenceSettingsToken)),
        __metadata("design:paramtypes", [Object, Object, Object, Object, Object,
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"], Object])
    ], AngularFirestore);
    return AngularFirestore;
}());

//# sourceMappingURL=firestore.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/firestore.module.js":
/*!******************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/firestore.module.js ***!
  \******************************************************************/
/*! exports provided: AngularFirestoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreModule", function() { return AngularFirestoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firestore */ "./node_modules/@angular/fire/firestore/firestore.js");
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/firestore */ "./node_modules/firebase/firestore/dist/index.esm.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AngularFirestoreModule = (function () {
    function AngularFirestoreModule() {
    }
    AngularFirestoreModule_1 = AngularFirestoreModule;
    AngularFirestoreModule.enablePersistence = function (persistenceSettings) {
        return {
            ngModule: AngularFirestoreModule_1,
            providers: [
                { provide: _firestore__WEBPACK_IMPORTED_MODULE_1__["EnablePersistenceToken"], useValue: true },
                { provide: _firestore__WEBPACK_IMPORTED_MODULE_1__["PersistenceSettingsToken"], useValue: persistenceSettings },
            ]
        };
    };
    var AngularFirestoreModule_1;
    AngularFirestoreModule = AngularFirestoreModule_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            providers: [_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]]
        })
    ], AngularFirestoreModule);
    return AngularFirestoreModule;
}());

//# sourceMappingURL=firestore.module.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/index.js ***!
  \*******************************************************/
/*! exports provided: EnablePersistenceToken, PersistenceSettingsToken, FirestoreSettingsToken, DefaultFirestoreSettings, associateQuery, AngularFirestore, AngularFirestoreModule, validateEventsArray, AngularFirestoreCollection, AngularFirestoreCollectionGroup, AngularFirestoreDocument, docChanges, sortedChanges, combineChanges, combineChange, fromRef, fromDocRef, fromCollectionRef */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _public_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./public_api */ "./node_modules/@angular/fire/firestore/public_api.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EnablePersistenceToken", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["EnablePersistenceToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PersistenceSettingsToken", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["PersistenceSettingsToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FirestoreSettingsToken", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["FirestoreSettingsToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultFirestoreSettings", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["DefaultFirestoreSettings"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "associateQuery", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["associateQuery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestore", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["AngularFirestore"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreModule", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["AngularFirestoreModule"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "validateEventsArray", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["validateEventsArray"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollection", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["AngularFirestoreCollection"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollectionGroup", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["AngularFirestoreCollectionGroup"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreDocument", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["AngularFirestoreDocument"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "docChanges", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["docChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sortedChanges", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["sortedChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "combineChanges", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["combineChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "combineChange", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["combineChange"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromRef", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["fromRef"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromDocRef", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["fromDocRef"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromCollectionRef", function() { return _public_api__WEBPACK_IMPORTED_MODULE_0__["fromCollectionRef"]; });


//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/observable/fromRef.js":
/*!********************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/observable/fromRef.js ***!
  \********************************************************************/
/*! exports provided: fromRef, fromDocRef, fromCollectionRef */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fromRef", function() { return fromRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fromDocRef", function() { return fromDocRef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fromCollectionRef", function() { return fromCollectionRef; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");


function _fromRef(ref) {
    return new rxjs__WEBPACK_IMPORTED_MODULE_0__["Observable"](function (subscriber) {
        var unsubscribe = ref.onSnapshot(subscriber);
        return { unsubscribe: unsubscribe };
    });
}
function fromRef(ref) {
    return _fromRef(ref).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["share"])());
}
function fromDocRef(ref) {
    return fromRef(ref)
        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (payload) { return ({ payload: payload, type: 'value' }); }));
}
function fromCollectionRef(ref) {
    return fromRef(ref).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (payload) { return ({ payload: payload, type: 'query' }); }));
}
//# sourceMappingURL=fromRef.js.map

/***/ }),

/***/ "./node_modules/@angular/fire/firestore/public_api.js":
/*!************************************************************!*\
  !*** ./node_modules/@angular/fire/firestore/public_api.js ***!
  \************************************************************/
/*! exports provided: EnablePersistenceToken, PersistenceSettingsToken, FirestoreSettingsToken, DefaultFirestoreSettings, associateQuery, AngularFirestore, AngularFirestoreModule, validateEventsArray, AngularFirestoreCollection, AngularFirestoreCollectionGroup, AngularFirestoreDocument, docChanges, sortedChanges, combineChanges, combineChange, fromRef, fromDocRef, fromCollectionRef */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _firestore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firestore */ "./node_modules/@angular/fire/firestore/firestore.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "EnablePersistenceToken", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["EnablePersistenceToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PersistenceSettingsToken", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["PersistenceSettingsToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FirestoreSettingsToken", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["FirestoreSettingsToken"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DefaultFirestoreSettings", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["DefaultFirestoreSettings"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "associateQuery", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["associateQuery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestore", function() { return _firestore__WEBPACK_IMPORTED_MODULE_0__["AngularFirestore"]; });

/* harmony import */ var _firestore_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./firestore.module */ "./node_modules/@angular/fire/firestore/firestore.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreModule", function() { return _firestore_module__WEBPACK_IMPORTED_MODULE_1__["AngularFirestoreModule"]; });

/* harmony import */ var _collection_collection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./collection/collection */ "./node_modules/@angular/fire/firestore/collection/collection.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "validateEventsArray", function() { return _collection_collection__WEBPACK_IMPORTED_MODULE_2__["validateEventsArray"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollection", function() { return _collection_collection__WEBPACK_IMPORTED_MODULE_2__["AngularFirestoreCollection"]; });

/* harmony import */ var _collection_group_collection_group__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./collection-group/collection-group */ "./node_modules/@angular/fire/firestore/collection-group/collection-group.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreCollectionGroup", function() { return _collection_group_collection_group__WEBPACK_IMPORTED_MODULE_3__["AngularFirestoreCollectionGroup"]; });

/* harmony import */ var _document_document__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./document/document */ "./node_modules/@angular/fire/firestore/document/document.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AngularFirestoreDocument", function() { return _document_document__WEBPACK_IMPORTED_MODULE_4__["AngularFirestoreDocument"]; });

/* harmony import */ var _collection_changes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./collection/changes */ "./node_modules/@angular/fire/firestore/collection/changes.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "docChanges", function() { return _collection_changes__WEBPACK_IMPORTED_MODULE_5__["docChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "sortedChanges", function() { return _collection_changes__WEBPACK_IMPORTED_MODULE_5__["sortedChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "combineChanges", function() { return _collection_changes__WEBPACK_IMPORTED_MODULE_5__["combineChanges"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "combineChange", function() { return _collection_changes__WEBPACK_IMPORTED_MODULE_5__["combineChange"]; });

/* harmony import */ var _observable_fromRef__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./observable/fromRef */ "./node_modules/@angular/fire/firestore/observable/fromRef.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromRef", function() { return _observable_fromRef__WEBPACK_IMPORTED_MODULE_6__["fromRef"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromDocRef", function() { return _observable_fromRef__WEBPACK_IMPORTED_MODULE_6__["fromDocRef"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fromCollectionRef", function() { return _observable_fromRef__WEBPACK_IMPORTED_MODULE_6__["fromCollectionRef"]; });








//# sourceMappingURL=public_api.js.map

/***/ }),

/***/ "./node_modules/firebase/firestore/dist/index.esm.js":
/*!***********************************************************!*\
  !*** ./node_modules/firebase/firestore/dist/index.esm.js ***!
  \***********************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _firebase_firestore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @firebase/firestore */ "./node_modules/@firebase/firestore/dist/index.cjs.js");
/* harmony import */ var _firebase_firestore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_firebase_firestore__WEBPACK_IMPORTED_MODULE_0__);


/**
 * @license
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//# sourceMappingURL=index.esm.js.map


/***/ }),

/***/ "./src/app/firebase/firebase-integration.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/firebase/firebase-integration.service.ts ***!
  \**********************************************************/
/*! exports provided: FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");
/* harmony import */ var dayjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(dayjs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shell/data-store */ "./src/app/shell/data-store.ts");
/* harmony import */ var _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./listing/firebase-listing.model */ "./src/app/firebase/listing/firebase-listing.model.ts");
/* harmony import */ var _user_firebase_user_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user/firebase-user.model */ "./src/app/firebase/user/firebase-user.model.ts");
/* harmony import */ var _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user/select-image/user-image.model */ "./src/app/firebase/user/select-image/user-image.model.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FirebaseService = /** @class */ (function () {
    function FirebaseService(afs) {
        this.afs = afs;
    }
    /*
      Firebase User Listing Page
    */
    FirebaseService.prototype.getListingDataSource = function () {
        var _this = this;
        return this.afs.collection('users').valueChanges({ idField: 'id' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (actions) { return actions.map(function (user) {
            var age = _this.calcUserAge(user.birthdate);
            return __assign({ age: age }, user);
        }); }));
    };
    FirebaseService.prototype.getListingStore = function (dataSource) {
        // Use cache if available
        if (!this.listingDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = [
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
                new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"]()
            ];
            this.listingDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"](shellModel);
            // Trigger the loading mechanism (with shell) in the dataStore
            this.listingDataStore.load(dataSource);
        }
        return this.listingDataStore;
    };
    // Filter users by age
    FirebaseService.prototype.searchUsersByAge = function (lower, upper) {
        var _this = this;
        // we save the dateOfBirth in our DB so we need to calc the min and max dates valid for this query
        var minDate = (dayjs__WEBPACK_IMPORTED_MODULE_4__(Date.now()).subtract(upper, 'year')).unix();
        var maxDate = (dayjs__WEBPACK_IMPORTED_MODULE_4__(Date.now()).subtract(lower, 'year')).unix();
        var listingCollection = this.afs.collection('users', function (ref) {
            return ref.orderBy('birthdate').startAt(minDate).endAt(maxDate);
        });
        return listingCollection.valueChanges({ idField: 'id' }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (actions) { return actions.map(function (user) {
            var age = _this.calcUserAge(user.birthdate);
            return __assign({ age: age }, user);
        }); }));
    };
    /*
      Firebase User Details Page
    */
    // Concat the userData with the details of the userSkills (from the skills collection)
    FirebaseService.prototype.getCombinedUserDataSource = function (userId) {
        var _this = this;
        return this.getUser(userId)
            .pipe(
        // Transformation operator: Map each source value (user) to an Observable (combineDataSources | throwError) which
        // is merged in the output Observable
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["concatMap"])(function (user) {
            if (user && user.skills) {
                // Map each skill id and get the skill data as an Observable
                var userSkillsObservables = user.skills.map(function (skill) {
                    return _this.getSkill(skill).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])());
                });
                // Combination operator: Take the most recent value from both input sources (of(user) & forkJoin(userSkillsObservables)),
                // and transform those emitted values into one value ([userDetails, userSkills])
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])([
                    Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(user),
                    Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["forkJoin"])(userSkillsObservables)
                ]).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (_a) {
                    var userDetails = _a[0], userSkills = _a[1];
                    // Spread operator (see: https://dev.to/napoleon039/how-to-use-the-spread-and-rest-operator-4jbb)
                    return __assign({}, userDetails, { skills: userSkills });
                }));
            }
            else {
                // Throw error
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('User does not have any skills.');
            }
        }));
    };
    FirebaseService.prototype.getCombinedUserStore = function (dataSource) {
        // Initialize the model specifying that it is a shell model
        var shellModel = new _user_firebase_user_model__WEBPACK_IMPORTED_MODULE_7__["FirebaseCombinedUserModel"]();
        this.combinedUserDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"](shellModel);
        // Trigger the loading mechanism (with shell) in the dataStore
        this.combinedUserDataStore.load(dataSource);
        return this.combinedUserDataStore;
    };
    // tslint:disable-next-line:max-line-length
    FirebaseService.prototype.getRelatedUsersDataSource = function (combinedUserDataSource) {
        var _this = this;
        return combinedUserDataSource
            .pipe(
        // Filter user values that are not shells. We need to add this filter if using the combinedUserDataStore timeline
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function (user) { return !user.isShell; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["concatMap"])(function (user) {
            if (user && user.skills) {
                // Get all users with at least 1 skill in common
                var relatedUsersObservable = _this.getUsersWithSameSkill(user.id, user.skills);
                return relatedUsersObservable;
            }
            else {
                // Throw error
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('Could not get related user');
            }
        }));
    };
    FirebaseService.prototype.getRelatedUsersStore = function (dataSource) {
        // Initialize the model specifying that it is a shell model
        var shellModel = [
            new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
            new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"](),
            new _listing_firebase_listing_model__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingItemModel"]()
        ];
        this.relatedUsersDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"](shellModel);
        // Trigger the loading mechanism (with shell) in the dataStore
        this.relatedUsersDataStore.load(dataSource);
        return this.relatedUsersDataStore;
    };
    /*
      Firebase Create User Modal
    */
    FirebaseService.prototype.createUser = function (userData) {
        return this.afs.collection('users').add(__assign({}, userData));
    };
    /*
      Firebase Update User Modal
    */
    FirebaseService.prototype.updateUser = function (userData) {
        return this.afs.collection('users').doc(userData.id).set(userData);
    };
    FirebaseService.prototype.deleteUser = function (userKey) {
        return this.afs.collection('users').doc(userKey).delete();
    };
    /*
      Firebase Select User Image Modal
    */
    FirebaseService.prototype.getAvatarsDataSource = function () {
        return this.afs.collection('avatars').valueChanges();
    };
    FirebaseService.prototype.getAvatarsStore = function (dataSource) {
        // Use cache if available
        if (!this.avatarsDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = [
                new _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__["UserImageModel"](),
                new _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__["UserImageModel"](),
                new _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__["UserImageModel"](),
                new _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__["UserImageModel"](),
                new _user_select_image_user_image_model__WEBPACK_IMPORTED_MODULE_8__["UserImageModel"]()
            ];
            this.avatarsDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"](shellModel);
            // Trigger the loading mechanism (with shell) in the dataStore
            this.avatarsDataStore.load(dataSource);
        }
        return this.avatarsDataStore;
    };
    /*
      FireStore utility methods
    */
    // Get list of all available Skills (used in the create and update modals)
    FirebaseService.prototype.getSkills = function () {
        return this.afs.collection('skills').valueChanges({ idField: 'id' });
    };
    // Get data of a specific Skill
    FirebaseService.prototype.getSkill = function (skillId) {
        return this.afs.doc('skills/' + skillId)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (a) {
            var data = a.payload.data();
            var id = a.payload.id;
            return __assign({ id: id }, data);
        }));
    };
    // Get data of a specific User
    FirebaseService.prototype.getUser = function (userId) {
        var _this = this;
        return this.afs.doc('users/' + userId)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (a) {
            var userData = a.payload.data();
            var id = a.payload.id;
            var age = _this.calcUserAge(userData.birthdate);
            return __assign({ id: id, age: age }, userData);
        }));
    };
    // Get all users who share at least 1 skill of the user's 'skills' list
    FirebaseService.prototype.getUsersWithSameSkill = function (userId, skills) {
        var _this = this;
        // Get the users who have at least 1 skill in common
        // Because firestore doesn't have a logical 'OR' operator we need to create multiple queries, one for each skill from the 'skills' list
        var queries = skills.map(function (skill) {
            return _this.afs.collection('users', function (ref) { return ref
                .where('skills', 'array-contains', skill.id); })
                .valueChanges({ idField: 'id' });
        });
        // Combine all these queries
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["combineLatest"])(queries).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (relatedUsers) {
            var _a;
            // Flatten the array of arrays of FirebaseListingItemModel
            var flattenedRelatedUsers = (_a = []).concat.apply(_a, relatedUsers);
            // Removes duplicates from the array of FirebaseListingItemModel objects.
            // Also remove the original user (userId)
            var filteredRelatedUsers = flattenedRelatedUsers
                .reduce(function (accumulatedUsers, user) {
                if ((accumulatedUsers.findIndex(function (accumulatedUser) { return accumulatedUser.id === user.id; }) < 0) && (user.id !== userId)) {
                    return accumulatedUsers.concat([user]);
                }
                else {
                    // If the user doesn't pass the test, then don't add it to the filtered users array
                    return accumulatedUsers;
                }
            }, []);
            return filteredRelatedUsers;
        }));
    };
    FirebaseService.prototype.calcUserAge = function (dateOfBirth) {
        return dayjs__WEBPACK_IMPORTED_MODULE_4__(Date.now()).diff(dayjs__WEBPACK_IMPORTED_MODULE_4__["unix"](dateOfBirth), 'year');
    };
    FirebaseService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_1__["AngularFirestore"]])
    ], FirebaseService);
    return FirebaseService;
}());



/***/ }),

/***/ "./src/app/firebase/listing/firebase-listing.model.ts":
/*!************************************************************!*\
  !*** ./src/app/firebase/listing/firebase-listing.model.ts ***!
  \************************************************************/
/*! exports provided: FirebaseListingItemModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseListingItemModel", function() { return FirebaseListingItemModel; });
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shell/data-store */ "./src/app/shell/data-store.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var FirebaseListingItemModel = /** @class */ (function (_super) {
    __extends(FirebaseListingItemModel, _super);
    function FirebaseListingItemModel() {
        return _super.call(this) || this;
    }
    return FirebaseListingItemModel;
}(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]));



/***/ }),

/***/ "./src/app/firebase/user/firebase-user.model.ts":
/*!******************************************************!*\
  !*** ./src/app/firebase/user/firebase-user.model.ts ***!
  \******************************************************/
/*! exports provided: FirebaseSkillModel, FirebaseUserModel, FirebaseCombinedUserModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseSkillModel", function() { return FirebaseSkillModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseUserModel", function() { return FirebaseUserModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseCombinedUserModel", function() { return FirebaseCombinedUserModel; });
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shell/data-store */ "./src/app/shell/data-store.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var FirebaseSkillModel = /** @class */ (function (_super) {
    __extends(FirebaseSkillModel, _super);
    function FirebaseSkillModel() {
        return _super.call(this) || this;
    }
    return FirebaseSkillModel;
}(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]));

var FirebaseUserModel = /** @class */ (function (_super) {
    __extends(FirebaseUserModel, _super);
    function FirebaseUserModel() {
        var _this = _super.call(this) || this;
        _this.skills = [
            '',
            '',
            ''
        ];
        _this.languages = {
            spanish: 0,
            english: 0,
            french: 0
        };
        return _this;
    }
    return FirebaseUserModel;
}(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]));

var FirebaseCombinedUserModel = /** @class */ (function (_super) {
    __extends(FirebaseCombinedUserModel, _super);
    function FirebaseCombinedUserModel() {
        var _this = _super.call(this) || this;
        _this.skills = [
            new FirebaseSkillModel(),
            new FirebaseSkillModel(),
            new FirebaseSkillModel()
        ];
        return _this;
    }
    return FirebaseCombinedUserModel;
}(FirebaseUserModel));



/***/ }),

/***/ "./src/app/firebase/user/select-image/select-user-image.modal.html":
/*!*************************************************************************!*\
  !*** ./src/app/firebase/user/select-image/select-user-image.modal.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"dismissModal()\">\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Select an Avatar</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"select-image-content\">\n  <ion-row class=\"images-wrapper\">\n    <ion-col size=\"4\" class=\"image-item\" *ngFor=\"let avatar of avatars\">\n      <app-image-shell [mode]=\"'cover'\" class=\"user-image\" animation=\"spinner\" [src]=\"avatar.link\" [alt]=\"'avatar image'\" (click)=\"dismissModal(avatar)\">\n        <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n        </app-aspect-ratio>\n      </app-image-shell>\n    </ion-col>\n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/firebase/user/select-image/select-user-image.modal.ts":
/*!***********************************************************************!*\
  !*** ./src/app/firebase/user/select-image/select-user-image.modal.ts ***!
  \***********************************************************************/
/*! exports provided: SelectUserImageModal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectUserImageModal", function() { return SelectUserImageModal; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SelectUserImageModal = /** @class */ (function () {
    function SelectUserImageModal(modalController, firebaseService) {
        this.modalController = modalController;
        this.firebaseService = firebaseService;
    }
    Object.defineProperty(SelectUserImageModal.prototype, "isShell", {
        get: function () {
            return (this.avatars && this.avatars.isShell) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    SelectUserImageModal.prototype.ngOnInit = function () {
        var _this = this;
        var dataSource = this.firebaseService.getAvatarsDataSource();
        var dataStore = this.firebaseService.getAvatarsStore(dataSource);
        dataStore.state.subscribe(function (state) {
            _this.avatars = state;
        }, function (error) { });
    };
    SelectUserImageModal.prototype.dismissModal = function (avatar) {
        this.modalController.dismiss(avatar);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.is-shell'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], SelectUserImageModal.prototype, "isShell", null);
    SelectUserImageModal = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-select-user-image',
            template: __webpack_require__(/*! ./select-user-image.modal.html */ "./src/app/firebase/user/select-image/select-user-image.modal.html"),
            styles: [__webpack_require__(/*! ./styles/select-user-image.modal.scss */ "./src/app/firebase/user/select-image/styles/select-user-image.modal.scss"), __webpack_require__(/*! ./styles/select-user-image.shell.scss */ "./src/app/firebase/user/select-image/styles/select-user-image.shell.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__["FirebaseService"]])
    ], SelectUserImageModal);
    return SelectUserImageModal;
}());



/***/ }),

/***/ "./src/app/firebase/user/select-image/styles/select-user-image.modal.scss":
/*!********************************************************************************!*\
  !*** ./src/app/firebase/user/select-image/styles/select-user-image.modal.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background); }\n\n.select-image-content {\n  --background: var(--page-background); }\n\n.select-image-content .images-wrapper {\n    --ion-grid-column-padding: calc(var(--page-margin) / 2);\n    padding: calc(var(--page-margin) / 2); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci9zZWxlY3QtaW1hZ2Uvc3R5bGVzL3NlbGVjdC11c2VyLWltYWdlLm1vZGFsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxxQ0FBYztFQUNkLHdDQUFrQixFQUFBOztBQUdwQjtFQUNFLG9DQUFhLEVBQUE7O0FBRGY7SUFJSSx1REFBMEI7SUFFMUIscUNBQXFDLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9maXJlYmFzZS91c2VyL3NlbGVjdC1pbWFnZS9zdHlsZXMvc2VsZWN0LXVzZXItaW1hZ2UubW9kYWwuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIC8vIEN1c3RvbSB2YXJpYWJsZXNcbi8vIC8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xuOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xufVxuXG4uc2VsZWN0LWltYWdlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XG5cbiAgLmltYWdlcy13cmFwcGVyIHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuXG4gICAgcGFkZGluZzogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/firebase/user/select-image/styles/select-user-image.shell.scss":
/*!********************************************************************************!*\
  !*** ./src/app/firebase/user/select-image/styles/select-user-image.shell.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.user-image {\n  --image-shell-border-radius: var(--app-fair-radius); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci9zZWxlY3QtaW1hZ2Uvc3R5bGVzL3NlbGVjdC11c2VyLWltYWdlLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtREFBNEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZpcmViYXNlL3VzZXIvc2VsZWN0LWltYWdlL3N0eWxlcy9zZWxlY3QtdXNlci1pbWFnZS5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYXBwLWltYWdlLXNoZWxsLnVzZXItaW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IHZhcigtLWFwcC1mYWlyLXJhZGl1cyk7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/firebase/user/select-image/user-image.model.ts":
/*!****************************************************************!*\
  !*** ./src/app/firebase/user/select-image/user-image.model.ts ***!
  \****************************************************************/
/*! exports provided: UserImageModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserImageModel", function() { return UserImageModel; });
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../shell/data-store */ "./src/app/shell/data-store.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var UserImageModel = /** @class */ (function (_super) {
    __extends(UserImageModel, _super);
    function UserImageModel() {
        return _super.call(this) || this;
    }
    return UserImageModel;
}(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]));



/***/ }),

/***/ "./src/app/shell/data-store.ts":
/*!*************************************!*\
  !*** ./src/app/shell/data-store.ts ***!
  \*************************************/
/*! exports provided: ShellModel, DataStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShellModel", function() { return ShellModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataStore", function() { return DataStore; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config/app-shell.config */ "./src/app/shell/config/app-shell.config.ts");



var ShellModel = /** @class */ (function () {
    function ShellModel() {
        this.isShell = false;
    }
    return ShellModel;
}());

var DataStore = /** @class */ (function () {
    function DataStore(shellModel) {
        this.shellModel = shellModel;
        // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length
        this.networkDelay = (_config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay) ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
    }
    // Static function with generics
    // (ref: https://stackoverflow.com/a/24293088/1116959)
    // Append a shell (T & ShellModel) to every value (T) emmited to the timeline
    DataStore.AppendShell = function (dataObservable, shellModel, networkDelay) {
        if (networkDelay === void 0) { networkDelay = 400; }
        var delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay));
        // Assign shell flag accordingly
        // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([
            delayObservable,
            dataObservable
        ]).pipe(
        // Dismiss unnecessary delayValue
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_a) {
            var delayValue = _a[0], dataValue = _a[1];
            return Object.assign(dataValue, { isShell: false });
        }), 
        // Set the shell model as the initial value
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, { isShell: true })));
    };
    DataStore.prototype.load = function (dataSourceObservable) {
        var _this = this;
        var dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
        dataSourceWithShellObservable
            .subscribe(function (dataValue) {
            _this.timeline.next(dataValue);
        });
    };
    Object.defineProperty(DataStore.prototype, "state", {
        get: function () {
            return this.timeline.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    return DataStore;
}());



/***/ }),

/***/ "./src/app/validators/checkbox-checked.validator.ts":
/*!**********************************************************!*\
  !*** ./src/app/validators/checkbox-checked.validator.ts ***!
  \**********************************************************/
/*! exports provided: CheckboxCheckedValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckboxCheckedValidator", function() { return CheckboxCheckedValidator; });
var CheckboxCheckedValidator = /** @class */ (function () {
    function CheckboxCheckedValidator() {
    }
    CheckboxCheckedValidator.minSelectedCheckboxes = function (min) {
        var validator = function (formArray) {
            var totalSelected = formArray.controls
                // get a list of checkbox values (boolean)
                .map(function (control) { return control.value; })
                // total up the number of checked checkboxes
                .reduce(function (prev, next) { return next ? prev + next : prev; }, 0);
            // if the total is not greater than the minimum, return the error message
            return totalSelected >= min ? null : { required: true };
        };
        return validator;
    };
    return CheckboxCheckedValidator;
}());



/***/ })

}]);
//# sourceMappingURL=default~firebase-firebase-integration-module~listing-firebase-listing-module~user-details-firebase-u~f5e4547a.js.map