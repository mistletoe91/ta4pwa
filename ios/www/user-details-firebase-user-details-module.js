(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-details-firebase-user-details-module"],{

/***/ "./src/app/firebase/user/details/firebase-user-details.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/firebase/user/details/firebase-user-details.module.ts ***!
  \***********************************************************************/
/*! exports provided: FirebaseUserDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseUserDetailsPageModule", function() { return FirebaseUserDetailsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _firebase_user_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./firebase-user-details.page */ "./src/app/firebase/user/details/firebase-user-details.page.ts");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _firebase_user_details_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./firebase-user-details.resolver */ "./src/app/firebase/user/details/firebase-user-details.resolver.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _firebase_user_details_page__WEBPACK_IMPORTED_MODULE_6__["FirebaseUserDetailsPage"],
        resolve: {
            data: _firebase_user_details_resolver__WEBPACK_IMPORTED_MODULE_8__["FirebaseUserDetailsResolver"]
        }
    }
];
var FirebaseUserDetailsPageModule = /** @class */ (function () {
    function FirebaseUserDetailsPageModule() {
    }
    FirebaseUserDetailsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]
            ],
            declarations: [_firebase_user_details_page__WEBPACK_IMPORTED_MODULE_6__["FirebaseUserDetailsPage"]],
            providers: [
                _firebase_integration_service__WEBPACK_IMPORTED_MODULE_7__["FirebaseService"],
                _firebase_user_details_resolver__WEBPACK_IMPORTED_MODULE_8__["FirebaseUserDetailsResolver"]
            ]
        })
    ], FirebaseUserDetailsPageModule);
    return FirebaseUserDetailsPageModule;
}());



/***/ }),

/***/ "./src/app/firebase/user/details/firebase-user-details.page.html":
/*!***********************************************************************!*\
  !*** ./src/app/firebase/user/details/firebase-user-details.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"firebase/listing\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>User Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openFirebaseUpdateModal()\">\n        Edit\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"user-content\">\n  <ion-row class=\"user-info-row\">\n    <ion-col class=\"user-image-col\" size=\"5\">\n      <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n        <app-image-shell [mode]=\"'cover'\" class=\"user-image\" animation=\"spinner\" [src]=\"user.avatar\" [alt]=\"'item image'\">\n        </app-image-shell>\n      </app-aspect-ratio>\n    </ion-col>\n    <ion-col class=\"user-details-col\" size=\"12\">\n      <h2 class=\"user-name\">\n        <app-text-shell animation=\"bouncing\" [data]=\"user.name?.concat(' ').concat(user.lastname)\"></app-text-shell>\n      </h2>\n      <span class=\"user-age\">\n        <app-text-shell animation=\"bouncing\" [data]=\"user.age?.toString().concat(' years young')\"></app-text-shell>\n      </span>\n    </ion-col>\n    <ion-col class=\"actions-col\" size=\"12\">\n      <ion-row>\n        <ion-col size=\"3\">\n          <ion-button class=\"action-btn\" color=\"secondary\">\n            <ion-icon class=\"btn-icon\" slot=\"icon-only\" name=\"mail\"></ion-icon>\n          </ion-button>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-button class=\"action-btn\" color=\"secondary\">\n            <ion-icon class=\"btn-icon\" slot=\"icon-only\" name=\"call\"></ion-icon>\n          </ion-button>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-button class=\"action-btn\" color=\"secondary\">\n            <ion-icon class=\"btn-icon\" slot=\"icon-only\" name=\"videocam\"></ion-icon>\n          </ion-button>\n        </ion-col>\n        <ion-col size=\"3\">\n          <ion-button class=\"action-btn\" color=\"secondary\">\n            <ion-icon class=\"btn-icon\" slot=\"icon-only\" name=\"text\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n  </ion-row>\n\n  <section class=\"content-section user-languages-wrapper\">\n    <h5 class=\"section-header\">Languages</h5>\n    <ion-row class=\"language-item-row\">\n      <ion-col class=\"language-item\">\n        <span class=\"language-name\">English</span>\n        <ion-progress-bar class=\"language-score\" type=\"determinate\" [value]=\"(user.languages?.english / 10)\"></ion-progress-bar>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"language-item-row\">\n      <ion-col class=\"language-item\">\n        <span class=\"language-name\">Spanish</span>\n        <ion-progress-bar class=\"language-score\" type=\"determinate\" [value]=\"(user.languages?.spanish / 10)\"></ion-progress-bar>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"language-item-row\">\n      <ion-col class=\"language-item\">\n        <span class=\"language-name\">French</span>\n        <ion-progress-bar class=\"language-score\" type=\"determinate\" [value]=\"(user.languages?.french / 10)\"></ion-progress-bar>\n      </ion-col>\n    </ion-row>\n  </section>\n\n  <section class=\"content-section user-experience-wrapper\">\n    <h5 class=\"section-header\">Skills</h5>\n    <ion-list class=\"experience-list\" lines=\"none\">\n      <ion-item class=\"experience-item\" *ngFor=\"let skill of user.skills\">\n        <label class=\"experience-label\">\n          <app-text-shell animation=\"bouncing\" [data]=\"skill?.name\"></app-text-shell>\n        </label>\n        <ion-icon class=\"experience-list-icon\" slot=\"start\" name=\"md-checkmark\" color=\"secondary\"></ion-icon>\n      </ion-item>\n    </ion-list>\n  </section>\n\n  <section class=\"content-section related-users-wrapper\" *ngIf=\"relatedUsers.length > 0\">\n    <h5 class=\"section-header\">Users with similar skills</h5>\n    <ion-row class=\"related-users-row\">\n      <ion-col class=\"related-user-item\" size=\"2\" *ngFor=\"let user of relatedUsers\">\n        <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n          <app-image-shell [mode]=\"'cover'\" class=\"related-user-picture\" [src]=\"user.avatar\" [routerLink]=\"['/firebase/details', user.id]\"></app-image-shell>\n        </app-aspect-ratio>\n        <span class=\"related-user-name\">\n          <app-text-shell [data]=\"user.name\"></app-text-shell>\n        </span>\n      </ion-col>\n    </ion-row>\n  </section>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/firebase/user/details/firebase-user-details.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/firebase/user/details/firebase-user-details.page.ts ***!
  \*********************************************************************/
/*! exports provided: FirebaseUserDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseUserDetailsPage", function() { return FirebaseUserDetailsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _update_firebase_update_user_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../update/firebase-update-user.modal */ "./src/app/firebase/user/update/firebase-update-user.modal.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var FirebaseUserDetailsPage = /** @class */ (function () {
    function FirebaseUserDetailsPage(firebaseService, modalController, router, route) {
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.router = router;
        this.route = route;
    }
    Object.defineProperty(FirebaseUserDetailsPage.prototype, "isShell", {
        get: function () {
            return ((this.user && this.user.isShell) || (this.relatedUsers && this.relatedUsers.isShell)) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    FirebaseUserDetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        this.route.data.subscribe(function (resolvedRouteData) {
            var resolvedDataStores = resolvedRouteData['data'];
            var combinedDataStore = resolvedDataStores.user;
            var relatedUsersDataStore = resolvedDataStores.relatedUsers;
            combinedDataStore.state.subscribe(function (state) {
                _this.user = state;
            });
            relatedUsersDataStore.state.subscribe(function (state) {
                _this.relatedUsers = state;
            });
        });
    };
    FirebaseUserDetailsPage.prototype.openFirebaseUpdateModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _update_firebase_update_user_modal__WEBPACK_IMPORTED_MODULE_4__["FirebaseUpdateUserModal"],
                            componentProps: {
                                'user': this.user
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.is-shell'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], FirebaseUserDetailsPage.prototype, "isShell", null);
    FirebaseUserDetailsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-firebase-user-details',
            template: __webpack_require__(/*! ./firebase-user-details.page.html */ "./src/app/firebase/user/details/firebase-user-details.page.html"),
            styles: [__webpack_require__(/*! ./styles/firebase-user-details.page.scss */ "./src/app/firebase/user/details/styles/firebase-user-details.page.scss"), __webpack_require__(/*! ./styles/firebase-user-details.shell.scss */ "./src/app/firebase/user/details/styles/firebase-user-details.shell.scss")]
        }),
        __metadata("design:paramtypes", [_firebase_integration_service__WEBPACK_IMPORTED_MODULE_3__["FirebaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], FirebaseUserDetailsPage);
    return FirebaseUserDetailsPage;
}());



/***/ }),

/***/ "./src/app/firebase/user/details/firebase-user-details.resolver.ts":
/*!*************************************************************************!*\
  !*** ./src/app/firebase/user/details/firebase-user-details.resolver.ts ***!
  \*************************************************************************/
/*! exports provided: FirebaseUserDetailsResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseUserDetailsResolver", function() { return FirebaseUserDetailsResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FirebaseUserDetailsResolver = /** @class */ (function () {
    function FirebaseUserDetailsResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    FirebaseUserDetailsResolver.prototype.resolve = function (route) {
        var userId = route.paramMap.get('id');
        // We created a FirebaseCombinedUserModel to combine the userData with the details of the userSkills (from the skills collection).
        // They are 2 different collections and we need to combine them into 1 dataSource.
        var combinedUserDataSource = this.firebaseService
            .getCombinedUserDataSource(userId);
        var combinedUserDataStore = this.firebaseService
            .getCombinedUserStore(combinedUserDataSource);
        // The user details page has a section with related users, showing users with the same skills
        // For this we created another datastore which depends on the combinedUser data store
        // The DataStore subscribes to the DataSource, to avoid creating two subscribers to the combinedUserDataSource,
        // use the combinedUserDataStore timeline instead. (The timeline is a Subject, and is intended to have many subscribers)
        // Using, and thus subscribing to the timeline won't trigger two requests to the firebase endpoint
        var relatedUsersDataSource = this.firebaseService
            .getRelatedUsersDataSource(combinedUserDataStore.state);
        var relatedUsersDataStore = this.firebaseService
            .getRelatedUsersStore(relatedUsersDataSource);
        return { user: combinedUserDataStore, relatedUsers: relatedUsersDataStore };
    };
    FirebaseUserDetailsResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__["FirebaseService"]])
    ], FirebaseUserDetailsResolver);
    return FirebaseUserDetailsResolver;
}());



/***/ }),

/***/ "./src/app/firebase/user/details/styles/firebase-user-details.page.scss":
/*!******************************************************************************!*\
  !*** ./src/app/firebase/user/details/styles/firebase-user-details.page.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-related-users-gutter: calc(var(--page-margin) / 2); }\n\n.user-content {\n  --background: var(--page-background); }\n\n.user-content .user-info-row {\n    --ion-grid-column-padding: 0px;\n    padding: var(--page-margin); }\n\n.user-content .user-info-row .user-image-col {\n      margin: 0px auto;\n      margin-bottom: calc(var(--page-margin) / 2); }\n\n.user-content .user-info-row .user-image-col .user-image {\n        border-radius: 50%; }\n\n.user-content .user-info-row .user-details-col {\n      display: -webkit-box;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n              flex-direction: column;\n      -webkit-box-align: center;\n              align-items: center;\n      text-align: center; }\n\n.user-content .user-info-row .user-details-col .user-name {\n        margin: 0px;\n        margin-bottom: calc(var(--page-margin) / 2);\n        min-width: 40%; }\n\n.user-content .user-info-row .user-details-col .user-age {\n        font-size: 14px;\n        font-weight: 300;\n        min-width: 60%; }\n\n.user-content .user-info-row .actions-col {\n      text-align: center;\n      margin-top: var(--page-margin); }\n\n.user-content .user-info-row .actions-col .action-btn {\n        --border-radius: 50%;\n        --padding-start: 0px;\n        --padding-end: 0px;\n        width: 4ch;\n        height: 4ch; }\n\n.user-content .user-info-row .actions-col .action-btn .btn-icon {\n          font-size: 24px; }\n\n.user-content .content-section {\n    padding: var(--page-margin);\n    border-top: solid 1px var(--ion-color-light-shade); }\n\n.user-content .content-section .section-header {\n      margin-top: 0px;\n      margin-bottom: var(--page-margin); }\n\n.user-content .user-languages-wrapper .language-item-row {\n    --ion-grid-column-padding: 0px;\n    margin-bottom: calc(var(--page-margin) / 2); }\n\n.user-content .user-languages-wrapper .language-item-row .language-item {\n      -webkit-padding-start: 0px;\n              padding-inline-start: 0px;\n      -webkit-padding-end: 0px;\n              padding-inline-end: 0px; }\n\n.user-content .user-languages-wrapper .language-item-row .language-item .language-name {\n        display: block;\n        font-size: 16px;\n        font-weight: 300;\n        margin-bottom: calc(var(--page-margin) / 3); }\n\n.user-content .user-languages-wrapper .language-item-row .language-item .language-score {\n        --buffer-background: rgba(var(--ion-color-secondary-rgb), .20);\n        --progress-background: rgba(var(--ion-color-secondary-rgb), 1);\n        height: var(--page-margin);\n        border-radius: calc(var(--page-margin) / 2); }\n\n.user-content .user-experience-wrapper .experience-list {\n    margin: 0px;\n    padding: 0px; }\n\n.user-content .user-experience-wrapper .experience-list .experience-item {\n      --min-height: initial;\n      --padding-start: 0px;\n      --padding-end: 0px;\n      --inner-padding-start: 0px;\n      --inner-padding-end: 0px; }\n\n.user-content .user-experience-wrapper .experience-list .experience-item:not(:last-child) {\n        padding-bottom: calc(var(--page-margin) / 2); }\n\n.user-content .user-experience-wrapper .experience-list .experience-item .experience-list-icon {\n        margin: 0px;\n        margin-right: calc(var(--page-margin) / 2); }\n\n.user-content .user-experience-wrapper .experience-list .experience-item .experience-label {\n        font-size: 16px;\n        font-weight: 300;\n        width: 100%; }\n\n.user-content .related-users-wrapper .related-users-row {\n    --ion-grid-columns: 7;\n    --ion-grid-column-padding: 0px;\n    flex-wrap: nowrap;\n    overflow-x: scroll;\n    will-change: scroll-position;\n    margin-left: calc(var(--page-margin) * -1);\n    margin-right: calc(var(--page-margin) * -1);\n    -ms-overflow-style: none;\n    overflow: -moz-scrollbars-none;\n    scrollbar-width: none; }\n\n.user-content .related-users-wrapper .related-users-row::-webkit-scrollbar {\n      display: none; }\n\n.user-content .related-users-wrapper .related-users-row::before, .user-content .related-users-wrapper .related-users-row::after {\n      content: '';\n      -webkit-box-flex: 0;\n              flex: 0 0 calc(var(--page-margin) - var(--page-related-users-gutter)); }\n\n.user-content .related-users-wrapper .related-users-row .related-user-item {\n      padding: 0px var(--page-related-users-gutter); }\n\n.user-content .related-users-wrapper .related-users-row .related-user-item .related-user-name {\n        display: block;\n        text-align: center;\n        margin-top: calc(var(--page-margin) / 2);\n        font-size: 14px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci9kZXRhaWxzL3N0eWxlcy9maXJlYmFzZS11c2VyLWRldGFpbHMucGFnZS5zY3NzIiwiL1VzZXJzL21hY2Jvb2twcm8vRG9jdW1lbnRzL2NvZGUvYXBwL3RhNHB3YS9pb25pYzQvc3JjL3RoZW1lL21peGlucy9zY3JvbGxiYXJzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDRSxxQ0FBYztFQUNkLHdDQUFrQjtFQUVsQix5REFBNEIsRUFBQTs7QUFJOUI7RUFDRSxvQ0FBYSxFQUFBOztBQURmO0lBSUksOEJBQTBCO0lBRTFCLDJCQUEyQixFQUFBOztBQU4vQjtNQVNNLGdCQUFnQjtNQUNoQiwyQ0FBMkMsRUFBQTs7QUFWakQ7UUFhUSxrQkFBa0IsRUFBQTs7QUFiMUI7TUFrQk0sb0JBQWE7TUFBYixhQUFhO01BQ2IsNEJBQXNCO01BQXRCLDZCQUFzQjtjQUF0QixzQkFBc0I7TUFDdEIseUJBQW1CO2NBQW5CLG1CQUFtQjtNQUNuQixrQkFBa0IsRUFBQTs7QUFyQnhCO1FBd0JRLFdBQVc7UUFDWCwyQ0FBMkM7UUFDM0MsY0FBYyxFQUFBOztBQTFCdEI7UUE4QlEsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixjQUFjLEVBQUE7O0FBaEN0QjtNQXFDTSxrQkFBa0I7TUFDbEIsOEJBQThCLEVBQUE7O0FBdENwQztRQXlDUSxvQkFBZ0I7UUFDaEIsb0JBQWdCO1FBQ2hCLGtCQUFjO1FBRWQsVUFBVTtRQUNWLFdBQVcsRUFBQTs7QUE5Q25CO1VBaURVLGVBQWUsRUFBQTs7QUFqRHpCO0lBd0RJLDJCQUEyQjtJQUMzQixrREFBa0QsRUFBQTs7QUF6RHREO01BNERNLGVBQWU7TUFDZixpQ0FBaUMsRUFBQTs7QUE3RHZDO0lBbUVNLDhCQUEwQjtJQUUxQiwyQ0FBMkMsRUFBQTs7QUFyRWpEO01Bd0VRLDBCQUF5QjtjQUF6Qix5QkFBeUI7TUFDekIsd0JBQXVCO2NBQXZCLHVCQUF1QixFQUFBOztBQXpFL0I7UUE0RVUsY0FBYztRQUNkLGVBQWU7UUFDZixnQkFBZ0I7UUFDaEIsMkNBQTJDLEVBQUE7O0FBL0VyRDtRQW1GVSw4REFBb0I7UUFDcEIsOERBQXNCO1FBRXRCLDBCQUEwQjtRQUMxQiwyQ0FBMkMsRUFBQTs7QUF2RnJEO0lBK0ZNLFdBQVc7SUFDWCxZQUFZLEVBQUE7O0FBaEdsQjtNQW1HUSxxQkFBYTtNQUNiLG9CQUFnQjtNQUNoQixrQkFBYztNQUNkLDBCQUFzQjtNQUN0Qix3QkFBb0IsRUFBQTs7QUF2RzVCO1FBMEdVLDRDQUE0QyxFQUFBOztBQTFHdEQ7UUE4R1UsV0FBVztRQUNYLDBDQUEwQyxFQUFBOztBQS9HcEQ7UUFtSFUsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixXQUFXLEVBQUE7O0FBckhyQjtJQTZITSxxQkFBbUI7SUFDbkIsOEJBQTBCO0lBRTFCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsNEJBQTRCO0lBQzVCLDBDQUEwQztJQUMxQywyQ0FBMkM7SUM3SS9DLHdCQUF3QjtJQUd4Qiw4QkFBOEI7SUFDOUIscUJBQXFCLEVBQUE7O0FES3ZCO01DREksYUFBYSxFQUFBOztBRENqQjtNQTBJUSxXQUFXO01BQ1gsbUJBQXFFO2NBQXJFLHFFQUFxRSxFQUFBOztBQTNJN0U7TUErSVEsNkNBQTZDLEVBQUE7O0FBL0lyRDtRQWtKVSxjQUFjO1FBQ2Qsa0JBQWtCO1FBQ2xCLHdDQUF3QztRQUN4QyxlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9maXJlYmFzZS91c2VyL2RldGFpbHMvc3R5bGVzL2ZpcmViYXNlLXVzZXItZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vLi4vdGhlbWUvbWl4aW5zL3Njcm9sbGJhcnNcIjtcblxuLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgLS1wYWdlLXJlbGF0ZWQtdXNlcnMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi51c2VyLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XG5cbiAgLnVzZXItaW5mby1yb3cge1xuICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKTtcblxuICAgIC51c2VyLWltYWdlLWNvbCB7XG4gICAgICBtYXJnaW46IDBweCBhdXRvO1xuICAgICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAgICAgLnVzZXItaW1hZ2Uge1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLnVzZXItZGV0YWlscy1jb2wge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAudXNlci1uYW1lIHtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAgICAgIG1pbi13aWR0aDogNDAlO1xuICAgICAgfVxuXG4gICAgICAudXNlci1hZ2Uge1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgICAgIG1pbi13aWR0aDogNjAlO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5hY3Rpb25zLWNvbCB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtYXJnaW4tdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG5cbiAgICAgIC5hY3Rpb24tYnRuIHtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XG5cbiAgICAgICAgd2lkdGg6IDRjaDtcbiAgICAgICAgaGVpZ2h0OiA0Y2g7XG5cbiAgICAgICAgLmJ0bi1pY29uIHtcbiAgICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuY29udGVudC1zZWN0aW9uIHtcbiAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgYm9yZGVyLXRvcDogc29saWQgMXB4IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG5cbiAgICAuc2VjdGlvbi1oZWFkZXIge1xuICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgbWFyZ2luLWJvdHRvbTogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIH1cbiAgfVxuXG4gIC51c2VyLWxhbmd1YWdlcy13cmFwcGVyIHtcbiAgICAubGFuZ3VhZ2UtaXRlbS1yb3cge1xuICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuXG4gICAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuXG4gICAgICAubGFuZ3VhZ2UtaXRlbSB7XG4gICAgICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiAwcHg7XG4gICAgICAgIHBhZGRpbmctaW5saW5lLWVuZDogMHB4O1xuXG4gICAgICAgIC5sYW5ndWFnZS1uYW1lIHtcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDMpO1xuICAgICAgICB9XG5cbiAgICAgICAgLmxhbmd1YWdlLXNjb3JlIHtcbiAgICAgICAgICAtLWJ1ZmZlci1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktcmdiKSwgLjIwKTtcbiAgICAgICAgICAtLXByb2dyZXNzLWJhY2tncm91bmQ6IHJnYmEodmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeS1yZ2IpLCAxKTtcblxuICAgICAgICAgIGhlaWdodDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgICAgICAgIGJvcmRlci1yYWRpdXM6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAudXNlci1leHBlcmllbmNlLXdyYXBwZXIge1xuICAgIC5leHBlcmllbmNlLWxpc3Qge1xuICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICBwYWRkaW5nOiAwcHg7XG5cbiAgICAgIC5leHBlcmllbmNlLWl0ZW0ge1xuICAgICAgICAtLW1pbi1oZWlnaHQ6IGluaXRpYWw7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLXBhZGRpbmctZW5kOiAwcHg7XG4gICAgICAgIC0taW5uZXItcGFkZGluZy1zdGFydDogMHB4O1xuICAgICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XG5cbiAgICAgICAgJjpub3QoOmxhc3QtY2hpbGQpIHtcbiAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5leHBlcmllbmNlLWxpc3QtaWNvbiB7XG4gICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICB9XG5cbiAgICAgICAgLmV4cGVyaWVuY2UtbGFiZWwge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnJlbGF0ZWQtdXNlcnMtd3JhcHBlciB7XG4gICAgLnJlbGF0ZWQtdXNlcnMtcm93IHtcbiAgICAgIC0taW9uLWdyaWQtY29sdW1uczogNzsgLy8gV2Ugd2FudCB0byBzaG93IHRocmVlIHVzZXJzIGFuZCBhIGhhbGYuIEVhY2ggdXNlciBmaWxscyAyIGNvbHMgPT4gKDMuNSAqIDIgPSA3IGNvbHMpXG4gICAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgICAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICAgICAgd2lsbC1jaGFuZ2U6IHNjcm9sbC1wb3NpdGlvbjtcbiAgICAgIG1hcmdpbi1sZWZ0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIC0xKTtcbiAgICAgIG1hcmdpbi1yaWdodDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAtMSk7XG5cbiAgICAgIEBpbmNsdWRlIGhpZGUtc2Nyb2xsYmFycygpO1xuXG4gICAgICAmOjpiZWZvcmUsXG4gICAgICAmOjphZnRlciB7XG4gICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICBmbGV4OiAwIDAgY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLSB2YXIoLS1wYWdlLXJlbGF0ZWQtdXNlcnMtZ3V0dGVyKSk7XG4gICAgICB9XG5cbiAgICAgIC5yZWxhdGVkLXVzZXItaXRlbSB7XG4gICAgICAgIHBhZGRpbmc6IDBweCB2YXIoLS1wYWdlLXJlbGF0ZWQtdXNlcnMtZ3V0dGVyKTtcblxuICAgICAgICAucmVsYXRlZC11c2VyLW5hbWUge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICBtYXJnaW4tdG9wOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuIiwiLy8gSGlkZSBzY3JvbGxiYXJzOiBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMzg5OTQ4MzcvMTExNjk1OVxuQG1peGluIGhpZGUtc2Nyb2xsYmFycygpIHtcbiAgLy8gSUUgMTArXG4gIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTtcblxuICAvLyBGaXJlZm94XG4gIG92ZXJmbG93OiAtbW96LXNjcm9sbGJhcnMtbm9uZTtcbiAgc2Nyb2xsYmFyLXdpZHRoOiBub25lO1xuXG4gIC8vIFNhZmFyaSBhbmQgQ2hyb21lXG4gICY6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/firebase/user/details/styles/firebase-user-details.shell.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/firebase/user/details/styles/firebase-user-details.shell.scss ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.user-image {\n  --image-shell-border-radius: 50%; }\n\n.user-name > app-text-shell {\n  --text-shell-line-height: 24px; }\n\n.user-age > app-text-shell {\n  --text-shell-line-height: 14px; }\n\n.experience-label > app-text-shell {\n  --text-shell-line-height: 16px;\n  max-width: 40%; }\n\n.experience-label > app-text-shell.text-loaded {\n    max-width: inherit; }\n\napp-image-shell.related-user-picture {\n  --image-shell-border-radius: var(--app-fair-radius); }\n\n.related-user-name > app-text-shell {\n  --text-shell-line-height: 14px;\n  max-width: 80%; }\n\n.related-user-name > app-text-shell.text-loaded {\n    max-width: inherit; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvdXNlci9kZXRhaWxzL3N0eWxlcy9maXJlYmFzZS11c2VyLWRldGFpbHMuc2hlbGwuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdDQUE0QixFQUFBOztBQUc5QjtFQUNFLDhCQUF5QixFQUFBOztBQUczQjtFQUNFLDhCQUF5QixFQUFBOztBQUczQjtFQUNFLDhCQUF5QjtFQUN6QixjQUFjLEVBQUE7O0FBRmhCO0lBS0ksa0JBQWtCLEVBQUE7O0FBSXRCO0VBQ0UsbURBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsOEJBQXlCO0VBQ3pCLGNBQWMsRUFBQTs7QUFGaEI7SUFLSSxrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZpcmViYXNlL3VzZXIvZGV0YWlscy9zdHlsZXMvZmlyZWJhc2UtdXNlci1kZXRhaWxzLnNoZWxsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtaW1hZ2Utc2hlbGwudXNlci1pbWFnZSB7XG4gIC0taW1hZ2Utc2hlbGwtYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG4udXNlci1uYW1lID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDI0cHg7XG59XG5cbi51c2VyLWFnZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uZXhwZXJpZW5jZS1sYWJlbCA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xuICBtYXgtd2lkdGg6IDQwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cblxuYXBwLWltYWdlLXNoZWxsLnJlbGF0ZWQtdXNlci1waWN0dXJlIHtcbiAgLS1pbWFnZS1zaGVsbC1ib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtZmFpci1yYWRpdXMpO1xufVxuXG4ucmVsYXRlZC11c2VyLW5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbiAgbWF4LXdpZHRoOiA4MCU7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiBpbmhlcml0O1xuICB9XG59XG4iXX0= */"

/***/ })

}]);
//# sourceMappingURL=user-details-firebase-user-details-module.js.map