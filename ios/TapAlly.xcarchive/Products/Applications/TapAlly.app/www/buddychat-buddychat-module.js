(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["buddychat-buddychat-module"],{

/***/ "./src/app/buddychat/buddychat.module.ts":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.module.ts ***!
  \***********************************************/
/*! exports provided: BuddychatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPageModule", function() { return BuddychatPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _buddychat_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./buddychat.page */ "./src/app/buddychat/buddychat.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _buddychat_page__WEBPACK_IMPORTED_MODULE_5__["BuddychatPage"]
    }
];
var BuddychatPageModule = /** @class */ (function () {
    function BuddychatPageModule() {
    }
    BuddychatPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_buddychat_page__WEBPACK_IMPORTED_MODULE_5__["BuddychatPage"]]
        })
    ], BuddychatPageModule);
    return BuddychatPageModule;
}());



/***/ }),

/***/ "./src/app/buddychat/buddychat.page.html":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar  color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button class=\"show-back-button\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Anguls Dev</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content #content padding class=\"buddychat-content\">\n\n    <div class=\"buddychat\">\n      <p>his talk-bubble uses .left-in class to show a triangle on the left slightly indented. Still a blocky square.</p>\n    </div>\n\n    <div class=\"mychat\">\n       <p> Hello this is sample chat and lore ipusm and good thoughts of nice air </p>\n    </div>\n\n    <div class=\"mychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Referral Sent</span></h4>\n      <p>Hi, I am sending a referral to you Hi, I am sending a referral to you Hi, I am sending a referral to you</p>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Referral Sent</span></h4>\n      <p>Hi, I am sending a referral to you (Gurbakhsak Kalhi) (647) 123-1234</p>\n\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"primary\" >Save Contact</ion-button>\n      </div>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"medium\" >Open Contact</ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Loyality Redeemed</span></h4>\n      <p>Hi, I have redeemed the loyality points and got Free Ice Cream</p>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Request To Release Incentive</span></h4>\n      <p>Please release my incentives for the referrals I sent you so far</p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"primary\" >Check Request</ion-button>\n      </div>\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Asking For Deal</span></h4>\n      <p>Hi, I am intersting in your deal. Please let me know how to redeem it </p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"success\" >Check Deal</ion-button>\n      </div>\n\n    </div>\n\n    <div class=\"buddychat\">\n      <h4 class=\"headinginchat\" (click)=\"fnSendToEarningsPage ()\" class=\"headinginchat\"><span>Asking For Referral</span></h4>\n      <p>Hi, I am looking for a (Accountant). Please send me a trusted referral </p>\n      <div>\n        <ion-button size=\"small\" (click)=\"saveContactInPhone(item)\" color=\"dark\" >Send A Referral</ion-button>\n      </div>\n    </div>\n\n</ion-content>\n\n<ion-footer ion-fixed>\n        <ion-toolbar class=\"no-border\" color=\"white\">\n          <!-- Textarea in an item with a placeholder -->\n          <ion-item lines=\"none\">\n            <ion-textarea placeholder=\"Type to send message\"></ion-textarea>\n            <ion-buttons end>\n                 <ion-button color=\"primary\"><ion-icon name=\"send\"></ion-icon></ion-button>\n            </ion-buttons>\n          </ion-item>\n\n        </ion-toolbar>\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.scss":
/*!***********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background-shade);\n  --page-tags-gutter: 5px;\n  --page-color-radio-items-per-row: 5;\n  --page-color-radio-gutter: 2%;\n  --page-color-radio-width: calc((100% - (2 * var(--page-color-radio-gutter) * var(--page-color-radio-items-per-row))) / var(--page-color-radio-items-per-row)); }\n\n.buddychat-content {\n  --background: var(--page-background); }\n\n.buddychat-content ion-item-divider {\n    --background: var(--page-background);\n    --padding-bottom: calc(var(--page-margin) / 2);\n    --padding-top: calc(var(--page-margin) / 2);\n    --padding-start: var(--page-margin);\n    --padding-end: var(--page-margin);\n    border: none; }\n\n.headinginchat {\n  border-bottom: 1px solid #ccc; }\n\n.buddychat, .mychat {\n  padding: 7px 15px;\n  margin-bottom: 15px;\n  background-color: #fff;\n  width: 80%;\n  clear: both; }\n\n.buddychat {\n  border-radius: 0px 20px 20px 20px;\n  border: 1px solid #ccc;\n  color: #000;\n  background-color: #eee;\n  float: left; }\n\n.mychat {\n  border-radius: 20px 20px 0px 20px;\n  border: 1px solid #fff;\n  float: right; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvYnVkZHljaGF0L2J1ZGR5Y2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxxQ0FBYztFQUNkLDhDQUFrQjtFQUVsQix1QkFBbUI7RUFFbkIsbUNBQWlDO0VBQ2pDLDZCQUEwQjtFQUMxQiw2SkFBeUIsRUFBQTs7QUFHM0I7RUFDRSxvQ0FBYSxFQUFBOztBQURmO0lBSUUsb0NBQWE7SUFDYiw4Q0FBaUI7SUFDakIsMkNBQWM7SUFDZCxtQ0FBZ0I7SUFDaEIsaUNBQWM7SUFFZCxZQUFZLEVBQUE7O0FBR2Q7RUFDRSw2QkFBNkIsRUFBQTs7QUFFL0I7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0QixVQUFVO0VBQ1YsV0FBVSxFQUFBOztBQUVaO0VBQ0UsaUNBQWlDO0VBQ2pDLHNCQUFzQjtFQUN0QixXQUFVO0VBQ1Ysc0JBQXNCO0VBQ3RCLFdBQVUsRUFBQTs7QUFFWjtFQUNFLGlDQUFpQztFQUNqQyxzQkFBc0I7RUFDdEIsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvYnVkZHljaGF0L2J1ZGR5Y2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLWZhaXItbWFyZ2luKTtcbiAgLS1wYWdlLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kLXNoYWRlKTtcblxuICAtLXBhZ2UtdGFncy1ndXR0ZXI6IDVweDtcblxuICAtLXBhZ2UtY29sb3ItcmFkaW8taXRlbXMtcGVyLXJvdzogNTtcbiAgLS1wYWdlLWNvbG9yLXJhZGlvLWd1dHRlcjogMiU7XG4gIC0tcGFnZS1jb2xvci1yYWRpby13aWR0aDogY2FsYygoMTAwJSAtICgyICogdmFyKC0tcGFnZS1jb2xvci1yYWRpby1ndXR0ZXIpICogdmFyKC0tcGFnZS1jb2xvci1yYWRpby1pdGVtcy1wZXItcm93KSkpIC8gdmFyKC0tcGFnZS1jb2xvci1yYWRpby1pdGVtcy1wZXItcm93KSk7XG59XG5cbi5idWRkeWNoYXQtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcblxuXHRpb24taXRlbS1kaXZpZGVyIHtcblx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XG5cdFx0LS1wYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblx0XHQtLXBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuXHRcdC0tcGFkZGluZy1zdGFydDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuXHRcdC0tcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcblxuXHRcdGJvcmRlcjogbm9uZTtcblx0fVxufVxuLmhlYWRpbmdpbmNoYXQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbn1cbi5idWRkeWNoYXQsLm15Y2hhdCB7XG4gIHBhZGRpbmc6IDdweCAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICB3aWR0aDogODAlO1xuICBjbGVhcjpib3RoO1xufVxuLmJ1ZGR5Y2hhdCB7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAyMHB4IDIwcHggMjBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbiAgY29sb3I6IzAwMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgZmxvYXQ6bGVmdDtcbn1cbi5teWNoYXQge1xuICBib3JkZXItcmFkaXVzOiAyMHB4IDIwcHggMHB4IDIwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIGZsb2F0OiByaWdodDtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/buddychat/buddychat.page.ts":
/*!*********************************************!*\
  !*** ./src/app/buddychat/buddychat.page.ts ***!
  \*********************************************/
/*! exports provided: BuddychatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuddychatPage", function() { return BuddychatPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BuddychatPage = /** @class */ (function () {
    function BuddychatPage() {
    }
    BuddychatPage.prototype.scrollToBottomOnInit = function () {
        this.content.scrollToBottom(300);
    };
    BuddychatPage.prototype.ngOnInit = function () {
        this.scrollToBottomOnInit();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('content'),
        __metadata("design:type", Object)
    ], BuddychatPage.prototype, "content", void 0);
    BuddychatPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-buddychat',
            template: __webpack_require__(/*! ./buddychat.page.html */ "./src/app/buddychat/buddychat.page.html"),
            styles: [__webpack_require__(/*! ./buddychat.page.scss */ "./src/app/buddychat/buddychat.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BuddychatPage);
    return BuddychatPage;
}());



/***/ })

}]);
//# sourceMappingURL=buddychat-buddychat-module.js.map