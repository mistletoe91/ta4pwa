(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["firebase-firebase-integration-module"],{

/***/ "./src/app/firebase/firebase-integration.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/firebase/firebase-integration.module.ts ***!
  \*********************************************************/
/*! exports provided: FirebaseIntegrationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseIntegrationModule", function() { return FirebaseIntegrationModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _user_create_firebase_create_user_modal__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user/create/firebase-create-user.modal */ "./src/app/firebase/user/create/firebase-create-user.modal.ts");
/* harmony import */ var _user_update_firebase_update_user_modal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./user/update/firebase-update-user.modal */ "./src/app/firebase/user/update/firebase-update-user.modal.ts");
/* harmony import */ var _user_select_image_select_user_image_modal__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./user/select-image/select-user-image.modal */ "./src/app/firebase/user/select-image/select-user-image.modal.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var firebaseRoutes = [
    {
        path: 'listing',
        loadChildren: './listing/firebase-listing.module#FirebaseListingPageModule'
    },
    {
        path: 'details/:id',
        loadChildren: './user/details/firebase-user-details.module#FirebaseUserDetailsPageModule'
    }
];
var FirebaseIntegrationModule = /** @class */ (function () {
    function FirebaseIntegrationModule() {
    }
    FirebaseIntegrationModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(firebaseRoutes),
                _angular_fire__WEBPACK_IMPORTED_MODULE_5__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_8__["environment"].firebase),
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_6__["AngularFirestoreModule"]
            ],
            entryComponents: [
                _user_create_firebase_create_user_modal__WEBPACK_IMPORTED_MODULE_9__["FirebaseCreateUserModal"],
                _user_update_firebase_update_user_modal__WEBPACK_IMPORTED_MODULE_10__["FirebaseUpdateUserModal"],
                _user_select_image_select_user_image_modal__WEBPACK_IMPORTED_MODULE_11__["SelectUserImageModal"]
            ],
            declarations: [
                _user_create_firebase_create_user_modal__WEBPACK_IMPORTED_MODULE_9__["FirebaseCreateUserModal"],
                _user_update_firebase_update_user_modal__WEBPACK_IMPORTED_MODULE_10__["FirebaseUpdateUserModal"],
                _user_select_image_select_user_image_modal__WEBPACK_IMPORTED_MODULE_11__["SelectUserImageModal"]
            ]
        })
    ], FirebaseIntegrationModule);
    return FirebaseIntegrationModule;
}());



/***/ })

}]);
//# sourceMappingURL=firebase-firebase-integration-module.js.map