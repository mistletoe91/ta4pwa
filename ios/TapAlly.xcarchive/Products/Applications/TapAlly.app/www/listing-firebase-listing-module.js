(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["listing-firebase-listing-module"],{

/***/ "./src/app/firebase/listing/firebase-listing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/firebase/listing/firebase-listing.module.ts ***!
  \*************************************************************/
/*! exports provided: FirebaseListingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseListingPageModule", function() { return FirebaseListingPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _firebase_listing_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./firebase-listing.page */ "./src/app/firebase/listing/firebase-listing.page.ts");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _firebase_listing_resolver__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./firebase-listing.resolver */ "./src/app/firebase/listing/firebase-listing.resolver.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _firebase_listing_page__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingPage"],
        resolve: {
            data: _firebase_listing_resolver__WEBPACK_IMPORTED_MODULE_8__["FirebaseListingResolver"]
        }
    }
];
var FirebaseListingPageModule = /** @class */ (function () {
    function FirebaseListingPageModule() {
    }
    FirebaseListingPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_firebase_listing_page__WEBPACK_IMPORTED_MODULE_6__["FirebaseListingPage"]],
            providers: [
                _firebase_integration_service__WEBPACK_IMPORTED_MODULE_7__["FirebaseService"],
                _firebase_listing_resolver__WEBPACK_IMPORTED_MODULE_8__["FirebaseListingResolver"]
            ]
        })
    ], FirebaseListingPageModule);
    return FirebaseListingPageModule;
}());



/***/ }),

/***/ "./src/app/firebase/listing/firebase-listing.page.html":
/*!*************************************************************!*\
  !*** ./src/app/firebase/listing/firebase-listing.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"app/categories\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Firebase</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"openFirebaseCreateModal()\">\n        <ion-icon slot=\"icon-only\" name=\"add\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n  <ion-toolbar class=\"filters-toolbar\">\n    <ion-row class=\"searchbar-row\" align-items-center>\n      <ion-col>\n        <ion-searchbar class=\"items-searchbar\" animated mode=\"ios\" [(ngModel)]=\"searchQuery\" (ionChange)=\"searchList()\" placeholder=\"Filter by name...\"></ion-searchbar>\n      </ion-col>\n      <ion-col class=\"call-to-action-col\">\n        <ion-button fill=\"clear\" color=\"secondary\" class=\"filters-btn\" (click)=\"showAgeFilter = !showAgeFilter\">\n          <ion-icon slot=\"icon-only\" name=\"options\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n    <form [formGroup]=\"rangeForm\" [hidden]=\"!showAgeFilter\">\n      <ion-row class=\"range-item-row\">\n        <ion-col size=\"12\">\n          <div class=\"range-header\">\n            <span class=\"range-value\">{{ rangeForm.controls.dual.value.lower }}</span>\n            <span class=\"range-label\">Filter by age</span>\n            <span class=\"range-value\">{{ rangeForm.controls.dual.value.upper }}</span>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\">\n          <ion-range class=\"range-control\" formControlName=\"dual\" color=\"secondary\" pin=\"true\" dualKnobs=\"true\" (ionChange)=\"searchList()\" min=\"1\" max=\"100\" step=\"1\" debounce=\"400\"></ion-range>\n        </ion-col>\n      </ion-row>\n    </form>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"firebase-listing-content\">\n  <ion-list class=\"items-list\" *ngIf=\"items?.length > 0\">\n    <ion-item class=\"list-item\" *ngFor=\"let item of items\" [routerLink]=\"['/firebase/details', item.id]\">\n      <ion-row class=\"user-row\">\n        <ion-col size=\"3\" class=\"user-image-wrapper\">\n          <app-aspect-ratio [ratio]=\"{w: 1, h: 1}\">\n            <app-image-shell class=\"user-image\" animation=\"spinner\" [mode]=\"'cover'\" [src]=\"item.avatar\" [alt]=\"'item image'\"></app-image-shell>\n          </app-aspect-ratio>\n        </ion-col>\n        <ion-col class=\"user-data-wrapper\">\n          <div class=\"user-info\">\n            <h3 class=\"user-name\">\n              <app-text-shell animation=\"bouncing\" [data]=\"item.name?.concat(' ').concat(item.lastname)\"></app-text-shell>\n            </h3>\n            <h5 class=\"user-age\">\n              <app-text-shell animation=\"bouncing\" [data]=\"item.age?.toString().concat(' years old')\"></app-text-shell>\n            </h5>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-list>\n  <h3 *ngIf=\"items?.length == 0\" class=\"empty-list-message\">\n    No users found for the selected filters.\n  </h3>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/firebase/listing/firebase-listing.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/firebase/listing/firebase-listing.page.ts ***!
  \***********************************************************/
/*! exports provided: FirebaseListingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseListingPage", function() { return FirebaseListingPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
/* harmony import */ var _firebase_listing_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./firebase-listing.model */ "./src/app/firebase/listing/firebase-listing.model.ts");
/* harmony import */ var _user_create_firebase_create_user_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../user/create/firebase-create-user.modal */ "./src/app/firebase/user/create/firebase-create-user.modal.ts");
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../shell/data-store */ "./src/app/shell/data-store.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var FirebaseListingPage = /** @class */ (function () {
    function FirebaseListingPage(firebaseService, modalController, route) {
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.route = route;
        this.showAgeFilter = false;
        this.searchSubject = new rxjs__WEBPACK_IMPORTED_MODULE_4__["ReplaySubject"](1);
        this.searchFiltersObservable = this.searchSubject.asObservable();
    }
    Object.defineProperty(FirebaseListingPage.prototype, "isShell", {
        get: function () {
            return (this.items && this.items.isShell) ? true : false;
        },
        enumerable: true,
        configurable: true
    });
    FirebaseListingPage.prototype.ngOnDestroy = function () {
        this.stateSubscription.unsubscribe();
    };
    FirebaseListingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.searchQuery = '';
        this.rangeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            dual: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]({ lower: 1, upper: 100 })
        });
        // Route data is a cold subscription, no need to unsubscribe?
        this.route.data.subscribe(function (resolvedRouteData) {
            _this.listingDataStore = resolvedRouteData['data'];
            // We need to avoid having multiple firebase subscriptions open at the same time to avoid memory leaks
            // By using a switchMap to cancel previous subscription each time a new one arrives,
            // we ensure having just one subscription (the latest)
            var updateSearchObservable = _this.searchFiltersObservable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (filters) {
                var filteredDataSource = _this.firebaseService.searchUsersByAge(filters.lower, filters.upper);
                // Send a shell until we have filtered data from Firebase
                var searchingShellModel = [
                    new _firebase_listing_model__WEBPACK_IMPORTED_MODULE_7__["FirebaseListingItemModel"](),
                    new _firebase_listing_model__WEBPACK_IMPORTED_MODULE_7__["FirebaseListingItemModel"]()
                ];
                // Wait on purpose some time to ensure the shell animation gets shown while loading filtered data
                var searchingDelay = 400;
                var dataSourceWithShellObservable = _shell_data_store__WEBPACK_IMPORTED_MODULE_9__["DataStore"].AppendShell(filteredDataSource, searchingShellModel, searchingDelay);
                return dataSourceWithShellObservable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (filteredItems) {
                    // Just filter items by name if there is a search query and they are not shell values
                    if (filters.query !== '' && !filteredItems.isShell) {
                        var queryFilteredItems = filteredItems.filter(function (item) {
                            return item.name.toLowerCase().includes(filters.query.toLowerCase());
                        });
                        // While filtering we strip out the isShell property, add it again
                        return Object.assign(queryFilteredItems, { isShell: filteredItems.isShell });
                    }
                    else {
                        return filteredItems;
                    }
                }));
            }));
            // Keep track of the subscription to unsubscribe onDestroy
            // Merge filteredData with the original dataStore state
            _this.stateSubscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["merge"])(_this.listingDataStore.state, updateSearchObservable).subscribe(function (state) {
                _this.items = state;
            }, function (error) { return console.log(error); }, function () { return console.log('stateSubscription completed'); });
        }, function (error) { return console.log(error); });
    };
    FirebaseListingPage.prototype.openFirebaseCreateModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _user_create_firebase_create_user_modal__WEBPACK_IMPORTED_MODULE_8__["FirebaseCreateUserModal"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FirebaseListingPage.prototype.searchList = function () {
        this.searchSubject.next({
            lower: this.rangeForm.controls.dual.value.lower,
            upper: this.rangeForm.controls.dual.value.upper,
            query: this.searchQuery
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.is-shell'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [])
    ], FirebaseListingPage.prototype, "isShell", null);
    FirebaseListingPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-firebase-listing',
            template: __webpack_require__(/*! ./firebase-listing.page.html */ "./src/app/firebase/listing/firebase-listing.page.html"),
            styles: [__webpack_require__(/*! ./styles/firebase-listing.page.scss */ "./src/app/firebase/listing/styles/firebase-listing.page.scss"), __webpack_require__(/*! ./styles/firebase-listing.ios.scss */ "./src/app/firebase/listing/styles/firebase-listing.ios.scss"), __webpack_require__(/*! ./styles/firebase-listing.shell.scss */ "./src/app/firebase/listing/styles/firebase-listing.shell.scss")]
        }),
        __metadata("design:paramtypes", [_firebase_integration_service__WEBPACK_IMPORTED_MODULE_6__["FirebaseService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], FirebaseListingPage);
    return FirebaseListingPage;
}());



/***/ }),

/***/ "./src/app/firebase/listing/firebase-listing.resolver.ts":
/*!***************************************************************!*\
  !*** ./src/app/firebase/listing/firebase-listing.resolver.ts ***!
  \***************************************************************/
/*! exports provided: FirebaseListingResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseListingResolver", function() { return FirebaseListingResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../firebase-integration.service */ "./src/app/firebase/firebase-integration.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FirebaseListingResolver = /** @class */ (function () {
    function FirebaseListingResolver(firebaseService) {
        this.firebaseService = firebaseService;
    }
    FirebaseListingResolver.prototype.resolve = function () {
        var dataSource = this.firebaseService.getListingDataSource();
        var dataStore = this.firebaseService.getListingStore(dataSource);
        return dataStore;
    };
    FirebaseListingResolver = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_firebase_integration_service__WEBPACK_IMPORTED_MODULE_1__["FirebaseService"]])
    ], FirebaseListingResolver);
    return FirebaseListingResolver;
}());



/***/ }),

/***/ "./src/app/firebase/listing/styles/firebase-listing.ios.scss":
/*!*******************************************************************!*\
  !*** ./src/app/firebase/listing/styles/firebase-listing.ios.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host-context(.plt-mobile.ios) .list-item .user-data-wrapper {\n  -webkit-padding-end: calc(var(--page-margin) / 2);\n          padding-inline-end: calc(var(--page-margin) / 2); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvbGlzdGluZy9zdHlsZXMvZmlyZWJhc2UtbGlzdGluZy5pb3Muc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUlNLGlEQUFnRDtVQUFoRCxnREFBZ0QsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZpcmViYXNlL2xpc3Rpbmcvc3R5bGVzL2ZpcmViYXNlLWxpc3RpbmcuaW9zLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdC1jb250ZXh0KC5wbHQtbW9iaWxlLmlvcykge1xuICAvLyBDdXN0b20gcGxhdGZvcm0gc3R5bGVzIGhlcmVcbiAgLmxpc3QtaXRlbSB7XG4gICAgLnVzZXItZGF0YS13cmFwcGVyIHtcbiAgICAgIHBhZGRpbmctaW5saW5lLWVuZDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/firebase/listing/styles/firebase-listing.page.scss":
/*!********************************************************************!*\
  !*** ./src/app/firebase/listing/styles/firebase-listing.page.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background); }\n\n.filters-toolbar {\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin); }\n\n.filters-toolbar .searchbar-row {\n    --ion-grid-column-padding: 0px; }\n\n.filters-toolbar .searchbar-row ion-searchbar.items-searchbar {\n      padding: 0px;\n      height: 100%;\n      contain: content; }\n\n.filters-toolbar .searchbar-row .call-to-action-col {\n      -webkit-padding-start: var(--page-margin);\n              padding-inline-start: var(--page-margin);\n      max-width: -webkit-fit-content;\n      max-width: -moz-fit-content;\n      max-width: fit-content;\n      flex-shrink: 0;\n      -webkit-box-flex: 0;\n              flex-grow: 0;\n      display: -webkit-box;\n      display: flex;\n      -webkit-box-pack: end;\n              justify-content: flex-end; }\n\n.filters-toolbar .searchbar-row .call-to-action-col .filters-btn {\n        --padding-start: 0px;\n        --padding-end: 0px;\n        margin: 0px;\n        font-size: 18px;\n        height: initial; }\n\n.filters-toolbar .range-item-row {\n    --ion-grid-column-padding: 0px;\n    margin-top: var(--page-margin);\n    padding-top: var(--page-margin);\n    box-shadow: inset 0 6px 3px -8px var(--ion-color-darkest); }\n\n.filters-toolbar .range-item-row .range-header {\n      display: -webkit-box;\n      display: flex;\n      -webkit-box-pack: justify;\n              justify-content: space-between;\n      padding-bottom: calc(var(--page-margin) / 2); }\n\n.filters-toolbar .range-item-row .range-header .range-value {\n        font-size: 12px;\n        font-weight: 600;\n        letter-spacing: 0.2px;\n        color: var(--ion-color-medium); }\n\n.filters-toolbar .range-item-row .range-header .range-label {\n        font-size: 14px;\n        font-weight: 500;\n        letter-spacing: 0.2px;\n        color: var(--ion-color-medium); }\n\n.filters-toolbar .range-item-row .range-control {\n      --ion-text-color: var(--ion-color-medium);\n      padding-top: 0px;\n      padding-bottom: 0px; }\n\n.firebase-listing-content {\n  --background: var(--page-background); }\n\n.firebase-listing-content .items-list .list-item {\n    --padding-start: var(--page-margin);\n    --padding-end: 0px;\n    --inner-padding-start: 0px;\n    --inner-padding-end: var(--page-margin);\n    --inner-padding-top: calc(var(--page-margin) / 2);\n    --inner-padding-bottom: calc(var(--page-margin) / 2); }\n\n.firebase-listing-content .items-list .list-item .user-row {\n      --ion-grid-column-padding: 0px;\n      width: 100%; }\n\n.firebase-listing-content .items-list .list-item .user-row .user-image-wrapper {\n        -webkit-padding-end: calc(var(--page-margin) / 2);\n                padding-inline-end: calc(var(--page-margin) / 2); }\n\n.firebase-listing-content .items-list .list-item .user-row .user-data-wrapper {\n        -webkit-padding-start: calc(var(--page-margin) / 2);\n                padding-inline-start: calc(var(--page-margin) / 2);\n        display: -webkit-box;\n        display: flex;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n                flex-direction: column;\n        -webkit-box-pack: center;\n                justify-content: center; }\n\n.firebase-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info:not(:last-child) {\n          margin-bottom: calc(var(--page-margin) / 2); }\n\n.firebase-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info .user-name {\n          margin: 0px;\n          margin-bottom: calc(var(--page-margin) / 4);\n          font-size: 16px; }\n\n.firebase-listing-content .items-list .list-item .user-row .user-data-wrapper .user-info .user-age {\n          margin: 0px;\n          color: rgba(var(--ion-color-dark-rgb), 0.4);\n          font-size: 14px; }\n\n.firebase-listing-content .empty-list-message {\n    margin: calc(var(--page-margin) * 3);\n    color: rgba(var(--ion-color-dark-rgb), 0.4);\n    text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvbGlzdGluZy9zdHlsZXMvZmlyZWJhc2UtbGlzdGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxxQ0FBYztFQUNkLHdDQUFrQixFQUFBOztBQUlwQjtFQUNFLG1DQUFnQjtFQUNoQixpQ0FBYztFQUNkLGlDQUFjO0VBQ2Qsb0NBQWlCLEVBQUE7O0FBSm5CO0lBT0ksOEJBQTBCLEVBQUE7O0FBUDlCO01BVU0sWUFBWTtNQUVaLFlBQVk7TUFFWixnQkFBZ0IsRUFBQTs7QUFkdEI7TUFrQk0seUNBQXdDO2NBQXhDLHdDQUF3QztNQUN4Qyw4QkFBc0I7TUFBdEIsMkJBQXNCO01BQXRCLHNCQUFzQjtNQUN0QixjQUFjO01BQ2QsbUJBQVk7Y0FBWixZQUFZO01BRVosb0JBQWE7TUFBYixhQUFhO01BQ2IscUJBQXlCO2NBQXpCLHlCQUF5QixFQUFBOztBQXhCL0I7UUEyQlEsb0JBQWdCO1FBQ2hCLGtCQUFjO1FBRWQsV0FBVztRQUNYLGVBQWU7UUFFZixlQUFlLEVBQUE7O0FBakN2QjtJQXVDSSw4QkFBMEI7SUFFMUIsOEJBQThCO0lBQzlCLCtCQUErQjtJQUUvQix5REFBMEQsRUFBQTs7QUE1QzlEO01BK0NNLG9CQUFhO01BQWIsYUFBYTtNQUNiLHlCQUE4QjtjQUE5Qiw4QkFBOEI7TUFDOUIsNENBQTRDLEVBQUE7O0FBakRsRDtRQW9EUSxlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLHFCQUFxQjtRQUNyQiw4QkFBOEIsRUFBQTs7QUF2RHRDO1FBMkRRLGVBQWU7UUFDZixnQkFBZ0I7UUFDaEIscUJBQXFCO1FBQ3JCLDhCQUE4QixFQUFBOztBQTlEdEM7TUFvRU0seUNBQWlCO01BRWpCLGdCQUFnQjtNQUNoQixtQkFBbUIsRUFBQTs7QUFLekI7RUFDRSxvQ0FBYSxFQUFBOztBQURmO0lBS00sbUNBQWdCO0lBQ2hCLGtCQUFjO0lBQ2QsMEJBQXNCO0lBQ3RCLHVDQUFvQjtJQUNwQixpREFBb0I7SUFDcEIsb0RBQXVCLEVBQUE7O0FBVjdCO01BYVEsOEJBQTBCO01BRTFCLFdBQVcsRUFBQTs7QUFmbkI7UUFrQlUsaURBQWdEO2dCQUFoRCxnREFBZ0QsRUFBQTs7QUFsQjFEO1FBc0JVLG1EQUFrRDtnQkFBbEQsa0RBQWtEO1FBQ2xELG9CQUFhO1FBQWIsYUFBYTtRQUNiLDRCQUFzQjtRQUF0Qiw2QkFBc0I7Z0JBQXRCLHNCQUFzQjtRQUN0Qix3QkFBdUI7Z0JBQXZCLHVCQUF1QixFQUFBOztBQXpCakM7VUE2QmMsMkNBQTJDLEVBQUE7O0FBN0J6RDtVQWlDYyxXQUFXO1VBQ1gsMkNBQTJDO1VBQzNDLGVBQWUsRUFBQTs7QUFuQzdCO1VBdUNjLFdBQVc7VUFDWCwyQ0FBMkM7VUFDM0MsZUFBZSxFQUFBOztBQXpDN0I7SUFrREksb0NBQW9DO0lBQ3BDLDJDQUEyQztJQUMzQyxrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZpcmViYXNlL2xpc3Rpbmcvc3R5bGVzL2ZpcmViYXNlLWxpc3RpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG5cbi8vIE5vdGU6ICBBbGwgdGhlIENTUyB2YXJpYWJsZXMgZGVmaW5lZCBiZWxvdyBhcmUgb3ZlcnJpZGVzIG9mIElvbmljIGVsZW1lbnRzIENTUyBDdXN0b20gUHJvcGVydGllc1xuLmZpbHRlcnMtdG9vbGJhciB7XG4gIC0tcGFkZGluZy1zdGFydDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctZW5kOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy10b3A6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLS1wYWRkaW5nLWJvdHRvbTogdmFyKC0tcGFnZS1tYXJnaW4pO1xuXG4gIC5zZWFyY2hiYXItcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBpb24tc2VhcmNoYmFyLml0ZW1zLXNlYXJjaGJhciB7XG4gICAgICBwYWRkaW5nOiAwcHg7XG4gICAgICAvLyBvdmVycmlkZSBJb25pYyBmaXhlZCBoZWlnaHRcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIC8vIExlYXJuIG1vcmUgYWJvdXQgQ1NTIGNvbnRhaW4gcHJvcGVydHkgaGVyZTogaHR0cHM6Ly90ZXJtdmFkZXIuZ2l0aHViLmlvL2Nzcy1jb250YWluL1xuICAgICAgY29udGFpbjogY29udGVudDtcbiAgICB9XG5cbiAgICAuY2FsbC10by1hY3Rpb24tY29sIHtcbiAgICAgIHBhZGRpbmctaW5saW5lLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICBtYXgtd2lkdGg6IGZpdC1jb250ZW50O1xuICAgICAgZmxleC1zaHJpbms6IDA7XG4gICAgICBmbGV4LWdyb3c6IDA7XG5cbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXG4gICAgICAuZmlsdGVycy1idG4ge1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogMHB4O1xuXG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIC8vIG92ZXJyaWRlIElvbmljIGZpeGVkIGhlaWdodFxuICAgICAgICBoZWlnaHQ6IGluaXRpYWw7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLnJhbmdlLWl0ZW0tcm93IHtcbiAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICBtYXJnaW4tdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgcGFkZGluZy10b3A6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgICAvLyBib3gtc2hhZG93IGF0IHRoZSB0b3BcbiAgICBib3gtc2hhZG93OiBpbnNldCAwIDZweCAzcHggLThweCAgdmFyKC0taW9uLWNvbG9yLWRhcmtlc3QpO1xuXG4gICAgLnJhbmdlLWhlYWRlciB7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgcGFkZGluZy1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG5cbiAgICAgIC5yYW5nZS12YWx1ZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gICAgICB9XG5cbiAgICAgIC5yYW5nZS1sYWJlbCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDAuMnB4O1xuICAgICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLnJhbmdlLWNvbnRyb2wge1xuICAgICAgLy8gb3ZlcnJpZGUgdGhlIHBpbiBjb2xvclxuICAgICAgLS1pb24tdGV4dC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG5cbiAgICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIH1cbiAgfVxufVxuXG4uZmlyZWJhc2UtbGlzdGluZy1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xuXG4gIC5pdGVtcy1saXN0IHtcbiAgICAubGlzdC1pdGVtIHtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgICAgLS1wYWRkaW5nLWVuZDogMHB4O1xuICAgICAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwcHg7XG4gICAgICAtLWlubmVyLXBhZGRpbmctZW5kOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICAtLWlubmVyLXBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgLS1pbm5lci1wYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAgICAgLnVzZXItcm93IHtcbiAgICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuXG4gICAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgICAgIC51c2VyLWltYWdlLXdyYXBwZXIge1xuICAgICAgICAgIHBhZGRpbmctaW5saW5lLWVuZDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC51c2VyLWRhdGEtd3JhcHBlciB7XG4gICAgICAgICAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gICAgICAgICAgLnVzZXItaW5mbyB7XG4gICAgICAgICAgICAmOm5vdCg6bGFzdC1jaGlsZCkge1xuICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAudXNlci1uYW1lIHtcbiAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLnVzZXItYWdlIHtcbiAgICAgICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuNCk7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuZW1wdHktbGlzdC1tZXNzYWdlIHtcbiAgICBtYXJnaW46IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMyk7XG4gICAgY29sb3I6IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgMC40KTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/firebase/listing/styles/firebase-listing.shell.scss":
/*!*********************************************************************!*\
  !*** ./src/app/firebase/listing/styles/firebase-listing.shell.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "app-image-shell.user-image {\n  --image-shell-border-radius: var(--app-fair-radius); }\n\n.user-name > app-text-shell {\n  --text-shell-line-height: 16px;\n  max-width: 80%; }\n\n.user-name > app-text-shell.text-loaded {\n    max-width: inherit; }\n\n.user-age > app-text-shell {\n  --text-shell-line-height: 14px;\n  max-width: 40%; }\n\n.user-age > app-text-shell.text-loaded {\n    max-width: inherit; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9tYWNib29rcHJvL0RvY3VtZW50cy9jb2RlL2FwcC90YTRwd2EvaW9uaWM0L3NyYy9hcHAvZmlyZWJhc2UvbGlzdGluZy9zdHlsZXMvZmlyZWJhc2UtbGlzdGluZy5zaGVsbC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbURBQTRCLEVBQUE7O0FBRzlCO0VBQ0UsOEJBQXlCO0VBQ3pCLGNBQWMsRUFBQTs7QUFGaEI7SUFLSSxrQkFBa0IsRUFBQTs7QUFJdEI7RUFDRSw4QkFBeUI7RUFDekIsY0FBYyxFQUFBOztBQUZoQjtJQUtJLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmlyZWJhc2UvbGlzdGluZy9zdHlsZXMvZmlyZWJhc2UtbGlzdGluZy5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYXBwLWltYWdlLXNoZWxsLnVzZXItaW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWJvcmRlci1yYWRpdXM6IHZhcigtLWFwcC1mYWlyLXJhZGl1cyk7XG59XG5cbi51c2VyLW5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbiAgbWF4LXdpZHRoOiA4MCU7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiBpbmhlcml0O1xuICB9XG59XG5cbi51c2VyLWFnZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xuICBtYXgtd2lkdGg6IDQwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cbiJdfQ== */"

/***/ })

}]);
//# sourceMappingURL=listing-firebase-listing-module.js.map